/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-11-10 22:40:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ground_request_qualities`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_qualities`;
CREATE TABLE `ground_request_qualities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_id` int(11) DEFAULT '0',
  `response_sup` enum('prompt','fair','slow') DEFAULT NULL,
  `equip_pos` enum('good','not_bad','slow') DEFAULT NULL,
  `handling_sat` enum('yes','not_bad','bad') DEFAULT NULL,
  `needs` enum('yes','no') DEFAULT NULL,
  `fuel` enum('yes','no') DEFAULT NULL,
  `catering` enum('yes','no') DEFAULT NULL,
  `catering_quality` enum('yes','no') DEFAULT NULL,
  `general_assessment` enum('excellent','good','fair','bad') DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gr_fk` (`ground_request_id`),
  CONSTRAINT `ground_request_qualities_ibfk_1` FOREIGN KEY (`ground_request_id`) REFERENCES `ground_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ground_request_qualities
-- ----------------------------
