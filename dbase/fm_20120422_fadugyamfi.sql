/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2012-04-23 08:27:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `chart_categories`
-- ----------------------------
DROP TABLE IF EXISTS `chart_categories`;
CREATE TABLE `chart_categories` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `parent_id` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of chart_categories
-- ----------------------------
INSERT INTO `chart_categories` VALUES ('1', 'Ground Handling', null);
INSERT INTO `chart_categories` VALUES ('2', 'Operations Control', null);

-- ----------------------------
-- Table structure for `chart_parameters`
-- ----------------------------
DROP TABLE IF EXISTS `chart_parameters`;
CREATE TABLE `chart_parameters` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `chart_id` int(5) unsigned DEFAULT NULL,
  `type` enum('text','number','date','datetime','month','year') DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `initial_value` varchar(100) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `chart_id` (`chart_id`),
  CONSTRAINT `chart_parameters_ibfk_1` FOREIGN KEY (`chart_id`) REFERENCES `charts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of chart_parameters
-- ----------------------------
INSERT INTO `chart_parameters` VALUES ('1', '1', 'date', 'Start Date', '@S_DATE', null, '0');
INSERT INTO `chart_parameters` VALUES ('2', '1', 'date', 'End Date', '@E_DATE', null, '0');
INSERT INTO `chart_parameters` VALUES ('3', '2', 'date', 'Start Date', '@S_DATE', null, '0');
INSERT INTO `chart_parameters` VALUES ('4', '2', 'date', 'End Date', '@E_DATE', null, '0');
INSERT INTO `chart_parameters` VALUES ('5', '3', 'month', 'Start Month', '@S_DATE', null, '0');
INSERT INTO `chart_parameters` VALUES ('6', '3', 'month', 'End Month', '@E_DATE', null, '0');

-- ----------------------------
-- Table structure for `charts`
-- ----------------------------
DROP TABLE IF EXISTS `charts`;
CREATE TABLE `charts` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `chart_category_id` tinyint(3) unsigned DEFAULT NULL,
  `type` enum('pie','line','bar') DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(150) DEFAULT NULL,
  `query` text,
  `x_axis_label` varchar(100) DEFAULT NULL,
  `x_axis_col_name` varchar(50) DEFAULT NULL,
  `y_axis_label` varchar(100) DEFAULT NULL,
  `y_axis_col_name` varchar(50) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `modified_dt` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `chart_category_id` (`chart_category_id`),
  CONSTRAINT `charts_ibfk_1` FOREIGN KEY (`chart_category_id`) REFERENCES `chart_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of charts
-- ----------------------------
INSERT INTO `charts` VALUES ('1', '1', 'line', 'Handling Requests Per Day', null, 'SET @START_DATE = \'@S_DATE\';\r\nSET @END_DATE = \'@E_DATE\';\r\n\r\nSELECT DATE_FORMAT(created_dt, \'%b %D\') as \"day\", count(id) as \"Requests Per Day\"  FROM ground_request_schedules\r\nWHERE created_dt BETWEEN @START_DATE AND @END_DATE\r\nGROUP BY DATE(created_dt);', 'Day Of Request\r\n', 'day', 'Requests Per Day', 'requests', '2012-04-21 22:17:04', null, '0');
INSERT INTO `charts` VALUES ('2', '1', 'line', 'Completed Requests Per Day', null, 'SET @START_DATE = \'@S_DATE\';\r\nSET @END_DATE = \'@E_DATE\';\r\n\r\nSELECT DATE_FORMAT(dept_atd, \'%b %D\') as \"day\", count(id) as \"requests\"  FROM ground_request_schedules\r\nWHERE dept_atd BETWEEN @START_DATE AND @END_DATE\r\nGROUP BY DATE(dept_atd);', 'Day of Completion', 'day', 'Completed Requests', 'requests', '2012-04-23 06:17:56', null, '0');
INSERT INTO `charts` VALUES ('3', '2', 'line', 'Permit Requests Per Month', null, 'SET @START_DATE = STR_TO_DATE(\'@S_DATE\', \'%Y-%m\');\r\nSET @END_DATE = STR_TO_DATE(\'@E_DATE\', \'%Y-%m\');\r\n\r\nSELECT DATE_FORMAT(created_dt, \'%b %Y\') as \"month\", count(id) as \"requests\"  FROM permits\r\nWHERE created_dt BETWEEN @START_DATE AND @END_DATE\r\nGROUP BY YEAR(created_dt), MONTH(created_dt);', 'Date of Request', 'month', 'Requests Per Day', 'requests', '2012-04-23 06:47:44', null, '0');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `parent_id` int(3) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `position` tinyint(2) DEFAULT NULL,
  `level` tinyint(2) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` varchar(100) DEFAULT NULL,
  `icon_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', '5', 'Staff Management', 'Manage Users and Accounts', '1', '2', 'admin', 'users', 'index', null, 'core/16_48x48.png');
INSERT INTO `menus` VALUES ('2', null, 'Ground Handling', 'Go To Ground Handling Dashboard', '2', '1', 'admin', 'ground', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('3', null, 'Operations Control', 'Go To Operations Control Dashboard', '3', '1', 'admin', 'operations', 'index', null, 'core/5_48x48.png');
INSERT INTO `menus` VALUES ('4', null, 'Fuel Management', 'Go To Fuel Management Control', '3', '3', 'admin', 'fuel', 'index', null, 'core/3_48x48.png');
INSERT INTO `menus` VALUES ('5', null, 'Home', 'System Home Screen', '1', '1', 'admin', 'dashboard', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('8', '17', 'Services', 'Manage The List of Services Provided', '4', '3', 'admin', 'managers', 'services', null, 'core/26_48x48.png');
INSERT INTO `menus` VALUES ('9', '17', 'Handlers', 'Manage The List Of Handlers Available', '3', '3', 'admin', 'managers', 'handlers', null, 'core/4_48x48.png');
INSERT INTO `menus` VALUES ('10', '5', 'Reports', 'Generate Reports', '2', '2', 'admin', 'reports', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('11', '1', 'Manage Users', 'Add A New Staff To The System', '2', '3', 'admin', 'users', 'manage', null, 'core/16_48x48.png');
INSERT INTO `menus` VALUES ('12', '1', 'Audit Trails', 'View Audit Trails of Various users', '3', '3', 'admin', 'users', 'audit_trail', null, 'core/audit_48x48.png');
INSERT INTO `menus` VALUES ('13', '1', 'Roles & Permissions', 'Roles and Permissions', '1', '3', 'admin', 'users', 'roles', '', 'core/perm_48x48.png');
INSERT INTO `menus` VALUES ('15', '2', 'Manage Requests', 'Manage Handling Requests', '2', '3', 'admin', 'ground', 'requests', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('17', '5', 'Data Management', 'Manage Handlers and Other Data', '2', '2', 'admin', 'managers', 'index', null, 'core/20_48x48.png');
INSERT INTO `menus` VALUES ('18', '17', 'CAA Directory', 'Manage Civil Aviations', '2', '3', 'admin', 'managers', 'civil_aviations', null, 'core/16_48x48.png');
INSERT INTO `menus` VALUES ('19', '3', 'Dashboard', 'Operations Dashboard', '1', '3', 'admin', 'operations', 'dashboard', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('20', '3', 'Manage Permits', 'Manage Permit Statuses', '2', '3', 'admin', 'operations', 'permits', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('22', '2', 'Dashboard', 'Ground Handling Dashboard', '1', '3', 'admin', 'ground', 'dashboard', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('24', '1', 'Manage Departments', 'Manage Departments and Setup Supervisors', '4', '3', 'admin', 'users', 'departments', null, 'core/dept_48x48.png');
INSERT INTO `menus` VALUES ('25', '17', 'Hotels', 'Manage List of Hotels', '4', '3', 'admin', 'managers', 'hotels', null, 'core/21_48x48.png');
INSERT INTO `menus` VALUES ('26', '17', 'Caterers', 'Manage List of Caterers', '5', '3', 'admin', 'managers', 'caterers', null, 'core/cat_48x48.png');
INSERT INTO `menus` VALUES ('27', '10', 'Reports', 'Generate Reports In Tabular Format', '1', '3', 'admin', 'reports', 'reports', null, 'core/4_48x48.png');
INSERT INTO `menus` VALUES ('28', '10', 'Charts', 'Generate Reports In Chart Format', '2', '3', 'admin', 'reports', 'charts', null, 'core/14_48x48.png');
