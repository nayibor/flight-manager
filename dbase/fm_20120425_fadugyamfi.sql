ALTER TABLE `ground_request_schedule_services` ADD `permit_id` INT( 11 ) NULL AFTER `into_plane`;

ALTER TABLE `ground_request_schedules`
    ADD handling_fees_on ENUM('cash','credit') NULL DEFAULT NULL,
    ADD airport_fees_on ENUM('cash','credit') NULL DEFAULT NULL,
    ADD handling_remarks VARCHAR(255) NULL DEFAULT NULL;

UPDATE ground_request_schedules grs 
    SET handling_fees_on = (SELECT payment_type_requested FROM ground_requests WHERE ground_requests.id = grs.ground_request_id);

UPDATE ground_request_schedules grs 
    SET airport_fees_on = (SELECT airport_fees_on FROM ground_requests WHERE ground_requests.id = grs.ground_request_id);
    