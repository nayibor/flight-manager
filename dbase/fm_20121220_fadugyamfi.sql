/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2012-12-19 23:43:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `document_categories`
-- ----------------------------
DROP TABLE IF EXISTS `document_categories`;
CREATE TABLE `document_categories` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(4) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of document_categories
-- ----------------------------
INSERT INTO `document_categories` VALUES ('1', null, 'Ground Handling', null);
INSERT INTO `document_categories` VALUES ('2', null, 'Operations Control', null);
INSERT INTO `document_categories` VALUES ('3', null, 'Fuel Management', null);
INSERT INTO `document_categories` VALUES ('4', null, 'Handlers', null);
INSERT INTO `document_categories` VALUES ('5', null, 'Aviation Authorities', null);
INSERT INTO `document_categories` VALUES ('6', null, 'Miscellaneous', null);

-- ----------------------------
-- Table structure for `documents`
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `document_category_id` int(5) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `abstract` text,
  `mime_type` varchar(100) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `content` longblob,
  `user_id` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `document_category_id` (`document_category_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`document_category_id`) REFERENCES `document_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;