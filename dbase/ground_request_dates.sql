/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-11-24 16:56:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ground_request_dates`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_dates`;
CREATE TABLE `ground_request_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_id` int(11) DEFAULT '0',
  `date_arrival` timestamp NULL DEFAULT NULL,
  `date_departure` timestamp NULL DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `country_code_eta` varchar(255) DEFAULT NULL,
  `country_code_etd` varchar(255) DEFAULT NULL,
  `comment_date` varchar(255) DEFAULT NULL,
  `handler_id` int(11) DEFAULT NULL,
  `comments_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gr_fk1` (`country_code_eta`),
  KEY `gr_fk` (`ground_request_id`),
  CONSTRAINT `gr_fk` FOREIGN KEY (`ground_request_id`) REFERENCES `ground_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ground_request_dates
-- ----------------------------

-- ----------------------------
-- Table structure for `ground_request_services`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_services`;
CREATE TABLE `ground_request_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_date_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0',
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ground_date` (`ground_date_id`),
  CONSTRAINT `ground_date` FOREIGN KEY (`ground_date_id`) REFERENCES `ground_request_dates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_request_services
-- ----------------------------
