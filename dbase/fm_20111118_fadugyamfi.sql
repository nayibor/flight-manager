/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-11-18 09:07:44
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `departments`
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES ('1', 'Ground Handling', null);
INSERT INTO `departments` VALUES ('2', 'Operations Control', null);
INSERT INTO `departments` VALUES ('3', 'Accounts', null);

-- ----------------------------
-- Table structure for `department_supervisors`
-- ----------------------------
DROP TABLE IF EXISTS `department_supervisors`;
CREATE TABLE `department_supervisors` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `department_id` smallint(2) unsigned DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `start_dt` datetime DEFAULT NULL,
  `end_dt` datetime DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(5) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `department_supervisors_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `department_supervisors_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of department_supervisors
-- ----------------------------
INSERT INTO `department_supervisors` VALUES ('1', '1', '1', '2011-11-18 00:52:36', '2011-11-30 00:52:43', '2011-11-18 00:52:48', '1', '0');
