/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-06-23 12:55:17
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `aerodromes`
-- ----------------------------
DROP TABLE IF EXISTS `aerodromes`;
CREATE TABLE `aerodromes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `abbrv` varchar(50) DEFAULT NULL,
  `runway_length` int(5) DEFAULT NULL COMMENT 'Length in KM',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aerodromes
-- ----------------------------

-- ----------------------------
-- Table structure for `aerodrome_equipments`
-- ----------------------------
DROP TABLE IF EXISTS `aerodrome_equipments`;
CREATE TABLE `aerodrome_equipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aerodrome_id` int(5) DEFAULT NULL,
  `equipment_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ae_aerod_fk` (`aerodrome_id`),
  KEY `ae_equip_fk` (`equipment_id`),
  CONSTRAINT `ae_aerod_fk` FOREIGN KEY (`aerodrome_id`) REFERENCES `aerodromes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ae_equip_fk` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aerodrome_equipments
-- ----------------------------

-- ----------------------------
-- Table structure for `aerodrome_hours`
-- ----------------------------
DROP TABLE IF EXISTS `aerodrome_hours`;
CREATE TABLE `aerodrome_hours` (
  `id` int(11) NOT NULL DEFAULT '0',
  `aerodrome_id` int(11) DEFAULT NULL,
  `days` set('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') DEFAULT NULL,
  `open` time DEFAULT NULL,
  `close` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aerodrome_hours
-- ----------------------------

-- ----------------------------
-- Table structure for `equipment`
-- ----------------------------
DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of equipment
-- ----------------------------

-- ----------------------------
-- Table structure for `handlers`
-- ----------------------------
DROP TABLE IF EXISTS `handlers`;
CREATE TABLE `handlers` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('GroundOps','Permits','Fuel','Other') DEFAULT NULL,
  `handle` varchar(100) DEFAULT NULL COMMENT 'Call Handle, e.g. OAKB/KBL',
  `name` varchar(200) DEFAULT NULL,
  `telephone` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of handlers
-- ----------------------------

-- ----------------------------
-- Table structure for `hotels`
-- ----------------------------
DROP TABLE IF EXISTS `hotels`;
CREATE TABLE `hotels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL COMMENT 'Star Rating',
  `distance` int(5) DEFAULT NULL COMMENT 'Distance from airport in KMs',
  `stars` tinyint(2) DEFAULT NULL,
  `contact_number` varchar(30) DEFAULT NULL,
  `rooms` tinyint(3) DEFAULT NULL COMMENT 'Total number of rooms available',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hotels
-- ----------------------------

-- ----------------------------
-- Table structure for `hotel_bookings`
-- ----------------------------
DROP TABLE IF EXISTS `hotel_bookings`;
CREATE TABLE `hotel_bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobsheet_id` int(11) unsigned DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `reservation_no` varchar(100) DEFAULT NULL,
  `reservation_name` varchar(255) DEFAULT NULL,
  `room_type` enum('Single','Double') DEFAULT NULL,
  `room_smoking` enum('Smoking','Non-Smoking') DEFAULT NULL,
  `room_rate` double(10,2) DEFAULT NULL,
  `hotel_policy` text,
  PRIMARY KEY (`id`),
  KEY `bk_hotel_fk` (`hotel_id`),
  KEY `bk_jobsheet_fk` (`jobsheet_id`),
  CONSTRAINT `bk_hotel_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bk_jobsheet_fk` FOREIGN KEY (`jobsheet_id`) REFERENCES `job_sheets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hotel_bookings
-- ----------------------------

-- ----------------------------
-- Table structure for `job_sheets`
-- ----------------------------
DROP TABLE IF EXISTS `job_sheets`;
CREATE TABLE `job_sheets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_number` varchar(50) DEFAULT NULL,
  `handler_id` int(5) DEFAULT NULL,
  `card_number` varchar(200) DEFAULT NULL,
  `holder_name` varchar(200) DEFAULT NULL,
  `card_expiry_date` date DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `est_arrival_dt` datetime DEFAULT NULL,
  `arrival_dt` datetime DEFAULT NULL,
  `est_departure_dt` datetime DEFAULT NULL,
  `departure_dt` datetime DEFAULT NULL,
  `flight_no` varchar(100) DEFAULT NULL,
  `flight_origin` varchar(100) DEFAULT NULL COMMENT 'ICAO/IATA',
  `flight_dest` varchar(100) DEFAULT NULL COMMENT 'ICAO/IATA',
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_registration` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_sheets
-- ----------------------------

-- ----------------------------
-- Table structure for `job_sheet_services`
-- ----------------------------
DROP TABLE IF EXISTS `job_sheet_services`;
CREATE TABLE `job_sheet_services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_sheet_id` int(11) unsigned DEFAULT NULL,
  `service_id` int(5) unsigned DEFAULT NULL,
  `details` text,
  PRIMARY KEY (`id`),
  KEY `ss_jobsheet_fk` (`job_sheet_id`),
  KEY `ss_services_fk` (`service_id`),
  CONSTRAINT `ss_jobsheet_fk` FOREIGN KEY (`job_sheet_id`) REFERENCES `job_sheets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ss_services_fk` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_sheet_services
-- ----------------------------

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `parent_id` int(3) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `position` tinyint(2) DEFAULT NULL,
  `level` tinyint(2) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` varchar(100) DEFAULT NULL,
  `icon_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', '5', 'User Management', 'Manage Users and Accounts', '1', '2', 'admin', 'users', 'index', null, 'core/16_48x48.png');
INSERT INTO `menus` VALUES ('2', null, 'Ground Handling', 'Go To Ground Handling Dashboard', '2', '1', 'admin', 'ground', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('3', null, 'Operations Control', 'Go To Operations Control Dashboard', '3', '1', 'admin', 'operations', 'index', null, 'core/5_48x48.png');
INSERT INTO `menus` VALUES ('4', '3', 'Fuel Management', 'Go To Fuel Management Control', '3', '3', 'admin', 'operations', 'fuel', null, 'core/3_48x48.png');
INSERT INTO `menus` VALUES ('5', null, 'Dashboard', 'System Home Screen', '1', '1', 'admin', 'dashboard', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('7', '5', 'Manage Hotels', 'Manage The List of Hotels Service Providers', '5', '2', 'admin', 'hotels', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('8', '5', 'Manage Services', 'Manage The List of Services Provided', '4', '2', 'admin', 'services', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('9', '5', 'Manage Handlers', 'Manage The List Of Handlers Available', '3', '2', 'admin', 'handlers', 'index', null, 'core/4_48x48.png');
INSERT INTO `menus` VALUES ('10', '5', 'Reports', 'Generate Reports', '2', '2', 'admin', 'report', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('11', '1', 'Manage Users', 'Add A New Staff To The System', '2', '3', 'admin', 'users', 'index', null, null);
INSERT INTO `menus` VALUES ('12', '1', 'Audit Trails', 'View Audit Trails of Various users', '3', '3', 'admin', 'users', 'audit_trail', null, null);
INSERT INTO `menus` VALUES ('13', '1', 'Manage Roles And Permissions', 'Manage User Roles and Permissions', '1', '3', 'admin', 'users', 'roles', '', null);
INSERT INTO `menus` VALUES ('14', '3', 'Permits', 'Manage Permits', '2', '3', 'admin', 'operations', 'permits', null, null);

-- ----------------------------
-- Table structure for `permits`
-- ----------------------------
DROP TABLE IF EXISTS `permits`;
CREATE TABLE `permits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('overflight','landing') DEFAULT NULL,
  `application_dt` datetime DEFAULT NULL,
  `ref_no` varchar(100) DEFAULT NULL,
  `permit_location` varchar(100) DEFAULT NULL COMMENT 'Country of Overflight or City of Landing',
  `permit_number` varchar(100) DEFAULT NULL COMMENT 'Permit Number for Overflight or Landing',
  `operator` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `aircraft_regn` varchar(100) DEFAULT NULL,
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_call_sign` varchar(100) DEFAULT NULL,
  `aircraft_base` varchar(100) DEFAULT NULL,
  `flight_eta` datetime DEFAULT NULL,
  `dept_dest` varchar(150) DEFAULT NULL COMMENT 'Point Of Departure And Destination',
  `captain_name` varchar(200) DEFAULT NULL,
  `crew_number` int(11) DEFAULT NULL COMMENT 'Number of Crew Members Excluding Flight Captain',
  `flight_purpose` varchar(200) DEFAULT NULL,
  `received_by` varchar(200) DEFAULT NULL COMMENT 'Company or Person Who Received Permit',
  `user_id` int(5) unsigned DEFAULT NULL COMMENT 'Duty Officer Who Served Permit',
  `status` enum('closed','cancelled','open') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permits
-- ----------------------------

-- ----------------------------
-- Table structure for `services`
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES ('1', 'Supervision');
INSERT INTO `services` VALUES ('2', 'HotAcc');
INSERT INTO `services` VALUES ('3', 'Landing Permit');
INSERT INTO `services` VALUES ('4', 'Transport');
INSERT INTO `services` VALUES ('5', 'Security');
INSERT INTO `services` VALUES ('6', 'Catering');
INSERT INTO `services` VALUES ('7', 'Landing & Enroute');
INSERT INTO `services` VALUES ('8', 'Parking');
INSERT INTO `services` VALUES ('9', 'Lighting');
INSERT INTO `services` VALUES ('10', 'G. P. U.');
INSERT INTO `services` VALUES ('11', 'Airstart');
INSERT INTO `services` VALUES ('12', 'Pushback / Tow Truck');
INSERT INTO `services` VALUES ('13', 'High Loader (FMC)');
INSERT INTO `services` VALUES ('14', 'Nitrogen Gas');
INSERT INTO `services` VALUES ('15', 'Stairs');
INSERT INTO `services` VALUES ('16', 'Conveyor Belt');
INSERT INTO `services` VALUES ('17', 'Toilet');
INSERT INTO `services` VALUES ('18', 'Water');
INSERT INTO `services` VALUES ('19', 'Cleaning');
INSERT INTO `services` VALUES ('20', 'Fuel');
INSERT INTO `services` VALUES ('21', 'Chocks');
INSERT INTO `services` VALUES ('22', 'Miscellaneous');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `pass_salt` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_role_id` int(5) DEFAULT NULL,
  `security_question` varchar(200) DEFAULT NULL,
  `security_answer` varchar(200) DEFAULT NULL,
  `last_access_dt` datetime DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `usr_role_fk` (`user_role_id`),
  CONSTRAINT `us_role_fk` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Francis', 'Adu-Gyamfi', 'icewalker2g', 'c691348d7285c2eb6248812aa4792f58', '12345', 'icewalker2g@yahoo.co.uk', '1', null, null, '0000-00-00 00:00:00', '1');
INSERT INTO `users` VALUES ('2', 'Mike', 'Darkwa', 'mike.darkwa', 'mikey', null, 'mike.darkwa@yahoo.com', null, null, null, '0000-00-00 00:00:00', '0');
INSERT INTO `users` VALUES ('3', 'Nuku', 'Ameyibor', 'nameyibor', 'thelordrabbi', null, 'namyibor@gmail.com', '1', null, null, '0000-00-00 00:00:00', '0');

-- ----------------------------
-- Table structure for `user_actions`
-- ----------------------------
DROP TABLE IF EXISTS `user_actions`;
CREATE TABLE `user_actions` (
  `id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(5) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL COMMENT 'The ID of the item being worked on, e.g. jobsheet_id',
  `action_desc` varchar(200) DEFAULT NULL COMMENT 'A Desc of the Action Carried Out, e.g. "created jobsheet"',
  `action_dt` datetime DEFAULT NULL COMMENT 'Date and Time of action',
  PRIMARY KEY (`id`),
  KEY `act_user_fk` (`user_id`),
  CONSTRAINT `act_user_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Handles the audit trail for each user.';

-- ----------------------------
-- Records of user_actions
-- ----------------------------

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `role_tag` varchar(20) NOT NULL,
  `role_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', 'Super Admin', 'super', null);
INSERT INTO `user_roles` VALUES ('2', 'Administration', 'admin', null);
INSERT INTO `user_roles` VALUES ('3', 'General Staff', 'general', null);
INSERT INTO `user_roles` VALUES ('4', 'My Role', 'my_role', 'Some Nice ROle');
INSERT INTO `user_roles` VALUES ('5', 'Another Nice Role', 'another_role', 'some description');

-- ----------------------------
-- Table structure for `user_role_menus`
-- ----------------------------
DROP TABLE IF EXISTS `user_role_menus`;
CREATE TABLE `user_role_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(5) DEFAULT NULL,
  `menu_id` int(5) DEFAULT NULL COMMENT 'References the Manager Categories item',
  PRIMARY KEY (`id`),
  KEY `usr_menu_id` (`menu_id`),
  KEY `user_role_fk` (`user_role_id`),
  CONSTRAINT `user_role_fk` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_role_menus_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_role_menus
-- ----------------------------
INSERT INTO `user_role_menus` VALUES ('1', '1', '1');
INSERT INTO `user_role_menus` VALUES ('2', '1', '2');
INSERT INTO `user_role_menus` VALUES ('3', '1', '3');
INSERT INTO `user_role_menus` VALUES ('4', '1', '4');
INSERT INTO `user_role_menus` VALUES ('6', '1', '7');
INSERT INTO `user_role_menus` VALUES ('7', '1', '8');
INSERT INTO `user_role_menus` VALUES ('8', '1', '9');
INSERT INTO `user_role_menus` VALUES ('11', '1', '5');
INSERT INTO `user_role_menus` VALUES ('12', '1', '10');
