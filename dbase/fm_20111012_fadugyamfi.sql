/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-10-12 00:29:08
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `aviation_authorities`
-- ----------------------------
DROP TABLE IF EXISTS `aviation_authorities`;
CREATE TABLE `aviation_authorities` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(70) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `address` tinytext,
  `telephone` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `telex` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `aftn` varchar(150) DEFAULT NULL,
  `notes` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aviation_authorities
-- ----------------------------
INSERT INTO `aviation_authorities` VALUES ('1', 'ALBANIA', 'ALBTRANSPORT Rruga Kongresi Permetit', 'ALBTRANSPORT Rruga Kongresi Permetit,\r\n202 Tirana,\r\nAlbania\r\n', '+335 42 3026 777', '+335 42 27773', '2124 AIRTAN AB', 'dpac2@albnet.nett', 'LATIYAYX (PERMITS)', '5 WORKING DAYS REQUIRED.\r\n24HRS PERMIT VALIDITY\r\n');
INSERT INTO `aviation_authorities` VALUES ('2', '', 'DIRECTION GENERAL DE Lâ€™AVIATION CIVILE', 'MINISTRY OF TRANSPORT, 119\r\nRUE DIDOUCHE MOURAD,\r\nALGER, ALGERIA 16000', '+213 21 929885-89 (FRENCH)', '+213 21 929894 / 747614', '+213 21 924515 (ENGLISH)', '', 'DAALYAYA (F/PLAN)  TELEX: 66129/66063/66137', '48 HOURS REQUIRED. 72 HOURS VALIDITY (NON-SCHEDULED PRIVATE) \r\n15 DAYS (NON-SCHEDULED COMMERCIAL)\r\n');
INSERT INTO `aviation_authorities` VALUES ('22', 'AMERICAN SAMOA', 'FEDERAL AVIATION ADMINISTRATION', '800 INDEPENDENCE AVE. S.W\r\nWASHINGTON D.C 20591, USA\r\n', '', '+1 684 6999152', '', '', 'KRWAYAYX', 'ONLY 48 HOURS NOTIFICATION OF INTENT TO LAND IS REQUIRED');
INSERT INTO `aviation_authorities` VALUES ('23', 'ANGOLA', 'DIRECCAO NACIONAL DE AVIACAO CIVIL', 'RUA D. MIGUEL DE MELO 92-6\r\nCIAXA POSTAL 569\r\nLUANDA\r\n', '+244 222 338596 / 339413', '(244) 222 390529 / 394180', '4118 DNAC AN, +244 222 350 678 (CLEARANCE)', 'dnav@snet.co.ao', 'FNLUYAYX ', '1) 72 HOURS REQUIRED\r\n2) REF. AIC C003/09 â€“ 26FEBâ€™09 â€“ (A) REQUESTS FOR AUTHORIZATION OF FLIGHTS OVER THE TERRITORY UNDER THE JURISDICTION OF ANGOLA, WILL BE MADE THROUGH ACCREDITED AGENCIES AS FOLLOWS :- CELTA SERVICOS E COMERCIO, LDA, RYDE â€“ FLIGHT SUPPORT, GLOBAL SUPPORT, AIR NAVE, LDA.(B) SUBMISSION SHOULD BE MADE TO I.N.A.V.I.C AT LEAST 72 HOURS BEFORE ARRIVAL IN ANGOLA. (C)AFTER WORKING HOURS, ONLY EMERGENCY AND DIPLOMATIC FLIGHTS WILL BE ATTENDED TO. APPLICATIONS WILL BE SENT DIRECTLY TO A.I.S AT LUANDA INTERNATIONAL AIRPORT.  \r\n*** CIRCULAR EFFECT ON DATE OF PUBLICATION â€“ 26TH FEBRUARY 2009 UNTIL FURTHER NOTICE ***\r\nSend Permit requests as follows :- carlos.sebastiao@globalsupport-ao.com and copy :- info@globalsupport-ao.com.');
INSERT INTO `aviation_authorities` VALUES ('24', 'ANGUILLA', 'WALLBLAKE AIRPORT', '', '', '264 497 5928', '9313 ANGGOUTLA', '', '', '1.PRIVATE & NON â€“ COMMERCIAL FLIGHTS NEED NOT OBTAIN PRIOR APPROVAL TO LAND AND OVERFLY.\r\n2.COMMERCIAL NON-SCHEDULE LANDING MUST OBTAIN PRIOR APPROVAL â€“ AIRPORT MGR, WALLBLAKE AIRPORT, ANGUILLA, FAX#264 497 5928, TELEX-9313 ANGGOUTLA.\r\n\r\nOVERFLYING ANGUILLA IS PROHIBITED EXCEPT TO LAND AND TAKE-OFF.\r\n');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `parent_id` int(3) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `position` tinyint(2) DEFAULT NULL,
  `level` tinyint(2) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` varchar(100) DEFAULT NULL,
  `icon_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
REPLACE INTO `menus` VALUES ('1', '5', 'User Management', 'Manage Users and Accounts', '1', '2', 'admin', 'users', 'index', null, 'core/16_48x48.png');
REPLACE INTO `menus` VALUES ('2', null, 'Ground Handling', 'Go To Ground Handling Dashboard', '2', '1', 'admin', 'ground', 'index', null, 'core/7_48x48.png');
REPLACE INTO `menus` VALUES ('3', null, 'Operations Control', 'Go To Operations Control Dashboard', '3', '1', 'admin', 'operations', 'index', null, 'core/5_48x48.png');
REPLACE INTO `menus` VALUES ('4', null, 'Fuel Management', 'Go To Fuel Management Control', '3', '3', 'admin', 'operations', 'fuel', null, 'core/3_48x48.png');
REPLACE INTO `menus` VALUES ('5', null, 'Home', 'System Home Screen', '1', '1', 'admin', 'dashboard', 'index', null, 'core/14_48x48.png');
REPLACE INTO `menus` VALUES ('7', '17', 'Hotels', 'Manage The List of Hotels Service Providers', '5', '3', 'admin', 'managers', 'hotels', null, 'core/7_48x48.png');
REPLACE INTO `menus` VALUES ('8', '17', 'Services', 'Manage The List of Services Provided', '4', '3', 'admin', 'managers', 'services', null, 'core/7_48x48.png');
REPLACE INTO `menus` VALUES ('9', '17', 'Handlers', 'Manage The List Of Handlers Available', '3', '3', 'admin', 'managers', 'handlers', null, 'core/4_48x48.png');
REPLACE INTO `menus` VALUES ('10', '5', 'Reports', 'Generate Reports', '2', '2', 'admin', 'reports', 'index', null, 'core/14_48x48.png');
REPLACE INTO `menus` VALUES ('11', '1', 'Manage Users', 'Add A New Staff To The System', '2', '3', 'admin', 'users', 'index', null, null);
REPLACE INTO `menus` VALUES ('12', '1', 'Audit Trails', 'View Audit Trails of Various users', '3', '3', 'admin', 'users', 'audit_trail', null, null);
REPLACE INTO `menus` VALUES ('13', '1', 'Manage Roles And Permissions', 'Manage User Roles and Permissions', '1', '3', 'admin', 'users', 'roles', '', null);
REPLACE INTO `menus` VALUES ('15', '2', 'Handling Requests', 'Manage Handling Requests', '1', '3', 'admin', 'ground', 'admin_view_reqlist', null, 'core/7_48x48.png');
REPLACE INTO `menus` VALUES ('16', '17', 'Airports', 'Manage List of Airports', '2', '3', 'admin', 'managers', 'airports', null, null);
REPLACE INTO `menus` VALUES ('17', '5', 'Data Management', 'Manage Handlers and Other Data', '2', '2', 'admin', 'managers', 'index', null, 'core/7_48x48.png');
REPLACE INTO `menus` VALUES ('18', '17', 'Aviations Authorities', 'Manage Civil Aviations', '2', '3', 'admin', 'managers', 'civil_aviations', null, null);
