/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50518
 Source Host           : localhost
 Source Database       : sb_opman

 Target Server Type    : MySQL
 Target Server Version : 50518
 File Encoding         : utf-8

 Date: 02/01/2012 13:05:48 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `permits`
-- ----------------------------
DROP TABLE IF EXISTS `permits`;
CREATE TABLE `permits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('overflight','landing') DEFAULT NULL,
  `flight_dt` datetime DEFAULT NULL,
  `ref_no` varchar(20) DEFAULT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_po_no` varchar(100) NOT NULL COMMENT 'Purchase Order No',
  `company_tel` varchar(30) NOT NULL,
  `company_email` varchar(50) NOT NULL,
  `company_contact` varchar(50) NOT NULL,
  `aviation_authority_id` int(10) unsigned DEFAULT NULL,
  `permit_location` varchar(50) DEFAULT NULL COMMENT 'Country of Overflight or City of Landing',
  `permit_ref_no` varchar(30) DEFAULT NULL,
  `permit_number` varchar(30) DEFAULT NULL COMMENT 'Permit Number for Overflight or Landing',
  `permit_validity` varchar(20) DEFAULT NULL COMMENT 'Validity in string format, e.g. 72 HRS',
  `operator_name` varchar(100) DEFAULT NULL,
  `operator_addr` varchar(200) DEFAULT NULL,
  `principal` varchar(200) DEFAULT NULL,
  `aircraft_regn` varchar(100) DEFAULT NULL,
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_call_sign` varchar(100) DEFAULT NULL,
  `aircraft_base` varchar(100) DEFAULT NULL,
  `flight_purpose` varchar(200) DEFAULT NULL,
  `flight_times` varchar(255) DEFAULT NULL COMMENT 'ETD/ETA/ETD of Flight',
  `flight_routes` varchar(255) DEFAULT NULL,
  `dept_dest` varchar(150) DEFAULT NULL COMMENT 'Point Of Departure And Destination',
  `captain_name` varchar(200) DEFAULT NULL,
  `captain_nationality` varchar(100) DEFAULT NULL,
  `crew_on_board` smallint(3) DEFAULT NULL,
  `souls_on_board` smallint(3) DEFAULT NULL COMMENT 'Number of Crew Members Excluding Flight Captain',
  `cargo_type` varchar(100) DEFAULT NULL,
  `arms_type` varchar(100) DEFAULT NULL,
  `est_dof` datetime DEFAULT NULL,
  `est_doa` datetime DEFAULT NULL,
  `misc_info` tinytext,
  `created_by` int(5) unsigned DEFAULT NULL COMMENT 'Duty Officer Who Served Permit',
  `created_dt` timestamp NULL DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('closed','canceled','open','deleted','approved','caa','on hold') DEFAULT 'open',
  `permit_application_caa_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `permit_receive_caa_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `permit_client_send_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

