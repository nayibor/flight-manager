SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for `permit_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `permit_statuses`;
CREATE TABLE `permit_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permit_id` int(10) unsigned DEFAULT NULL,
  `status` enum('open','caa','approved','forwarded','completed','canceled','closed') DEFAULT NULL,
  `change_dt` datetime DEFAULT NULL,
  `changed_by` int(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `permits` DROP COLUMN `received_by`;

ALTER TABLE `permits` 
    ADD `company_name` VARCHAR( 200 ) NOT NULL AFTER `ref_no` ,
    ADD `company_addr` VARCHAR( 255 ) NOT NULL AFTER `company_name` ,
    ADD `company_tel` VARCHAR( 30 ) NOT NULL AFTER `company_addr` ,
    ADD `company_email` VARCHAR( 50 ) NOT NULL AFTER `company_tel` ,
    ADD `company_contact` VARCHAR( 50 ) NOT NULL AFTER `company_email`;