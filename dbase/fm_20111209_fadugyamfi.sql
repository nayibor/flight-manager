/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-12-09 09:57:10
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `ground_request_schedule_attachments`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_schedule_attachments`;
CREATE TABLE `ground_request_schedule_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_schedule_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `mime_type` varchar(40) DEFAULT NULL,
  `attachment` longblob,
  `created_dt` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(5) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ground_request_schedule_id` (`ground_request_schedule_id`),
  CONSTRAINT `ground_request_schedule_attachments_ibfk_1` FOREIGN KEY (`ground_request_schedule_id`) REFERENCES `ground_request_schedules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_request_schedule_attachments
-- ----------------------------
