#
# MySQLDiff 1.5.0
#
# http://www.mysqldiff.org
# (c) 2001-2004, Lippe-Net Online-Service
#
# Create time: 24.02.2012 15:44
#
# --------------------------------------------------------
# Source info
# Host: localhost
# Database: sbman_op_live
# --------------------------------------------------------
# Target info
# Host: localhost
# Database: sb_opman
# --------------------------------------------------------
#

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE IF NOT EXISTS  `fuel_request_statuses` (
    `id` int(10) unsigned NOT NULL COMMENT '' auto_increment,
    `fuel_request_id` int(10) unsigned NULL DEFAULT NULL COMMENT '',
    `status` enum('open','caa','approved','on hold','forwarded','completed','canceled','closed') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `comments` text NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `change_dt` datetime NULL DEFAULT NULL COMMENT '',
    `changed_by` int(5) unsigned NULL DEFAULT NULL COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `permit_id` (`fuel_request_id`),
    CONSTRAINT FOREIGN KEY (`fuel_request_id`) REFERENCES `fuel_requests` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS  `reports` (
    `id` int(3) NOT NULL COMMENT '' auto_increment,
    `title` varchar(100) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `position` tinyint(2) NULL DEFAULT NULL COMMENT '',
    `level` tinyint(2) NULL DEFAULT NULL COMMENT '',
    `action` varchar(100) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `params` varchar(100) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `controller` varchar(255) NOT NULL DEFAULT '' COMMENT '' COLLATE latin1_swedish_ci,
    `category` enum('ground','permits','fuel') NOT NULL DEFAULT 'ground' COMMENT '' COLLATE latin1_swedish_ci,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

ALTER TABLE `fuel_requests`
    ADD `company_card_info` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER company_email,
    ADD `company_card_expiry_dt` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER company_card_info,
    ADD `aircraft_pci` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER aircraft_optr,
    ADD `fuel_dest` varchar(20) NULL DEFAULT NULL COMMENT 'Location of Actual Fueling' COLLATE latin1_swedish_ci AFTER aircraft_flight_no,
    ADD `est_uplift_ltrs` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER est_uplift,
    ADD `est_uplift_dt` datetime NULL DEFAULT NULL COMMENT '' AFTER est_uplift_ltrs,
    ADD `actual_uplift` varchar(20) NULL DEFAULT NULL COMMENT 'Actual Qty Used' COLLATE latin1_swedish_ci AFTER est_uplift_dt,
    ADD `actual_uplift_ltrs` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER actual_uplift,
    ADD `product_selling_price` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER product,
    ADD `confirmation_ticket_no` varchar(30) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER confirmation_code,
    ADD `billed` tinyint(1) NULL DEFAULT NULL COMMENT '' AFTER status,
    MODIFY `est_uplift` varchar(50) NULL DEFAULT NULL COMMENT 'Requested Quantity' COLLATE latin1_swedish_ci,
    MODIFY `status` enum('open','uplifted','canceled','on hold') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci;
#
#  Fieldformats of
#    fuel_requests.est_uplift changed from varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci to varchar(50) NULL DEFAULT NULL COMMENT 'Requested Quantity' COLLATE latin1_swedish_ci.
#    fuel_requests.status changed from varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci to enum('open','uplifted','canceled','on hold') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci.
#  Possibly data modifications needed!
#


ALTER TABLE `permits`
    ADD `receiving_party` varchar(255) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER est_doa,
    MODIFY `type` enum('overflight','landing','tech stop','block overflight') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci;
#
#  Fieldformat of
#    permits.type changed from enum('overflight','landing') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci to enum('overflight','landing','tech stop','block overflight') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci.
#  Possibly data modifications needed!
#

#
# DDL END
#

SET FOREIGN_KEY_CHECKS = 1;

UPDATE ground_request_schedules SET `status` = 'open' WHERE `status` = '';