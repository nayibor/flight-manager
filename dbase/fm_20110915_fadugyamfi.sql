/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-09-15 13:45:32
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `parent_id` int(3) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `position` tinyint(2) DEFAULT NULL,
  `level` tinyint(2) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` varchar(100) DEFAULT NULL,
  `icon_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', '5', 'User Management', 'Manage Users and Accounts', '1', '2', 'admin', 'users', 'index', null, 'core/16_48x48.png');
INSERT INTO `menus` VALUES ('2', null, 'Ground Handling', 'Go To Ground Handling Dashboard', '2', '1', 'admin', 'ground', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('3', null, 'Operations Control', 'Go To Operations Control Dashboard', '3', '1', 'admin', 'operations', 'index', null, 'core/5_48x48.png');
INSERT INTO `menus` VALUES ('4', null, 'Fuel Management', 'Go To Fuel Management Control', '3', '3', 'admin', 'operations', 'fuel', null, 'core/3_48x48.png');
INSERT INTO `menus` VALUES ('5', null, 'Home', 'System Home Screen', '1', '1', 'admin', 'dashboard', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('7', '17', 'Manage Hotels', 'Manage The List of Hotels Service Providers', '5', '3', 'admin', 'managers', 'hotels', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('8', '17', 'Manage Services', 'Manage The List of Services Provided', '4', '3', 'admin', 'managers', 'services', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('9', '17', 'Manage Handlers', 'Manage The List Of Handlers Available', '3', '3', 'admin', 'managers', 'handlers', null, 'core/4_48x48.png');
INSERT INTO `menus` VALUES ('10', '5', 'Reports', 'Generate Reports', '2', '2', 'admin', 'reports', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('11', '1', 'Manage Users', 'Add A New Staff To The System', '2', '3', 'admin', 'users', 'index', null, null);
INSERT INTO `menus` VALUES ('12', '1', 'Audit Trails', 'View Audit Trails of Various users', '3', '3', 'admin', 'users', 'audit_trail', null, null);
INSERT INTO `menus` VALUES ('13', '1', 'Manage Roles And Permissions', 'Manage User Roles and Permissions', '1', '3', 'admin', 'users', 'roles', '', null);
INSERT INTO `menus` VALUES ('14', '3', 'Permits', 'Manage Permits', '2', '3', 'admin', 'operations', 'permits', null, null);
INSERT INTO `menus` VALUES ('15', '2', 'Handling Requests', 'Manage Handling Requests', '1', '3', 'admin', 'ground', 'admin_view_reqlist', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('16', '17', 'Manage Airports', 'Manage List of Airports', '2', '3', 'admin', 'managers', 'airports', null, null);
INSERT INTO `menus` VALUES ('17', '5', 'Data Management', 'Manage Handlers and Other Data', '2', '2', 'admin', 'managers', 'index', null, 'core/7_48x48.png');
