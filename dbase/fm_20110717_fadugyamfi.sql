/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-07-17 20:01:57
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `permits`
-- ----------------------------
DROP TABLE IF EXISTS `permits`;
CREATE TABLE `permits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('overflight','landing') DEFAULT NULL,
  `application_dt` datetime DEFAULT NULL,
  `ref_no` varchar(100) DEFAULT NULL,
  `permit_location` varchar(100) DEFAULT NULL COMMENT 'Country of Overflight or City of Landing',
  `permit_number` varchar(100) DEFAULT NULL COMMENT 'Permit Number for Overflight or Landing',
  `operator` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `principal` varchar(200) DEFAULT NULL,
  `aircraft_regn` varchar(100) DEFAULT NULL,
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_call_sign` varchar(100) DEFAULT NULL,
  `aircraft_base` varchar(100) DEFAULT NULL,
  `flight_purpose` varchar(200) DEFAULT NULL,
  `flight_times` varchar(255) DEFAULT NULL COMMENT 'ETD/ETA/ETD of Flight',
  `flight_routes` varchar(255) DEFAULT NULL,
  `dept_dest` varchar(150) DEFAULT NULL COMMENT 'Point Of Departure And Destination',
  `captain_name` varchar(200) DEFAULT NULL,
  `souls_on_board` smallint(3) DEFAULT NULL COMMENT 'Number of Crew Members Excluding Flight Captain',
  `cargo_type` varchar(100) DEFAULT NULL,
  `arms_type` varchar(100) DEFAULT NULL,
  `actual_dof` datetime DEFAULT NULL,
  `actual_doa` datetime DEFAULT NULL,
  `misc_info` tinytext,
  `received_by` varchar(200) DEFAULT NULL COMMENT 'Company or Person Who Received Permit',
  `user_id` int(5) unsigned DEFAULT NULL COMMENT 'Duty Officer Who Served Permit',
  `status` enum('closed','cancelled','open') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

