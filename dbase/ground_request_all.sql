/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-09-14 18:14:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ground_request_dates`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_dates`;
CREATE TABLE `ground_request_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_id` int(11) DEFAULT '0',
  `date_arrival` date DEFAULT NULL,
  `date_departure` date DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gr_fk` (`ground_request_id`),
  CONSTRAINT `gr_fk` FOREIGN KEY (`ground_request_id`) REFERENCES `ground_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ground_request_dates
-- ----------------------------

-- ----------------------------
-- Table structure for `ground_request_services`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_services`;
CREATE TABLE `ground_request_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ground_equest_fk` (`ground_request_id`),
  CONSTRAINT `ground_equest_fk` FOREIGN KEY (`ground_request_id`) REFERENCES `ground_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_request_services
-- ----------------------------

-- ----------------------------
-- Table structure for `ground_requests`
-- ----------------------------
DROP TABLE IF EXISTS `ground_requests`;
CREATE TABLE `ground_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `mtow` varchar(255) DEFAULT NULL,
  `flight_no` varchar(255) DEFAULT NULL,
  `crew` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `limit_location` enum('0','1') DEFAULT '0',
  `limit_airport` enum('0','1') DEFAULT '0',
  `icc_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_requests
-- ----------------------------
