@echo off

echo Starting Backup of Mysql Database on server 

For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set dt=%%c-%%a-%%b)

For /f "tokens=1-4 delims=:." %%a in ('echo %time%') do (set tm=%%a%%b%%c%%d)

set bkupfilename=%1 %dt% %tm%.sql

echo Backing up to file: %bkupfilename%

mysqldump  --user=root --password=""  sb_opman > D:\mysql_daily_backups\"sb_opman%bkupfilename%"

echo on

echo delete old backup

forfiles /p D:\mysql_daily_backups /s /m *.* /d -3 /c "cmd /c del @file : date >= 3days"



echo Backup Complete! Have A Nice Day