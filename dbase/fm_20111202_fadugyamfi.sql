/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-12-02 00:39:22
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `ground_request_schedules`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_schedules`;
CREATE TABLE `ground_request_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_id` int(11) DEFAULT '0',
  `handler_id` int(11) unsigned DEFAULT NULL,
  `arr_etd` datetime DEFAULT NULL,
  `arr_eta` datetime DEFAULT NULL,
  `arr_origin` varchar(20) DEFAULT NULL,
  `arr_dest` varchar(20) DEFAULT NULL,
  `arr_cargo` varchar(30) DEFAULT NULL,
  `dept_etd` datetime DEFAULT NULL,
  `dept_eta` datetime DEFAULT NULL,
  `dept_origin` varchar(20) DEFAULT NULL,
  `dept_dest` varchar(20) DEFAULT NULL,
  `dept_cargo` varchar(30) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_dt` timestamp NULL DEFAULT NULL,
  `created_by` int(5) unsigned DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gr_fk` (`ground_request_id`),
  KEY `handler_id` (`handler_id`),
  CONSTRAINT `ground_request_schedules_ibfk_1` FOREIGN KEY (`ground_request_id`) REFERENCES `ground_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ground_request_schedules_ibfk_2` FOREIGN KEY (`handler_id`) REFERENCES `handlers` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ground_request_schedules
-- ----------------------------
INSERT INTO `ground_request_schedules` VALUES ('1', '23', '1', '2011-11-29 19:45:19', '2011-11-30 19:45:24', 'FNJT', 'DGAA', null, '2011-12-01 13:57:53', '2011-12-02 01:57:58', 'DGAA', 'FNJT', null, '', '2011-12-01 13:58:23', '1', '2011-12-01 13:58:32', null);
INSERT INTO `ground_request_schedules` VALUES ('2', '23', '1', '2011-11-30 19:45:05', '2011-12-01 19:45:08', 'DGAA', 'FNJT', null, null, null, null, null, null, null, null, null, '2011-12-01 13:57:51', null);

-- ----------------------------
-- Table structure for `ground_request_schedule_services`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_schedule_services`;
CREATE TABLE `ground_request_schedule_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_schedule_id` int(11) DEFAULT NULL,
  `service_id` int(11) unsigned DEFAULT NULL,
  `status` enum('requested','completed') DEFAULT 'requested',
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ground_request_schedule_id` (`ground_request_schedule_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `ground_request_schedule_services_ibfk_1` FOREIGN KEY (`ground_request_schedule_id`) REFERENCES `ground_request_schedules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ground_request_schedule_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_request_schedule_services
-- ----------------------------
INSERT INTO `ground_request_schedule_services` VALUES ('1', '1', '1', '', null);
INSERT INTO `ground_request_schedule_services` VALUES ('2', '1', '2', '', null);
