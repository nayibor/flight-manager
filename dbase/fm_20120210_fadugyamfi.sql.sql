SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `fuel_requests`
    DROP FOREIGN KEY `fuel_requests_ibfk_1`;

ALTER TABLE `fuel_requests`
    ADD `remarks` text NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER product,
    ADD `confirmation_code` varchar(30) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER remarks,
    ADD `fueler` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER confirmation_code,
    ADD `confirmation_dt` datetime NULL DEFAULT NULL COMMENT '' AFTER modified_by,
    ADD `confirmed_by` int(5) unsigned NULL DEFAULT NULL COMMENT '' AFTER confirmation_dt,
    DROP `fueler_id`,
    DROP `purchase_order_no`,
    DROP INDEX `fueler_id`,
    ADD INDEX `fueler_id` (`fueler`);





#
# DDL END
#

SET FOREIGN_KEY_CHECKS = 1;