/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-07-05 23:01:46
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `airport_codes`
-- ----------------------------
DROP TABLE IF EXISTS `airport_codes`;
CREATE TABLE `airport_codes` (
  `id` int(11) NOT NULL,
  `itao_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `itao_key` (`itao_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of airport_codes
-- ----------------------------

-- ----------------------------
-- Table structure for `airport_details`
-- ----------------------------
DROP TABLE IF EXISTS `airport_details`;
CREATE TABLE `airport_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iata_code` varchar(255) NOT NULL,
  `jobsheet` int(11) NOT NULL,
  `icao_code` varchar(255) NOT NULL,
  `airport_entry` enum('0','1') DEFAULT NULL,
  `custom_immigration` enum('0','1') DEFAULT NULL,
  `job_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of airport_details
-- ----------------------------

-- ----------------------------
-- Table structure for `country_codes`
-- ----------------------------
DROP TABLE IF EXISTS `country_codes`;
CREATE TABLE `country_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icao_code` varchar(255) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_key` (`icao_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of country_codes
-- ----------------------------
