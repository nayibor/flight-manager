ALTER TABLE `ground_requests`
    MODIFY COLUMN `card_expiry_date`  varchar(20) NULL DEFAULT NULL AFTER `holder_name`;


ALTER TABLE `ground_request_schedules`
    MODIFY COLUMN `arr_pax_no`  varchar(6) NULL DEFAULT 'N/A' AFTER `arr_cargo`,
    MODIFY COLUMN `arr_crew_no`  varchar(6) NULL DEFAULT 'N/A' AFTER `arr_pax_no`,
    MODIFY COLUMN `dept_pax_no`  varchar(6) NULL DEFAULT 'N/A' AFTER `dept_cargo`,
    MODIFY COLUMN `dept_crew_no`  varchar(6) NULL DEFAULT 'N/A' AFTER `dept_pax_no`;