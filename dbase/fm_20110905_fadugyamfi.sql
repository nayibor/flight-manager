/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-09-05 17:16:45
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `permits`
-- ----------------------------
DROP TABLE IF EXISTS `permits`;
CREATE TABLE `permits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('overflight','landing') DEFAULT NULL,
  `application_dt` datetime DEFAULT NULL,
  `ref_no` varchar(100) DEFAULT NULL,
  `permit_location` varchar(100) DEFAULT NULL COMMENT 'Country of Overflight or City of Landing',
  `permit_number` varchar(100) DEFAULT NULL COMMENT 'Permit Number for Overflight or Landing',
  `operator_name` varchar(100) DEFAULT NULL,
  `operator_addr` varchar(200) DEFAULT NULL,
  `principal` varchar(200) DEFAULT NULL,
  `aircraft_regn` varchar(100) DEFAULT NULL,
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_call_sign` varchar(100) DEFAULT NULL,
  `aircraft_base` varchar(100) DEFAULT NULL,
  `flight_purpose` varchar(200) DEFAULT NULL,
  `flight_times` varchar(255) DEFAULT NULL COMMENT 'ETD/ETA/ETD of Flight',
  `flight_routes` varchar(255) DEFAULT NULL,
  `dept_dest` varchar(150) DEFAULT NULL COMMENT 'Point Of Departure And Destination',
  `captain_name` varchar(200) DEFAULT NULL,
  `souls_on_board` smallint(3) DEFAULT NULL COMMENT 'Number of Crew Members Excluding Flight Captain',
  `cargo_type` varchar(100) DEFAULT NULL,
  `arms_type` varchar(100) DEFAULT NULL,
  `est_dof` datetime DEFAULT NULL,
  `est_doa` datetime DEFAULT NULL,
  `misc_info` tinytext,
  `received_by` varchar(200) DEFAULT NULL COMMENT 'Company or Person Who Received Permit',
  `created_by` int(5) unsigned DEFAULT NULL COMMENT 'Duty Officer Who Served Permit',
  `created_dt` timestamp NULL DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('closed','cancelled','open','deleted','approved','caa') DEFAULT 'open',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permits
-- ----------------------------
INSERT INTO `permits` VALUES ('1', 'overflight', '2011-07-16 13:42:00', 'SBMAN1', 'HAAB', '1471/YA', 'Bold Fuelers', 'Accra', null, 'N583JH', 'CESSNA Caravan 2052', 'N5083JH', 'Nothern', 'Transport', '07AUG10 ETD GOOY 1000UTC ETA DGAA 1300UTC\r\n07AUG10 ETD DGAA 1700UTC ETA GOOY 2011UTC', 'Approved ATS Routes', 'ACC', 'Francis Adu-Gyamfi', '10', 'now cargo', 'n/a', '2011-08-20 11:00:00', '2011-08-20 17:00:00', 'something', null, null, null, '1', '2011-09-04 00:48:17', 'open');
INSERT INTO `permits` VALUES ('2', null, null, null, null, null, 'sdfsd', 'sdfsd', null, 'sdf', 'sdf', 'sdf', 'sdf', 'd', 'sdf', 'Approved ATS Routes', '', '', '0', 'sdf', 'sadf', '2011-08-24 17:15:00', '2011-08-25 19:00:00', '', null, null, null, null, null, 'open');
INSERT INTO `permits` VALUES ('3', null, null, null, null, null, 'Moer', 'asdasda', null, 'asdasd', 'asdasd', '12111', '', '', 'asdasd', 'Approved ATS Routes', '', '', null, '', '', '2011-08-26 09:00:00', '2011-08-27 11:00:00', '', null, null, null, null, null, 'open');
INSERT INTO `permits` VALUES ('4', 'overflight', null, 'SBMAN4', null, '1473/YA', 'asdas', 'asdasd', null, 'sa', 'asd', 'asdas', '', '', 'qasdas', 'Approved ATS Routes', '', '', null, '', '', '2011-08-25 19:00:00', '2011-08-27 12:21:00', '', null, null, null, null, null, 'deleted');
INSERT INTO `permits` VALUES ('5', 'overflight', null, 'SBMAN5', null, 'YHNO/Y1', 'Transglobal', 'P.O. Box 1iasda akasd jasd asd', null, 'HNJ011', 'Cessna Caravan 100', 'HNO1', '', '', '20110101', 'Approved ATS Routes', '', '', null, '', '', '2011-09-29 12:22:00', '2011-09-30 02:10:00', '', null, '1', '2011-09-04 00:22:05', null, '2011-09-04 00:22:05', 'open');
INSERT INTO `permits` VALUES ('6', 'landing', null, 'SBMAN6', null, 'SBOC881-10', 'SIGNIA JETS', 'C/O SBMAN &CO LTD, TEL: 00233 244 321771, FAX: 233 21 237768, SITA: ACCSM7X, AFTN: KACCGCKX ', null, 'N75GA', 'HS-125', 'N75GA', 'USA', 'TECH STOP FOR FUEL', 'FEFF-DRZA-LETO\r\n25JUNE2010 ETD FEFF 2115UTC ETA DRZA 2345UTC\r\n26JUNE2010 ETD DRZA 0030UTC ETA LETO TBA', 'Approved ATS Routes', 'HKJK-FEFF-DRZA-LETO', 'CAPT. JOSE LUIS', '3', 'NIL', 'NIL', null, null, '72HRS LEEWAY INCASE OF DELAYED FLIGHT', null, '1', '2011-09-04 00:46:03', null, '2011-09-04 00:46:03', 'open');
