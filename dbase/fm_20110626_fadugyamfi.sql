/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-06-26 22:05:35
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `parent_id` int(3) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `position` tinyint(2) DEFAULT NULL,
  `level` tinyint(2) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` varchar(100) DEFAULT NULL,
  `icon_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', '5', 'User Management', 'Manage Users and Accounts', '1', '2', 'admin', 'users', 'index', null, 'core/16_48x48.png');
INSERT INTO `menus` VALUES ('2', null, 'Ground Handling', 'Go To Ground Handling Dashboard', '2', '1', 'admin', 'ground', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('3', null, 'Operations Control', 'Go To Operations Control Dashboard', '3', '1', 'admin', 'operations', 'index', null, 'core/5_48x48.png');
INSERT INTO `menus` VALUES ('4', '3', 'Fuel Management', 'Go To Fuel Management Control', '3', '3', 'admin', 'operations', 'fuel', null, 'core/3_48x48.png');
INSERT INTO `menus` VALUES ('5', null, 'Home', 'System Home Screen', '1', '1', 'admin', 'dashboard', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('7', '5', 'Manage Hotels', 'Manage The List of Hotels Service Providers', '5', '2', 'admin', 'hotels', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('8', '5', 'Manage Services', 'Manage The List of Services Provided', '4', '2', 'admin', 'services', 'index', null, 'core/7_48x48.png');
INSERT INTO `menus` VALUES ('9', '5', 'Manage Handlers', 'Manage The List Of Handlers Available', '3', '2', 'admin', 'handlers', 'index', null, 'core/4_48x48.png');
INSERT INTO `menus` VALUES ('10', '5', 'Reports', 'Generate Reports', '2', '2', 'admin', 'report', 'index', null, 'core/14_48x48.png');
INSERT INTO `menus` VALUES ('11', '1', 'Manage Users', 'Add A New Staff To The System', '2', '3', 'admin', 'users', 'index', null, null);
INSERT INTO `menus` VALUES ('12', '1', 'Audit Trails', 'View Audit Trails of Various users', '3', '3', 'admin', 'users', 'audit_trail', null, null);
INSERT INTO `menus` VALUES ('13', '1', 'Manage Roles And Permissions', 'Manage User Roles and Permissions', '1', '3', 'admin', 'users', 'roles', '', null);
INSERT INTO `menus` VALUES ('14', '3', 'Permits', 'Manage Permits', '2', '3', 'admin', 'operations', 'permits', null, null);
INSERT INTO `menus` VALUES ('15', '2', 'Jobsheet', 'Create New Jobsheet', '1', '2', 'admin', 'jobsheet', 'add', null, null);

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `role_tag` varchar(20) NOT NULL,
  `role_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', 'Super Admin', 'super', '');
INSERT INTO `user_roles` VALUES ('2', 'Administration', 'admin', null);
INSERT INTO `user_roles` VALUES ('3', 'General Staff', 'general', null);
INSERT INTO `user_roles` VALUES ('5', 'Another Nice Role', 'another_role', 'some description');

-- ----------------------------
-- Table structure for `user_role_menus`
-- ----------------------------
DROP TABLE IF EXISTS `user_role_menus`;
CREATE TABLE `user_role_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(5) DEFAULT NULL,
  `menu_id` int(5) DEFAULT NULL COMMENT 'References the Manager Categories item',
  PRIMARY KEY (`id`),
  KEY `usr_menu_id` (`menu_id`),
  KEY `user_role_fk` (`user_role_id`),
  CONSTRAINT `user_role_fk` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_role_menus_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_role_menus
-- ----------------------------
INSERT INTO `user_role_menus` VALUES ('28', '2', '5');
INSERT INTO `user_role_menus` VALUES ('29', '2', '10');
INSERT INTO `user_role_menus` VALUES ('30', '2', '9');
INSERT INTO `user_role_menus` VALUES ('31', '2', '8');
INSERT INTO `user_role_menus` VALUES ('32', '2', '7');
INSERT INTO `user_role_menus` VALUES ('37', '1', '5');
INSERT INTO `user_role_menus` VALUES ('38', '1', '10');
INSERT INTO `user_role_menus` VALUES ('39', '1', '9');
INSERT INTO `user_role_menus` VALUES ('40', '1', '8');
INSERT INTO `user_role_menus` VALUES ('41', '1', '7');
INSERT INTO `user_role_menus` VALUES ('42', '1', '2');
INSERT INTO `user_role_menus` VALUES ('43', '1', '3');
INSERT INTO `user_role_menus` VALUES ('44', '1', '14');
INSERT INTO `user_role_menus` VALUES ('45', '1', '4');
INSERT INTO `user_role_menus` VALUES ('46', '1', '1');
INSERT INTO `user_role_menus` VALUES ('47', '1', '13');
INSERT INTO `user_role_menus` VALUES ('48', '1', '11');
INSERT INTO `user_role_menus` VALUES ('49', '1', '12');
INSERT INTO `user_role_menus` VALUES ('59', '2', '1');
INSERT INTO `user_role_menus` VALUES ('60', '2', '13');
INSERT INTO `user_role_menus` VALUES ('61', '2', '11');
INSERT INTO `user_role_menus` VALUES ('62', '2', '12');
INSERT INTO `user_role_menus` VALUES ('63', '1', '15');
