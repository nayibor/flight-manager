#
# MySQLDiff 1.5.0
#
# http://www.mysqldiff.org
# (c) 2001-2004, Lippe-Net Online-Service
#
# Create time: 23.03.2012 19:11
#
# --------------------------------------------------------
# Source info
# Host: localhost
# Database: sbman_op_live
# --------------------------------------------------------
# Target info
# Host: localhost
# Database: sb_opman
# --------------------------------------------------------
#

SET FOREIGN_KEY_CHECKS = 0;

#
# DDL START
#
CREATE TABLE IF NOT EXISTS  `caterers` (
    `id` int(11) unsigned NOT NULL COMMENT '' auto_increment,
    `name` varchar(200) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `address` varchar(255) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `email` varchar(100) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `telephone` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `country` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `distance` varchar(30) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `stars` tinyint(2) NULL DEFAULT NULL COMMENT '',
    `website` varchar(200) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `created_dt` datetime NULL DEFAULT NULL COMMENT '',
    `modified_dt` timestamp NULL DEFAULT NULL COMMENT '' on update CURRENT_TIMESTAMP,
    `active` tinyint(1) NULL DEFAULT '1' COMMENT '',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS  `permit_attachments` (
    `id` int(11) unsigned NOT NULL COMMENT '' auto_increment,
    `permit_id` int(11) NULL DEFAULT NULL COMMENT '',
    `attachment` longblob NULL DEFAULT NULL COMMENT '',
    `file_name` varchar(200) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `mime_type` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `created_dt` datetime NULL DEFAULT NULL COMMENT '',
    `created_by` int(5) unsigned NULL DEFAULT NULL COMMENT '',
    `modified_dt` timestamp NULL DEFAULT NULL COMMENT '' on update CURRENT_TIMESTAMP,
    `modified_by` int(5) unsigned NULL DEFAULT NULL COMMENT '',
    `deleted` tinyint(1) NULL DEFAULT '0' COMMENT '',
    PRIMARY KEY (`id`),
    INDEX `permit_id` (`permit_id`),
    CONSTRAINT FOREIGN KEY (`permit_id`) REFERENCES `permits` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

ALTER TABLE `hotels`
    ADD `address` varchar(255) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER name,
    ADD `email` varchar(100) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER address,
    ADD `country` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER email,
    ADD `website` varchar(200) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER rooms,
    DROP `location`;


#
# DDL END
#

SET FOREIGN_KEY_CHECKS = 1;