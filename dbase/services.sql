CREATE TABLE `ground_request_schedule_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_schedule_id` int(11) DEFAULT NULL,
  `service_id` int(11) unsigned DEFAULT NULL,
  `status` enum('requested','completed') DEFAULT 'requested',
  `name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `hotel_reservation_city` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `hotel_reservation_no` varchar(30) DEFAULT NULL,
  `hotel_reservation_rm_type` varchar(100) DEFAULT NULL,
  `hotel_reservation_rooms` tinyint(3) DEFAULT NULL,
  `transport_vehicle_type` varchar(100) DEFAULT NULL,
  `transport_driver_name` varchar(100) DEFAULT NULL,
  `into_plane` varchar(255) DEFAULT NULL,
  `permit_no` varchar(255) DEFAULT NULL,
  `permit_landing` varchar(255) DEFAULT NULL,
  `permit_overflight` varchar(255) DEFAULT NULL,
  `permit_validity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ground_request_schedule_id` (`ground_request_schedule_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `ground_request_schedule_services_ibfk_1` FOREIGN KEY (`ground_request_schedule_id`) REFERENCES `ground_request_schedules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ground_request_schedule_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

