/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2012-06-27 22:42:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `fuel_request_attachments`
-- ----------------------------
DROP TABLE IF EXISTS `fuel_request_attachments`;
CREATE TABLE `fuel_request_attachments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fuel_request_id` int(11) unsigned DEFAULT NULL,
  `attachment` longblob,
  `file_name` varchar(200) DEFAULT NULL,
  `mime_type` varchar(50) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `created_by` int(5) unsigned DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(5) unsigned DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `permit_id` (`fuel_request_id`),
  CONSTRAINT `fuel_request_attachments_ibfk_1` FOREIGN KEY (`fuel_request_id`) REFERENCES `fuel_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fuel_request_attachments
-- ----------------------------

-- HOTELS ALTER SCRIPT
ALTER TABLE `hotels` ADD COLUMN `cancellation_policy`  text NULL AFTER `website`;
