ALTER TABLE `ground_request_schedule_services`
    ADD `hotel_pic_room_no` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER hotel_reservation_rooms,
    ADD `hotel_pic_cell_no` varchar(30) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER hotel_pic_room_no,
    ADD `transport_pickup_dt` datetime NULL DEFAULT NULL COMMENT '' AFTER transport_driver_name,
    ADD `transport_vehicle_types` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER transport_pickup_dt,
    ADD `transport_cars_in` tinyint(2) NULL DEFAULT NULL COMMENT '' AFTER transport_vehicle_types,
    ADD `transport_cars_out` tinyint(2) NULL DEFAULT NULL COMMENT '' AFTER transport_cars_in,
    ADD `transport_pax_coordinator` varchar(120) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER transport_cars_out;


ALTER TABLE `ground_request_schedules`
    ADD `arr_atd` datetime NULL DEFAULT NULL COMMENT 'Origin ATD' AFTER arr_ata,
    ADD `status` enum('open','completed','cancelled') NULL DEFAULT 'open' COMMENT '' COLLATE latin1_swedish_ci AFTER comment,
    MODIFY `arr_ata` datetime NULL DEFAULT NULL COMMENT 'Dest ATD',
    MODIFY `dept_atd` datetime NULL DEFAULT NULL COMMENT 'Dest ATD';
#
#  Fieldformats of
#    ground_request_schedules.arr_ata changed from datetime NULL DEFAULT NULL COMMENT '' to datetime NULL DEFAULT NULL COMMENT 'Dest ATD'.
#    ground_request_schedules.dept_atd changed from datetime NULL DEFAULT NULL COMMENT '' to datetime NULL DEFAULT NULL COMMENT 'Dest ATD'.
#  Possibly data modifications needed!
#

ALTER TABLE `ground_requests`
    ADD `payment_type_requested` enum('cash','credit') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER nature_cargo,
    ADD `payment_type_used` enum('cash','credit') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER payment_type_requested,
    ADD `status` enum('completed','delayed','cancelled','open') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER payment_type_used,
    DROP `ref_no`,
    DROP `flight_no`,
    DROP `handler_id`,
    DROP `payment_type`;


DROP TABLE `job_sheet_services`;

DROP TABLE `job_sheets`;

ALTER TABLE `permits`
    MODIFY `status` enum('closed','canceled','open','deleted','approved','caa','on hold','agent') NULL DEFAULT 'open' COMMENT '' COLLATE latin1_swedish_ci;
#
#  Fieldformat of
#    permits.status changed from enum('closed','canceled','open','deleted','approved','caa','on hold') NULL DEFAULT 'open' COMMENT '' COLLATE latin1_swedish_ci to enum('closed','canceled','open','deleted','approved','caa','on hold','agent') NULL DEFAULT 'open' COMMENT '' COLLATE latin1_swedish_ci.
#  Possibly data modifications needed!
#

ALTER TABLE `hotel_bookings`
    DROP FOREIGN KEY `bk_jobsheet_fk`;


#
# DDL END
#

SET FOREIGN_KEY_CHECKS = 1;