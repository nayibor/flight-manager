#
# MySQLDiff 1.5.0
#
# http://www.mysqldiff.org
# (c) 2001-2004, Lippe-Net Online-Service
#
# Create time: 03.02.2012 09:20
#
# --------------------------------------------------------
# Source info
# Host: localhost
# Database: sbman_op_live
# --------------------------------------------------------
# Target info
# Host: localhost
# Database: sb_opman
# --------------------------------------------------------
#

SET FOREIGN_KEY_CHECKS = 0;

#
# DDL START
#
CREATE TABLE IF NOT EXISTS  `fuel_requests` (
    `id` int(11) unsigned NOT NULL COMMENT '' auto_increment,
    `ref_no` varchar(30) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `fueler_id` int(5) unsigned NULL DEFAULT NULL COMMENT '',
    `company_name` varchar(200) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `company_attn` varchar(40) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `company_tel` varchar(40) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `company_fax` varchar(40) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `company_email` varchar(40) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `purchase_order_no` varchar(40) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `aircraft_optr` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `aircraft_regn` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `aircraft_type` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `aircraft_flight_no` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `arr_dest` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `dept_dest` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `arr_eta` datetime NULL DEFAULT NULL COMMENT '',
    `dept_etd` datetime NULL DEFAULT NULL COMMENT '',
    `est_uplift` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `product` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `created_dt` datetime NULL DEFAULT NULL COMMENT '',
    `created_by` int(5) unsigned NULL DEFAULT NULL COMMENT '',
    `modified_dt` datetime NULL DEFAULT NULL COMMENT '',
    `modified_by` int(5) unsigned NULL DEFAULT NULL COMMENT '',
    `status` varchar(20) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    PRIMARY KEY (`id`),
    INDEX `fueler_id` (`fueler_id`),
    CONSTRAINT FOREIGN KEY (`fueler_id`) REFERENCES `fuelers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS  `fuelers` (
    `id` int(5) unsigned NOT NULL COMMENT '' auto_increment,
    `name` varchar(200) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `description` varchar(255) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `telephone` varchar(30) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `email` varchar(50) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    `fax` varchar(30) NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

ALTER TABLE `ground_request_schedules`
    ADD INDEX `ref_no` (`ref_no`);


ALTER TABLE `permits`
    ADD `handler_id` int(10) unsigned NULL DEFAULT NULL COMMENT '' AFTER aviation_authority_id,
    ADD `permit_application_caa_date` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '' AFTER status,
    ADD `permit_receive_caa_date` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '' AFTER permit_application_caa_date,
    ADD `permit_client_send_date` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '' AFTER permit_receive_caa_date,
    ADD INDEX `aviation_authority_id` (`aviation_authority_id`),
    ADD INDEX `handler_id` (`handler_id`),
    ADD INDEX `ref_no` (`ref_no`);


ALTER TABLE `users`
    ADD `team` enum('Alpha Team','Bravo Team','Charlie Team','Delta Team') NULL DEFAULT NULL COMMENT '' COLLATE latin1_swedish_ci AFTER department_id;


ALTER TABLE `permits`
    ADD CONSTRAINT FOREIGN KEY (`aviation_authority_id`) REFERENCES `aviation_authorities` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT FOREIGN KEY (`handler_id`) REFERENCES `handlers` (`id`) ON DELETE CASCADE;


#
# DDL END
#

SET FOREIGN_KEY_CHECKS = 1;