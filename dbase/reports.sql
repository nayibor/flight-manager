/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50518
 Source Host           : localhost
 Source Database       : sb_opman

 Target Server Type    : MySQL
 Target Server Version : 50518
 File Encoding         : utf-8

 Date: 02/20/2012 20:44:40 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `reports`
-- ----------------------------
DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `position` tinyint(2) DEFAULT NULL,
  `level` tinyint(2) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `params` varchar(100) DEFAULT NULL,
  `controller` varchar(255) NOT NULL,
  `category` enum('ground','permits','fuel') NOT NULL DEFAULT 'ground',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `reports`
-- ----------------------------
INSERT INTO `reports` VALUES ('26', 'Report Tester Flex', '2', '3', 'viewreport', '  Select t1.* from ground_requests t1 limit 1;', '', 'ground'), ('27', 'View Closed Ground Request', '2', '3', 'viewreport', 'select * from asdfasdfa', '', 'ground'), ('40', 'RIP ', '2', '3', 'viewreport', 'rip tester', '', 'ground'), ('42', 'fresh', '2', '3', 'viewreport', 'tester', '', 'ground');

