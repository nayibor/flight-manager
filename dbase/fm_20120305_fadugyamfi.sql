/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2012-03-05 17:22:29
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `permit_attachments`
-- ----------------------------
DROP TABLE IF EXISTS `permit_attachments`;
CREATE TABLE `permit_attachments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `permit_id` int(11) DEFAULT NULL,
  `attachment` longblob,
  `file_name` varchar(200) DEFAULT NULL,
  `mime_type` varchar(50) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `created_by` int(5) unsigned DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(5) unsigned DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `permit_id` (`permit_id`),
  CONSTRAINT `permit_attachments_ibfk_1` FOREIGN KEY (`permit_id`) REFERENCES `permits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

