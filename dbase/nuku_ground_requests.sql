/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-09-08 20:49:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ground_requests`
-- ----------------------------
DROP TABLE IF EXISTS `ground_requests`;
CREATE TABLE `ground_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `mtow` varchar(255) DEFAULT NULL,
  `flight_no` varchar(255) DEFAULT NULL,
  `crew` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `sch_date_departure` date DEFAULT NULL,
  `sch_date_arrival` date DEFAULT NULL,
  `ground_handle_service` enum('0','1') DEFAULT '0',
  `ontime_messages` enum('0','1') DEFAULT '0',
  `name_number` enum('0','1') DEFAULT '0',
  `name_notam` enum('0','1') DEFAULT '0',
  `limit_location` enum('0','1') DEFAULT '0',
  `limit_airport` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_requests
-- ----------------------------
INSERT INTO `ground_requests` VALUES ('1', 'asdf', 'sad', 'asdfas', 'asdf', 'asdf', 'asdf', 'asdf', 'asdsf', 'asdf', '0000-00-00', '0000-00-00', '1', '1', '0', '1', '0', '0');
INSERT INTO `ground_requests` VALUES ('2', 'asfd', 'asfd', 'assdf', 'asdf', 'sdaf', 'asddf', 'daf', 'asdf', 'adf', '0000-00-00', '0000-00-00', '1', '1', '1', '1', '0', '0');
INSERT INTO `ground_requests` VALUES ('3', 'dfa', 'sadf', 'adsf', 'asf', 'sdaf', 'asdf', 'sadf', 'adsfaf', 'adsf', '0000-00-00', '0000-00-00', '1', '1', '1', '1', '0', '0');

-- ----------------------------
-- Table structure for `job_sheets`
-- ----------------------------
DROP TABLE IF EXISTS `job_sheets`;
CREATE TABLE `job_sheets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_number` varchar(50) DEFAULT NULL,
  `handler_id` int(5) DEFAULT NULL,
  `card_number` varchar(200) DEFAULT NULL,
  `holder_name` varchar(200) DEFAULT NULL,
  `card_expiry_date` date DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `est_arrival_dt` datetime DEFAULT NULL,
  `arrival_dt` datetime DEFAULT NULL,
  `est_departure_dt` datetime DEFAULT NULL,
  `departure_dt` datetime DEFAULT NULL,
  `flight_no` varchar(100) DEFAULT NULL,
  `flight_origin` varchar(100) DEFAULT NULL COMMENT 'ICAO/IATA',
  `flight_dest` varchar(100) DEFAULT NULL COMMENT 'ICAO/IATA',
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_registration` varchar(100) DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `status_request` enum('complete','processing') DEFAULT NULL,
  `gr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ground_req_fk` (`gr_id`),
  CONSTRAINT `ground_req_fk` FOREIGN KEY (`gr_id`) REFERENCES `ground_requests` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_sheets
-- ----------------------------
INSERT INTO `job_sheets` VALUES ('1', '1', '1', '12', 'nuku', '2011-07-04', 'takoradi harbour', 'tema', 'infonaligy', '2011-07-03 23:43:16', '2011-07-03 23:43:19', '2011-07-10 23:42:26', '2011-07-03 23:43:26', '1', 'osu', 'winneba', 'tema', 'apache', '1,2,3', 'tema', null, null);
INSERT INTO `job_sheets` VALUES ('20', 'sb0985', '1', 'sb00121', 'nuku-tech', '2011-07-10', 'madina', 'box 343,tema', 'east coast neggars', null, '2011-07-10 23:42:26', null, '2011-07-10 23:42:26', '432432', 'hab city', 'ra city', null, 'DADADFA123', '1,2,3,4', 'tester', null, null);
INSERT INTO `job_sheets` VALUES ('21', 'sb0985', '1', 'sb00121', 'nuku-tech', '2011-07-10', 'madina', 'box 343,tema', 'east coast neggars', null, '2011-07-10 23:42:26', null, '2011-07-11 23:42:26', '432432', 'hab city', 'ra city', null, 'DADADFA123', '1,2,3,4', 'tester', null, null);
INSERT INTO `job_sheets` VALUES ('22', 'sb0985', '1', 'sb00121', 'nuku-tech', '2011-07-10', 'madina', 'box 343,tema', 'east coast neggars', null, '2011-07-10 23:42:26', null, '2011-07-12 23:42:26', '432432', 'hab city', 'ra city', null, 'DADADFA123', '1,2,3,4', 'tester', null, null);
INSERT INTO `job_sheets` VALUES ('23', 'sb0985', '1', 'sb00121', 'nuku-tech', '2011-07-10', 'madina', 'box 343,tema', 'east coast neggars', null, '2011-07-10 23:42:26', null, '2011-07-13 23:42:26', '432432', 'hab city', 'ra city', null, 'DADADFA123', '1,2,3,4', 'tester', null, null);
INSERT INTO `job_sheets` VALUES ('24', '4545445', '1', 'sb-009876', 'nuku', '2011-07-10', 'tema', 'box 3434 tema', 'stexx', null, '2011-07-10 23:42:26', null, '2011-07-14 23:42:26', '433', 'asfda', 'afasdf', null, '43434', '4,5,6', 'stx', null, null);
INSERT INTO `job_sheets` VALUES ('25', '4545445', '1', 'sb-009876', 'nuku', '2011-07-10', 'tema', 'box 3434 tema', 'stexx', null, '2011-07-10 23:42:26', null, '2011-07-15 23:42:26', '433', 'asfda', 'afasdf', null, '43434', '4,5,6', 'stx', null, null);
INSERT INTO `job_sheets` VALUES ('26', '454541290', '2', 'sb-43434', 'taxademis', '2011-07-10', 'locci', 'box 441, Tema', 'boola', null, '2011-07-10 23:42:26', null, '2011-07-16 23:42:26', '5454', 'fadfa', 'afafa', null, 'afdlakdfa', '6', 'adfads', null, null);
