/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-09-15 22:09:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ground_request_dates`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_dates`;
CREATE TABLE `ground_request_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_id` int(11) DEFAULT '0',
  `date_arrival` timestamp NULL DEFAULT NULL,
  `date_departure` timestamp NULL DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gr_fk` (`ground_request_id`),
  CONSTRAINT `gr_fk` FOREIGN KEY (`ground_request_id`) REFERENCES `ground_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ground_request_dates
-- ----------------------------

-- ----------------------------
-- Table structure for `ground_request_services`
-- ----------------------------
DROP TABLE IF EXISTS `ground_request_services`;
CREATE TABLE `ground_request_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ground_request_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ground_equest_fk` (`ground_request_id`),
  CONSTRAINT `ground_equest_fk` FOREIGN KEY (`ground_request_id`) REFERENCES `ground_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_request_services
-- ----------------------------

-- ----------------------------
-- Table structure for `ground_requests`
-- ----------------------------
DROP TABLE IF EXISTS `ground_requests`;
CREATE TABLE `ground_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `mtow` varchar(255) DEFAULT NULL,
  `flight_no` varchar(255) DEFAULT NULL,
  `crew` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `limit_location` enum('0','1') DEFAULT '0',
  `limit_airport` enum('0','1') DEFAULT '0',
  `icc_code` varchar(255) DEFAULT NULL,
  `jobsheet_status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ground_requests
-- ----------------------------

-- ----------------------------
-- Table structure for `job_sheet_services`
-- ----------------------------
DROP TABLE IF EXISTS `job_sheet_services`;
CREATE TABLE `job_sheet_services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_sheet_id` int(11) unsigned DEFAULT NULL,
  `service_id` int(5) unsigned DEFAULT NULL,
  `details` text,
  PRIMARY KEY (`id`),
  KEY `ss_jobsheet_fk` (`job_sheet_id`),
  KEY `ss_services_fk` (`service_id`),
  CONSTRAINT `ss_jobsheet_fk` FOREIGN KEY (`job_sheet_id`) REFERENCES `job_sheets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ss_services_fk` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_sheet_services
-- ----------------------------

-- ----------------------------
-- Table structure for `job_sheets`
-- ----------------------------
DROP TABLE IF EXISTS `job_sheets`;
CREATE TABLE `job_sheets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `job_number` varchar(50) DEFAULT NULL,
  `handler_id` int(5) DEFAULT NULL,
  `card_number` varchar(200) DEFAULT NULL,
  `holder_name` varchar(200) DEFAULT NULL,
  `card_expiry_date` date DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `est_arrival_dt` datetime DEFAULT NULL,
  `arrival_dt` datetime DEFAULT NULL,
  `est_departure_dt` datetime DEFAULT NULL,
  `departure_dt` datetime DEFAULT NULL,
  `flight_no` varchar(100) DEFAULT NULL,
  `flight_origin` varchar(100) DEFAULT NULL COMMENT 'ICAO/IATA',
  `flight_dest` varchar(100) DEFAULT NULL COMMENT 'ICAO/IATA',
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_registration` varchar(100) DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `status_request` enum('complete','created','processing') DEFAULT NULL,
  `gr_id` int(11) DEFAULT NULL,
  `ref_no` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ground_req_fk` (`gr_id`),
  CONSTRAINT `ground_req_fk` FOREIGN KEY (`gr_id`) REFERENCES `ground_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_sheets
-- ----------------------------
