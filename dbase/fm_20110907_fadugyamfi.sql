/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : sb_opman

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2011-09-08 21:45:32
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `permits`
-- ----------------------------
DROP TABLE IF EXISTS `permits`;
CREATE TABLE `permits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('overflight','landing') DEFAULT NULL,
  `application_dt` datetime DEFAULT NULL,
  `ref_no` varchar(100) DEFAULT NULL,
  `permit_location` varchar(100) DEFAULT NULL COMMENT 'Country of Overflight or City of Landing',
  `permit_number` varchar(100) DEFAULT NULL COMMENT 'Permit Number for Overflight or Landing',
  `operator_name` varchar(100) DEFAULT NULL,
  `operator_addr` varchar(200) DEFAULT NULL,
  `principal` varchar(200) DEFAULT NULL,
  `aircraft_regn` varchar(100) DEFAULT NULL,
  `aircraft_type` varchar(100) DEFAULT NULL,
  `aircraft_call_sign` varchar(100) DEFAULT NULL,
  `aircraft_base` varchar(100) DEFAULT NULL,
  `flight_purpose` varchar(200) DEFAULT NULL,
  `flight_times` varchar(255) DEFAULT NULL COMMENT 'ETD/ETA/ETD of Flight',
  `flight_routes` varchar(255) DEFAULT NULL,
  `dept_dest` varchar(150) DEFAULT NULL COMMENT 'Point Of Departure And Destination',
  `captain_name` varchar(200) DEFAULT NULL,
  `souls_on_board` smallint(3) DEFAULT NULL COMMENT 'Number of Crew Members Excluding Flight Captain',
  `cargo_type` varchar(100) DEFAULT NULL,
  `arms_type` varchar(100) DEFAULT NULL,
  `est_dof` datetime DEFAULT NULL,
  `est_doa` datetime DEFAULT NULL,
  `misc_info` tinytext,
  `received_by` varchar(200) DEFAULT NULL COMMENT 'Company or Person Who Received Permit',
  `created_by` int(5) unsigned DEFAULT NULL COMMENT 'Duty Officer Who Served Permit',
  `created_dt` timestamp NULL DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_dt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('closed','canceled','open','deleted','approved','caa') DEFAULT 'open',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permits
-- ----------------------------
INSERT INTO `permits` VALUES ('1', 'overflight', '2011-07-16 13:42:00', 'SBOC1-11', 'HAAB', '1471/YA', 'Bold Fuelers 1', 'Accra', null, 'N583JH', 'CESSNA Caravan 2052', 'N5083JH', 'Nothern', 'Transport', '07AUG10 ETD GOOY 1000UTC ETA DGAA 1300UTC\r\n07AUG10 ETD DGAA 1700UTC ETA GOOY 2011UTC', 'Approved ATS Routes', 'ACC', 'Francis Adu-Gyamfi', '10', 'now cargo', 'n/a', '2011-08-20 11:00:00', '2011-08-20 17:00:00', 'something', null, null, null, '1', '2011-09-08 11:24:07', 'open');
INSERT INTO `permits` VALUES ('2', 'landing', null, 'SBOC2-11', null, 'AAAA', 'sdfsd', 'sdfsd', null, 'sdf', 'sdf', 'sdf', 'sdf', 'd', 'sdf', 'Approved ATS Routes', '', '', '0', 'sdf', 'sadf', '2011-08-24 17:15:00', '2011-08-25 19:00:00', '', null, null, null, '1', '2011-09-08 11:18:31', 'caa');
INSERT INTO `permits` VALUES ('3', 'landing', null, 'SBOC3-11', null, '', 'Moer', 'asdasda', null, 'asdasd', 'asdasd', '12111', '', '', 'asdasd', 'Approved ATS Routes', '', '', null, '', '', '2011-08-26 09:00:00', '2011-08-27 11:00:00', '', null, null, null, '1', '2011-09-08 11:15:53', 'open');
INSERT INTO `permits` VALUES ('4', 'overflight', null, 'SBOC4-11', null, '1473/YA', 'asdas', 'asdasd', null, 'sa', 'asd', 'asdas', '', '', 'qasdas', 'Approved ATS Routes', '', '', null, '', '', '2011-08-25 19:00:00', '2011-08-27 12:21:00', '', null, null, null, '1', '2011-09-08 11:24:25', 'caa');
INSERT INTO `permits` VALUES ('5', 'overflight', null, 'SBOC5-11', null, 'YHNO/Y1', 'Transglobal', 'P.O. Box 1iasda akasd jasd asd', null, 'HNJ011', 'Cessna Caravan 100', 'HNO1', '', '', '20110101', 'Approved ATS Routes', '', '', null, '', '', '2011-09-29 12:22:00', '2011-09-30 02:10:00', '', null, '1', '2011-09-04 00:22:05', '1', '2011-09-08 11:25:04', 'approved');
INSERT INTO `permits` VALUES ('6', 'landing', null, 'SBOC6-11', null, 'SBOC881-10', 'SIGNIA JETS', 'C/O SBMAN &CO LTD, TEL: 00233 244 321771, FAX: 233 21 237768, SITA: ACCSM7X, AFTN: KACCGCKX ', null, 'N75GA', 'HS-125', 'N75GA', 'USA', 'TECH STOP FOR FUEL', 'FEFF-DRZA-LETO\r\n25JUNE2010 ETD FEFF 2115UTC ETA DRZA 2345UTC\r\n26JUNE2010 ETD DRZA 0030UTC ETA LETO TBA', 'Approved ATS Routes', 'HKJK-FEFF-DRZA-LETO', 'CAPT. JOSE LUIS', '3', 'NIL', 'NIL', null, null, '72HRS LEEWAY INCASE OF DELAYED FLIGHT', null, '1', '2011-09-04 00:46:03', '1', '2011-09-08 11:24:50', 'canceled');
INSERT INTO `permits` VALUES ('7', 'overflight', null, 'SBOC7-11', null, '', 'jhgjg', 'dsf', null, 'jgj', 'vd', 'mn', '', '', 'nb', 'Approved ATS Routes', '', '', null, '', '', null, null, '', null, '1', '2011-09-05 22:35:58', null, '2011-09-08 10:04:18', 'approved');
INSERT INTO `permits` VALUES ('8', 'overflight', null, 'SBOC8-11', null, '', 'No', 'a', null, 's', 'as', 'asd', 'sd', 's', 'sfds', 'Approved ATS Routes', 'sd', 'as', '0', 'q', '', null, null, '', null, '1', '2011-09-08 11:00:40', null, '2011-09-08 11:00:40', 'open');
INSERT INTO `permits` VALUES ('9', 'landing', null, 'SBOC9-11', null, '', 'sdf', 'sdf', null, 'sdf', 'sdf', 'ad', 'as', 'asdas', 'af', 'Approved ATS Routes', 'sad', 'sd', '0', '', '', null, null, '', null, '1', '2011-09-08 11:04:57', null, '2011-09-08 11:04:57', 'open');
INSERT INTO `permits` VALUES ('10', 'landing', null, 'SBOC10-11', null, 'asd', 'Jack Professionals', 'sdf', null, 'dsf', 'sdf', 'sf', 'sdf', '', 'ssdf', 'Approved ATS Routes', 's', 'sdf', '0', '', '', null, null, '', null, '1', '2011-09-08 11:05:54', '1', '2011-09-08 12:29:29', 'open');
INSERT INTO `permits` VALUES ('11', 'overflight', null, 'SBOC11-11', null, 'asdsd', 'sdf', 'sdf', null, 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', 'Approved ATS Routes', 'sf', 'sdf', '0', '', '', null, null, '', null, '1', '2011-09-08 11:06:29', null, '2011-09-08 11:06:29', 'open');
INSERT INTO `permits` VALUES ('12', 'landing', null, 'SBOC12-11', null, '', 'asdad', 'asfd', null, 'sdf', 'asdf', 'sdf', 'sdf', '', 'sad', 'Approved ATS Routes', 'sdf', 'sdf', '0', '', '', null, null, '', null, '1', '2011-09-08 11:09:27', null, '2011-09-08 11:09:27', 'open');
INSERT INTO `permits` VALUES ('13', 'overflight', null, 'SBOC13-11', null, '', 'sad', 'asd', null, 'as', 'sf', 'sdf', 'sf', '', 'sad', 'Approved ATS Routes', 'asd', 'asf', '0', '', '', null, null, '', null, '1', '2011-09-08 11:13:23', null, '2011-09-08 11:13:23', 'open');
INSERT INTO `permits` VALUES ('14', 'overflight', null, 'SBOC14-11', null, '', 'hfgfh', 'gfdg', null, 'da', 'fds', 'fds', 'fds', '', 'dsdads', 'Approved ATS Routes', 'ds', '', null, '', '', null, null, '', null, '1', '2011-09-08 11:25:38', null, '2011-09-08 11:25:38', 'open');
INSERT INTO `permits` VALUES ('15', 'landing', null, 'SBOC15-11', null, '', 'hggf', 'fds', null, 'fds', 'fds', 'gfdgd', 'gf', '', 'ds', 'Approved ATS Routes', 'gf', 'fds', '0', '', '', null, null, '', null, '1', '2011-09-08 11:26:06', null, '2011-09-08 11:26:06', 'open');
INSERT INTO `permits` VALUES ('16', 'overflight', null, 'SBOC16-11', null, '', 'jhj', 'fds', null, 'fds', 'fds', 'fdsf', 'fsd', '', 'dssa', 'Approved ATS Routes', 'sa', '', null, '', '', null, null, '', null, '1', '2011-09-08 11:31:49', null, '2011-09-08 11:31:49', 'open');
