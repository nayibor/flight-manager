<?php

class BugReport extends AppModel {
    
    public $name = "BugReport";
    public $displayField = "subject";
    
    public $belongsTo = array(
        'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
    );
}
?>
