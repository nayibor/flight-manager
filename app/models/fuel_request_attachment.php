<?php

class FuelRequestAttachment extends AppModel {
    
    var $name = "FuelRequestAttachment";
    
    public $belongsTo = array(
        'FuelRequest' => array(
            'className' => 'FuelRequest',
            'foreignKey' => 'fuel_request_id',
            'dependent' => false,
			'conditions' => '',
			'fields' => '',
        )
    );
}
?>
