<?php
class Fueler extends AppModel {
	var $name = 'Fueler';
	var $displayField = 'name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'FuelRequest' => array(
			'className' => 'FuelRequest',
			'foreignKey' => 'fueler_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>