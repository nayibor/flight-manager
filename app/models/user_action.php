<?php

class UserAction extends AppModel {

    var $name = 'UserAction';
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function purgeActions() {
        $thirtyAgo = new DateTime();
        $thirtyAgo->sub( new DateInterval("P30D") );
        
        $this->deleteAll(array('UserAction.action_dt <' => $thirtyAgo->format("Y-m-d H:i:s")));
    }

}

?>