<?php
class PermitBilling extends AppModel {
	var $name = 'PermitBilling';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Permit' => array(
			'className' => 'Permit',
			'foreignKey' => 'permit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>