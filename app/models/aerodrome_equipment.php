<?php
class AerodromeEquipment extends AppModel {
	var $name = 'AerodromeEquipment';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Aerodrome' => array(
			'className' => 'Aerodrome',
			'foreignKey' => 'aerodrome_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Equipment' => array(
			'className' => 'Equipment',
			'foreignKey' => 'equipment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>