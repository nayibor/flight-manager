<?php
class FuelRequestStatus extends AppModel {
	var $name = 'FuelRequestStatus';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
    
    var $belongsTo = array(
		'FuelRequest' => array(
			'className' => 'FuelRequest',
			'foreignKey' => 'fuel_request_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'changed_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    public function getDisplayValue() {
        $this->FuelRequest->id = $this->field('fuel_request_id');
        return " changed " . $this->FuelRequest->field('ref_no') . " status to " . $this->field("status");
    }
}
?>