<?php
class Aerodrome extends AppModel {
	var $name = 'Aerodrome';
	var $displayField = 'name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'AerodromeEquipment' => array(
			'className' => 'AerodromeEquipment',
			'foreignKey' => 'aerodrome_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'AerodromeHour' => array(
			'className' => 'AerodromeHour',
			'foreignKey' => 'aerodrome_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>