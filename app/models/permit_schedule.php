<?php
class PermitSchedule extends AppModel {
	var $name = 'PermitSchedule';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Permit' => array(
			'className' => 'Permit',
			'foreignKey' => 'permit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    public function getDisplayValue() {
        $this->Permit->id = $this->field('permit_id');
        return " for " . $this->Permit->field('ref_no');
    }
}
?>