<?php
class UserRole extends AppModel {
	var $name = 'UserRole';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_role_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    
    public function getUserRoles() {
        $roles = array();
        
        if( $this->isSuperUser() ) {
            $roles = $this->find('all', array(
                'order' => 'name',
                'recursive' => -1)
            );
        }
        
        else {
            $roles = $this->find('all', array(
                'conditions' => array(
                    'NOT' => array('id' => 1)
                ),
                'order' => 'name',
                'recursive' => -1)
            );
        }
        
        return $roles;
    }
    
    function isSuperUser() {
        return $_SESSION['user']['User']['user_role_id'] == 1;
    }

}
?>