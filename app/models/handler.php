<?php

class Handler extends AppModel {

    var $name = 'Handler';
    var $displayField = 'name';
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    function findHandlers($type=null) {
        $handlers = NULL;
        
        if ($type != null) {
            $handlers = $this->find('all', array(
                'recursive' => -1,
                'conditions' => array('Handler.type' => $type)
             ));
        } else {
            $handlers = $this->find('list', array(
                'fields' => array('id', 'name', 'country'),
                'order' => array('country', 'name'),
                'recursive' => -1
            ));
        }

        return $handlers;
    }
    
    public function getHandlerId($handler_name = '') {
        
        if( !$handler_name ) {
            return NULL;
        }
        
        $handler = $this->find('first', array(
            'fields' => array('id'),
            'conditions' => array('name LIKE' => '%' . $handler_name . "%"),
            'recursive' => -1
        ));
        
        return $handler['Handler']['id'];
    }

}

?>