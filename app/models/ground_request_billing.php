<?php
class GroundRequestBilling extends AppModel {
	var $name = 'GroundRequestBilling';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'GroundRequest' => array(
			'className' => 'GroundRequest',
			'foreignKey' => 'ground_request_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>