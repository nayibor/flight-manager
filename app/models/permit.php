<?php

class Permit extends AppModel {

    var $name = 'Permit';
    var $displayField = "ref_no";
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'modified_by',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AviationAuthority' => array(
            'className' => 'AviationAuthority',
            'foreignKey' => 'aviation_authority_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Handler' => array(
            'className' => 'Handler',
            'foreignKey' => 'handler_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'PermitStatus' => array(
            'className' => 'PermitStatus',
            'foreignKey' => 'permit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PermitAttachment' => array(
            'className' => 'PermitStatus',
            'foreignKey' => 'permit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PermitSchedule' => array(
            'className' => 'PermitSchedule',
            'foreignKey' => 'permit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasOne = array(
        'PermitBilling' => array(
            'className' => 'PermitBilling',
            'foreignKey' => 'permit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public $general_fields = array('id', 'ref_no', 'type', 'application_dt', 'permit_location', 'permit_number', 'company_name', 'operator_name', 'aircraft_regn', 'aircraft_call_sign', 'status', 'created_dt');

    public function getPermitsByRefNo($ref_no, $offset = 0, $limit = 15) {
        return $this->find('all', array(
            'fields' => $this->general_fields,
            'conditions' => array(
                'ref_no LIKE' => '%' . $ref_no . '%'
            ),
            'order' => array('id' => 'desc'),
            'offset' => $offset,
            'limit' => $limit,
            'recursive' => -1
        ));
    }

    public function getPermitsByKey($key, $val, $offset = 0, $limit = 15) {
        return $this->find('all', array(
            'fields' => $this->general_fields,
            'conditions' => array(
                $key . ' LIKE' => '%' . $val . '%'
            ),
            'order' => array('id' => 'desc'),
            'offset' => $offset,
            'limit' => $limit
        ));
    }
    
    public function getTotalPermitsByKey($key, $val) {
        return $this->find('count', array(
            'conditions' => array(
                $key . ' LIKE' => '%' . $val . '%'
            )
        ));
    }

    public function getOpenPermits($offset = 0, $limit = 15) {
        return $this->find('all', array(
                    'contain' => array('PermitSchedule'),
                    'fields' => $this->general_fields,
                    'conditions' => array(
                        'status' => $this->openStatuses()
                    ),
                    'order' => array('id' => 'desc'),
                    'offset' => $offset,
                    'limit' => $limit
                ));
    }

    public function getClosedPermits($offset = 0, $limit = 15) {
        return $this->find('all', array(
                    'contain' => array('PermitSchedule'),
                    'fields' => $this->general_fields,
                    'conditions' => array(
                        'NOT' => array('status' => $this->openStatuses())
                    ),
                    'order' => array('id' => 'desc'),
                    'offset' => $offset,
                    'limit' => $limit
                ));
    }

    public function getOpenActivePermits($offset = 0, $limit = 20) {
        return $this->find('all', array(
            'contain' => array('PermitSchedule' => array(
                    'fields' => array('arr_eta', 'arr_etd')
            )),
            'fields' => $this->general_fields,
            'conditions' => array(
                'Permit.status' => $this->openStatuses()//,
                //'PermitSchedule.arr_etd >=' => date('Y-m-d')
            ),
            'order' => array('Permit.created_dt' => 'desc'),
            'offset' => $offset,
            'limit' => $limit
        ));
    }

    public function getTotalOpen() {
        return $this->find('count', array(
                    'conditions' => array('status' => $this->openStatuses())
                ));
    }

    public function getTotalClosed() {
        return $this->find('count', array(
                    'conditions' => array(
                        'NOT' => array('status' => $this->openStatuses())
                    )
                ));
    }

    public function getTotalOpenDueToday() {
        return $this->find('count', array(
            'conditions' => array(
                'status' => $this->openStatuses(),
                'DATE(flight_dt)' => date('Y-m-d')
            )
        ));
    }

    public function setStatus($id, $status, $user_id = NULL, $comments = '') {

        $this->save(array(
            'id' => $id,
            'status' => $status
        ));

        $this->PermitStatus->save(array(
            'permit_id' => $id,
            'status' => $status,
            'comments' => $comments,
            'change_dt' => date('Y-m-d H:i:s'),
            'changed_by' => $user_id
        ));
    }

    public function openStatuses() {
        return array('open', 'approved', 'caa', 'agent', 'on hold');
    }

    public function setAviationAuthority($id, $authority_id, $user_id, $handler_id = NULL) {

        $this->save(array(
            'aviation_authority_id' => $authority_id,
            'handler_id' => $handler_id,
            'id' => $id
        ));

        if( $handler_id ) {
            $this->setStatus($id, 'agent', $user_id);
        }
        
        else {
            $this->setStatus($id, 'caa', $user_id);
        }
    }

    public function getForRequestForm($permit_id) {
        return $this->find('first', array(
            'contain' => array(
                'AviationAuthority', 'Handler',
                'PermitStatus' => array(
                    'conditions' => array('status' => 'caa'),
                    'order' => array('id' => 'desc'),
                    'limit' => 1
                ),
                'PermitSchedule'
            ),
            'conditions' => array('Permit.id' => $permit_id))
        );
    }

    public function getForRequestFormPrint($permit_id) {
        return $this->find('first', array(
                    'contain' => array(
                        'AviationAuthority', 'PermitSchedule',
                        'PermitStatus' => array(
                            'conditions' => array('status' => array('caa','agent') ),
                            'order' => array('id' => 'desc'),
                            'limit' => 1,
                            'User' => array(
                                'fields' => array('first_name', 'last_name'),
                                'UserRole'
                            )
                        )
                    ),
                    'conditions' => array('Permit.id' => $permit_id))
        );
    }

    public function clearInfo($permit_id) {
        $this->id = $permit_id;
        
        $this->set(array(
            'permit_ref_no' => null,
            'permit_number' => null,
            'permit_validity' => null,
            'permit_receive_caa_date' => null,
            'status' => $this->field('handler_id') != '' ? 'agent' : 'caa'
        ));
        
        $this->save();
    }
}

?>