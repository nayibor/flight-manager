<?php
class HotelBooking extends AppModel {
	var $name = 'HotelBooking';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Jobsheet' => array(
			'className' => 'Jobsheet',
			'foreignKey' => 'jobsheet_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Hotel' => array(
			'className' => 'Hotel',
			'foreignKey' => 'hotel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>