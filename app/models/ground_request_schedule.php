<?php

class GroundRequestSchedule extends AppModel {

    var $name = 'GroundRequestSchedule';
    var $displayField = "ref_no";
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'GroundRequest' => array(
            'className' => 'GroundRequest',
            'foreignKey' => 'ground_request_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Handler' => array(
            'className' => 'Handler',
            'foreignKey' => 'handler_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'GroundRequestScheduleService' => array(
            'className' => 'GroundRequestScheduleService',
            'foreignKey' => 'ground_request_schedule_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'GroundRequestScheduleAttachment' => array(
            'className' => 'GroundRequestScheduleAttachment',
            'foreignKey' => 'ground_request_schedule_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasOne = array(
        'GroundRequestQuality' => array(
            'className' => 'GroundRequestQuality',
            'foreignKey' => 'ground_request_schedule_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    
    public function getTotalDueToday() {
        return $this->find('count', array(
            'contain' => array('GroundRequest'),
            'conditions' => array(
                'OR' => array(
                    'GroundRequestSchedule.arr_eta BETWEEN ? AND ?' => array(date('Y-m-d H:i:s', strtotime('today')), date('Y-m-d H:i:s', strtotime('tomorrow'))),
                    'GroundRequestSchedule.dept_etd BETWEEN ? AND ?' => array(date('Y-m-d H:i:s', strtotime('today')), date('Y-m-d H:i:s', strtotime('tomorrow')))
                ),
                'GroundRequest.jobsheet_status' => 'open'
            )
        ));
    }
}

?>