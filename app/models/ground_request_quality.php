<?php

class GroundRequestQuality extends AppModel {

    var $name = 'GroundRequestQuality';
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'GroundRequestSchedule' => array(
            'className' => 'GroundRequestSchedule',
            'foreignKey' => 'ground_request_schedule_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function findRequests($id = null) {
        $requests = NULL;

        if ( $id == null ) {
            $requests = $this->find('all', array('recursive' => -1, 'order' => array('id')));
        } else {
            $requests = $this->find('all', array('recursive' => -1, 'conditions' => array('GroundRequestQuality.id' => $id)));
        }

        return $requests;
    }
    
    public function getDisplayValue() {
        $this->GroundRequestSchedule->id = $this->field('ground_request_schedule_id');
        return " for " . $this->GroundRequestSchedule->field('ref_no');
    }

}

?>