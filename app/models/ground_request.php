<?php

class GroundRequest extends AppModel {

    var $name = 'GroundRequest';
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $hasMany = array(
        'GroundRequestSchedule' => array(
            'className' => 'GroundRequestSchedule',
            'foreignKey' => 'ground_request_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'GroundRequestBilling' => array(
            'className' => 'GroundRequestBilling',
            'foreignKey' => 'ground_request_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
    public function getDisplayValue() {
        return " for " . $this->field('company_name');
    }

    function findRequests($id = null) {
        $requests = NULL;

        if ( $id == null ) {
            $requests = $this->find('all', array('contain' => array('JobSheet')));
        }
        else {
            $requests = $this->find('all', array(
                'conditions' => array('GroundRequest.id' => $id),
                'contain' => array('JobSheet')
                ));
        }

        return $requests;
    }

    function findreqstatus($status, $offset = 0, $limit = 15) {
        $requests = NULL;
        $order = array('id' => 'desc');
        $contains = array();
        $conditions = array();

        if ( $status == "departures" ) {
            $conditions = array('GroundRequest.jobsheet_status <>' => '');
            $cont = array('GroundRequestDate.date_departure <' => date('Y-m-d H:i:s', strtotime("now")));
            $order = array('GroundRequestDate.date_departure' => 'desc');
            $contains = array(
                'GroundRequestSchedule' => array(
                    'conditions' => $cont,
                    'order' => $order
                )
            );
        }
        else if ( $status == "arrivals" ) {
            $conditions = array('GroundRequest.jobsheet_status <>' => '');
            $cont = array('GroundRequestDate.date_departure >' => date('Y-m-d H:i:s', strtotime("now")));
            $order = array('GroundRequestDate.date_arrival' => 'desc');
            $contains = array(
                'GroundRequestDate' => array(
                    'conditions' => $cont,
                    'order' => $order
                )
            );
        }
        else if ( $status == "open" or $status == "closed" ) {
            $conditions = array('GroundRequest.jobsheet_status' => $status);
        }

        $requests = $this->find('all', array(
            'conditions' => $conditions,
            'recursive' => -1,
            'contain' => $contains,
            'offset' => $offset,
            'limit' => $limit,
            'order' => $order)
        );

        return $requests;
    }
}

?>