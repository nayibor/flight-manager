<?php
class ChartParameter extends AppModel {
	var $name = 'ChartParameter';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Chart' => array(
			'className' => 'Chart',
			'foreignKey' => 'chart_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>