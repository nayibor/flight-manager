<?php
class PermitStatus extends AppModel {
	var $name = 'PermitStatus';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Permit' => array(
			'className' => 'Permit',
			'foreignKey' => 'permit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'changed_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    public function getDisplayValue() {
        $this->Permit->id = $this->field('permit_id');
        return " changed " . $this->Permit->field('ref_no') . " status to " . $this->field("status");
    }
}
?>