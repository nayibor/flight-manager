<?php

class Hotel extends AppModel {

    var $name = 'Hotel';
    var $displayField = 'name';
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $hasMany = array(
        'HotelBooking' => array(
            'className' => 'HotelBooking',
            'foreignKey' => 'hotel_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    function findHotels($id=null) {
        $hotels = NULL;
        
        if ($id == null) {
            $hotels = $this->find('all', array('recursive' => -1, 'order' => array('name')));
        } else {
            $hotels = $this->find('all', array('recursive' => -1, 'conditions' => array('Hotel.id' => $id)));
        }
        
        return $hotels;
    }

}

?>