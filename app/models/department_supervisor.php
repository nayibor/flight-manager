<?php
class DepartmentSupervisor extends AppModel {
	var $name = 'DepartmentSupervisor';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Department' => array(
			'className' => 'Department',
			'foreignKey' => 'department_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    public function getSupervisor($user) {
        return $this->find('first', array(
            'fields' => array('User.first_name','User.last_name'),
            'conditions' => array(
                'start_dt <=' => date('Y-m-d'),
                'end_dt >=' => date('Y-m-d'),
                'DepartmentSupervisor.team' => $user['User']['team']
            )
        ));
    }
}
?>