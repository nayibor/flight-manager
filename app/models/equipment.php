<?php
class Equipment extends AppModel {
	var $name = 'Equipment';
	var $displayField = 'name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasAndBelongsToMany = array(
		'Aerodrome' => array(
			'className' => 'Aerodrome',
			'joinTable' => 'aerodrome_equipments',
			'foreignKey' => 'equipment_id',
			'associationForeignKey' => 'aerodrome_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
?>