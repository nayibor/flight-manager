<?php
class GroundRequestScheduleService extends AppModel {
    var $name = 'GroundRequestScheduleService';
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
		'Service' => array(
			'className' => 'Service',
			'foreignKey' => 'service_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GroundRequestSchedule' => array(
			'className' => 'GroundRequestSchedule',
			'foreignKey' => 'ground_request_schedule_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    function findRequests($id=null) {
        $requests = NULL;

        if ($id == null) {
            $requests = $this->find('all', array('recursive' => -1, 'order' => array('id')));
        } else {
            $requests = $this->find('all', array('recursive' => -1, 'conditions' => array('GroundRequestService.id' => $id)));
        }

        return $requests;
    }

    public function getDisplayValue() {
        $this->Service->id = $this->field('service_id');
        $this->GroundRequestSchedule->id = $this->field('ground_request_schedule_id');
        return $this->Service->field('name') . " for " . $this->GroundRequestSchedule->field('ref_no');
    }
}
?>