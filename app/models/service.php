<?php

class Service extends AppModel {

    var $name = 'Service';
    var $displayField = 'name';
//The Associations below have been created with all possible keys, those that are not needed can be removed

    
    function findServices($id=null) {
        $services;

        if ($id == null) {
            $services = $this->find('all', array('recursive' => -1));
        } else {
            $services = $this->find('first', array('recursive' => -1, 'conditions' => array('Service.id' => $id)));
        }
        return $services;
    }

}

?>