<?php
class AerodromeHour extends AppModel {
	var $name = 'AerodromeHour';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Aerodrome' => array(
			'className' => 'Aerodrome',
			'foreignKey' => 'aerodrome_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>