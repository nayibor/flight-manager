<?php

class PermitAttachment extends AppModel {
    
    var $name = "PermitAttachment";
    
    public $belongsTo = array(
        'Permit' => array(
            'className' => 'Permit',
            'foreignKey' => 'permit_id',
            'dependent' => false,
			'conditions' => '',
			'fields' => '',
        )
    );
}
?>
