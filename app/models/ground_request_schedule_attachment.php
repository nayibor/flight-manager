<?php
class GroundRequestScheduleAttachment extends AppModel {
	var $name = 'GroundRequestScheduleAttachment';
	var $displayField = 'file_name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'GroundRequestSchedule' => array(
			'className' => 'GroundRequestSchedule',
			'foreignKey' => 'ground_request_schedule_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>