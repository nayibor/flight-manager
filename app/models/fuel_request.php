<?php
class FuelRequest extends AppModel {
	var $name = 'FuelRequest';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
    
    var $hasMany = array(
        'FuelRequestStatus' => array(
            'className' => 'FuelRequestStatus',
            'foreignKey' => 'fuel_request_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public function getRequests($status = 'open', $offset = 0, $limit = 25) {
        
        return $this->find('all', array(
            'conditions' => array('status' => $status),
            'order' => array('FuelRequest.id' => 'desc'),
            'offset' => $offset,
            'limit' => $limit
        ));
    }
    
    public function getRequestsByKey($key, $val, $offset = 0, $limit = 15) {
        return $this->find('all', array(
            'conditions' => array(
                $key . ' LIKE' => '%' . $val . '%'
            ),
            'order' => array('id' => 'desc'),
            'offset' => $offset,
            'limit' => $limit,
            'recursive' => -1
        ));
    }
    
    public function getOpenRequests($offset = 0, $limit = 25) {
        return $this->getRequests(array('open','wfs','approved','forwarded'), $offset, $limit);
    }
    
    public function getClosedRequests($offset = 0, $limit = 25) {
        return $this->getRequests(array('closed', 'deleted'), $offset, $limit);
    }
    
    public function getTotalByStatus($status = 'open') {
        return $this->find('count', array(
            'conditions' => array('status' => $status)
        ));
    }
    
    public function getTotalOpen() {
        return $this->getTotalByStatus(array('open','wfs','approved','forwarded'));
    }
    
    public function getTotalClosed() {
        return $this->getTotalByStatus(array('closed','deleted'));
    }
}
?>