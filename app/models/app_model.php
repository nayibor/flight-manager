<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Application model for Cake.
 *
 * This is a placeholder class.
 * Create the same file in app/app_model.php
 * Add your application-wide methods to the class, your models will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake.libs.model
 */
App::import('Lib', 'LazyModel.LazyModel');

class AppModel extends LazyModel {

    var $actsAs = array('Containable');
    
    var $inserted_ids = array();
    
    /**
     * Saves a UserAction record for the action that was just performed
     * 
     * @param type $user_id
     * @param type $action_id
     * @param type $action_desc 
     */
    public function recordAction($user_id, $action_id, $action_desc) {
        $userAction = ClassRegistry::init('UserAction');

        $userAction->set(array(
            'user_id' => $user_id,
            'action_id' => $action_id,
            'action_desc' => $action_desc,
            'action_dt' => date('Y-m-d H:i:s')
        ));
        $userAction->save();
    }

    /**
     * Returns the value that can be used with the UserAction description
     * 
     * @return mixed 
     */
    public function getDisplayValue() {
        return $this->displayField != "id" ? $this->field($this->displayField) : "";
    }

    /**
     * Called after each save() operation to update the UserAction information for
     * the Model that was just saved
     * 
     * @param type $created
     * @return type 
     */
    public function afterSave($created) {

        if( $created ) {
            $this->inserted_ids[] = $this->getInsertID();
        }
        
        if( get_class($this) == "UserAction" ) {
            return;
        }

        parent::afterSave($created);

        if( isset($_SESSION['user']) ) {
            $staff = $_SESSION['user'];
            $id = $created ? $this->getInsertID() : $this->id;
            $description = ($created ? "Added" : "Updated") . " " . get_class($this) . " " . $this->getDisplayValue();
            $this->recordAction($staff['User']['id'], $id, $description);
        }
    }

    function getNextAutoIncrement() {

        $table = Inflector::tableize($this->name);
        $info = $this->query("SHOW TABLE STATUS LIKE '$table'");
        return $info[0]['TABLES']['Auto_increment'];
    }

    /**
     * Returns the IDs for all models saved using the saveAll function
     * 
     * @return type 
     */
    public function getInsertIDs() {
        return $this->inserted_ids;
    }
}
