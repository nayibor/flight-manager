<style>
    div.message a {
        text-decoration: none;
        color: #069;
    }
    
    div.message a:hover {
        color: #096;
    }
</style>

<?php $user = $this->Session->read('user'); ?>

<h1>Welcome <!--To <?php echo Configure::read('app_name'); ?>--> <span><?php echo $user['User']['first_name']; ?> (<?php echo $user['User']['team']; ?>)</span>!</h1>
<p>What would you like to do today?</p>

<div class="pad20">
    <!-- Big buttons -->


    <ul class="dash">
        <?php foreach ($menus as $menu) { ?>
            <li>
                <a href="<?php echo $html->url(array($menu['Menu']['prefix'] => true, 'controller' => $menu['Menu']['controller'], 'action' => $menu['Menu']['action'])); ?>" title="<?php echo $menu['Menu']['subtitle']; ?>" class="tooltip">
                    <?php echo $html->image($menu['Menu']['icon_path']); ?>
                    <span><?php echo $menu['Menu']['title']; ?></span>
                </a>
            </li>
        <?php } ?>

    </ul>
</div>


<hr />



<h1>Notifications</h1>
<input type="hidden" name="notifications_url" id="notifications_url" value="<?php echo $html->url(array('controller' => 'Operations', 'action' => 'index')); ?>" />
<div>
    <div classs="pad20">
        
        <div class="message warning">
            <!--<h2>Notices</h2>-->
            <p>You Have <b><?php echo $permit_due_today; ?></b> Uncompleted Permits For Flights Due Today. Go To <a href="<?php echo $html->url(array('controller' => 'operations', 'action' => 'index')); ?>">Operations Control</a></p>
            <p>You Have <b><?php echo $flights_today; ?></b> Flights Scheduled For Today. View <a href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'index')); ?>">Ground Handling Dashboard</a></p>
        </div>

    </div>
</div>



<?php echo $html->script('app/notifications.js'); ?>