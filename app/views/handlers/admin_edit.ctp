
<style>
    #handler_form td {
        padding: 5px;
    }
</style>
<form id="handler_form" name="handler_form" method="post" action="<?php echo $html->url( array('controller' => 'handlers', 'action' => 'edit')); ?>">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td width="16%">Name:</td>
            <td width="84%">
                <input type="text" name="data[Handler][name]" id="data[Handler][name]" required="" size="50" value="<?php echo $handler['Handler']['name']; ?>" />
            </td>
        </tr>
        <tr>
            <td>Country:</td>
            <td>
                <input type="text" name="data[Handler][country]" id="data[Handler][country]" value="<?php echo $handler['Handler']['country']; ?>" />
            </td>
        </tr>
        <tr>
            <td>City:</td>
            <td>
                <input type="text" name="data[Handler][city]" id="data[Handler][city]" value="<?php echo $handler['Handler']['city']; ?>" />
            </td>
        </tr>
        <tr>
            <td>Airport/Location:</td>
            <td>
                <input type="text" name="data[Handler][location]" id="data[Handler][location]" value="<?php echo $handler['Handler']['location']; ?>" />
            </td>
        </tr>
        <tr>
            <td valign="top">Phone Numbers:</td>
            <td>
                <textarea name="data[Handler][telephone]" id="data[Handler][telephone]" cols="45" rows="2" style="height: 50px; width: 300px"><?php echo $handler['Handler']['telephone']; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Fax:</td>
            <td>
                <input type="text" name="data[Handler][fax]" id="data[Handler][fax]" value="<?php echo $handler['Handler']['fax']; ?>" />
            </td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>
                <input name="data[Handler][email]" type="text" id="data[Handler][email]" size="60" value="<?php echo $handler['Handler']['email']; ?>" />
            </td>
        </tr>
        
        
        <tr>
            <td>SITA</td>
            <td>
                <input type="text" name="data[Handler][sita]" id="data[Handler][sita]"  value="<?php echo $handler['Handler']['sita']; ?>" />
            </td>
        </tr>
        <tr>
            <td>Radio Freq.</td>
            <td>
                <input type="text" name="data[Handler][radio_freq]" id="data[Handler][radio_freq]"  value="<?php echo $handler['Handler']['radio_freq']; ?>" />
            </td>
        </tr>
        <tr>
            <td valign="top">ATTN(s):</td>
            <td>
                <textarea name="data[Handler][attn]" id="data[Handler][attn]" cols="45" rows="2" style="height: 80px; width: 300px"><?php echo $handler['Handler']['attn']; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" class="button" name="submit_btn" id="submit_btn" value="Save Changes" />
                <input type="button" class="button" name="cancel_btn" id="cancel-form" value="Cancel" />
                <input type="hidden" name="data[Handler][id]" value="<?php echo $handler['Handler']['id']; ?>" />
            </td>
        </tr>
    </table>
</form>