<table class="fullwidth" cellspacing="0" cellpadding="3" border="0">
    <thead>
        <tr>
            <th width="10"><input type="checkbox" class="check_all" /></th>
            <th width="140">Country</th>
            <th width="240">Agent</th>
            <th style="text-align: center;">Options</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $count = 0;
        $showCount = false;
        $country = "";
        foreach ($handlers as $handler) {
            if ($country != $handler['Handler']['country']) {
                $country = $handler['Handler']['country'];
                $count++;
                $showCount = true;
            } else {
                $showCount = false;
            }
            ?>
            <tr data-url="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'details', $handler['Handler']['id'])); ?>">
                <td><input type="checkbox" value="<?php echo $handler['Handler']['id']; ?>" /></td>
                <td style="font-weight: bold;"><?php echo $showCount ? $country : ""; ?></td>
                <td><?php echo $handler['Handler']['name']; ?></td>
                <td class="options" align="center" nowrap>
                    <a class="dialog_opener" title="Edit Handler Information" data-height="580" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'edit', $handler['Handler']['id'])); ?>">Edit</a>
                    <a class="delete_link" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'delete', $handler['Handler']['id'])); ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>