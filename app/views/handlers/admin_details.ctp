<style>
    .details_table td {
        padding: 8px;
        font-size: 11px;
    }
</style>

<h2>Handler Details</h2>

<table border="0" cellspacing="0" cellpadding="10" width="96%" class="details_table" align="center" style="margin: 0px auto;">
    <tr>
        <td width="120">Name</td>
        <td><?php echo $handler['Handler']['name']; ?></td>
    </tr>
    
    <tr>
        <td>Country</td>
        <td><?php echo $handler['Handler']['country']; ?></td>
    </tr>
    
    <tr>
        <td>City</td>
        <td><?php echo $handler['Handler']['city']; ?></td>
    </tr>
    
    <tr>
        <td>Airport/Location</td>
        <td><?php echo $handler['Handler']['location']; ?></td>
    </tr>

    <tr>
        <td valign="top">Contact Number</td>
        <td><?php echo str_replace("\n", "<br />", $handler['Handler']['telephone']); ?></td>
    </tr>
    <tr>
        <td>Fax Number:</td>
        <td><?php echo $handler['Handler']['fax']; ?></td>
    </tr>
    
    <tr>
        <td>Email Address</td>
        <td><a href="mailto:<?php echo $handler['Handler']['email']; ?>"><?php echo $handler['Handler']['email']; ?></a></td>
    </tr>
    
    <tr>
        <td>Sita</td>
        <td><?php echo $handler['Handler']['sita']; ?></td>
    </tr>
    
    <tr>
        <td>Radio Freq.</td>
        <td><?php echo $handler['Handler']['radio_freq']; ?></td>
    </tr>
    
    <tr>
        <td valign="top">ATTN(s)</td>
        <td><?php echo str_replace("\n", "<br />", $handler['Handler']['attn']); ?></td>
    </tr>
</table>