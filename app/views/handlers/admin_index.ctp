
<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->script('app/handlers.js'); ?>

<h2><?php __('Manage Directory of Handling Agents'); ?></h2>

<div class="columns">
    
    <div class="column" id="users_column">
        <div class="header">Directory Of Handling Agents</div>
        <div class="subheader">
            <ul>
                <li><a class="dialog_opener" id="add_handler" title="Add New Handler" href="<?php echo $html->url( array('controller' => 'handlers', 'action' => 'add')); ?>">Add New Handler</a></li>
                <li><a class="delete_selected" href="<?php echo $html->url( array('controller' => 'handlers', 'action' => 'delete_selected')); ?>">Delete Selected Handlers</a></li>
            </ul>
        </div>
        <div class="content">
            
            <table class="fullwidth" cellspacing="0" cellpadding="5" border="0">
                <thead>
                    <tr>
                        <th width="10"><input type="checkbox" class="check_all" /></th>
                        <th width="230" colspan="2">Company Name</th>
                        <th width="110">Location</th>
                        <th width="140">Telephone</th>
                        <th width="200">Email Address</th>
                        <th style="text-align: center;">Options</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php $count = 1; ?>
                    <?php foreach($handlers as $handler) { ?>
                    <tr>
                        <td><input type="checkbox" value="<?php echo $handler['Handler']['id']; ?>" /></td>
                        <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                        <td><?php echo $handler['Handler']['name']; ?></td>
                        <td><?php echo $handler['Handler']['location']; ?></td>
                        <td><?php echo $handler['Handler']['telephone']; ?></td>
                        <td><a href='mailto:<?php echo $handler['Handler']['email']; ?>'><?php echo $handler['Handler']['email']; ?></a></td>
                        <td class="options" align="center">
                            <a class="dialog_opener" href="<?php echo $html->url( array('controller' => 'handlers', 'action' => 'edit', $handler['Handler']['id']) ); ?>">Edit</a>
                            <a class="delete_link" href="<?php echo $html->url( array('controller' => 'handlers', 'action' => 'delete', $handler['Handler']['id'])); ?>">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            
        </div>
        
        <div class="footer">
            <?php echo $this->Paginator->numbers(); ?>
        </div>
    </div>
    
</div>
