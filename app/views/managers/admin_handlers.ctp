
<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->script('app/handlers.js'); ?>

<h2><?php __('Manage Directory of Handling Agents'); ?></h2>

<div class="columns">

    <div class="column" id="users_column">
        <div class="header">Directory Of Handling Agents</div>
        <div class="subheader">
            <ul style="display: inline-block;">
                <li class="button"><a class="dialog_opener" data-height="580" id="add_handler" title="Add New Handler" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'add')); ?>">Add New Handler</a></li>
                <li class="button"><a class="delete_selected" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'delete_selected')); ?>">Delete Selected Handlers</a></li>
            </ul>

            <div style="display: inline-block; margin-left: 100px;">
                Find Handler By: <select id="search_by" style="font-size: 11px;">
                    <option value="name">Name</option>
                    <option value="country">Country</option>
                    <option value="email">Email</option>
                    <option value="location">ICAO/IATA</option>
                </select>
                
                <input type="text" id="search_field" title="Enter Value and Hit Enter" placeholder="e.g. Name" style="font-size: 11px;" data-url="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'list')); ?>"/>
            </div>
        </div>

        <div class="content">

            <div class="master_block" style="width: 50%">
                <table class="fullwidth" cellspacing="0" cellpadding="3" border="0">
                    <thead>
                        <tr>
                            <th width="10"><input type="checkbox" class="check_all" /></th>
                            <th width="140">Country</th>
                            <th width="240">Agent</th>
                            <!--<th width="180">Telephone</th>
                            <th width="160">Email Address</th>-->
                            <th style="text-align: center;">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $count = 0;
                        $showCount = false;
                        $country = "";
                        foreach ($handlers as $handler) {
                            if ($country != $handler['Handler']['country']) {
                                $country = $handler['Handler']['country'];
                                $count++;
                                $showCount = true;
                            } else {
                                $showCount = false;
                            }
                            ?>
                            <tr data-url="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'details', $handler['Handler']['id'])); ?>">
                                <td><input type="checkbox" value="<?php echo $handler['Handler']['id']; ?>" /></td>
                                <!--<td align="right" width="20"><?php echo ($showCount ? $count . "." : ""); ?></td>-->
                                <td style="font-weight: bold;"><?php echo $showCount ? $country : ""; ?></td>
                                <td><?php echo $handler['Handler']['name']; ?></td>
                                <!--<td><?php echo $handler['Handler']['location']; ?></td>
                                <td><?php echo $handler['Handler']['telephone']; ?></td>
                                <td><a href='mailto:<?php echo $handler['Handler']['email']; ?>'><?php echo $handler['Handler']['email']; ?></a></td>-->
                                <td class="options" align="center" nowrap>
                                    <a class="dialog_opener" title="Edit Handler Information" data-height="580" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'edit', $handler['Handler']['id'])); ?>">Edit</a>
                                    <a class="delete_link" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'delete', $handler['Handler']['id'])); ?>">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <div class="details_block" style="width: 50%">
                
            </div>
        </div>

        <?php echo $this->element('layout/pagination_footer'); ?>

        <!--<div class="footer">
            Pages: <?php echo $this->Paginator->numbers(); ?>
        </div>-->
    </div>

</div>
