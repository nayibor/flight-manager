
<?php
if (isset($results) && count($results) == 1) {
    ?>
    <table border="0" cellspacing="0" cellpadding="5" width="100%">
        <?php
        foreach ($results[0][$area] as $field => $value) {
            if ($field != "id") {
                ?>
                <tr>
                    <td valign="top" width="100"><?php echo ucwords(str_replace("_", " ", $field)); ?>: </td>
                    <td valign="top">
                        <?php
                        switch ($field) {
                            case 'email':
                                echo "<a href='mailto:$value'>$value</a>";
                                break;
                            
                            case 'website':
                                echo "<a href='$value' target='_blank'>$value</a>";
                                break;
                            default:
                                echo $value;
                                break;
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>

<?php } else { ?>

    <table border="0" width="100%" cellspacing="0" cellpadding="5">
        <thead>
            <tr>
                <td colspan="2"><b>Name</b></td>
                <td><b>Country</b></td>
            </tr>
        </thead>

        <tbody>
            <?php $count = 1;
            foreach ($results as $result) {
                ?>
                <tr>
                    <td align="right"><?php echo $count++ . "."; ?></td>
                    <?php
                    foreach ($result[$area] as $field => $value) {

                        if ($field == "name" && in_array($area, array('Handler', 'Airport', 'Hotel'))) {
                            ?>
                            <td><a data-id="<?php echo $result[$area]['id']; ?>" href="<?php echo $html->url(array('directory_search')); ?>" class="result_link"><?php echo strtoupper($value); ?></a></td>
                            <?php
                        } else if ($field == "country" && in_array($area, array('AviationAuthority'))) {
                            ?>
                            <td><a data-id="<?php echo $result[$area]['id']; ?>" href="<?php echo $html->url(array('directory_search')); ?>" class="result_link"><?php echo strtoupper($value); ?></a></td>
                            <?php
                        }

                        if ($field == "country" && !in_array($area, array('AviationAuthority'))) {
                            echo "<td>$value</td>";
                        }

                        if ($field == "iso_region" && in_array($area, array('Airport'))) {
                            echo "<td>$value</td>";
                        }
                    }
                    ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>

<?php } ?>

<?php
if (count($results) == 15) {
    echo "<br />Showing Top 15 Results";
}
?>