<?php $user = $this->Session->read('user'); ?>

<?php
echo $html->css('app/columns.css');
echo $html->css('rating/jquery.rating.css');
echo $html->script('rating/jquery.rating.pack.js');
echo $html->script('app/hotel.js');
?>

<style>
    .fullwidth th, .fullwidth td {
        padding: 7px !important;
    }
</style>

<h2>Hotels</h2>


<div class="columns">

    <div class="column" id="users_column">
        <div class="header">List of Hotels</div>
        <div class="subheader">
            <ul style="display: inline-block;">
                <li class="button"><a class="dialog_opener" id="add_hotel" title="Add New Hotel" href="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'add')); ?>">Add New Hotel</a></li>
                <!--<li><a class="delete_selected" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'delete_selected')); ?>">Delete Selected Handlers</a></li>-->
            </ul>
            
            <div style="display: inline-block; margin-left: 100px;">
                Find Hotel: <input type="text" id="search_field" placeholder="e.g. Name" style="font-size: 11px;" data-url="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'list')); ?>"/>
                
                By <select id="search_by" style="font-size: 11px;">
                    <option value="name">Name</option>
                    <option value="country">Country</option>
                    <option value="email">Email</option>
                </select>
            </div>
        </div>
        <div class="content">

            <div class="master_block" style="width: 50%" >
                <table border="0" class="fullwidth" cellpadding="8" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="20" align="right"><input type="checkbox" value="" id="checkall" /></th>
                            <th colspan="2">Hotel Name</th>
                            <th style="text-align: center;" width="100">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = $this->Paginator->counter('%start%');
                        foreach ($hotels as $val) {
                            ?>
                            <tr data-url="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'details', $val['Hotel']['id'])); ?>">
                                <td align="right"><input type="checkbox" value="<?php echo $val['Hotel']['id']; ?>" /></td>
                                <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                                <td class="hotel_name"><?php echo $val['Hotel']['name'] ?></td>
                                <td align="center">
                                    <a class="dialog_opener edit_link" title="Edit Hotel Information" href="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'edit', $val['Hotel']['id'])); ?>">Edit</a>
                                    <a class="del" data-id="<?php echo $val['Hotel']['id'] ?>" del_url="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'delete', $val['Hotel']['id'])); ?>">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div>

            <div class="details_block" style="width: 50%">
                &nbsp;
            </div>
        </div>

        <?php echo $this->element('layout/pagination_footer'); ?>
    </div>

</div>
