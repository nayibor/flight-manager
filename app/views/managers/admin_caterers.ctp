<?php $user = $this->Session->read('user'); ?>

<?php
echo $html->css('app/columns.css');
echo $html->css('rating/jquery.rating.css');
echo $html->script('rating/jquery.rating.pack.js');
echo $html->script('app/caterers.js');
?>

<style>
    .fullwidth th, .fullwidth td {
        padding: 7px !important;
    }
</style>

<h2>Caterers</h2>


<div class="columns">

    <div class="column" id="users_column">
        <div class="header">List of Caters</div>
        <div class="subheader">
            <ul style="display: inline-block;">
                <li class="button"><a class="dialog_opener" id="add_caterer" title="Add New Caterer" href="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'add')); ?>">Add New Caterer</a></li>
                <!--<li><a class="delete_selected" href="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'delete_selected')); ?>">Delete Selected Handlers</a></li>-->
            </ul>
            
            <div style="display: inline-block; margin-left: 100px;">
                Find Caterer: <input type="text" id="search_field" placeholder="e.g. Name" style="font-size: 11px;" data-url="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'list')); ?>"/>
                
                By <select id="search_by" style="font-size: 11px;">
                    <option value="name">Name</option>
                    <option value="country">Country</option>
                    <option value="email">Email</option>
                </select>
            </div>
        </div>
        <div class="content">

            <div class="master_block" style="width: 50%" >
                <table border="0" class="fullwidth" cellpadding="8" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="20" align="right"><input type="checkbox" value="" id="checkall" /></th>
                            <th colspan="2">Caterer Name</th>
                            <th style="text-align: center;" width="100">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = $this->Paginator->counter('%start%');
                        foreach ($caterers as $val) {
                            ?>
                            <tr data-url="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'details', $val['Caterer']['id'])); ?>">
                                <td align="right"><input type="checkbox" value="<?php echo $val['Caterer']['id']; ?>" /></td>
                                <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                                <td class="caterer_name"><?php echo $val['Caterer']['name'] ?></td>
                                <td align="center">
                                    <a class="dialog_opener edit_link" title="Edit Caterer Information" href="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'edit', $val['Caterer']['id'])); ?>">Edit</a>
                                    <a class="del" data-id="<?php echo $val['Caterer']['id'] ?>" del_url="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'delete', $val['Caterer']['id'])); ?>">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div>

            <div class="details_block" style="width: 50%">
                &nbsp;
            </div>
        </div>

        <?php echo $this->element('layout/pagination_footer'); ?>
    </div>

</div>
