<?php $user = $this->Session->read('user'); ?>

<?php
echo $html->css('app/columns.css');

echo $html->script('app/service.js');
?>

<h2><?php __('Manage Services'); ?></h2>

<div class="columns">

    <div class="column" id="users_column">
        <div class="header">List of Available Services</div>
        <div class="subheader">
            <ul>
                <li class="button"><a class="dialog_opener" id="add_service" title="Add New Service" href="<?php echo $html->url(array('controller' => 'services', 'action' => 'addservice')); ?>">Add New Service</a></li>
                <!--<li><a class="delete_selected" href="<?php echo $html->url(array('controller' => 'handlers', 'action' => 'delete_selected')); ?>">Delete Selected Handlers</a></li>-->
            </ul>
        </div>
        <div class="content">

            <table border="0" class="fullwidth" cellpadding="8" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="2">Service Name</th>
                        <th>Service Description</th>
                        <th colspan="2" style="text-align: center; width: 200px;">Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count = 1;
                    foreach ($services as $val) { ?>
                        <tr>
                            <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                            <td width="150"><?php echo $val['Service']['name'] ?></td>
                            <td><?php echo $val['Service']['description'] ?></td>
                            <td align="center">
                                <a class="edit_link dialog_opener" title="Editing Service" href="<?php echo $html->url(array('controller' => 'Services', 'action' => 'admin_editservice', $val['Service']['id'])); ?>">Edit</a>
                                <a class="del" data-id="<?php echo $val['Service']['id'] ?>" del_url="<?php echo $html->url(array('controller' => 'Services', 'action' => 'admin_delservice', $val['Service']['id'])); ?>">Delete</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <?php echo $this->element('layout/pagination_footer'); ?>
        
        <!--<div class="footer">
            Pages: <?php echo $this->Paginator->numbers(); ?>
        </div>-->
    </div>

</div>