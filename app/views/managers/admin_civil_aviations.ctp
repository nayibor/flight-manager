<?php
echo $html->css('app/columns.css');
echo $html->script('app/authorities.js');
?>


<div class="columns">

    <div class="column" id="users_column">
        <div class="header">Civil Aviation Authorities</div>
        <div class="subheader">
            <ul style="display: inline;">
                <li class="button"><a class="dialog_opener add_link" id="add_AviationAuthority" title="Add New Authority" data-height="600" href="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'add')); ?>">Add New Authority</a></li>
                <li class="button"><a class="delete_selected" href="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'delete_selected')); ?>">Delete Selected</a></li>
            </ul>
            
            <div style="display: inline-block; margin-left: 100px;">
                Find Aviation Authority: <input type="text" id="search_field" placeholder="e.g. Belarus" style="font-size: 11px;" data-url="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'list')); ?>"/>

                By <select id="search_by" style="font-size: 11px;">
                    <option value="country">Country</option>
                    <option value="name">Name</option>
                    <option value="email">Email</option>
                </select>
            </div>
        </div>

        <div class="content">
            <div class="master_block" style="width: 55%;">
                <table id="authorities-table" class="fullwidth" cellspacing="0" cellpadding="5" border="0">
                    <thead>
                        <tr>
                            <th width="10"><input type="checkbox" class="check_all" /></th>
                            <th width="230" colspan="3">Country / Authority Name</th>
                            <th style="text-align: center;">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $count = (($this->Paginator->current() - 1) * 25) + 1; //debug($this->Paginator);  ?>
                        <?php foreach ($authorities as $authority) { ?>
                            <tr id="row-<?php echo $authority['AviationAuthority']['id']; ?>" data-id="<?php echo $authority['AviationAuthority']['id']; ?>">
                                <td><input type="checkbox" value="<?php echo $authority['AviationAuthority']['id']; ?>" /></td>
                                <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                                <td class="country" nowrap><?php echo $authority['AviationAuthority']['country']; ?></td>
                                <td class="name"><?php echo $authority['AviationAuthority']['name']; ?></td>
                                <td class="options" align="center" nowrap>
                                    <a class="dialog_opener edit_link" title="Edit Aviation Authority Information" data-height="600" href="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'add', $authority['AviationAuthority']['id'])); ?>">Edit</a>
                                    <a class="delete_link" href="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'delete', $authority['AviationAuthority']['id'])); ?>">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <div class="details_block" style="width: 45%;">
                <div class="display" style="max-height: 600px; overflow: auto;">
                    &nbsp;
                </div>
            </div>


            <input type="hidden" id="edit_link_base" value="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'add')); ?>" />
            <input type="hidden" id="delete_link_base" value="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'delete')); ?>" />
        </div>

        <?php echo $this->element('layout/pagination_footer'); ?>

        <!--<div class="footer">
            Pages: <?php echo $this->Paginator->numbers(); ?>
        </div>-->
    </div>
</div>

<input type="hidden" id="authority_details_url" value="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'details')); ?>" />