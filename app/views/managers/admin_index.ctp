<h2>List Of Management Interfaces</h2>

<div class="pad20">
    <!-- Big buttons -->


    <ul class="dash">
        <?php foreach ($menus as $menu) { ?>
            <li>
                <a href="<?php echo $html->url(array($menu['Menu']['prefix'] => true, 'controller' => $menu['Menu']['controller'], 'action' => $menu['Menu']['action'])); ?>" title="<?php echo $menu['Menu']['subtitle']; ?>" class="tooltip">
                    <?php echo $html->image($menu['Menu']['icon_path']); ?>
                    <span><?php echo $menu['Menu']['title']; ?></span>
                </a>
            </li>
        <?php } ?>

    </ul>
</div>