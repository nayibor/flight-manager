<?php echo $html->css('app/columns.css'); ?>

<style>
    .columns .header {

        font-size: 13px !important;
    }
    #chart_column .subheader ul li {
        display: inline-block;
        padding: 5px;
    }

    #chart_column .subheader ul li label {
        width: auto;
        margin-right: 10px;
        font-size: 11px;
    }

    #chart_column .subheader input {
        font-size: 11px !important;
    }

    #reports-list li {
        font-size: 11px;
        padding: 5px;
    }

    #reports-list li ul {
        padding-left: 10px;
        padding-top: 5px;
    }

    #reports-list li ul li:not(:last-child) {
        border-bottom: dotted #f1f1f1 1px;
    }

    #reports-list a {
        text-decoration: none;
        color: #333; 
    }
</style>

<div class="columns">
    <div class="column span1">
        <div class="header">List of Reports</div>
        <div class="content">
            <ul id="reports-list" >
                <?php
                $menurep = ClassRegistry::init("Report");
                $menureport = $menurep->getreports();

                $repcat = ClassRegistry::init("ReportCategory");
                 $categories=$repcat->getreports();


                foreach ($menureport as $key => $rptval) {
                    ?>
                    <li>
                        <b><?php echo strtoupper($key); ?></b>
                        <ul>
                            <?php
                            foreach ($rptval as $childrept) {
                                ?>
                                <li data-id="<?php echo $childrept['id']; ?>">
                                    <a data-type="report" 
                                       data-params-id="<?php echo $html->url(array("controller" => "Reports", "action" => "admin_getParameters", $childrept['id'])) ?>"
                                       href="<?php echo $html->url(array("controller" => "Reports", "action" => "admin_vreport", $childrept['id'])) ?>"
                                       dlink="<?php echo $html->url(array("controller" => "Reports", "action" => "admin_vreport", $childrept['id'], "download")) ?>"
                                       plink="<?php echo $html->url(array("controller" => "Reports", "action" => "admin_preport", $childrept['id'], "print")) ?>"

                                       >

                                        <?php echo strtoupper($childrept['name']); ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>

        </div>
    </div>

    <div class="column span3" id="chart_column">
        <div class="header">Report Details</div>
        <div class="subheader">
            <div style="display: inline-block;"><b>Parameters:</b> </div>
            <div style="display: inline-block; margin-left: 20px;" id="param_area">

            </div>
            <div style="display: inline-block; margin-left: 20px;">
                <input id="generate-button" type="button" class="button" data-url="<?php echo $html->url(array('controller' => 'charts', 'action' => 'getChartData')); ?>" value="Generate Report" style="display: none;" />
            </div>
        </div>


        <div class="content">
            <div id="report_area" >
                Select Category On Left To View  Report
            </div>
        </div>
    </div>

</div>

<?php echo $html->script('high-charts/js/highcharts.js'); ?>
<?php echo $html->script('high-charts/js/modules/exporting.js'); ?>
<?php echo $html->script('app/reports/charts.js'); ?>