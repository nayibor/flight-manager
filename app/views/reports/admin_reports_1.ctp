<?php
echo $html->css('app/columns.css');
echo $html->css('app/jobsheet.css');
echo $html->script('app/reports.js');

?>
<style type="text/css">
    .top-nav {
        list-style: none;
        margin-bottom: 10px;
    }

    .top-nav li {
        display: inline-block;
        border: solid #ddd 1px;
        border-radius: 5px;
        background: -moz-linear-gradient(top, rgba(245,245,220,1) 0%, rgba(255,255,255,0) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(245,245,220,1)), color-stop(100%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* IE10+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f5dc', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
        background: linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* W3C */
    }

    .top-nav li a {
        text-decoration: none;
        color: #444;
        font-size: 11px;
        line-height: 25px;
        display: block;
        padding: 0px 10px;
    }

    .ui-datepicker-trigger {
        vertical-align: middle;
        width: 24px;
        height: 24px;
        
      
    }
   
#report_list{
    
    display: block !important;
    width:100%;
    margin-top: 20px;
}

  
</style>

<div class="columns">

    <div class="column" id="users_column">
        <div class="header">Reports</div>
        <div class="subheader" style="position: relative;">
            <ul>
                <li class="button">
                    <a id="add_new_report"  href="#">New Report</a>
                </li>
             
            </ul></div>
        <input type="hidden" name="add_new_url" id="add_new_url" value="<?php echo $html->url(array("controller"=>"reports","action"=>"admin_add")); ?>" />
    </div>
    
    
       </div>

<div id="report_list" name="report_list">
    </div>
       <input type="hidden" name="report_url" id="report_url" value="<?php echo $html->url(array("controller"=>"reports","action"=>"admin_reportlist"));?>"/>
       <input type="hidden" name="report_test" id="report_test" value="report_test"/>

