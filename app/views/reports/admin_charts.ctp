<?php echo $html->css('app/columns.css'); ?>

<style>
    #chart_column .subheader ul li {
        display: inline-block;
        padding: 5px;
    }

    #chart_column .subheader ul li label {
        width: auto;
        margin-right: 10px;
        font-size: 11px;
    }

    #chart_column .subheader input {
        font-size: 11px !important;
    }

    #charts-list li {
        font-size: 11px;
        padding: 5px;
    }

    #charts-list li ul {
        padding-left: 10px;
        padding-top: 5px;
    }

    #charts-list li ul li:not(:last-child) {
        border-bottom: dotted #f1f1f1 1px;
    }

    #charts-list a {
        text-decoration: none;
        color: #333; 
    }
</style>

<div class="columns">
    <div class="column span1">
        <div class="header">List of Charts</div>
        <div class="content">
            <ul id="charts-list" >
                <?php foreach ($chart_categories as $chart_category) { ?>
                    <li>
                        <b><?php echo $chart_category['ChartCategory']['name']; ?></b>
                        <ul>
                            <?php foreach ($chart_category['Chart'] as $chart) { ?>
                                <li data-id="<?php echo $chart['id']; ?>">
                                    <a href="<?php echo $html->url(array('controller' => 'charts', 'action' => 'getParameters', $chart['id'])); ?>"><?php echo $chart['title']; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>

        </div>
    </div>

    <div class="column span3" id="chart_column">
        <div class="header">Chart Details</div>

        <div class="subheader">
            <div style="display: inline-block;"><b>Parameters:</b> </div>
            <div style="display: inline-block; margin-left: 20px;" id="param_area">

            </div>
            <div style="display: inline-block; margin-left: 20px;">
                <input id="generate-button" type="button" class="button" data-url="<?php echo $html->url(array('controller' => 'charts', 'action' => 'getChartData')); ?>" value="Generate Chart" style="display: none;" />
            </div>
        </div>


        <div class="content">
            <div id="chart_area" >
                Select Category On Left To Load Users Here
            </div>
        </div>
    </div>

</div>

<?php echo $html->script('high-charts/js/highcharts.js'); ?>
<?php echo $html->script('high-charts/js/modules/exporting.js'); ?>
<?php echo $html->script('app/reports/charts.js'); ?>