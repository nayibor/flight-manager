<style>
    div.message a {
        text-decoration: none;
        color: #069;
    }
    
    div.message a:hover {
        color: #096;
    }
</style>

<?php $user = $this->Session->read('user'); ?>

<!--<h1>Welcome To <?php echo Configure::read('app_name'); ?>, <span><?php echo $user['User']['first_name']; ?></span>!</h1>
<p>What would you like to do today?</p>-->
<h1>Data Reporting</h1>

<div class="pad20">
    <!-- Big buttons -->


    <ul class="dash">
        <?php foreach ($menus as $menu) { ?>
            <li>
                <a href="<?php echo $html->url(array($menu['Menu']['prefix'] => true, 'controller' => $menu['Menu']['controller'], 'action' => $menu['Menu']['action'])); ?>" title="<?php echo $menu['Menu']['subtitle']; ?>" class="tooltip">
                    <?php echo $html->image($menu['Menu']['icon_path']); ?>
                    <span><?php echo $menu['Menu']['title']; ?></span>
                </a>
            </li>
        <?php } ?>

    </ul>
</div>