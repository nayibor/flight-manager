
<form  method="POST" id="hotel_process_form" action="<?php echo $this->here; ?>">
    <fieldset>
        <div class="input">
            <label for="role_name">Name</label>
            <input type="text" class="required" required name="data[Caterer][name]" size="50" />
        </div>
        <div class="input">
            <label for="role_name" style="vertical-align: top;">Address</label>
            <textarea name="data[Caterer][address]" cols="40" rows="4" style="display: inline-block; width: 400px; height: 50px;"></textarea>
        </div>
        
        <div class="input">
            <label for="role_name">Country</label>
            <input type="text" name="data[Caterer][country]" size="30" />
        </div>
        
        <div class="input">
            <label for="role_name">Contact Number </label>
            <input type="text" name="data[Caterer][telephone]" />
        </div>
        <div class="input">
            <label for="role_name">Fax Number </label>
            <input type="text" name="data[Caterer][fax]" />
        </div>
        <div class="input">
            <label for="role_name">Email </label>
            <input type="text" name="data[Caterer][email]" />
        </div>
        <div class="input">
            <label for="role_name">Distance From Airport</label>
            <input type="number" name="data[Caterer][distance]" id="data[Caterer][distance]" size="7" /> Km
        </div>
        <div class="input">
            <label for="role_name">Stars</label>
            <div style="display: inline-block;">
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    echo "<input type='checkbox' name='data[Caterer][stars]' value='{$i}' class='star' />";
                }
                ?>
            </div>
        </div>
        
        <div class="input">
            <label for="role_name">Website</label>
            <input type="text" name="data[Caterer][website]" size="60" />
        </div>

    </fieldset>

    <input type="submit" class="button" id="process_button_btn" value="Add Caterer"/>
    <input type="button" class="button" id="cancel-form" value="Cancel" />

</form>

