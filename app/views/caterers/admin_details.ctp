<style>
    .details_table td {
        padding: 8px;
        font-size: 11px;
    }
</style>

<h2>Caterer Details</h2>

<table border="0" cellspacing="0" cellpadding="10" width="96%" class="details_table" align="center" style="margin: 0px auto;">
    <tr>
        <td width="120">Name</td>
        <td><?php echo $caterer['Caterer']['name']; ?></td>
    </tr>

    <tr>
        <td>Address</td>
        <td><?php echo $caterer['Caterer']['address']; ?></td>
    </tr>
    
    <tr>
        <td>Country</td>
        <td><?php echo $caterer['Caterer']['country']; ?></td>
    </tr>

    <tr>
        <td>Contact Number</td>
        <td><?php echo $caterer['Caterer']['telephone']; ?></td>
    </tr>
    
    <tr>
        <td>Fax Number</td>
        <td><?php echo $caterer['Caterer']['fax']; ?></td>
    </tr>
    
    <tr>
        <td>Email Address</td>
        <td><a href="mailto:<?php echo $caterer['Caterer']['email']; ?>"><?php echo $caterer['Caterer']['email']; ?></a></td>
    </tr>

    <tr>
        <td>Rating</td>
        <td>
            <?php
            for ($i = 1; $i <= 5; $i++) {
                $checked = $caterer['Caterer']['stars'] == $i ? "checked=checked" : "";
                echo "<input name='star{$caterer['Caterer']['id']}' type='radio' value='{$i}' class='star' {$checked} />";
            }
            ?>
        </td>
    </tr>

    <tr>
        <td>Distance from Airport</td>
        <td><?php echo $caterer['Caterer']['distance'] . " Km"; ?></td>
    </tr>
    
    <tr>
        <td>Website</td>
        <td><a href="<?php echo $caterer['Caterer']['website']; ?>" target="_blank"><?php echo $caterer['Caterer']['website']; ?></a></td>
    </tr>
</table>