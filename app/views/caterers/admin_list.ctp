<table border="0" class="fullwidth" cellpadding="8" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th width="20" align="right"><input type="checkbox" value="" id="checkall" /></th>
            <th colspan="2">Caterer Name</th>
            <th style="text-align: center;" width="100">Options</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $count = $this->Paginator->counter('%start%');
        foreach ($caterers as $val) {
            ?>
            <tr data-url="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'details', $val['Caterer']['id'])); ?>">
                <td align="right"><input type="checkbox" value="<?php echo $val['Caterer']['id']; ?>" /></td>
                <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                <td class="caterer_name"><?php echo $val['Caterer']['name'] ?></td>
                <td align="center">
                    <a class="dialog_opener edit_link" title="Edit Caterer Information" href="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'edit', $val['Caterer']['id'])); ?>">Edit</a>
                    <a class="del" data-id="<?php echo $val['Caterer']['id'] ?>" del_url="<?php echo $html->url(array('controller' => 'caterers', 'action' => 'delete', $val['Caterer']['id'])); ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>