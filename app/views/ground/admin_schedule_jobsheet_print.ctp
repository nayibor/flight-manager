
<div class="flyout_content">

    <section class="summary">
        <?php echo $this->element('ground/jobsheet_summary'); ?>
    </section>

    <footer style="margin-top: 20px; text-align: left; font-size: 10pt;">

        <div><em>NB: Please ensure -  Capt. Signs, Carnet Card not on Invalid List, Tail# is depicted on card.</em></div>

        <table width="100%" cellpadding="3" cellspacing="0" border="0">
            <tr>
                <td width="50%">
                    Supervisor -  Signature:________________
                </td>
                <td width="50%">
                    Capt./FM Sign:_____________________
                </td>
            </tr>
            <tr>
                <td>
                    Name:___________________
                </td>
                <td>
                    Name: ___________________
                </td>
            </tr>
            <tr>
                <td>
                    Date:___________________
                </td>
                <td>
                    Date: ___________________
                </td>
            </tr>
        </table>	


        <div class="foot-note" style="margin-top: 20px; font-size: 8pt;">
            S.B.MAN & CO - Ground Ops Job Sheet Doc <!--08/06 – Issue 1 – rev.12’09	-->
            <a href="http://www.sbmangh.com/" target="_blank">www.sbmangh.com</a>
        </div>
    </footer>
</div>
