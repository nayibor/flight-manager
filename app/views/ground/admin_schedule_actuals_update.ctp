<style>
    #update_form td {
        padding: 10px;
        font-size: 13px;
    }

    #update_form h3 {
        font-size: 13px;
        margin-bottom: 20px;
        font-weight: bold;
    }

    #update_form .dates {
        display: inline-block;
        vertical-align: top;
        width: 400px;
        line-height: 16px;
        font-family: monospace;
        font-size: 12px;
    }
</style>

<h2 style="margin-bottom: 15px;">Update ATA / ATD</h2>

<form id="update_form" method="post" action="<?php echo $this->here; ?>">

    <section>
        <h3>Flight Schedule</h3>

        <div class="dates">
            <?php echo $this->element('ground/schedule_format', $schedule); ?>
        </div>

        <hr />
    </section>

    <section>
        <table border="0" cellpadding="10" width="100%">
            <tr>
                <td width="30%">ATD From Origin (<b><?php echo $schedule['GroundRequestSchedule']['arr_origin']; ?></b>)</td>
                <td><input class="date" type="text" name="data[GroundRequestSchedule][arr_atd]" value="<?php echo $schedule['GroundRequestSchedule']['arr_atd']; ?>" /></td>
            </tr>
            
            <tr>
                <td>ATA At Dest (<b><?php echo $schedule['GroundRequestSchedule']['arr_dest']; ?></b>)</td>
                <td><input class="date" type="text" name="data[GroundRequestSchedule][arr_ata]" value="<?php echo $schedule['GroundRequestSchedule']['arr_ata']; ?>" /></td>
            </tr>

            <tr>
                <td>ATD From Dest (<b><?php echo $schedule['GroundRequestSchedule']['dept_origin']; ?>)</b></td>
                <td><input class="date" type="text" name="data[GroundRequestSchedule][dept_atd]" value="<?php echo $schedule['GroundRequestSchedule']['dept_atd']; ?>" /></td>
            </tr>
        </table>    
    </section>


    <footer style="padding: 15px 10px;">
        <input type="submit" class="button" value="Save Changes" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
        <input type="hidden" name="data[GroundRequestSchedule][id]" value="<?php echo $schedule['GroundRequestSchedule']['id']; ?>" />
    </footer>
</form>

<script>
    $("#update_form .date").datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'hh:mm:ss',
        changeMonth: true,
        changeYear: true
    });
</script>