<?php
$isOpen = (isset($status) && $status == 'open');
?>

<table border="0" cellpadding="5" cellspacing="0" class="fullwidth" id="gr_table">
    <thead>
        <tr>
            <th colspan="2" style="padding-left: 1px;">
                <input type="checkbox" class="check_all" />
                REF No.
            </th>
            <th>Company</th>
            <!--<th>Flight No.</th>-->

            <?php if ($isOpen) { ?>
                <th width="60" nowrap>Options</th>
            <?php } ?>
        </tr>
    </thead>

    <tbody>

        <?php
        $count = $this->Paginator->counter('%start%');
        foreach ($req as $val) {
            $isOpen = $val['GroundRequestSchedule']['status'] == "" || $val['GroundRequestSchedule']['status'] == "open";
            ?>
            <tr id="<?php echo $val['GroundRequest']['id']; ?>">

                <td width="50" align="left" style="padding: 7px 1px;" nowrap>
                    <input type="checkbox" value="<?php echo $val['GroundRequest']['id']; ?>" class="check" />
                    <?php echo $count++ . "."; ?>
                </td>
                <td nowrap>
                    <?php
                    if (isset($val['GroundRequestSchedule'][0])) {
                        echo $val['GroundRequestSchedule'][0]['ref_no'];
                    } else if (isset($val['GroundRequestSchedule']['ref_no'])) {
                        echo $val['GroundRequestSchedule']['ref_no'];
                    }
                    ?>
                </td>
                <td><?php echo $val['GroundRequest']['company_name']; ?></td>

                <td nowrap>
                    <?php if ($isOpen) { ?>

                        <a class="edit_link flyout_opener" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_add_handRequest', $val['GroundRequest']['id'])); ?>" ><?php echo $html->image('pencil.png'); ?></a>
                        <a class="del_req" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_del_req', $val['GroundRequest']['id'])); ?>"><?php echo $html->image('cross.png'); ?></a>

                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php echo $this->element('layout/pagination_footer', array('url_params' => array($status), 'ajax' => true)); ?>