<table border="0" cellpadding="5" cellspacing="0" class="fullwidth">
    <thead>
        <tr>
            <th>REF No / LOC</th>
            <th>OPTR</th>
            <th>FLT No</th>
            <th>A/C Regn</th>
            <th>A/C Type</th>
            <th>Principal</th>
            <th>Origin</th>
            <th>SVCS Ops</th>
            <th>ETA</th>
            <th>ATA</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($jobsheets as $jobsheet) { ?>
            <tr>
                <td><?php echo $jobsheet['JobSheet']['ref_no']; ?></td>
                <td><?php echo $jobsheet['JobSheet']['operator']; ?></td>
                <td><?php echo $jobsheet['JobSheet']['flight_no']; ?></td>
                <td><?php echo $jobsheet['JobSheet']['aircraft_registration']; ?></td>
                <td><?php echo $jobsheet['JobSheet']['aircraft_type']; ?></td>
                <td><?php echo $jobsheet['JobSheet']['company_name']; ?></td>
                <td><?php echo $jobsheet['JobSheet']['flight_origin']; ?></td>
                <td><?php echo $dateFormat->formatZulu($jobsheet['JobSheet']['est_arrival_dt']); ?></td>
                <td><?php echo $dateFormat->formatZulu($jobsheet['JobSheet']['arrival_dt']); ?></td>
                <td><a href="#">Services</a></td>
            </tr>
        <?php } ?>
    </tbody>
</table>