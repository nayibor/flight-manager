
<style type="text/css">


    #request_details h3 {
        font-size: 12px;
        font-weight: bold;
        border-bottom: dotted #ccc 1px;
        padding-bottom: 3px;
        color: #222;
    }

    #request_details .schedule {
        padding: 4px;
        margin: 3px 0px;
        border-radius: 4px;
        overflow: visible;
        width: 600px;
    }

    #request_details .schedule.expanded {
        border: solid #ddd 1px;
        border-radius: 5px;
    }

    #request_details .schedule h4.divider {
        font-size: 11px;
        font-family: "Tahoma", sans-serif;
        color: #333;
        position: relative;
    }

    #request_details .schedule h4.divider .expander {
        background-image: url(../../img/expand_3.png);
        background-position: top;
        background-repeat: no-repeat;
        padding: 9px;
        margin-right: 5px;
        text-decoration: none;
        display: inline-block;
        vertical-align: top;
        background-image: none;
    }

    #request_details .schedule h4.divider .expander.collapsed {
        background-image: url(../../img/expand_3.png);
    }

    #request_details .schedule h4.divider .expander.expanded {
        background-image: url(../../img/collapse_3.png);
    }

    #request_details .schedule h4.divider .dates {
        display: inline-block;
        vertical-align: top;
        width: 400px;
        line-height: 16px;
        font-family: monospace;
        font-size: 12px;
    }

    #request_details .schedule h4.divider .options {
        width: 150px;
        top: 0px;
        right: 0px;
        display: block;
        position: absolute;
        /*display: none;*/
    }
    
    #request_details .schedule h4.divider .options .open_only { 
        display: none;
    } 

    #request_details .schedule h4.divider .options a {
        color: #069;
        margin-right: 10px;
        text-decoration: none;
    }

    #request_details .schedule .services {
        padding: 10px;
    }

    #request_details .schedule .services td {
        font-size: 11px;
        padding: 5px 10px;
    }

    #request_details .schedule .services td ul {
        list-style-type: square;
        padding-left: 15px;
        margin-bottom: 15px;
    }

    #request_details .schedule .services td ul li {
        padding: 3px;
    }

    #request_details #summary_table td {
        padding: 6px;
        font-size: 11px;
    }

    #request_details #summary_table td:first-child {
        font-weight: bold;
        width: 30%;
    }
</style>

<div id="request_details">
    <?php 
        $groundRequest = array('GroundRequest'); 
        $groundRequest['GroundRequest'] = $schedule['GroundRequest'];
        
        echo $this->element('ground/request_summary', array('groundRequest' => $groundRequest)); 
    ?>
    
    <h3>Schedule And Services</h3>
    
    <?php echo $this->element('ground/schedule_summary', array('schedule' => $schedule)); ?>
</div>
