<?php $user = $this->Session->read('user'); ?>

<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/jobsheet.css'); ?>

<style>
    .dash_header {
        padding: 8px;
        border: solid #ddd 1px;
        border-radius: 3px;
        background-color: #f9f9f9;
        margin-bottom: 10px;
        position: relative;
    }
    
    .dash_header h2 {
        margin: 0px;
    }
    
    .dash_header span {
        position: absolute;
        right: 10px;
        top: 6px;
    }
    
    .dash_header span input[type=text] {
        font-size: 11px;
    }
    
    .dash_keys {
        background-color: #f9f9f9;
        border: solid 1px #eee;
        margin: 5px 0px;
        padding: 5px;
    }
    
    .dash_keys li {
        display: inline-block;
        margin-right: 5px;
        padding: 5px;
        font-size: 11px;
    }

    
    #gr_table td {
        font-size: 11px;
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        text-transform: uppercase;
    }

    #gr_table tbody tr:hover {
        background-color: #ececec;
    }

    .status {
        padding: 10px;
        display: block;
        border-radius: 15px;
        background-color: #eee;
        box-shadow: 1px 1px 2px #ccc;
    }
    
    .status.small {
        padding: 7px;
        display: inline-block !important;
        border-radius: none;
        vertical-align: middle;
        margin-right: 5px;
    }

    .status.critical {
        background-color: #900;
    }
    
    .status.emergency {
        background-color: #F00;
    }

    .status.warning {
        background-color: #ffcc00;
    }
    
    .status.operational {
        background-color: #1c94c4;
    }
    
    .status.completed {
        background-color: green;
    }
</style>


<div class="dash_header">
    <h2>Ground Handling Dashboard</h2>
    
    <span>
        Filter By: <input type="text" placeholder="SBG001-12" id="filter_field" />
    </span>
</div>

<div class="dash_keys">
    <ul>
        <li><b>Legend: </b></li>
        <li><span class="status small"></span> Job Pending</li>
        <li><span class="status small warning"></span> < 10 Hours To Flight</li>
        <li><span class="status small emergency"></span> < 2 Hours To Flight</li>
        <li><span class="status small operational"></span> Mov't Advised / At Dest.</li>
        <li><span class="status small completed"></span> Arrived / Departed</li>
        <li><span class="status small critical"></span> Overdue / Canceled</li>
    </ul>
</div>

<div id="tabs">
    <ul>
        <li><a class="lk_open"  title="open_perms" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_dashboard_list', 'arrivals')); ?>">Arrivals</a></li>
        <li><a class="lk_open"  title="closed_perms" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_dashboard_list', 'departures')); ?>">Departures</a></li>
        <li><a class="lk_open"  title="completed_perms" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_dashboard_list', 'completed')); ?>">Completed</a></li>
        <li><a class="lk_open"  title="canceled_perms" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_dashboard_list', 'cancelled')); ?>">Canceled</a></li>
    </ul>
</div>
<?php echo $html->script('app/dashboard_ground.js'); ?>


<input type="hidden" name="dash_type" id="dash_type" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_dashboard_list', 'departures')); ?>"/>