<style type="text/css">
    #process_form .form_table > tbody > tr > td {
        padding: 5px;
    }

    #process_form #attachments a {
        text-decoration: none;
        color: #069;
    }

    #process_form #attachments a:hover {
        color: #444;
    }

    #process_form #attachments label {
        width: 170px !important;
    }

    #process_form #attachments .vertical_block {
        width: 50%;
        padding: 5px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        float: left;
    }

    
</style>

<h2>Sending Handling Request</h2>

<form  method="POST" id="process_form">

    <?php
    $user = $this->Session->read('user');
    
    $emails = explode(",", $schedule['Handler']['email']);
    $to = array($schedule['Handler']['name'] => $emails[0]);
    $cc = array('S.B. Man & Co.' => 'fltops@sbmangh.com');
    
    array_shift($emails);
    foreach($emails as $email) {
        $cc[] = trim($email);
    }
    
    echo $this->element('email_composition_form', array(
        'to' => $to,
        'cc' => $cc,
        'subject' => 'SB MAN & Co: Handling Request',
        'message' => "<p>Hi,</p>

                    <p>Please find attached the necessary documents for this Handling Request</p>

                    <p>Regards,<br />
                    
                    {$user['User']['first_name']}  {$user['User']['last_name']}

                    <br />
                    SBMAN Ground Handling"
    ));
    ?>

    <table border="0" cellpadding="5" cellspacing="0" width="100%" class="form_table">    
        <tr>
            <td valign="top" width="80">Attachments</td>
            <td valign="top" id="attachments">
                <div class="vertical_block">
                    <input type="checkbox" name="attach_handling_request" id="handler_option" value="handler_option" checked="checked" />
                    <label for="handler_option">Handling Request</label> 
                    <a class="dialog_opener" data-width="800" data-height="550" title="Handling Request" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'admin_request_template', $schedule['GroundRequestSchedule']['id'])); ?>">(Preview)</a> <br /><br />

                    <input type="checkbox" name="attach_crew_brief" id="crew_brief_option" value="crew_brief" checked="checked" />
                    <label for="crew_brief_option">Crew Brief Summary</label> 
                    <a class="dialog_opener" data-width="800" data-height="550" title="Crew Brief Summary" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'crew_brief', $schedule['GroundRequestSchedule']['id'])); ?>">(Preview)</a> <br /><br />

                    <input type="checkbox" name="attach_qcr" id="job_quality_option" value="job_quality" checked="checked" />
                    <label for="job_quality_option">Quality Control Request</label> 
                    <a class="dialog_opener" data-width="800" data-height="550" title="Quality Control Request" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'service_quality_report', $schedule['GroundRequestSchedule']['id'])); ?>">(Preview)</a> <br /><br />

                    <input type="checkbox" name="attach_jobsheet" id="jobsheet_option" value="job_quality" />
                    <label for="jobsheet_option">Jobsheet</label> 
                    <a class="dialog_opener" data-width="800" data-height="550" title="Handling Request Jobsheet" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'schedule_jobsheet', $schedule['GroundRequestSchedule']['id'])); ?>">(Preview)</a> <br /><br />
                </div>

                <div class="vertical_block">

                    <?php foreach ($schedule['GroundRequestScheduleAttachment'] as $attachment) { ?>
                        <input type="checkbox" name="schedule_attachments[]" value="<?php echo $attachment['id']; ?>" />
                        <label>
                            <a target="_blank" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'attachments_view', $attachment['id'])); ?>">
                                <?php echo $attachment['file_name']; ?>
                            </a>
                        </label> 
                        <?php
                    }
                    ?>
                </div>

            </td>
        </tr>
    </table>

    <br /><br />

    <input type="button" class="button" id="print_btn" name="print_btn" value="Send Request" />
    <input type="button" class="button" id="cancel-form" name="cancel-form" value="Cancel" />

    <input type="hidden" name="email_link" id="email_link" value="<?php echo $html->url(array('controller' => 'email', 'action' => 'sendHandlingRequest')); ?>" />
    <input type="hidden" name="handler_id" id="handler_id" value="<?php echo $schedule['GroundRequestSchedule']['handler_id']; ?>" />

    <input type="hidden" name="ground_request_id" id="ground_request_id" value="<?php echo $schedule['GroundRequest']['id']; ?>" />
    <input type="hidden" name="schedule_id" id="schedule_id" value="<?php echo $schedule['GroundRequestSchedule']['id']; ?>"   />

</form>

<script>
    $(document).ready(function(){

        $("#print_btn").click( function(){

            if( (!$("#handler_option").is(":checked") && !$("#job_quality_option").is(":checked")) ||
                $("#handler_id").val()=='') {
                humane.error("Please Check One Of The Above Options And Select A Handler");
                return;
            }

            if( ($("#handler_option").is(":checked")) && $("#handler_email").val() == "" ) {
                humane.error("There Is No Email Address For Handler.");
                return;
            }

            if( (($("#job_quality_option").is(":checked")) && $("#company_email").val() == "")) {
                humane.error("There Is No Email Address For Client.");
                return;
            }


            var url = $("#email_link").val();
            var query = $("#process_form").serialize();
            
            $.ajax({
                url:url,
                data: query,
                dataType: 'json',
                type:'POST',
                
                beforeSend:function(){
                    $("#print_btn").attr("disabled","disabled").val("Sending Email...");
                    
                    humane.timeout = 3000;
                    humane.info("Sending Handling Request And Attachments");
                },
                
                success: function(data) {
                    if( data.result == "failed" ){
                        humane.error(data.error);
                        console.log(data.error);
                    } else {
                        humane.info("Handling Request Sent Successfully");
                        
                        setTimeout(function() {
                            core.closeFlyout();
                        }, 3000);
                    }
                    
                    $("#print_btn").removeAttr("disabled").val("Send Request");
                },
                
                error: function(xhr) {

                    humane.error("Failed To Send Email Due To The Following Error\n\n" + xhr.responseText);
                    console.log(xhr.responseText);

                    $("#print_btn").removeAttr("disabled").val("Send Request");
                }
            });
        });
    });

</script>