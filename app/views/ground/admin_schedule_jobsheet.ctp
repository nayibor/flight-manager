

<div class="flyout_content">

    <section class="summary">
        <?php echo $this->element('ground/jobsheet_summary'); ?>
    </section>
    
    <footer style="padding: 15px;">
        <div class="input">
            <input type="button" class="button" id="sjs_print_form" value="Print"  data-url="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'schedule_jobsheet_print',$schedule_id)); ?>" />
            <input type="button" class="button" id="sjs_download_form" value="Download"  data-url="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'schedule_jobsheet_print',$schedule_id, 'download')); ?>" />
            <input type="button" class="button" id="cancel-form" value="Close" /> 
        </div>
    </footer>

</div>

<script>
    $("#sjs_print_form").click(function(){
        window.open($(this).data("url"));
    });
    
    $("#sjs_download_form").click(function() {
        if( 'humane' in window ) {
            humane.timeout = 3000;
            humane.info('Generating Flight Schedule Jobsheet PDF');
        } 

        var $this = $(this);
        setTimeout(function() {
            window.location.href = $this.data('url');
        }, 1000);
    });
</script>
