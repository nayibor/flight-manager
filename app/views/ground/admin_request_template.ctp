<?php echo $this->element('ground/handling_request_form'); ?>

<?php if (!isset($_GET['print_form'])) { ?>
    <div style="padding: 10px; text-align: center;">
        <input type="button" class="button" value="Print" onclick="template.print();"/>
        <input type="button" class="button" value="Download" onclick="template.download(); " />
        <input type="button" class="button" id="cancel-form" value="Cancel" />
    </div>

    <script>
        var template = {
            print: function() {
                var url = '<?php echo $this->here; ?>' + "?print_form";
                window.open(url);
            },
            
            download: function() {
                if( 'humane' in window ) {
                    humane.info("Generating Handling Request PDF. Please Wait...");
                }
                
                setTimeout(function() {
                    var url = '<?php echo $this->here; ?>' + "?download";
                    window.location.href = url;
                }, 500);
            }
        }
    </script>
<?php } ?>