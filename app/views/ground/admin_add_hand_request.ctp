<?php
echo $html->css('app/columns.css');
echo $html->css('app/roles.css');
echo $html->css('app/jobsheet.css');
?>
<style>
    ul li.input {
        display: inline-block;
        margin-right: 10px;
        font-size: 13px;
        min-width: 160px;
        padding: 5px 0px;
    }

    .date_input {
        padding: 3px;
    }

    .date_input input[type=text] {
        margin-right: 5px;
    }

    .date_input input.location,
    .date_input input.cargo {
        min-width: 45px !important;
        width: 45px;
        font-size: 11px;
    }

    .date_input input.date {
        min-width: 110px !important;
        font-size: 11px;
    }

    #ground_handling_form td {
        padding: 5px;
    }
</style>

<h2>
    <?php
    if ((isset($id))) {
        __('Edit Ground Handling  Request');
    } else {
        __('Add New Ground Handling  Request');
    }
    ?>
</h2>

<form action="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_add_handRequest')); ?>" method="POST" id="ground_handling_form">
    <?php if ((isset($id))) { ?>
        <input type="hidden" name="data[GroundRequest][id]" id="data[GroundRequest][id]" value="<?php echo $id ?>"/>
    <?php } ?>

    <fieldset>

        <legend><b>Company To Invoice</b></legend>

        <table border="0" cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td>Name</td>
                <td>
                    <input type="text" class="required" required=""  name="data[GroundRequest][company_name]" id="data[GroundRequest][company_name]"
                           value="<?php echo isset($req_data) ? $req_data['GroundRequest']['company_name'] : ""; ?>"/>
                </td>
                <td>Email</td>
                <td>
                    <input type="text" class="required" required=""  name="data[GroundRequest][company_email]" id="data[GroundRequest][company_email]"
                           value="<?php echo isset($req_data) ? $req_data['GroundRequest']['company_email'] : ""; ?>"/>
                </td>
            </tr>

            <tr>
                <td>Phone</td>
                <td>
                    <input type="text"  name="data[GroundRequest][company_phone]" id="data[GroundRequest][company_phone]"
                           value="<?php echo isset($req_data) ? $req_data['GroundRequest']['company_phone'] : ""; ?>"/>
                </td>
                <td>Fax</td>
                <td>
                    <input type="text"  name="data[GroundRequest][company_fax]" id="data[GroundRequest][company_fax]"
                           value="<?php echo isset($req_data) ? $req_data['GroundRequest']['company_fax'] : ""; ?>"/>
                </td>
            </tr>

            <tr>
                <td>ATTN: </td>
                <td>
                    <input type="text" class="required" required=""  name="data[GroundRequest][company_contact]" id="data[GroundRequest][company_contact]"
                           value="<?php echo isset($req_data) ? $req_data['GroundRequest']['company_contact'] : ""; ?>"/>
                </td>
            </tr>

            <tr>
                <td colspan="2"><h3><b>Merchant Card Details</b></h3></td>
            </tr>
            <tr>
                <td nowrap>Card Name / Number:</td>
                <td><input type="text" size="25" name="data[GroundRequest][card_number]" value="<?php echo isset($req_data) ? $req_data['GroundRequest']['card_number'] : ""; ?>" /></td>
                <td nowrap>Holders Name:</td>
                <td><input type="text" size="25" name="data[GroundRequest][holder_name]" value="<?php echo isset($req_data) ? $req_data['GroundRequest']['holder_name'] : ""; ?>" /></td>
            </tr>
            <tr>
                <td nowrap>Card Expiry Date:</td>
                <td><input type="text" size="18" name="data[GroundRequest][card_expiry_date]" value="<?php echo isset($req_data) ? $req_data['GroundRequest']['card_expiry_date'] : ""; ?>" placeholder="MM/YY" /></td>
                <td nowrap>Purchase Order Number:</td>
                <td><input type="text" size="25" name="data[GroundRequest][po_number]" value="<?php echo isset($req_data) ? $req_data['GroundRequest']['po_number'] : ""; ?>" /></td>-->
            </tr>
        </table>

    </fieldset>

    <fieldset>
        <legend><b>Aircraft Details</b></legend>

        <table>
            <tr>
                <td>Operator</td>
                <td>
                    <input type="text" class="required" size="25" required name="data[GroundRequest][operator]" id="data[GroundRequest][operator]" 
                           value="<?php echo isset($req_data) ? $req_data['GroundRequest']['operator'] : ""; ?>">
                </td>
                <td>
                    Aircraft Type
                </td>
                <td>
                    <input type="text" class="required" required name="data[GroundRequest][type]" id="data[GroundRequest][type]" value="<?php echo isset($req_data) ? $req_data['GroundRequest']['type'] : ""; ?>">
                </td>
            </tr>

            <tr>
                <td>
                    Aircraft Regn.
                </td>
                <td>
                    <input type="text" class="required" required name="data[GroundRequest][region]" id="data[GroundRequest][region]" value="<?php echo isset($req_data) ? $req_data['GroundRequest']['region'] : ""; ?>">
                </td>
                <td>MTOW (KGS)</td>
                <td>
                    <input type="text" size="10" class="required" name="data[GroundRequest][mtow]" id="mtow_kgs" value="<?php echo isset($req_data) ? $req_data['GroundRequest']['mtow'] : "0"; ?>">
                    or 
                    <span id="mtow_lbs" style="font-size: 12px; font-weight: bold;">0 Lbs</span>
                </td>

            </tr>

            <tr>

                <td>Purpose of Flight</td>
                <td>
                    <input type="text" placeholder="e.g. PAX, CGO, AMB" class="required" required name="data[GroundRequest][purpose]" id="data[GroundRequest][purpose]"  value="<?php echo isset($id) ? $req_data['GroundRequest']['purpose'] : ""; ?>" >
                </td>
                <td>Nature of Cargo</td>
                <td>
                    <input type="text" name="data[GroundRequest][nature_cargo]" id="data[GroundRequest][nature_cargo]"  value="<?php echo isset($id) ? $req_data['GroundRequest']['nature_cargo'] : ""; ?>" >
                </td>
            </tr>

            <tr>
                <td>
                    Name of Captain
                </td>
                <td>
                    <input type="text" size="25" class="required" required name="data[GroundRequest][name_captain]" id="data[GroundRequest][name_captain]"  value="<?php echo isset($id) ? $req_data['GroundRequest']['name_captain'] : ""; ?>" >
                </td>
                <td>
                    Lead Passenger
                </td>
                <td>
                    <input type="text" class="required" required name="data[GroundRequest][lead_passenger]" id="data[GroundRequest][lead_passenger]"  value="<?php echo isset($id) ? $req_data['GroundRequest']['lead_passenger'] : ""; ?>" >
                </td>
            </tr>

            <tr>
                <td valign="top">Other Crew:</td>
                <td colspan="3">
                    <textarea name="data[GroundRequest][crew]" id="data[GroundRequest][crew]" 
                              style="width: 100%; height: 50px; display: inline-block"><?php echo isset($id) ? $req_data['GroundRequest']['crew'] : ""; ?></textarea>
                </td>
            </tr>
        </table>

    </fieldset>

    <div class="input" style="text-align: center; padding-bottom: 10px;">
        <input type="submit" value="Save Changes" class="button" />
        <input type="button" id="cancel-form" value="Cancel" class="button" />

        <input type="hidden" id="member_game_status_update_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_add_handRequest')); ?>" />
        <input type="hidden" id="redirect_request" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'requests')); ?>" />
        <input type="hidden" id="services_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_services')); ?>" />
    </div>
</form>