

<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php
echo $html->css('app/jobsheet.css');
echo $html->script('app/ground.js');
?>


<h2 style="display:inline"><?php
if ( sizeof($jobSheet) != 0 ) {
    echo "Edit Jobsheet for ";
} else {
    echo 'Add New JobSheet for ';
}
?></h2><span style="font-size:15px; color:red; weight:bold;"><?php echo $gr_ref; ?></span>
<form  method="POST" id="process_form">
    <?php if ( sizeof($jobSheet) != 0 ) { ?>
        <input type="hidden" name="data[JobSheet][id]" id="data[JobSheet][id]" value="<?php echo $jobSheet[0]['JobSheet']['id']; ?>"/>
    <?php } ?>
    <input type="hidden" name="data[JobSheet][gr_id]" id="data[JobSheet][gr_id]" value="<?php echo $grid; ?>"/>
    <fieldset>
        <div class="input">
            <label for="role_name">INT’L MERCHANT A/C – CARD NAME & NUMBER:</label>
            <input type="text" class="required" required name="data[JobSheet][card_number]" id="data[JobSheet][card_number]"
                   value="<?php
    if ( sizeof($jobSheet) != 0 ) {
        echo $jobSheet[0]['JobSheet']['card_number'];
    }
    ?>"/>

        </div>
        <div class="input">
            <label for="role_name">HOLDERS NAME</label>
            <input type="text" class="required" required name="data[JobSheet][holder_name]" id="data[JobSheet][holder_name]"
                   value="<?php
                   if ( sizeof($jobSheet) != 0 ) {
                       echo $jobSheet[0]['JobSheet']['holder_name'];
                   }
    ?>"/>


        </div>

        <div class="input">
            <label for="role_name">CARD EXPIRY DATE</label>
            <input type="text" class="data_expire_date required" required name="data[JobSheet][card_expiry_date]" id="data[JobSheet][card_expiry_date]"
                   value="<?php
                   if ( sizeof($jobSheet) != 0 ) {
                       echo $jobSheet[0]['JobSheet']['card_expiry_date'];
                   }
    ?>"/>


        </div>
        <div class="input">
            <label for="role_name">PO No.</label>
            <input type="text" class="required" required name="data[JobSheet][address]" id="data[JobSheet][address]"
                   value="<?php
                   if ( sizeof($jobSheet) != 0 ) {
                       echo $jobSheet[0]['JobSheet']['address'];
                   }
    ?>"/>

        </div>
        <div class="input">
            <!--has to come from the db-->
            <label for="role_name">HANDLER</label>
            <select class="required" required name="data[JobSheet][handler_id]" id="data[JobSheet][handler_id]">
                <?php foreach( $handlers as $val ) { ?>
                    <option value="<?php echo $val['Handler']['id'] ?>"
                    <?php
                    if ( sizeof($jobSheet) != 0 && $jobSheet[0]['JobSheet']['handler_id'] == $val['Handler']['id'] ) {
                        echo "selected=selected";
                    }
                    ?>   ><?php echo $val['Handler']['name'] ?></option>
                        <?php } ?>
            </select>
        </div>
        <div class="input">
            <label for="role_name">COMPANY TO INVOICE</label>
            <!--has to come from the db-->
            <input type="text" class="required" required name="data[JobSheet][company_name]" id="data[JobSheet][company_name]"
                   value="<?php
                        if ( sizeof($jobSheet) != 0 ) {
                            echo $jobSheet[0]['JobSheet']['company_name'];
                        }
                        ?>"/>

        </div>
        <!--
        <div class="input">
            <label for="role_name">Arrival Date</label>
            <input type="text" class="required" required name="data[JobSheet][arrival_dt]" id="data[JobSheet][arrival_dt]" >
            <label for="role_name">Origin (ICAO/IATA)</label>
            <input type="text" class="required" required name="data[JobSheet][flight_origin]" id="data[JobSheet][flight_origin]" >

        </div>
        -->

    </fieldset>

    <?php if ( sizeof($jobSheet) != 0 ) { ?>
        <input type="button" value="Send JobCard" class="send_handler" />
        <input type="button" value="Print JobCard" class="print_form" />
        <input type="button" value="View JobSheet Details" class="flyout_opener" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_jobsheet_form', $jobSheet[0]['JobSheet']['gr_id'], $jobSheet[0]['JobSheet']['id'])); ?>"/>
    <?php } ?>

    <input type="hidden" id="print_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_jobsheet_form', $jobSheet[0]['JobSheet']['gr_id'], $jobSheet[0]['JobSheet']['id'], "true")); ?>"/>
    <input type="hidden" id="job_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_jobsheet_form', $jobSheet[0]['JobSheet']['gr_id'], $jobSheet[0]['JobSheet']['id'])); ?>"/>
    <input type="button" id="cancel-form" value="Close" />
    <input type="submit" id="process_button_btn" value="Save" />
</form>

<input type="hidden" id="member_game_status_update_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'addJob')); ?>" />
<input type="hidden" id="add_redir" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_index')); ?>" />
