<?php
echo $html->css('app/columns.css');
echo $html->css('app/jobsheet.css');
?>
<style type="text/css">
    .top-nav {
        list-style: none;
        margin-bottom: 10px;
    }

    .top-nav li {
        display: inline-block;
        border: solid #ddd 1px;
        border-radius: 5px;
        background: -moz-linear-gradient(top, rgba(245,245,220,1) 0%, rgba(255,255,255,0) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(245,245,220,1)), color-stop(100%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* IE10+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f5dc', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
        background: linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* W3C */
    }

    .top-nav li a {
        text-decoration: none;
        color: #444;
        font-size: 11px;
        line-height: 25px;
        display: block;
        padding: 0px 10px;
    }

    .ui-datepicker-trigger {
        vertical-align: middle;
        width: 24px;
        height: 24px;
    }
</style>

<div class="columns">

    <div class="column" id="users_column">
        <div class="header" style="overflow: hidden;">
            Ground Handling
            
            <div style="float: right; right: 10px; top: 0px; font-size: 11px; font-weight: normal; margin-top: -5px;">
                Find Request: <input id="schedule-search" type="search" size="30" placeholder="SBG0001-11" style="font-size: 11px;" data-url="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_getreq', 'open')); ?>" />
                By: <select id="search_term">
                    <option value=""></option>
                    <option value="op" data-placeholder="Operator">Operator</option>
                    <option value="at" data-placeholder="A/C Type">AirCraft Type</option>
                    <option value="rn" data-placeholder="A/C Regn No.">AirCraft Regn No</option>
                    <option value="fn" data-placeholder="Flight No.">Flight Number</option>
                    <option value="sn" data-placeholder="SBG0001-11" selected="selected">Ref Number</option>
                    <option value="cn" data-placeholder="Company Name">Company Name</option>
                </select>
            </div>
        </div>

        <div class="content">

            <div class="master_block" style="width: 45%;">

                <div id="tabs">
                    <ul>
                        <li><a class="lk_open"  title="open_perms" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_billing_list', 'open')); ?>">Open</a></li>
                        <li><a class="lk_open"  title="closed_perms" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_billing_list', 'completed')); ?>">Completed</a></li>
                        <li><a class="lk_open"  title="cancelled_perms" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_billing_list', 'cancelled')); ?>">Canceled</a></li>
                    </ul>
                </div>

            </div>

            <!--<div class="details_block" style="width: 55%;">

                <div class="details_options">
                    <h3 style="font-weight: bold; font-size: 12px; color: #222;">Request Details</h3>
                </div>

                <div class="details_pane" id="services_show" style="padding: 10px;">
                    Select Request On The Left To View Details of Flight Schedules and Handlers Here
                </div>
            </div>-->
        </div>
    </div>
</div>
<?php
//echo $html->script('app/ground_request.js');
//echo $html->script('app/schedules.js');
?>
<input type="hidden" id="service_list" name="service_list" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_services')); ?>" />
