<?php
$isOpen = (isset($status) && $status == 'open');
?>

<table border="0" cellpadding="5" cellspacing="0" class="fullwidth" id="gr_table">
    <thead>
        <tr>
            <th colspan="2" style="padding-left: 1px;">
                <input type="checkbox" class="check_all" /> REF No.
            </th>
            <th>Company</th>
            <th>Operator</th>
            <th>Aircraft Type</th>
            <!--<th>Flight No.</th>-->
            <th nowrap>Vendor Invoice #</th>
            <th nowrap>Invoice #</th>
            <th width="60" nowrap>Billed On</th>
            <th width="60" style="text-align:center;">Actions</th>
        </tr>
    </thead>

    <tbody>

        <?php
        $count = $this->Paginator->counter('%start%');
        foreach ($req as $val) {
            $isOpen = $val['GroundRequestSchedule']['status'] == "" || $val['GroundRequestSchedule']['status'] == "open";
            ?>
            <tr id="<?php echo $val['GroundRequest']['id']; ?>">

                <td width="50" align="left" style="padding: 7px 1px;" nowrap>
                    <input type="checkbox" value="<?php echo $val['GroundRequest']['id']; ?>" class="check" />
                    <?php echo $count++ . "."; ?>
                </td>
                <td nowrap>
                    <?php
                    if (isset($val['GroundRequestSchedule'][0])) {
                        echo $val['GroundRequestSchedule'][0]['ref_no'];
                    } else if (isset($val['GroundRequestSchedule']['ref_no'])) {
                        echo $val['GroundRequestSchedule']['ref_no'];
                    }
                    ?>
                </td>
                <td><?php echo $val['GroundRequest']['company_name']; ?></td>
                <td><?php echo $val['GroundRequest']['operator']; ?></td>
                <td><?php echo $val['GroundRequest']['type']; ?></td>
                <td><?php echo isset($val['GroundRequestBilling'][0]) ? $val['GroundRequestBilling'][0]['vendor_invoice_number'] : "---"; ?></td>
                <td><?php echo isset($val['GroundRequestBilling'][0]) ? $val['GroundRequestBilling'][0]['invoice_number'] : "---"; ?></td>
                <td align="right"><?php echo isset($val['GroundRequestBilling'][0]) ? date('d/m/y', strtotime($val['GroundRequestBilling'][0]['billing_dt'])) : "Pending"; ?></td>
                <td nowrap>
                    <a class="view_link dialog_opener" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_schedule_summary', $val['GroundRequest']['id'])); ?>" >Summary</a>
                    <a class="edit_link dialog_opener" title="Updating Billing Information For <?php echo $val['GroundRequestSchedule']['ref_no']; ?>" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_bill', $val['GroundRequest']['id'])); ?>" ><?php echo $html->image('pencil.png', array('align' => 'absmiddle')); ?> Update Billing</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php echo $this->element('layout/pagination_footer', array('url_params' => array($status), 'ajax' => true)); ?>