<style>
    #attachment_area {

    }

    #attachment_area td {
        padding: 8px;
    }
</style>

<div id="attachment_area">
    <form name="attachment_add" id="AttachmentAddForm" method="post" action="<?php echo $this->here; ?>" target="upload-frame" enctype="multipart/form-data">
        <h2>Add New Attachment</h2>

        <table border="0" cellpadding="0" cellspacing="5" width="100%">
            <tr>
                <td width="150" valign="top">
                    Name
                </td>
                <td>
                    <input type="text" size="50" value="" name="data[GroundRequestScheduleAttachment][name]" /> <br /> <br />
                    
                    (Multiple Files May Be Selected At Once. Max Total File Upload Size <b id="max_size_value"><?php echo ini_get("upload_max_filesize"); ?></b>)
                </td>
            </tr>
            
            <tr>
                <td valign="top">
                    Select Attachment(s):
                </td>
                <td>
                    <input type="file" id="attachments_fs" name="attachments[]" multiple="" size="50" />

                    <div id="file_list" style="padding: 10px;"></div>
                    <div id="upload_status" style="padding: 10px; font-weight: bold;"></div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <input type="button" value="Upload & Save" id="up_save_btn" class="button" disabled="" />
                    <input type="button" value="Cancel" class="button" id="cancel-form" />
                    <input type="hidden" name="data[GroundRequestScheduleAttachment][ground_request_schedule_id]" value="<?php echo $ground_request_schedule_id; ?>" />
                </td>
            </tr>
        </table>

    </form>

    <iframe width="10" height="10" src="about:home" style="opacity: 0;" name="upload-frame" id="upload-frame" />
</div>
