<?php
echo $html->css('app/columns.css');
?>

<style type="text/css">
    #schedule_form_wrapper section {
        margin: 0px;
        margin-bottom: 10px;
        border-bottom: dashed #eee 1px;
        background-color: f5f5f5;
        border-radius: 5px;
        padding: 5px;
    }

    .top-nav {
        list-style: none;
        margin-bottom: 10px;
    }

    .top-nav li {
        display: inline-block;
        border: solid #ddd 1px;
        border-radius: 5px;
        background: -moz-linear-gradient(top, rgba(245,245,220,1) 0%, rgba(255,255,255,0) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(245,245,220,1)), color-stop(100%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* IE10+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f5dc', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
        background: linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* W3C */
    }

    .top-nav li a {
        text-decoration: none;
        color: #444;
        font-size: 11px;
        line-height: 25px;
        display: block;
        padding: 0px 10px;
    }

    .ui-datepicker-trigger {
        vertical-align: middle;
        width: 24px;
        height: 24px;


    }
    ul li.input {
        display: inline-block;
        margin-right: 10px;
        font-size: 13px;
        min-width: 160px;
        padding: 5px 0px;
    }

    .date_input {
        padding: 3px;
    }

    .date_input input[type=text] {
        margin-right: 5px;
    }

    .date_input input.location,
    .date_input input.cargo {
        min-width: 45px !important;
        width: 45px;
        font-size: 11px;
    }

    .date_input input.date {
        min-width: 110px !important;
        font-size: 11px;
    }
    #update_form{
        padding: 10px !important;
    }
    #form_sub{
        float: left;
    }

    #schedule_save_status {
        padding: 10px;
        text-align: center;
        color: #c1a03f;
        font-size: 11px;
        font-weight: bold;
        display: none;
    }

    .text_button  {
        padding: 5px 10px !important; 
        text-align: center !important; 
        color: #fff !important; 
        margin-left: 10px !important;
    }

    .input_labels label {
        font-size: 11px;
        font-weight: bold;
        width: auto;
        padding-bottom: 4px;
    }
    
    .handling_table td {
        padding: 5px;
    }
    
    .handling_table td select {
        font-size: 12px;
    }
</style>

<div id="schedule_form_wrapper">
    <form  action="<?php echo $html->url(array('controller' => 'ground', 'action' => 'schedule_edit')); ?>" method="POST" id="update_form">

        <section id="dates_section">
            <h2>Arrival & Departure Information</h2>

            <div class="ui-state-highlight ui-corner-all" style="padding: 5px; margin-bottom: 10px; font-size: 11px;">ETD/ETA fields not entered would show up as <b>TBA</b> when viewed in other sections</div>

            <div class="input_labels">
                <label style="width: 70px !important;">Flt. No.</label>
                <label style="width: 125px !important;">ETD Origin</label>
                <label style="width: 64px !important;">Origin</label>
                <label style="width: 125px !important;">ETA Dest</label>
                <label style="width: 62px !important;">Dest</label>
                <label style="width: 62px !important;">Cargo</label>
                <label style="width: 62px !important;">PAX No.</label>
                <label style="width: 62px !important;">Crew No.</label>
            </div>

            <div class="date_input">
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_flight_num'] : ""; ?>" class="cargo" type="text"  name="data[GroundRequestSchedule][arr_flight_num]" placeholder=" Flt. No" size="12" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_etd'] : ""; ?>" class="date required " required="" type="text" id="arr_etd" name="data[GroundRequestSchedule][arr_etd]" placeholder="ETD" size="12" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_origin'] : ""; ?>" class="location ui-autocomplete-input" required="" type="text" id="arr_origin" name="data[GroundRequestSchedule][arr_origin]" placeholder="Origin" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_eta'] : ""; ?>" class="date required " type="text" id="arr_eta" name="data[GroundRequestSchedule][arr_eta]" placeholder="ETA" size="12" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_dest'] : ""; ?>" class="location ui-autocomplete-input" type="text" id="arr_dest" name="data[GroundRequestSchedule][arr_dest]" placeholder="Dest" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_cargo'] : ""; ?>" class="cargo" type="text" name="data[GroundRequestSchedule][arr_cargo]" placeholder="" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_pax_no'] : ""; ?>" class="cargo" type="text"  name="data[GroundRequestSchedule][arr_pax_no]" placeholder="" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['arr_crew_no'] : ""; ?>" class="cargo" type="text"  name="data[GroundRequestSchedule][arr_crew_no]" placeholder="" />
            </div>

            <div class="date_input">
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_flight_num'] : ""; ?>" class="cargo" type="text"  name="data[GroundRequestSchedule][dept_flight_num]" placeholder="Flt. No" size="12" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_etd'] : ""; ?>" class="date " type="text" id="dept_etd" name="data[GroundRequestSchedule][dept_etd]" placeholder="ETD" size="12" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_origin'] : ""; ?>" class="location ui-autocomplete-input" type="text" id="dept_origin" name="data[GroundRequestSchedule][dept_origin]" placeholder="Origin" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_eta'] : ""; ?>" class="date " type="text" id="dept_eta" name="data[GroundRequestSchedule][dept_eta]" placeholder="ETA" size="12" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_dest'] : ""; ?>" class="location ui-autocomplete-input" type="text" id="dept_dest" name="data[GroundRequestSchedule][dept_dest]" placeholder="Dest" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_cargo'] : ""; ?>" class="cargo" type="text" name="data[GroundRequestSchedule][dept_cargo]" placeholder="" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_pax_no'] : ""; ?>" class="cargo" type="text" name="data[GroundRequestSchedule][dept_pax_no]" placeholder="" />
                <input value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['dept_crew_no'] : ""; ?>" class="cargo" type="text" name="data[GroundRequestSchedule][dept_crew_no]" placeholder="" />
            </div>
        </section>




        <section style="margin-bottom: 20px;">

            <h2>Handling</h2>

            <div id="handler_select_notice" style="font-size: 11px; padding: 5px; margin: 5px 0px; display: block; vertical-align: top;">(Select Handler To Show Service Selection Area)</div>

            <table border="0" cellpadding="5" cellspacing="0" width="100%" class="handling_table">
                <tr>
                    <td>Select Handler:</td>
                    <td colspan="3">
                        <!--has to come from the db-->
                        <!--<label for="handler_id" style="vertical-align: top; padding-top: 5px;">HANDLER</label>-->
                        <select id="handler_id" style="width: 500px;"  name="data[GroundRequestSchedule][handler_id]" >
                            <option value="">Select Handler</option>
                            <?php
                            foreach ($handlers as $country => $data) {

                                foreach ($data as $id => $name) {
                                    $selected = $id == $grdates['GroundRequestSchedule']['handler_id'] ? "selected" : "";
                                    ?>
                                    <option value="<?php echo $id ?>" <?php echo $selected; ?>>
                                        <?php echo $country . " - " . $name ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Handling Fees:</td>
                    <td>
                        <select name="data[GroundRequestSchedule][handling_fees_on]" required="">
                            <option value="">Select Payment Method</option>
                            <?php
                            $payment_types = array("credit" => 'ON S.B. MAN Account', 'cash' => 'ON CASH BASIS');
                            foreach ($payment_types as $key => $value) {
                                $selected = isset($grdates) && $grdates['GroundRequestSchedule']['handling_fees_on'] == $key ? "selected=selected" : "";
                                echo "<option value='$key' $selected>$value</option>";
                            }
                            ?>
                        </select>
                    </td>
                    <td>Airport Fees</td>
                    <td>
                        <select name="data[GroundRequestSchedule][airport_fees_on]" required="">
                            <option value="">Select Payment Method</option>
                            <?php
                            $payment_types = array("credit" => 'ON S.B. MAN Account', 'cash' => 'ON CASH BASIS');
                            foreach ($payment_types as $key => $value) {
                                $selected = isset($grdates) && $grdates['GroundRequestSchedule']['handling_fees_on'] == $key ? "selected=selected" : "";
                                echo "<option value='$key' $selected>$value</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td>Remarks</td>
                    <td colspan="2"><input type="text" name="data[GroundRequestSchedule][handling_remarks]" value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['handling_remarks'] : ''; ?>" size="35" /></td>
                    <td><span>Remarks will be displayed on Crew Brief Document</span></td>
                </tr>
            </table>

            <div id="schedule_save_status">
                Updating Schedule. Please Wait...
            </div>
        </section>

        <section id="services_section">
            <h2>Services <a id="services_add_new" class="button dialog_opener text_button" href="<?php echo $html->url(array('controller' => 'services', 'action' => 'scheduleService', (isset($grdates) ? $grdates['GroundRequestSchedule']['id'] : ""))); ?>" title="Add New Service" data-height="600" >Add New</a></h2>

            <table border="0" cellspacing="0" cellpadding="6" width="100%" class="fullwidth" id="schedule_service_list">
                <thead>
                    <tr>
                        <th align="left" colspan="2">Service Type</th>
                        <th>Service Provider</th>
                        <th>Provider Contact</th>
                        <th style="text-align: center; width: 150px;">Options</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (isset($grdates)) {
                        $count = 1;
                        foreach ($grdates['GroundRequestScheduleService'] as $service) {
                            ?>
                            <tr>
                                <td class="count" align="right" style="width: 20px;"><?php echo $count++; ?>. </td>
                                <td><?php echo isset($service['Service']) ? $service['Service']['name'] : ""; ?></td>
                                <td><?php echo $service['name']; ?></td>
                                <td><?php echo $service['contact_number']; ?></td>
                                <td align="center" >
                                    <!--<a href="#">Details</a> -->
                                    <a class="edit_link" data-id="<?php echo $service['id']; ?>" href="#">Edit</a>
                                    <a class="del_service_link" data-id="<?php echo $service['id']; ?>" href="#">Delete</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>

            <input type="hidden" id="services_delete_url" value="<?php echo $html->url(array('controller' => 'services', 'action' => 'deleteScheduleService')); ?>" />
            <input type="hidden" id="services_edit_url" value="<?php echo $html->url(array('controller' => 'services', 'action' => 'scheduleServiceEdit', (isset($grdates) ? $grdates['GroundRequestSchedule']['id'] : ""))); ?>" />
        </section>




        <section>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td width="50%" valign="top" style="border-right: dotted #ccc 1px; border-radius: 5px; padding: 5px;">
                        <h2>
                            Attachments 
                            <a id="attachments_add_new" class="button dialog_opener text_button" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'attachments_add', (isset($grdates) ? $grdates['GroundRequestSchedule']['id'] : ""))); ?>" title="Add New Attachment" >Add New</a>
                        </h2>

                        <div id="attachment_list" data-url="<?php echo $html->url(array('controller' => 'ground', 'action' => 'attachments_list', (isset($grdates) ? $grdates['GroundRequestSchedule']['id'] : ""))); ?>">
                            <?php if (isset($grdates) && isset($grdates['GroundRequestScheduleAttachment']) && count($grdates['GroundRequestScheduleAttachment']) > 0) { ?>
                                <table border="0" cellspacing="5" width="100%">
                                    <?php foreach ($grdates['GroundRequestScheduleAttachment'] as $attachment) { ?>
                                        <tr>
                                            <td>
                                                <?php echo $attachment['file_name']; ?>
                                            </td>
                                            <td>
                                                <a target="_blank" class="preview_link" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'attachments_view', $attachment['id'])); ?>">
                                                    <?php echo $html->image('magnifier.png'); ?>
                                                </a>

                                                <a class="delete_link" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'attachments_delete', $attachment['id'])); ?>">
                                                    <?php echo $html->image('cross.png'); ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                                <?php
                            } else {
                                echo "No attachments added";
                            }
                            ?>

                        </div>
                    </td>
                    <td width="50%" valign="top" style="padding: 5px 15px;">
                        <h2>Comments</h2>
                        <textarea name="data[GroundRequestSchedule][comment]" id="comment_date" style="width: 100%; height: 100px; box-sizing: border-box;"><?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['comment'] : ""; ?></textarea>
                    </td>
                </tr>
            </table>

        </section>

        <div class="input" style="text-align: center; padding-bottom: 10px;">
            <input type="submit" id="form_btn" value="Save" class="button" />
            <input type="button" id="cancel-form" value="Cancel" class="button" />

            <input type="hidden" id="ground_request_id" name="data[GroundRequestSchedule][ground_request_id]" value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['ground_request_id'] : $ground_request_id; ?>" />
            <input type="hidden" id="ground_request_schedule_id" name="data[GroundRequestSchedule][id]" value="<?php echo isset($grdates) ? $grdates['GroundRequestSchedule']['id'] : ""; ?>" />
            <input type="hidden" id="save_service" name="save_service" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_save_service')); ?>" />
            <input type="hidden" id="edit_service" class="flyout_opener" name="edit_service" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_edit_service')); ?>" />
            <input type="hidden" id="del_service" name="del_service" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_del_service')); ?>" />
        </div>
    </form>
</div>
<?php
echo $html->script('schedules.js');
?>
