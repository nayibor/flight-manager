<?php echo $this->element('ground/crew_brief_summary'); ?>

<div style="padding: 20px; text-align: center;">
    <input type="button" class="button" value="Print Brief" id="cb_print_form" data-url="<?php echo $html->url( array('controller' => 'ground', 'action' => 'crew_brief_print', $schedule_id) ); ?>" />
    <input type="button" class="button" value="Download" id="cb_download_form" data-url="<?php echo $html->url( array('controller' => 'ground', 'action' => 'crew_brief_print', $schedule_id, 'download') ); ?>" />
    <input type="button" class="button" value="Cancel" id="cancel-form" />
</div>

<script>
    $(document).ready(function() {
        $("#cb_print_form").click(function() {
            window.open($(this).data('url'));
        });
        
        $("#cb_download_form").click(function() {
            if( 'humane' in window ) {
                humane.info('Generating Crew Brief PDF For Download');
            } 
            
            var $this = $(this);
            setTimeout(function() {
                window.location.href = $this.data('url');
            }, 1000);
        });
    });
</script>