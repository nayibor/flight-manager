

<style>
    #qt label{
        /**  display: inline !important;
           **/
        width: 200px !important;
        padding-right: 5px !important;

    }
    label{
        width: 120px !important;
    }

</style>


<div class="flyout_content">
    <?php echo $this->element('ground/quality_service_request', array('schedule_id' => $req_data['GroundRequestSchedule']['id'])); ?>
</div>


<div style="padding: 30px 0px; overflow: hidden;">
    <input type="button" class="button" id="qcr_save_btn" value="Save Changes" />
    <input type="button" class="button" id="qcr_print_btn" value="Print" />
    <input type="button" class="button" id="qcr_download_btn" value="Download" />
    <input type="button" class="button" id="cancel-form" value="Cancel" />

    <?php if( isset($req_data['GroundRequestQuality']['id']) ) { ?>
        <input type="hidden" name="data[GroundRequestQuality][id]" id="data[GroundRequestQuality][id]" value="<?php echo $req_data['GroundRequestQuality']['id'] ?>" />
        <?php
    }
    ?>
    <input type="hidden" name="data[GroundRequestQuality][ground_request_schedule_id]" id="data[GroundRequestQuality][ground_request_schedule_id]" value="<?php echo $req_data['GroundRequestSchedule']['id'] ?>" />

    <input type="hidden" id="save_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_service_quality_report')) ?>" />
    <input type="hidden" id="print_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_service_quality_print', $req_data['GroundRequestSchedule']['id'])) ?>" />
    <input type="hidden" id="qcr_download_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_service_quality_print', $req_data['GroundRequestSchedule']['id'], 'download')) ?>" />

</div>

<script>
    $(document).ready(function(){
        $("#qcr_print_btn").click(function(){
            window.open( $("#print_url").val());
        });

        $("#qcr_save_btn").click(function(event){
            event.preventDefault();
            var url_req = $("#save_url").val();
            var query = $("#service_report_form").serialize();
            $.ajax({
                url:url_req,
                data: query,
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    alert("data saved");
                },
                error: function(xhr) {
                    alert(xhr.responseText);
                }
            });
        });
        
        $("#qcr_download_btn").click(function(e) {
            if( 'humane' in window ) {
                humane.info('Generating Quality Control Report In PDF');
            }
            
            setTimeout(function() {
                window.location.href = $("#qcr_download_url").val();
            }, 100);
        });
    });


</script>