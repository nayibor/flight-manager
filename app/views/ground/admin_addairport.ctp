<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php echo $html->css('app/jobsheet.css');
echo $html->script('app/ground.js');
//print_r($handlers);

?>


<h2><?php __('Add New Airport Request'); ?></h2>

<form  method="POST" id="airport_process_form">
    <fieldset>
        <div class="input">
            <label for="role_name">JOB NUMBER</label>
            <select name="data[AirportDetail][job_number]" id="data[AirportDetail][job_number]">
                <?php foreach($jobSheets as $val) { ?>
                <option value="<?php echo $val['JobSheet']['id']  ?>"><?php echo $val['JobSheet']['job_number']."--".$val['JobSheet']['company_name']  ?></option>
                    <?php } ?>
            </select>
        </div>
        <div class="input">
            <label for="role_name"> IATA</label>
            <input type="text" class="required" required="" name="data[AirportDetail][iata_code]" id="data[JobSheet][card_number]" >
            <label for="role_name">ICAO</label>
            <input type="text" class="required" required="" name="data[JobSheet][holder_name]" id="data[JobSheet][holder_name]" >

        </div>
        <div class="input">
            <label for="role_name"> CITY NAME</label>
            <input type="text" class="required" required="" name="data[AirportDetail][city_name]" id="data[JobSheet][city_name]" >
            <label for="role_name">COUNTRY</label>
            <input type="text" class="required" required="" name="data[AirportDetail][country]" id="data[AirportDetail][country]" >

        </div>
        <div class="input">
            <label for="role_name"> TIME ZONE (GMT) DURING SUMMER</label>
            <input type="text" class="required" required="" name="data[AirportDetail][city_name]" id="data[AirportDetail][time_summer]" >
            <label for="role_name">TIME ZONE (GMT) DURING WINTER</label>
            <input type="text" class="required" required="" name="data[AirportDetail][country]" id="data[AirportDetail][time_winter]" >

        </div>

        <div class="input">
            <label for="role_name"> AIRPORT NAME</label>
            <input type="text" class="required" required="" name="data[AirportDetail][airport_name]" id="data[AirportDetail][airport_name]" >
            <label for="role_name">DISTANCE TO CITY</label>
            <input type="text" class="required" required="" name="data[AirportDetail][country]" id="data[AirportDetail][airport_dtc]" >

        </div>
        <hr/>
        <h2 class="services_text">AIRPORT INFORMATION</h2>
        <p>
            V.H.F FREQUENCY - TOWER
            <input type="text" class="required" required="" name="data[AirportDetail][freq_tower]" id="data[AirportDetail][freq_tower]" >
            GROUND
            <input type="text" class="required" required="" name="data[AirportDetail][ground]" id="data[AirportDetail][ground]" >

        </p>
        <br>
        <p>
            EMERGANCY PHONE NUMBER
        </p>
        <p>
            #1
            <input type="text" class="required" required="" name="data[AirportDetail][emeg_num][]" id="data[AirportDetail][emeg_num][]" >
            #2
            <input type="text" class="required" required="" name="data[AirportDetail][emeg_num][]" id="data[AirportDetail][emeg_num][]" >

        </p>
        <br>
        <p>
            AIRPORT OPERATING HOURS:
            <input type="text" class="required" required="" name="data[AirportDetail][airport_ophrs]" id="data[AirportDetail][airport_ophrs]" >
        </p>

        <div>
            <label for="role_name" class="services_text">SERVICE</label>
            <label for="role_name" class="services_text">PLEASE TICK (YES/NO)</label>
        </div>
        <br>
        <div class="input">
            <label for="role_name" class="long_label">AIRPORT OF ENTRY</label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][airport_entry]" id="data[AirportDetail][airport_entry]" >
        </div>
        <div class="input">
            <label for="role_name">CUSTOMS-IMMIGRATION </label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][customs_immi]" id="data[AirportDetail][customs_immi]" >
        </div>

        <div class="input">
            <label for="role_name">WATER SERVICE </label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][water_service]" id="data[AirportDetail][water_service]" >
        </div>
        <div class="input">
            <label for="role_name">LAVATORY SERVICE </label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][lavatory_service]" id="data[AirportDetail][lavatory_service]" >
        </div>
        <div class="input">
            <label for="role_name">AIRCONDITIONING UNIT </label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][acu]" id="data[AirportDetail][acu]" >
        </div>
        <div class="input">
            <label for="role_name"> AIRSTARTER UNIT(ASU)</label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][asu]" id="data[AirportDetail][asu]" >
        </div>
        <div class="input">
            <label for="role_name"> GROUND POWER UNIT(ASU)</label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][gpu]" id="data[AirportDetail][gpu]" >
        </div>

        <div class="input">
            <label for="role_name"> NITROGEN REFILLING SERVS</label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][nitrogen_ref]" id="data[AirportDetail][nitrogen_ref]" >
        </div>

        <div class="input">
            <label for="role_name"> SLOTS REQUIRED</label>
            <input type="checkbox" class="required" required="" name="data[AirportDetail][slots_req]" id="data[AirportDetail][slots_req]" >
        </div>
        <p>
            ARE THERE ANY AIRCRAFT REGISTRATIONS NOT ALLOWED INTO THIS AIRPORT ? IF YES PLEASE LIST:
        </p>
        <div class="input">
            <input type="text" class="required lf" required="" name="data[AirportDetail][disallowed_reg]" id="data[AirportDetail][disallowd_reg]" >
        </div>
        <p>
            ANY RESTRICTIONS OPERATING TO THIS AIRPORT COMING/GOING TO A CERTAIN AIRPORT OR LOCATION:</p>
        <div class="input">
            <input type="text" class="required lf" required="" name="data[AirportDetail][restrictions]" id="data[AirportDetail][restrictions]" >
        </div>

        <h2 class="services_text">FUEL AVAILABILITY</h2>
        <div class="input">
            <label for="role_name" class="">JET A1 FUEL</label>
            <input type="checkbox" name="data[AirportDetail][jet_avail]" id="data[JobSheet][jet_avail]" value="1">
        </div>
        <div class="input">
            <label for="role_name" class="">AVGAS  FUEL</label>
            <input type="checkbox" name="data[AirportDetail][avg_avail]" id="data[JobSheet][avg_avail]" value="1">
        </div>

        <h2 class="services_text">CREW VISAS</h2>
        <div class="input">
            <label for="role_name" class="">REQUIRED</label>
            <input type="checkbox" name="data[AirportDetail][visa_req]" id="data[JobSheet][visa_req]" value="1">
        </div>
        <div class="input">
            <label for="role_name" class="">CAN IT BE GIVEN OR ARRIVAL</label>
            <input type="checkbox" name="data[AirportDetail][visa_garrive]" id="data[JobSheet][visa_garrive]" value="1">
        </div>
        <div class="input">
            <label for="role_name" class="">ANY RESTRICTIONS ON NATIONALITY</label>
            <input type="checkbox" name="data[AirportDetail][visa_restnat]" id="data[JobSheet][visa_restnat]" value="1">
        </div>

        <p>  ANY OTHER INFORMATION THAT MAY BE USEFUL</p>
        <div class="input">
            <textarea name="data[AirportDetail][info]" id="data[JobSheet][info]"></textarea>
        </div>
        <br>
        <h2 class="services_text">HOTEL INFORMATION ( 4 – 5 STAR HOTELS ONLY )</h2>
        <p>5 STAR HOTELS (*****)</p>
        <div>
            <label for="role_name" class="services_text">HOTEL NAME </label>
            <label for="role_name" class="services_text">PHONE NUMBER</label>
            <label for="role_name" class="services_text">DISTANCE FROM AP</label>

        </div>
        <br><br><br><br<br><br>
        <p>4 STAR HOTELS (****)</p>
        <div>
            <label for="role_name" class="services_text">HOTEL NAME </label>
            <label for="role_name" class="services_text">PHONE NUMBER</label>
            <label for="role_name" class="services_text">DISTANCE FROM AP</label>

        </div>
        <br><br><br>
        <p> LOAN/CURRENCY INFORMATION</p>
        <p>
            EXCHANGE RATE : 1 USD =<input type="text" class="required" required="" name="data[AirportDetail][dollar_rate]" id="data[AirportDetail][dollar_rate]">
            1 EURO =<input type="text" class="required" required="" name="data[AirportDetail][euro_rate]" id="data[AirportDetail][euro_rate]">

        </p>
    </fieldset>

    <input type="reset" id="reset_button_btn"/>
    <input type="submit" id="process_button_btn"/>


</form>