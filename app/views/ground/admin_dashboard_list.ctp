<style>
    .pane-footer {
        margin-bottom: 10px;
    }
    
    .pane-footer span a, .pane-footer span.current {
        padding: 3px 8px;
        display: inline-block;
        font-weight: bold;
        text-decoration: none;
        background-color: #aaa;
        color: #333;
        margin: 0px 2px;
    }
    
    .pane-footer span.current {
        background-color: transparent;
    }
    
    .pane-footer span a:hover {
        background-color: #ccc;
    }
</style>
<div id="dashboard_list">
    
    <div class="pane-footer">
        <ul>
            <li style="background: none; font-weight: bold;">Page </li>
            <?php echo $this->Paginator->numbers(); ?>
        </ul>
    </div>


    <table border="0" cellpadding="5" cellspacing="0" class="fullwidth" id="gr_table">
        <thead>
            <tr>
                <th colspan="3">REF No / LOC</th>
                <th>OPTR</th>
                <th>FLT No</th>
                <th>A/C REGN</th>
                <th>A/C TYPE</th>
                <th>Principal</th>
                <th><?php echo $type == "departures" ? "DEST" : "ORIGIN"; ?></th>
                <th style="text-align: center;"><?php echo $type == "departures" ? "ETD" : "ETA"; ?></th>
                <th style="text-align: center;"><?php echo $type == "departures" ? "ATD" : "ATA"; ?></th>
                <!--<th>SVC OPS</th>-->
                <!--<th>Options</th>-->
            </tr>
        </thead>

        <tbody>

            <?php
            $count = $this->Paginator->counter('%start%');
            foreach ($req as $val) {
                $date = $type == "departures" ? $val['GroundRequestSchedule']['dept_etd'] : $val['GroundRequestSchedule']['arr_etd'];

                $date_dt = new DateTime($date);
                $interval = $date_dt->diff(new DateTime());
                $period_remaining = ($type == "departures" ? "ETD: " : "ETA: ") . $interval->format("%D days %H hours %I mins");
                $class = "";

                // either we are close to the arrival or departure time, or it has passed
                if (($interval->format("%D") <= 0 && $interval->format("%H") < 2)) {
                    $class = "emergency";
                }

                // we have less than 24 hours to this departure so we need to be on gaurd
                else if ($interval->format("%D") <= 0 && $interval->format("%H") < 10) {
                    $class = "warning";
                }

                // this work has not been indicated as completed, yet flight date has passed.
                else if ($date_dt < new DateTime()) {
                    $class = "critical";
                    $period_remaining .= " ago";
                }


                // conditions for tracking mov't of arrivals
                if ($type == "arrivals" && strtotime($val['GroundRequestSchedule']['arr_ata']) != false) {
                    $class = "completed";
                    $period_remaining = "Aircraft Arrived";
                } else if ($type == "arrivals" && strtotime($val['GroundRequestSchedule']['arr_atd']) != false) {
                    $class = "operational";
                    $period_remaining = "Mov't Advised, " . $period_remaining;
                }

                // conditions for tracking mov't of departures
                if ($type == "departures" && strtotime($val['GroundRequestSchedule']['dept_atd']) != false) {
                    $class = "completed";
                    $period_remaining = "Aircraft Departed";
                } else if ($type == "departures" && strtotime($val['GroundRequestSchedule']['arr_ata']) != false) {
                    $class = "operational";
                    $period_remaining = "Aircraft Arrived. " . $period_remaining;
                }

                if ($val['GroundRequestSchedule']['status'] == 'completed') {
                    $class = "completed";
                    $period_remaining = "Flight Handling Completed";
                }
                ?>
                <tr class="dialog_opener" data-height="550" title="Flight Schedule Details" style="margin: 0px;" data-url="<?php echo $html->url(array('controller' => 'ground', 'action' => 'schedule_summary', $val['GroundRequestSchedule']['id'])); ?>">
                    <td width="20" valign="middle"><a style="margin: 0px;"><span class="status <?php echo $class; ?>" title="<?php echo $period_remaining; ?>"></span></a></td>
                    <td width="15" align="right"><?php echo $count++ . "."; ?></td>
                    <td class="ref_no"><b><?php echo $val['GroundRequestSchedule']['ref_no']; ?></b><br /> <?php echo ($type == "departures" ? $val['GroundRequestSchedule']['dept_origin'] : $val['GroundRequestSchedule']['arr_dest']) ?></td>
                    <td><?php echo $val['GroundRequest']['operator'] ?></td>
                    <td><?php echo ($type == "departures" ? $val['GroundRequestSchedule']['dept_flight_num'] : $val['GroundRequestSchedule']['arr_flight_num']); ?></td>
                    <td><?php echo $val['GroundRequest']['region'] ?></td>
                    <td><?php echo $val['GroundRequest']['type'] ?></td>
                    <td><?php echo $val['GroundRequest']['company_name'] ?></td>
                    <td><?php echo $type == "departures" ? $val['GroundRequestSchedule']['dept_dest'] : $val['GroundRequestSchedule']['arr_origin']; ?></td>
                    <td><?php echo $type == "departures" ? $dateFormat->formatZulu($val['GroundRequestSchedule']['dept_etd']) : $dateFormat->formatZulu($val['GroundRequestSchedule']['arr_eta']); ?></td>
                    <td><?php echo $type == "departures" ? $dateFormat->formatZulu($val['GroundRequestSchedule']['dept_atd']) : $dateFormat->formatZulu($val['GroundRequestSchedule']['arr_ata']); ?></td>
                    <!--<td>
                        <a class="dialog_opener" data-height="550" title="Flight Schedule Details" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'schedule_summary', $val['GroundRequestSchedule']['id'])); ?>">Details</a>
                    </td>-->
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div> 