

<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php
echo $html->css('app/jobsheet.css');
echo $html->script('app/ground.js');
$service_types = explode(",", $jobSheet[0]['JobSheet']['service_type']);
?>


<h2><?php __('Edit JobSheet'); ?></h2>
<form  method="POST" id="process_form">
    <fieldset>
        <div class="input">
            <label for="role_name"> CARD NAME & NUMBER</label>
            <input type="text" class="required" required name="data[JobSheet][card_number]" id="data[JobSheet][card_number]" value="<?php echo $jobSheet[0]['JobSheet']['card_number']; ?>" >
            <label for="role_name">HOLDERS NAME</label>
            <input type="text" class="required" required name="data[JobSheet][holder_name]" id="data[JobSheet][holder_name]" value="<?php echo $jobSheet[0]['JobSheet']['holder_name']; ?>">

        </div>

        <div class="input">
            <label for="role_name">CARD EXPIRY DATE</label>
            <input type="text" class="required hasDatePicker" required name="data[JobSheet][card_expiry_date]" id="data[JobSheet][card_expiry_date]" value="<?php echo $jobSheet[0]['JobSheet']['card_expiry_date']; ?>" >

            <label for="role_name">LOCATION</label>
            <input type="text" class="required" required name="data[JobSheet][location]" id="data[JobSheet][location]" value="<?php echo $jobSheet[0]['JobSheet']['location']; ?>" >

        </div>
        <div class="input">
            <label for="role_name">JOB No.</label>
            <input type="text" class="required" required name="data[JobSheet][job_number]" id="data[JobSheet][job_number]" value="<?php echo $jobSheet[0]['JobSheet']['job_number']; ?>" >
            <label for="role_name">PO No.</label>
            <input type="text" class="required" required name="data[JobSheet][address]" id="data[JobSheet][address]" value="<?php echo $jobSheet[0]['JobSheet']['address']; ?>">
        </div>
        <div class="input">
            <!--has to come from the db-->
            <label for="role_name">HANDLER</label>
            <select class="required" required name="data[JobSheet][handler_id]" id="data[JobSheet][handler_id]">
                <?php foreach ($handlers as $val) { ?>
                    <option value="<?php echo $val['Handler']['id'] ?>"
                    <?php
                    if (($jobSheet[0]['JobSheet']['handler_id']) == $val['Handler']['id']) {
                        echo htmlspecialchars(" selected=selected ");
                    }
                    ?>
                            ><?php echo $val['Handler']['name'] ?></option>
                        <?php } ?>
            </select>
        </div>
        <div class="input">
            <label for="role_name">SERVICE TYPE</label>
            <!--has to come from the db-->
            <input type="checkbox" name="data[JobSheet][service_type][]" id="data[JobSheet][service_type]" value="1" <?php
                        if (in_array(1, $service_types)) {
                            echo htmlspecialchars(" checked=checked ");
                        }
                        ?>/>T/S
            <input type="checkbox" name="data[JobSheet][service_type][]" id="data[JobSheet][service_type]" value="2"  <?php
                   if (in_array(2, $service_types)) {
                       echo htmlspecialchars(" checked=checked ");
                   }
                        ?> />PAX
            <input type="checkbox" name="data[JobSheet][service_type][]" id="data[JobSheet][service_type]" value="3"
            <?php
            if (in_array(3, $service_types)) {
                echo htmlspecialchars(" checked=checked ");
            }
            ?>      />CGO
            <input type="checkbox" name="data[JobSheet][service_type][]" id="data[JobSheet][service_type]" value="4"
            <?php
            if (in_array(4, $service_types)) {
                echo htmlspecialchars(" checked=checked ");
            }
            ?>   />AMB
            <input type="checkbox" name="data[JobSheet][service_type][]" id="data[JobSheet][service_type]" value="5"
            <?php
            if (in_array(5, $service_types)) {
                echo htmlspecialchars(" checked=checked ");
            }
            ?>
                   />VIP
            <input type="checkbox" name="data[JobSheet][service_type][]" id="data[JobSheet][service_type]" value="6"
            <?php
            if (in_array(6, $service_types)) {
                echo htmlspecialchars(" checked=checked ");
            }
            ?>
                   />FERRY
        </div>
        <div class="input">
            <label for="role_name">OPERATOR</label>
            <input type="text" class="required" required name="data[JobSheet][operator]" id="data[JobSheet][operator]" value="<?php echo $jobSheet[0]['JobSheet']['operator'] ?>" >
            <label for="role_name">COMPANY TO INVOICE</label>
            <!--has to come from the db-->
            <input type="text" class="required" required name="data[JobSheet][company_name]" id="data[JobSheet][company_name]" value="<?php echo $jobSheet[0]['JobSheet']['company_name'] ?>">
        </div>
        <div class="input">
            <label for="role_name">Arrival Date</label>
            <input type="text" class="required" required name="data[JobSheet][arrival_dt]" id="data[JobSheet][arrival_dt]" value="<?php echo $jobSheet[0]['JobSheet']['arrival_dt'] ?>">
            <label for="role_name">Origin (ICAO/IATA)</label>
            <input type="text" class="required" required name="data[JobSheet][flight_origin]" id="data[JobSheet][flight_origin]" value="<?php echo $jobSheet[0]['JobSheet']['flight_origin'] ?>">

        </div>
        <div class="input">
            <label for="role_name">Departure Date</label>
            <input type="text" class="required" required name="data[JobSheet][departure_dt]" id="data[JobSheet][departure_dt]" value="<?php echo $jobSheet[0]['JobSheet']['departure_dt'] ?>">
            <label for="role_name">Origin (ICAO/IATA)</label>
            <input type="text" class="required" required name="data[JobSheet][flight_dest]" id="data[JobSheet][flight_dest]" value="<?php echo $jobSheet[0]['JobSheet']['flight_dest'] ?>">

        </div>
        <div class="input">
            <label for="role_name">FLIGHT No</label>
            <input type="text" class="required" required name="data[JobSheet][flight_no]" id="data[JobSheet][flight_no]" value="<?php echo $jobSheet[0]['JobSheet']['flight_no'] ?>">
            <label for="role_name">Aircraft Type & Registration</label>
            <input type="text" class="small_field required" required name="data[JobSheet][aircraft_registration]" id="data[JobSheet][aircraft_registration]" value="<?php echo $jobSheet[0]['JobSheet']['aircraft_registration'] ?>">

        </div>


        <hr/>
        <h2>TECHNICAL / SERVICE  CHECK LIST (PLS TICK WHERE APPLICABLE )</h2>
        <div>
            <label for="role_name" class="services_text">SERVICES</label>
            <label for="role_name" class="services_text">REQUESTED</label>

        </div>
        <br>
        <!--has to come from the db-->
        <?php foreach ($user as $service) { ?>
            <div class="input">
                <label for="role_name" class=""><?php echo $service['Service']['name']; ?> </label>
                <input type="checkbox"  name="data[JobSheet][req_serivice_no][]" id="data[JobSheet][req_serivice_no]" value="<?php echo $service['Service']['id']; ?>"
                <?php
                if (in_array($service['Service']['id'], $jobsheets_jobs_services)) {
                    echo htmlspecialchars(" checked=checked ");
                }
                ?>
                       />
            </div>

        <?php } ?>
    </fieldset>
    <input type="reset" id="reset_button_btn" />
    <input type="submit" id="process_button_btn" />
    <input type="hidden" id="data[JobSheet][id]" name="data[JobSheet][id]" value="<?php echo $jobSheet[0]['JobSheet']['id'] ?>" />

</form>
<input type="hidden" id="member_game_status_update_url" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_editjob')); ?>" />
<input type="hidden" id="update_redir" value="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_index')); ?>" />
