<table border="0" cellspacing="5" width="100%">
    <?php foreach($attachments as $attachment) { ?>
    <tr>
        <td>
            <?php echo $attachment['GroundRequestScheduleAttachment']['file_name']; ?>
        </td>
        <td width="100" align="center">
            <a target="_blank" class="preview_link" href="<?php echo $html->url( array('controller' => 'ground', 'action' => 'attachments_view', $attachment['GroundRequestScheduleAttachment']['id'])); ?>">
                <?php echo $html->image('magnifier.png'); ?>
            </a>
            <a class="delete_link" href="<?php echo $html->url( array('controller' => 'ground', 'action' => 'attachments_delete', $attachment['GroundRequestScheduleAttachment']['id'])); ?>">
                <?php echo $html->image('cross.png'); ?>
            </a>
        </td>
    </tr>
    <?php } ?>
</table>