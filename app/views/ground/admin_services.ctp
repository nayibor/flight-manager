<?php
echo $html->css('app/columns.css');
echo $html->css('app/jobsheet.css');
?>

<style type="text/css">


    #request_details h3 {
        font-size: 12px;
        font-weight: bold;
        border-bottom: dotted #ccc 1px;
        padding-bottom: 3px;
        color: #222;
    }

    #request_details .schedule {
        padding: 4px;
        margin: 3px 0px;
        border-radius: 4px;
        /*width: 560px;*/
    }

    #request_details .schedule.expanded {
        border: solid #ddd 1px;
        border-radius: 5px;
    }

    #request_details .schedule h4.divider {
        font-size: 11px;
        font-family: "Tahoma", sans-serif;
        color: #333;
        position: relative;
    }

    #request_details .schedule h4.divider .expander {
        background-image: url(../../img/expand_3.png);
        background-position: top;
        background-repeat: no-repeat;
        padding: 9px;
        margin-right: 5px;
        text-decoration: none;
        display: inline-block;
        vertical-align: top;
    }

    #request_details .schedule h4.divider .expander.collapsed {
        background-image: url(../../img/expand_3.png);
    }

    #request_details .schedule h4.divider .expander.expanded {
        background-image: url(../../img/collapse_3.png);
    }

    #request_details .schedule h4.divider .dates {
        display: inline-block;
        vertical-align: top;
        width: 380px;
        line-height: 16px;
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 11px;
        text-transform: uppercase;
    }

    #request_details .schedule h4.divider .options {
        width: 140px;
        top: 0px;
        right: 0px;
        display: block;
        position: absolute;
        text-align: right;
        padding: 3px;
    }

    #request_details .schedule h4.divider .options a {
        color: #069;
        margin-right: 10px;
        text-decoration: none;
    }

    #request_details .schedule .services {
        padding: 10px;
        display: none;
    }

    #request_details .schedule .services td {
        font-size: 11px;
        padding: 5px 10px;
    }

    #request_details .schedule .services td ul {
        list-style-type: square;
        padding-left: 15px;
        margin-bottom: 15px;
    }

    #request_details .schedule .services td ul li {
        padding: 3px;
    }
    
    #request_details #attachment_link_list a {
        text-decoration: none;
        color: #069;
    }
    
    #request_details #attachment_link_list a:hover {
        text-decoration: none;
        color: #096;
    }

    #request_details #summary_table td {
        padding: 6px;
        font-size: 11px;
    }

    #request_details #summary_table td:first-child {
        font-weight: bold;
        width: 30%;
    }

    #request_details .menu_button {
        padding-right: 25px;
        background-image: url('../../css/core/menu_down.png');
        background-position: 95%;
        background-repeat: no-repeat;
        text-decoration: none;
        font-size: 11px;
        border: none;
        background-color: transparent;
        display: inline-block;
    }

    #request_details .menu_button .menu {
        position: absolute;
        border: solid #eee 1px;
        top: 27px;
        left: -100px;
        background-color: #f9f9f9;
        display: none;
        box-shadow: 1px 1px 3px #ccc;
    }

    #request_details .menu_button .menu li {
        padding: 5px 10px;
        min-width: 130px;
        max-width: 180px;
        width: auto;
        text-align: left;
        font-size: 11px;
    }

    #request_details .menu_button .menu li:hover {
        background-color: #fcfcfc;
    }

    #request_details .menu_button .menu li:not(:last-child) {
        border-bottom: solid #eee 1px;
    }
</style>



<div id="request_details">

    <?php echo $this->element('ground/request_summary', $groundRequest); ?>

    <h3>
        Schedules 
        <?php if ( $groundRequest['GroundRequest']['jobsheet_status'] == "open" ) { ?>
            (<a id="schedule_add_new" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_schedule_edit', $ground_request_id)); ?>" class="flyout_opener" style="color: #069; text-decoration: none;">Add New</a>)
        <?php } ?>
    </h3>

    <?php
    foreach( $schedules as $schedule ) {
        echo $this->element('ground/schedule_summary', array('schedule' => $schedule));
    }
    ?>
</div>


<script>
    $(".del").click(function(event){
        event.preventDefault();
        var _this=this;

        if(confirm("Are You Sure You Want To Delete This Schedule?")){
            $.ajax({
                url:$(this).attr("del_url"),
                dataType: 'json',
                success: function(data) {
                    $(_this).closest("div.schedule").remove();
                },
                error: function(xhr) {
                    //   alert(xhr.responseText);
                }
            });
        }
    });
</script>
