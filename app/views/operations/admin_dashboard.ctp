<!--<h1>Operations Control</h1> -->
<?php echo $html->css('app/columns.css'); ?>

<style type="text/css">
    .top-nav {
        list-style: none;
        margin-bottom: 10px;
    }

    .top-nav li {
        display: inline-block;
        border: solid #ddd 1px;
        border-radius: 5px;
        background: -moz-linear-gradient(top, rgba(245,245,220,1) 0%, rgba(255,255,255,0) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(245,245,220,1)), color-stop(100%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* IE10+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f5dc', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
        background: linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* W3C */
    }

    .top-nav li a {
        text-decoration: none;
        color: #444;
        font-size: 11px;
        line-height: 25px;
        display: block;
        padding: 0px 10px;
    }

    .ui-datepicker-trigger {
        vertical-align: middle;
        width: 24px;
        height: 24px;
    }
    
    .dash_header {
        padding: 8px;
        border: solid #ddd 1px;
        border-radius: 3px;
        background-color: #f9f9f9;
        margin-bottom: 10px;
        position: relative;
    }
    
    .dash_header h2 {
        margin: 0px;
    }
    
    .dash_header span {
        position: absolute;
        right: 10px;
        top: 6px;
    }
    
    .dash_header span input[type=text] {
        font-size: 11px;
    }
    
    .dash_keys {
        background-color: #f9f9f9;
        border: solid 1px #eee;
        margin: 5px 0px;
        padding: 5px;
    }
    
    .dash_keys li {
        display: inline-block;
        margin-right: 5px;
        padding: 5px;
        font-size: 11px;
    }

    
    #gr_table td {
        font-size: 11px;
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        text-transform: uppercase;
    }

    #gr_table tbody tr:hover {
        background-color: #ececec;
    }

    .dash_keys .status {
        padding: 10px;
        display: block;
        border-radius: 15px;
        background-color: #eee;
        box-shadow: 1px 1px 2px #ccc;
    }
    
    .dash_keys .status.small {
        padding: 7px;
        display: inline-block !important;
        border-radius: none;
        vertical-align: middle;
        margin-right: 5px;
    }

    .dash_keys .status.critical {
        background-color: #900;
    }
    
    .dash_keys .status.emergency {
        background-color: #F00;
    }

    .dash_keys .status.warning {
        background-color: #ffcc00;
    }
    
    .dash_keys .status.operational {
        background-color: #1c94c4;
    }
    
    .dash_keys .status.completed {
        background-color: green;
    }

    .pane-footer {
        margin-bottom: 10px;
    }
</style>

<div class="dash_header">
    <h2>Operations Control Dashboard</h2>
    
    <span>
        Filter By: <input type="text" placeholder="SBOC001-12" id="filter_field" />
    </span>
</div>

<div class="dash_keys">
    <ul>
        <li><b>Legend: </b></li>
        <li><span class="status small"></span> Job Pending</li>
        <li><span class="status small warning"></span> < 10 Hours To Flight</li>
        <li><span class="status small emergency"></span> < 2 Hours To Flight</li>
        <li><span class="status small completed"></span> Completed / Obtained</li>
        <li><span class="status small critical"></span> Overdue / Canceled</li>
    </ul>
</div>

<div id="tabs">
    <ul>
        <li><a class="lk_open"  title="open_perms" href="<?php echo $html->url(array('controller' => 'permits', 'action' => 'admin_dashboard_list', 'open')); ?>">Open</a></li>
        <li><a class="lk_open"  title="closed_perms" href="<?php echo $html->url(array('controller' => 'permits', 'action' => 'admin_dashboard_list', 'closed')); ?>">Completed</a></li>
        <li><a class="lk_open"  title="closed_perms" href="<?php echo $html->url(array('controller' => 'permits', 'action' => 'admin_dashboard_list', 'canceled')); ?>">Canceled</a></li>
    </ul>
</div>

<!--<div id="dashboard_list" style="padding: 10px 0px;"></div>-->

<input type="hidden" id="dashboard_list_url" value="<?php echo $html->url( array('controller' => 'permits', 'action' => 'dashboard_list', '?status=open') ); ?>" />
<?php echo $html->script('app/ops/dashboard.js'); ?>