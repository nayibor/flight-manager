<!--<h1>Operations Control</h1> -->
<?php echo $html->css('app/columns.css'); ?>

<style type="text/css">
    .top-nav {
        list-style: none;
        margin-bottom: 10px;
    }

    .top-nav li {
        display: inline-block;
        border: solid #ddd 1px;
        border-radius: 5px;
        background: -moz-linear-gradient(top, rgba(245,245,220,1) 0%, rgba(255,255,255,0) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(245,245,220,1)), color-stop(100%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* IE10+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f5dc', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */
        background: linear-gradient(top, rgba(245,245,220,1) 0%,rgba(255,255,255,0) 100%); /* W3C */
    }

    .top-nav li a {
        text-decoration: none;
        color: #444;
        font-size: 11px;
        line-height: 25px;
        display: block;
        padding: 0px 10px;
    }

    .ui-datepicker-trigger {
        vertical-align: middle;
        width: 24px;
        height: 24px;
    }

</style>

<div class="columns">

    <div class="column" id="users_column">
        <div class="header">Operations Control</div>
        <div class="subheader">
            <ul style="display: inline-block;">
                <li class="button">
                    <a id="btn_new_permit" title="New Permit Request" href="<?php echo $html->url(array('controller' => 'permits', 'action' => 'add')); ?>">New Permit Request</a>
                </li>
                <!--<li id="selected" class="menu_button" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'changeStatus')); ?>">
                    <a href="#">With Selected</a>

                    <ul class="menu">
                        <li data-status="open">Open Requests</li>
                        <li data-status="canceled">Cancel Requests</li>
                        <li data-status="closed">Close Requests</li>
                    </ul>
                </li>-->
            </ul>


            <div style="display: inline-block; margin-left: 40px;">
                Find Permit: <input class="search" type="text" placeholder="SBOC001-11" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'list', '?status=open')); ?>"  /> 
                By: <select id="search_term">
                    <option value=""></option>
                    <option value="Permit.ref_no" selected data-placeholder="SBOCXXX-XX">SBOC Ref. No.</option>
                    <option value="Permit.permit_number" data-placeholder=".e.g 011011">Permit No.</option>
                    <option value="Permit.operator_name" data-placeholder="Operator">Operator</option>
                    <option value="Permit.aircraft_regn" data-placeholder="Aircraft Regn">Aircraft Regn.</option>
                    <option value="Permit.aircraft_type" data-placeholder="Aircraft Type">Aircraft Type</option>
                    <option value="Permit.permit_location" data-placeholder="e.g. DNNA">Location</option>
                    <option value="Permit.status" data-placeholder="e.g. open">Status</option>
                </select>

            </div>

        </div>
        <div class="content">

            <div class="master_block" style="width: 55%;">

                <div id="tabs">
                    <ul>
                        <li><a title="open_perms" href="<?php echo $html->url(array('controller' => 'permits', 'action' => 'list', '?status=open')); ?>">Open Permits</a></li>
                        <li><a title="closed_perms" href="<?php echo $html->url(array('controller' => 'permits', 'action' => 'list', '?status=closed')); ?>">Closed Permits</a></li>
                        <li><a title="canceled_perms" href="<?php echo $html->url(array('controller' => 'permits', 'action' => 'list', '?status=cancelled')); ?>">Cancelled Permits</a></li>
                    </ul>
                </div>
            </div>

            <div class="details_block" style="width: 45%;">
                <!--<h3>Permit Details</h3>-->
                <div class="details_options">
                    <ul class="buttons">
                        <li id="edit-permit" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'add')); ?>">Edit</li>
                        <li id="delete-permit" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'delete')); ?>">Delete</li>

                        <li id="actions" class="menu_button activatable" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'setStatus')); ?>">
                            <span>Actions &nbsp;</span>
                            <ul class="menu" style="min-width: 170px;">
                                <li id="send-to-caa" data-type="html" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'requestForm')); ?>">Send To CAA</li>
                                <li id="enter-permit" data-type="html" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'updatePermitInfo')); ?>" title="Enter Permit Info">Enter/Change Permit Number</li>
                                <li id="gen-forwarding-form" data-type="html" data-status="approved" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'forwardingForm')); ?>">Forwarding Form</li>
                                <li class="divider" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                                <li id="attach_original_request" data-type="html" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'addAttachment')); ?>" >Add/View Request Documents</li>
                                <li class="divider" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                                <li id="hold-request" data-type="html" data-status="on hold" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'changeStatus')); ?>" >Hold Request</li>
                                <li id="cancel-request" data-type="html" data-status="canceled" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'changeStatus')); ?>">Cancel Request</li>
                                <li id="close-request" data-type="html" data-status="closed" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'changeStatus')); ?>">Close Request</li>
                                <li class="divider closed_only" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                                <li class="closed_only" id="reinstate-request" data-type="html" data-status="open" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'changeStatus')); ?>">Reinstate Request</li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="details_pane">

                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="authority_details_url" value="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'contact_details')); ?>" />

<?php echo $html->script('app/attachments.js'); ?>
<?php echo $html->script('app/ops/permits.js'); ?>