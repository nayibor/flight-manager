<style>
    .change_form td {
        padding: 5px;
    }
</style>

<form name="change_form" class="change_form" action="<?php echo $html->url(array('controller' => 'permits', 'action' => 'changeStatus')); ?>">

    <fieldset>
        <table>
            <tr>
                <td><b>Request Ref. No.:</b></td>
                <td><?php echo $permit['Permit']['ref_no']; ?></td>
            </tr>
            <tr>
                <td><b>New Status</b></td>
                <td>
                    <?php
                    $statuses = array('open' => 'Open', 'on hold' => 'On Hold', 'canceled' => 'Canceled', 'closed' => 'Closed', 'deleted' => "Deleted")
                    ?>
                    <select name="status" required>
                        <option value="">Select Status</option>
                        <?php
                        foreach ($statuses as $key => $value) {
                            $selected = $key == $status ? "selected" : "";
                            echo "<option value='$key' $selected>$value</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td valign="top"><b>Comments:</b></td>
                <td><textarea name="comments" style="width: 350px; height: 120px;" placeholder="Reason For Status Change" required></textarea></td>
            </tr>
        </table>
    </fieldset>
    
    <div style="padding: 10px;">
        <input type="submit" class="button" value="Change Status" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
        <input type="hidden" name="data[Permit][id]" value="<?php echo $permit['Permit']['id']; ?>" />
    </div>
</form>
