<style>
    .data_row {
        padding: 5px 0px;
    }
</style>

<div class="details" style="font-family: tahoma; font-size: 11px; line-height: 20px;">

    <?php
    //$statuses = array('on hold', 'canceled', 'closed');
    //if (in_array($permit['Permit']['status'], $statuses)) {
        ?>
        <div class="data_row">
            <b>Request Status</b>
            <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
                <tr>
                    <td width="110">Status: </td>
                    <td><?php echo strtoupper($permit['Permit']['status']); ?></td>
                </tr>
                <tr>
                    <td>Comments:</td>
                    <td><?php echo isset($permit['PermitStatus'][0]) ? $permit['PermitStatus'][0]['comments'] : ""; ?></td>
                </tr>
                <?php if( $permit['Permit']['handler_id'] != "" ) { ?>
                <tr>
                    <td>Used Agent:</td>
                    <td><?php echo $permit['Handler']['name']; ?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    <?php //} ?>

    <div class="data_row">
        <b>Permit Information</b> 
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td width="110">SB Ref No.</td>
                <td colspan="3"><?php echo $permit['Permit']['ref_no']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Permit Type: </td>
                <td width="180" style="text-transform: uppercase;"><?php echo $permit['Permit']['type']; ?></td>
                
                <td width="110" valign="top">Permit Number: </td>
                <td nowrap><?php echo $permit['Permit']['permit_number']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Permit Location: </td>
                <td><?php echo $permit['Permit']['permit_location']; ?></td>
                
                <td width="110" valign="top">Permit Validity: </td>
                <td><?php echo $permit['Permit']['permit_validity']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Permit Ref. No: </td>
                <td><?php echo $permit['Permit']['permit_ref_no']; ?></td>
                
                <td width="110" valign="top">Received On: </td>
                <td><?php echo strstr($permit['Permit']['permit_receive_caa_date'], '0000') == false ? date('d/m/y H:i e', strtotime($permit['Permit']['permit_receive_caa_date'])) : ""; ?></td>
            </tr>
        </table>
    </div>

    <div class="data_row">
        <b>Operator Information</b> 
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td width="110">Name:</td>
                <td><?php echo $permit['Permit']['operator_name']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Address:</td>
                <td><?php echo $permit['Permit']['operator_addr']; ?></td>
            </tr>
        </table>
    </div>

    <div class="data_row">
        <b>Flight Information</b> 
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td width="110" valign="top">Flight Schedule:</td>
                <td><?php echo $this->element('permits/schedule_format', array('permit' => $permit)); ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Flight Purpose:</td>
                <td><?php echo str_replace("\n", "<br / >", $permit['Permit']['flight_purpose']); ?></td>
            </tr>
            <tr>
                <td>Routes:</td>
                <td><?php echo $permit['Permit']['flight_routes']; ?></td>
            </tr>
        </table>
    </div>

    <div class="data_row">
        <b>Aircraft Information: </b>
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td width="110">Type:</td>
                <td width="180"><?php echo $permit['Permit']['aircraft_type']; ?></td>
                
                <td width="110">Registration:</td>
                <td><?php echo $permit['Permit']['aircraft_regn']; ?></td>
            </tr>
            <tr>
                <td>Base of Operation</td>
                <td><?php echo $permit['Permit']['aircraft_base']; ?></td>
                
                <td>Call Sign / Flgt. No.:</td>
                <td><?php echo $permit['PermitSchedule'][0]['arr_flight_no']  ?></td>
            </tr>
            <tr>
                <td>Flight Captain:</td>
                <td><?php echo $permit['Permit']['captain_name'] . ", " . $permit['Permit']['captain_nationality'] ; ?></td>
                
                <td>Cargo Type:</td>
                <td><?php echo $permit['Permit']['cargo_type']; ?></td>
            </tr>
            <tr>
                <td>Crew On Board:</td>
                <td><?php echo $permit['Permit']['crew_on_board']; ?></td>
                <td>Souls On Board:</td>
                <td><?php echo $permit['Permit']['souls_on_board']; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="data_row">
        <b>Other Information: </b>
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td>Receiving Party:</td>
                <td><?php echo $permit['Permit']['receiving_party']; ?></td>
            </tr>
            <tr>
                <td width="110">Miscellaneous:</td>
                <td><?php echo $permit['Permit']['misc_info']; ?></td>
            </tr>
        </table>
    </div>
</div>
