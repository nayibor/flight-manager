<div class="form">
    <?php echo $this->Form->create(); ?>
    <fieldset>
        <legend>Enter Permit Number From CAA</legend>

        <div class="input">
            <label>SBMAN Ref No: </label>
            <strong><?php echo $permit['Permit']['ref_no']; ?></strong>
        </div>

        <div class="input">
            <label>Permit Type: </label>
            <strong style="text-transform: uppercase;"><?php echo $permit['Permit']['type']; ?></strong>
        </div>

        <div class="input">
            <label>Permit Location: </label>
            <strong><?php echo $permit['Permit']['permit_location']; ?></strong>
        </div>

        <div class="input">
            <label>CAA File Ref. No</label>
            <input type="text" name="data[Permit][permit_ref_no]" required value="<?php echo $permit['Permit']['permit_ref_no']; ?>" />  
        </div>

        <div class="input">
            <label>Permit Number</label>
            <input type="text" name="data[Permit][permit_number]" required class="required" value="<?php echo $permit['Permit']['permit_number']; ?>" />  
        </div>

        <div class="input">
            <label>Permit Validity</label>
            <input type="text" name="data[Permit][permit_validity]" value="<?php echo $permit['Permit']['permit_validity']; ?>" />  
        </div>

        <div class="input">
            <label>Received On</label>
            <input type="text" name="data[Permit][permit_receive_caa_date]" class="datetime" value="<?php echo strtotime($permit['Permit']['permit_receive_caa_date']) === false ? $permit['Permit']['permit_receive_caa_date'] : date('Y-m-d'); ?>" />  
        </div>
    </fieldset>

    <div>
        <input type="submit" class="button" id="submit_btn" value="Update Permit" />
        <input type="button" class="button" id="cancel-form" value="Cancel" />
        <input type="hidden" name="data[Permit][id]" value="<?php echo $permit['Permit']['id']; ?>" />
        
        <?php if( $permit['Permit']['permit_number'] != "" ) { ?>
            <input type="button" class="button" id="clear-info" value="Clear Permit Info" data-url="<?php echo $this->Html->url(array('action' => 'admin_clearPermitInfo', $permit['Permit']['id'])); ?>" style="margin-left: 90px;" />
        <?php } ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>