
<div>
    <table width="100%" border="0" cellspacing="0" cellpadding="5" id="permits_table" class="fullwidth <?php echo $status; ?>">
        <thead>
            <tr>
                <th width="5%"><input type="checkbox" class="check_all" /></th>
                <th colspan="2">REF No.</th>
                <th nowrap>PERMIT No.</th>
                <th>TYPE</th>
                <th>LOCATION</th>
                <th>STATUS</th>
            </tr>
        </thead>

        <tbody>
            <?php 
            $count = $this->Paginator->counter('%start%');  // passed from controller
            foreach ($permits as $permit) { ?>
                <tr data-id="<?php echo $permit['Permit']['id']; ?>" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'details', $permit['Permit']['id'])); ?>">
                    <td><input type="checkbox" value="<?php echo $permit['Permit']['id']; ?>" /></td>
                    <td align="right"><span class="numbers"><?php echo $count++ . "."; ?></span> </td>
                    <td nowrap><?php echo $permit['Permit']['ref_no']; ?></td>
                    <td class="permit-number" nowrap><?php echo $permit['Permit']['permit_number']; ?></td>
                    <td style="text-transform: uppercase;"><?php echo $permit['Permit']['type']; ?></td>
                    <td><?php echo $permit['Permit']['permit_location']; ?></td>
                    <td class="status"><?php echo $permit['Permit']['status']; ?></td>

                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<?php echo $this->element('layout/pagination_footer', array('ajax' => true, 'url_params' => array('status' => $status))); ?>
