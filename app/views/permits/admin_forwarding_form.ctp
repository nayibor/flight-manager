
<div class="flyout_content">
    <section>
        <header>
            <h3>Preview For Forwarding Form</h3>    
        </header>
    </section>


    <section>
        <?php echo $this->element('permits/forwarding_form_content', $permit); ?>
    </section>

    <footer>
        <div class="input">
            <input type="button" class="button" id="print-form" value="Print" data-url="<?php echo $html->url( array('controller' => 'permits', 'action' => 'printForwardingForm', $permit['Permit']['id'])); ?>" />
            <input type="button" class="button" id="download-form" value="Download" data-url="<?php echo $html->url( array('controller' => 'permits', 'action' => 'printForwardingForm', $permit['Permit']['id'], 'download')); ?>" />
            <input type="button" class="button" id="send-form" value="Email" class="dialog_opener" data-url="<?php echo $html->url( array('controller' => 'permits', 'action' => 'sendForwardingForm', $permit['Permit']['id'])); ?>" />
            <input type="button" class="button" id="cancel-form" value="Cancel" />
        </div> 
    </footer>
</div>
