<style type="text/css">
    .flyout .content {
        padding: 0px;
    }

    #permit-add-form textarea {
        width: auto !important;
        height: auto !important;
    }

    #permit-add-form td {
        padding: 6px !important;
        color: #222;
    }

    .date_input {
        padding: 3px;
    }

    .date_input input[type=text] {
        margin-right: 5px;
    }

    .date_input input.location,
    .date_input input.cargo {
        min-width: 45px !important;
        width: 45px;
        font-size: 11px;
    }

    .date_input input.date {
        min-width: 110px !important;
        font-size: 11px;
    }

    #schedule_blocks {

    }

    .schedule_block {
        position: relative;
        background-color: #f9f9f9;
        border: solid #ddd 1px;
        padding: 3px;
        margin-bottom: 4px;
    }

    .schedule_block .block_del_btn {
        padding: 2px 4px;
        position: absolute;
        right: -5px;
        top: 5px;
    }
</style>

<form id="permit-add-form" name="form1" method="post" action="<?php echo $html->url(array('controller' => 'permits', 'action' => 'add')); ?>">

    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td colspan="4">
                <h3><b>Company Requesting</b></h3>
            </td>
        </tr>
        <tr>
            <td>
                Company Name:
            </td>
            <td>
                <input name="data[Permit][company_name]" type="text" value="<?php echo isset($permit) ? $permit['Permit']['company_name'] : ""; ?>" />
            </td>
            <td>
                For Attn Of:
            </td>
            <td>
                <input name="data[Permit][company_contact]" type="text" value="<?php echo isset($permit) ? $permit['Permit']['company_contact'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Telephone:
            </td>
            <td>
                <input name="data[Permit][company_tel]" type="text"  value="<?php echo isset($permit) ? $permit['Permit']['company_tel'] : ""; ?>"/>
            </td>
            <td>
                Email:
            </td>
            <td>
                <input name="data[Permit][company_email]" type="email" value="<?php echo isset($permit) ? $permit['Permit']['company_email'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Purchase Order No.:
            </td>
            <td colspan="3">
                <input name="data[Permit][company_po_no]" type="text" value="<?php echo isset($permit) ? $permit['Permit']['company_po_no'] : ""; ?>" />
            </td>
        </tr>

        <tr>
            <td colspan="4" style="padding-top: 20px !important;">
                <h3><b>Requested Permit Details</b></h3>
            </td>
        </tr>

        <tr>
            <td>Date Of Application</td>
            <td><input type="text" name="data[Permit][application_dt]" class="datetime" size="16" value="<?php echo isset($permit) ? $permit['Permit']['application_dt'] : ""; ?>" /></td>
        </tr>

        <tr>
            <td colspan="1">
                Permit Type:
            </td>
            <td>
                <select name="data[Permit][type]" id="permit_type" required class="required">
                    <option value="">Select Type</option>
                    <?php
                    $options = array('overflight' => 'Overflight', 'landing' => 'Landing', 'tech stop' => 'Tech Stop', 'block overflight' => 'Block Overflight');
                    foreach ($options as $key => $value) {
                        $selected = isset($permit) && $permit['Permit']['type'] == $key ? "selected" : "";
                        echo "<option value='$key' $selected>$value</option>";
                    }
                    ?>
                </select>
            </td>

            <td>
                <span id="perm_loc_label">Location:</span>
            </td>
            <td>
                <input type="text" name="data[Permit][permit_location]" id="permit_location" class="airport required" required="" value="<?php echo isset($permit) ? $permit['Permit']['permit_location'] : ""; ?>" />
            </td>
        </tr>
        <!--<tr>
            <td>Flight Date</td>
            <td nowrap>
                <input type="text" name="data[Permit][flight_dt]" size="15" id="application_dt" class="required" required="" value="<?php echo isset($permit) ? date('Y-m-d', strtotime($permit['Permit']['flight_dt'])) : date('Y-m-d'); ?>" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h3><strong>Operator Information</strong></h3>
            </td>
        </tr>-->
        <tr>
            <td width="21%" required valign="top">Operator Name</td>
            <td valign="top">
                <input type="text" name="data[Permit][operator_name]" id="operator_name" value="<?php echo isset($permit) ? $permit['Permit']['operator_name'] : ""; ?>" />
            </td>
            <td valign="top">Operator Address</td>
            <td colspan="3">
                <textarea name="data[Permit][operator_addr]" id="operator_addr" cols="20" rows="2"><?php echo isset($permit) ? $permit['Permit']['operator_addr'] : ""; ?></textarea>
            </td>
        </tr>

        <tr>
            <td>Aircraft Type:</td>
            <td width="23%">
                <input type="text" name="data[Permit][aircraft_type]" id="aircraft_type" required class="required" value="<?php echo isset($permit) ? $permit['Permit']['aircraft_type'] : ""; ?>" />            </td>
            <td width="19%">Aircraft Regn:</td>
            <td width="37%">
                <input type="text" name="data[Permit][aircraft_regn]" id="aircraft_regn" required class="required" value="<?php echo isset($permit) ? $permit['Permit']['aircraft_regn'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>Aircraft Base</td>
            <td>
                <input type="text" name="data[Permit][aircraft_base]" id="aircraft_base" value="<?php echo isset($permit) ? $permit['Permit']['aircraft_base'] : ""; ?>" />
            </td>
            <td>Flight Route</td>
            <td>
                <input type="text" name="data[Permit][flight_routes]" id="routes" value="<?php echo isset($permit) ? $permit['Permit']['flight_routes'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap>Flight Schedule</td>
            <td colspan="3" style="position: relative;">
                <div style="margin-bottom: 5px;">
                    <button type="button" id="block_add_btn" style="font-size: 11px;">Add Schedule</button>
                </div>

                <div class="ui-state-highlight ui-corner-all" style="padding: 5px; margin-bottom: 10px; font-size: 11px;">ETD/ETA fields not entered would show up as <b>TBA</b> when viewed in other sections</div>

                <div id="schedule_blocks">
                    <?php
                    if( isset($permit) ) {
                        $count = 0;
                        foreach ($permit['PermitSchedule'] as $schedule) {
                            ?>
                            <div class="schedule_block">
                                <div class="date_input">
                                    <input value="<?php echo $schedule['arr_flight_no']; ?>" class="cargo" type="text"  name="data[PermitSchedule][<?php echo $count; ?>][arr_flight_no]" placeholder=" Flt. No" />
                                    <input value="<?php echo $schedule['arr_etd']; ?>" class="date required " type="text" id="arr_etd" name="data[PermitSchedule][<?php echo $count; ?>][arr_etd]" placeholder="ETD" />
                                    <input value="<?php echo $schedule['arr_origin']; ?>" class="location ui-autocomplete-input" type="text" id="arr_origin" name="data[PermitSchedule][<?php echo $count; ?>][arr_origin]" placeholder="Origin" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                                    <input value="<?php echo $schedule['arr_eta']; ?>" class="date required " type="text" id="arr_eta" name="data[PermitSchedule][<?php echo $count; ?>][arr_eta]" placeholder="ETA" required="required" />
                                    <input value="<?php echo $schedule['arr_dest']; ?>" class="location ui-autocomplete-input" type="text" id="arr_dest" name="data[PermitSchedule][<?php echo $count; ?>][arr_dest]" required="required"  placeholder="Dest" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                                </div>

                                <div class="date_input">
                                    <input value="<?php echo $schedule['dept_flight_no']; ?>" class="cargo" type="text"  name="data[PermitSchedule][<?php echo $count; ?>][dept_flight_no]" placeholder="Flt. No" />
                                    <input value="<?php echo $schedule['dept_etd']; ?>" class="date required " type="text" id="dept_etd" name="data[PermitSchedule][<?php echo $count; ?>][dept_etd]" placeholder="ETD" />
                                    <input value="<?php echo $schedule['dept_origin']; ?>" class="location ui-autocomplete-input" type="text" id="dept_origin" name="data[PermitSchedule][<?php echo $count; ?>][dept_origin]" placeholder="Origin" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />
                                    <input value="<?php echo $schedule['dept_eta']; ?>" class="date required " type="text" id="dept_eta" name="data[PermitSchedule][<?php echo $count; ?>][dept_eta]" placeholder="ETA" />
                                    <input value="<?php echo $schedule['dept_dest']; ?>" class="location ui-autocomplete-input" type="text" id="dept_dest" name="data[PermitSchedule][<?php echo $count; ?>][dept_dest]" placeholder="Dest" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" />

                                    <input value="<?php echo $schedule['id']; ?>" type="hidden" name="data[PermitSchedule][<?php echo $count; ?>][id]" />
                                </div>

                                <button type="button" class="block_del_btn" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'delete_schedule', $schedule['id'])); ?>">x</button>
                            </div>
                            <?php
                            $count++;
                        }
                    }
                    ?>
                </div>
            </td>
        </tr>

        <tr>
            <td>Captain's Name</td>
            <td>
                <input type="text" name="data[Permit][captain_name]" id="pilot_name" value="<?php echo isset($permit) ? $permit['Permit']['captain_name'] : ""; ?>" />
            </td>
            <td>Captain's Nationality</td>
            <td>
                <input type="text" name="data[Permit][captain_nationality]" id="captain_nationality" value="<?php echo isset($permit) ? $permit['Permit']['captain_nationality'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>Crew On Board</td>
            <td>
                <input type="number" name="data[Permit][crew_on_board]" id="crew_on_board" min="1" value="<?php echo isset($permit) ? $permit['Permit']['crew_on_board'] : "3"; ?>" />
            </td>
            <td>Souls On Board</td>
            <td>
                <input type="number" name="data[Permit][souls_on_board]" id="souls_on_board" min="1" value="<?php echo isset($permit) ? $permit['Permit']['souls_on_board'] : "3"; ?>" />
            </td>
        </tr>
        <tr id="receiving_party_row">
            <td valign="top">Receiving Party <br />(Contact Info):</td>
            <td colspan="3">
                <textarea name="data[Permit][receiving_party]" cols="60" rows="4"><?php echo isset($permit) ? $permit['Permit']['receiving_party'] : "TBA"; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Purpose of Flight</td>
            <td>
                <input type="text" name="data[Permit][flight_purpose]" id="flight_purpose" value="<?php echo isset($permit) ? $permit['Permit']['flight_purpose'] : ""; ?>" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Type of Cargo</td>
            <td>
                <input type="text" name="data[Permit][cargo_type]" id="cargo_type" value="<?php echo isset($permit) ? $permit['Permit']['cargo_type'] : "NIL"; ?>" />
            </td>
            <td>Type of Ammunition</td>
            <td>
                <input type="text" name="data[Permit][arms_type]" id="arms_type" value="<?php echo isset($permit) ? $permit['Permit']['arms_type'] : "NIL"; ?>" />
            </td>
        </tr>
        <tr>
            <td colspan="4" valign="top">
                <h3><b>Other Information</b></h3>
            </td>
        </tr>
        <tr>
            <td valign="top">Any Other Information</td>
            <td colspan="3">
                <textarea name="data[Permit][misc_info]" id="other_info" cols="60" rows="2"><?php echo isset($permit) ? $permit['Permit']['misc_info'] : "PLEASE ALLOW 72 HRS LEEWAY IN CASE OF ANY TECHNICAL DELAYS"; ?></textarea>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"></td>
        </tr>

        <tr>
            <td colspan="4" align="center">
                <input type="submit" class="button" value="Create Request" />
                <input type="button" class="button" value="Cancel" id="cancel-form" />
                <input type="hidden" name="data[Permit][id]" value="<?php echo isset($permit) ? $permit['Permit']['id'] : ""; ?>" />
            </td>
        </tr>
    </table>
</form>