<style>
    #upload-form-table td {
        padding: 6px;
    }
</style>

<form id="AttachmentForm" method="post" action="<?php echo $this->here; ?>" target="upload-frame" enctype="multipart/form-data" >
    <fieldset>
        <table border="0" cellpadding="5" cellspacing="0" width="94%" align="center" id="upload-form-table">

            <?php if ($permit_attachment) { ?>
                <tr>
                    <td colspan="2">
                        <div class="ui-state-highlight ui-corner-all" style="padding: 5px;">
                            The Original Document for this request is already attached, i.e. 
                            <b><?php echo $permit_attachment['PermitAttachment']['file_name']; ?></b> 
                            (<a href="<?php echo $html->url(array('action' => 'viewOriginalDocument', $permit_attachment['PermitAttachment']['id'])); ?>">View File</a>) <br />

                            Uploading a new file will replace the existing document 
                        </div> <br />

                        <input type="hidden" name="data[PermitAttachment][id]" value="<?php echo $permit_attachment['PermitAttachment']['id']; ?>" />
                    </td>
                </tr>
            <?php } ?>

            <tr>
                <td valign="top" width="100">File Name: </td>
                <td>
                    <input type="file" name="attachments[]" size="50" id="attachments_fs" /> <br /><br />

                    (Multiple Files May Be Selected At Once. Max Total File Upload Size <b id="max_size_value"><?php echo ini_get("upload_max_filesize"); ?></b>)
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td height="50" colspan="2">
                    <div id="file_list" style="padding: 10px;"></div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <input type="submit" class="button" value="Upload & Attach Document" id="up_save_btn" disabled="" />
                    <input type="button" class="button" id="cancel-form" value="Cancel" />
                    <input type="hidden" name="data[PermitAttachment][permit_id]" value="<?php echo $permit_id; ?>" />
                </td>
            </tr>
        </table>    
    </fieldset>
</form>

<iframe width="10" height="10" src="about:home" style="opacity: 0;" name="upload-frame" id="upload-frame" />
