<style>
    .flyout_content .notice {color: #900; font-weight: bold; margin-bottom: 10px;}
</style>

<div class="flyout_content">
    <!--<section>
        <header>
            <h3>Preview For Request Form</h3>    
        </header>
    </section>-->

    <section> 
        <div class="notice" style="font-size: 13px;">Select Aviation Authority To Send Permit To For Approval</div>
        <section style="border-bottom: dotted 1px #ccc; margin-bottom: 10px; padding: 5px;">

            <input type="hidden" id="permit_id" value="<?php echo $permit['Permit']['id']; ?>" />
            <input type="hidden" id="permit_authority_id" value="<?php echo $permit['Permit']['aviation_authority_id']; ?>" />

            <table border="0" cellpadding="4" cellspacing="0" width="100%">
                <tr>
                    <td>Select CAA:</td>
                    <td>
                        <select name="authorities" class="chosen" id="authorities" style="width: 500px;">
                            <option value="">Select Aviation Authority</option>

                            <?php
                            foreach ($authorities as $country => $data) {
                                foreach ($data as $id => $name) {
                                    $selected = $permit['Permit']['aviation_authority_id'] == $id ? "selected" : "";
                                    ?>
                                    <option value="<?php echo $id; ?>" <?php echo $selected; ?>><?php echo $country . " - " . $name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <span style="vertical-align: top; display: inline-block; padding: 8px;">(required)</span>
                    </td>
                </tr>

                <tr>
                    <td>Use Agent:</td>
                    <td>
                        <select name="agents" class="chosen" id="agents" style="width: 500px;">
                            <option value="">Select Handling Agent To Use</option>

                            <?php
                            foreach ($handlers as $country => $data) {
                                foreach ($data as $id => $name) {
                                    $selected = $permit['Permit']['handler_id'] == $id ? "selected" : "";
                                    ?>
                                    <option value="<?php echo $id; ?>" <?php echo $selected; ?>><?php echo $country . " - " . $name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <span style="vertical-align: top; display: inline-block; padding: 8px;">(optional)</span>
                    </td>
                </tr>
            </table>

        </section>


        <?php echo $this->element('permits/request_form_content', $permit); ?>
    </section>

    <footer style="margin: 20px 0px;">
        <div class="input">
            <input class="button" type="button" value="Save Changes" id="save_send_btn" data-url="<?php echo $_SERVER['REQUEST_URI']; ?>" />
            <!--<input type="button" value="Print / Send Form" id="print_form_btn" />-->
            <div id="print_form_btn" data-permit-id="<?php echo $permit['Permit']['id']; ?>" style="display: none; vertical-align: bottom;">
                <!--<input type="button" class="button" id="send-form" data-type="html" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'sendRequestForm', $permit['Permit']['id'])); ?>" title="Send From By Email" value="Send By Mail" />-->
                <input type="button" class="button" id="print-form" data-type="html" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'printRequestForm', $permit['Permit']['id'])); ?>" title="Print Preview" value="Print Form" />
                <input type="button" class="button" id="download-form" data-type="html" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'printRequestForm', $permit['Permit']['id'], 'download')); ?>" title="Download Form" value="Download Form" />
            </div>
            <input class="button" type="button" value="Cancel" id="cancel-form"  />
        </div> 
    </footer>
</div>
