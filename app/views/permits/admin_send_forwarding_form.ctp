<style>
    #mail_form_content .form_table > tbody > tr > td {
        padding: 5px;
    }
</style>

<form action="<?php echo $html->url(array('controller' => 'email', 'action' => 'sendPermitForwardingForm')); ?>" method="post" >
    <div id="mail_form_content">
        <h2>Sending Forwarding Form By Mail</h2>

        <fieldset>
            <?php
            $user = $this->Session->read('user');

            echo $this->element('email_composition_form', array(
                'to' => array($permit['Permit']['company_name'] => $permit['Permit']['company_email']),
                'cc' => array('S. B. Man Flight Ops' => 'fltops@sbmangh.com'),
                'subject' => '', // 'Permission Granted For Your Flight Due ' . date('jS F, Y', strtotime($permit['PermitSchedule']['arr_etd'])),
                'editor_id' => 'ff_editor',
                'message' => "<p>Hi</p>
                <p>Please find attached a permit request for flight due on above date</p>
            
                <p>Best Regards,</p>
                <p>{$user['User']['first_name']} {$user['User']['last_name']} <br />
                Operations Control</p>"
            ));
            ?>

            <table border="0" cellspacing="0" id="">
                <tr>
                    <td>Attachments:</td>
                    <td>
                        <input type="checkbox" value="attach_forwarding_form" name="attach_forwarding_form" id="attach_forwarding_form" checked="" readonly />
                        <label for="request_form">Forwarding Form</label>
                    </td>
                </tr>
            </table>
        </fieldset>

        <div>
            <input type="submit" class="button" value="Send Form" />
            <input type="button" class="button" value="Cancel" id="cancel-form" />
            <input type="hidden" name="permit_id" value="<?php echo $permit['Permit']['id']; ?>" />
        </div>
    </div>
</form>

<script>
    setTimeout(function() {
        try {
            tinyMCE.init({
                mode : "exact",
                theme : "advanced",
                elements: "ff_editor",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
			
                theme_advanced_resizing : true,
			
                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,code", //,image,code
                theme_advanced_buttons2 : ""
            });
        } catch(e) {
            console.log(e);
        } 
    }, 500);
    
</script>