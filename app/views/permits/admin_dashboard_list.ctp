<style>
    .pane-footer {
        font-size: 11px;
        background-color: #f1f1f1;
        padding: 5px;
        border-top: solid #bbb 1px;
    }

    .pane-footer ul {
        display: inline-block;
        overflow: hidden;
        vertical-align: middle;
    }

    .pane-footer li {
        display: inline-block;
        padding: 3px 6px;
        background-color: #ccc;
        margin-right: 2px;
    }

    .pane-footer li:hover {
        background-color: #999;
        color: #fff;
        cursor: pointer;
    }

    #permits_table td {
        font-size: 11px;
        cursor: pointer;
    }

    #permits_table tbody tr:hover {
        background-color: #f4f1cd;
    }

    #permits_table tr td {border-bottom: solid #fff 1px; }
    #permits_table .caa {background-color: #ffcc00; }
    #permits_table .approved {background-color: yellowgreen; }
    #permits_table .canceled {background-color: #DC143C; }

    .status_icon {
        padding: 11px;
        display: inline-block;
        border-radius: 15px;
        background-color: #eee;
    }

    .status_icon.critical {
        background-color: #900;
        border: none;
    }

    .status_icon.emergency {
        background-color: #C33 !important;
        border: none;
    }

    .status_icon.warning {
        background-color: #ffcc00;
        border: none;
    }

    .status_icon.completed {
        background-color: green;
        border: none;
    }
</style>
<div id="dashboard_list">
    
    <div class="pane-footer">
        <ul>
            <li style="background: none; font-weight: bold;">Page </li>
            <?php
            for( $i = 1; $i <= $pages; $i++ ) {
                $offset = ($i - 1) * $pg_size; // passed from controller    
                $active = (isset($_GET['offset']) && $_GET['offset'] == $offset) || $offset == 0 && !isset($_GET['offset']) ? 'selected' : '';
                ?>
                <li class="<?php echo $active; ?>" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'dashboard_list', $status, "?offset=" . $offset)); ?>"><?php echo $i; ?></li>
            <?php }
            ?>
        </ul>
    </div>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="5" id="permits_table" class="fullwidth <?php echo $status; ?>">
        <thead>
            <tr>
                <!--<th width="5%"><input type="checkbox" class="check_all" /></th>-->
                <th colspan="3" title="Reference No. / Date of Application">REF No. / DOA</th>
                <th title="Date of Flight">DOF</th>
                <th nowrap>PRINCIPAL</th>
                <th nowrap>OPERATOR</th>
                <th title="Aircraft Call Sign">C/S</th>
                <th title="Aircraft Registration">A/C REGN</th>
                <th title="Location for Permit">LOCATION</th>
                <th>Permit No. /<br />Status</th>
            </tr>
        </thead>

        <tbody>
            <?php
            $count = $pg_offset;  // passed from controller
            foreach( $permits as $permit ) {

                $date_dt = new DateTime($permit['PermitSchedule'][0]['arr_eta']);
                $interval = $date_dt->diff(new DateTime());
                $period_remaining = $interval->format("ETD: %D days %H hours %I mins");
                $class = "";

                // either we are close to the arrival or departure time, or it has passed
                if( $interval->format("%D") <= 0 && $interval->format("%H") < 24 ) {
                    $class = "emergency";
                }

                // we have less than 24 hours to this departure so we need to be on gaurd
                else if( $interval->format("%D") <= 1 && $interval->format("%H") < 48 ) {
                    $class = "warning";
                }


                // work for this schedule has been completed successfully
                if( $date_dt < new DateTime() ) {
                    $class = "critical";
                    $period_remaining .= " ago";
                }

                $class .= ' ' . $permit['Permit']['status'];
                
                if( $permit['Permit']['status'] == "closed") {
                    $class = "completed";
                }
                ?>
                <tr data-width="700" data-id="<?php echo $permit['Permit']['id']; ?>" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'dashboard_details', $permit['Permit']['id'])); ?>" >

                    <td width="20"><span class="status_icon <?php echo $class; ?>" title="<?php echo $period_remaining; ?>"></span></td>
                    <td align="right">
                        <span class="numbers"><?php echo $count++ . "."; ?></span>
                    </td>
                    <td nowrap class="ref_no">
                        <?php
                        echo "<b>" . $permit['Permit']['ref_no'] . "</b><br />" .
                        "<span style='font-size: 11px;'>" . date('d M Y', strtotime($permit['Permit']['created_dt'])) . "</span>";
                        ?>
                    </td>

                    <td class="dof"><?php echo date('d M Y', strtotime($permit['PermitSchedule'][0]['arr_eta'])); ?></td>
                    <td class="doa"><?php echo $permit['Permit']['company_name']; ?></td>

                    <td><?php echo $permit['Permit']['operator_name']; ?></td>
                    <td><?php echo $permit['Permit']['aircraft_call_sign']; ?></td>
                    <td><?php echo $permit['Permit']['aircraft_regn']; ?></td>
                    <td><?php echo $permit['Permit']['permit_location']; ?></td>
                    <td class="status"><?php echo $permit['Permit']['status'] == "approved" ? $permit['Permit']['permit_number'] : $permit['Permit']['status']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

</div>