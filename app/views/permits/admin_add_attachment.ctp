<style>
    #upload-form-table td {
        padding: 6px;
    }

    #attachments-block {
        padding: 5px 0px;
    }

    #attachments-block td,
    #attachments-block th {
        padding: 8px;
        font-size: 11px;
    }
    
    #attachments-block a {
        font-size: 11px;
    }
</style>

<div class="ui-state-highlight" style="padding: 10px; margin-bottom: 5px;">
    Use this section to upload and attach documents such as the "Original Request Document" for this permit request
</div>

<div id="attachments-block">

    <div style="padding: 10px 0px">
        <input class="button" type="button" value="Add Attachment" id="attachment_add_btn"/>
        <!--<input class="button" type="button" value="Delete Selected" />-->
    </div>

    <table border="0" cellpadding="5" cellspacing="0" width="100%" class="fullwidth">
        <thead>
            <tr>
                <th colspan="2" align="left">Document Name</th>
                <th>Date Added</th>
                <th>Options</th>
            </tr>
        </thead>

        <tbody>
            <?php
            $count = 1;
            foreach ($permit_attachments as $attachment) {
                ?>
                <tr>
                    <!--<td width="20"><input type="checkbox" value="<?php echo $attachment['PermitAttachment']['id']; ?>" /></td>-->
                    <td width="20" align="right"><?php echo $count++ . "."; ?></td>
                    <td><a target="_blank" href="<?php echo $html->url(array('action' => 'viewAttachment', $attachment['PermitAttachment']['id'])); ?>"><?php echo $attachment['PermitAttachment']['file_name']; ?></a></td>
                    <td><?php echo date('d.m.y h:ia', strtotime($attachment['PermitAttachment']['created_dt'])); ?></td>
                    <td width="100">
                        <a class="delete-link" href="<?php echo $html->url(array('action' => 'deleteAttachment', $attachment['PermitAttachment']['id'])); ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<div id="upload-block" style="display: none;">
    <form id="AttachmentForm" method="post" action="<?php echo $this->here; ?>" target="upload-frame" enctype="multipart/form-data" >
        <fieldset>
            <table border="0" cellpadding="5" cellspacing="0" width="94%" align="center" id="upload-form-table">

                <tr>
                    <td valign="top" width="100">File Name: </td>
                    <td>
                        <input type="file" name="attachments[]" size="50" id="attachments_fs" multiple="" /> <br /><br />

                        (Multiple Files May Be Selected At Once. Max Total File Upload Size <b id="max_size_value"><?php echo ini_get("upload_max_filesize"); ?></b>)
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td height="50" colspan="2">
                        <div id="file_list" style="padding: 10px;"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" class="button" value="Upload & Attach Document" id="up_save_btn" disabled="" />
                        <input type="button" class="button" id="cancel-form" value="Cancel" />
                        <input type="hidden" name="data[PermitAttachment][permit_id]" value="<?php echo $permit_id; ?>" />
                    </td>
                </tr>
            </table>    
        </fieldset>
    </form>

    <iframe width="10" height="10" src="about:home" style="opacity: 0;" name="upload-frame" id="upload-frame" />
</div>

