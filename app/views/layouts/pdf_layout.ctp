<!DOCTYPE html>
<html>
    <head>
        <!-- Meta -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8;" />
        <!-- End of Meta -->

        <!-- Page title -->
        <title>Flight Manager - <?php echo $title_for_layout; ?></title>
        <!-- End of Page title -->

        <!-- Libraries -->
        <?php echo $html->css('core/layout.css'); ?>
        <style type="text/css">
            body, table tr td, p {font-family: sans-serif !important; font-weight: normal; line-height: 12pt; }
            #pdf_wrapper > header {
                padding-bottom: 5px; border-bottom: dotted #ccc 2px;
                width: 100%;
                display: block;
            }
        </style>
    </head>
    
    <body>
        <div id="pdf_wrapper">
            <header>
                <?php echo $html->image($this->Html->url('/img/core/sbman-letterhead.png', true), array('height' => '50', 'width' => '')); ?>
            </header>
            
            <?php echo $content_for_layout; ?>
        </div>
    </body>
</html>