<!DOCTYPE html>
<html>
    <head>
        <!-- Meta -->
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1;charset=windows-1252" />
        <!-- End of Meta -->

        <!-- Page title -->
        <title>Flight Manager - Login</title>
        <!-- End of Page title -->

        <!-- Libraries -->
        <?php echo $html->css('core/login.css'); ?>
        <!-- End of Libraries -->	
    </head>
    <body>
        <div id="container">
            <div class="logo">
                <a href=""><?php echo $html->image('core/mpouch_login.png', array('alt' => "MPOUCH")); ?></a>
            </div>
            <div id="box">
                
                <?php 
                if( isset($_GET['error']) ) { ?>
                    <div class="validate_error" style="text-align: center; font-size: 11px; color: #900; padding-bottom: 15px;">Login Failed. Username or Password Incorrect</div><?php
                }
                ?>
                
                <form action="<?php echo $html->url(array('controller' => 'users', 'action' => 'login')); ?>" method="POST">
                    <p class="main">
                        <label>Username: </label>
                        <input name="username" placeholder="Username" /> 
                        <label>Password: </label>
                        <input type="password" name="password" placeholder="Password">	
                    </p>

                    <p class="space">
                        <span><input type="checkbox" />Remember me</span>
                        <input type="submit" value="Login" class="login" />
                    </p>
                </form>
            </div>
        </div>

    </body>
</html>
