<!DOCTYPE html>
<html>
    
    <head>
        <title><?php echo $title_for_layout; ?></title>

<!--        <style media="screen">
		body, td {font-family: Tahoma, Verdana; font-size: 10.5pt !important; line-height: 13pt; font-weight: normal;}
		#print_wrapper {width: 850px; margin-left: auto; margin-right: auto; padding: 0px 10px; box-sizing: border-box; border: solid #d9d9d9 1px; padding: 10px; box-shadow: 1px 1px 3px #333; }
		#print_wrapper > header {
                margin-bottom: 10px; padding: 5px; border-bottom: dotted #ccc 2px;
            }
	  </style>     -->   

        <style>
            body, td {font-family: Tahoma, Verdana; font-size: 10.5pt !important; line-height: 13pt; font-weight: normal;}
            #print_wrapper {width: 100%; margin-left: auto; margin-right: auto; padding: 0px 10px; box-sizing: border-box; }
            #print_wrapper > header {
                margin-bottom: 10px; padding: 5px; border-bottom: dotted #ccc 2px;
            }
        </style>
    </head>
    
    <body>
        <div id="print_wrapper">
            <header>
                <?php echo $html->image('core/sbman-letterhead.png'); ?>
            </header>
            
            <?php echo $content_for_layout; ?>
        </div>
    </body>
    
    <?php echo $html->script('core/jquery-1.7.min.js'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            window.print();
            
            setTimeout(function() {window.close()}, 20000);
        });
    </script>
</html>



