<?php $user = $this->Session->read('user'); ?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Meta -->
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1;charset=windows-1252" />
        <!-- End of Meta -->

        <!-- Page title -->
        <title>Flight Manager - <?php echo $title_for_layout; ?></title>
        <!-- End of Page title -->

        <!-- Libraries -->
        <?php echo $html->css('core/layout.css'); ?>
        <?php echo $html->css('app/chosen.css'); ?>
        <?php echo $html->css('humane-js/themes/jackedup.css'); ?>
        <?php //echo $html->css('ext/css/ext-all-gray.css'); ?>

        <?php //echo $html->script('ext/bootstrap.js'); ?>
        <?php echo $html->script('core/jquery-1.7.min.js'); ?>
        <?php echo $html->script('core/easyTool.js'); ?>
        <?php echo $html->script('core/jquery-ui.min.js'); ?>
        <?php echo $html->script('core/jquery-ui-timepicker-addon.js'); //echo $html->script('core/timepicker.js'); ?>
        <?php echo $html->script('core/jquery.tablesorter.min.js'); ?>
        <?php echo $html->script('core/jquery.wysiwyg.js'); ?>
        <?php echo $html->script('core/hoverIntent.js'); ?>
        <?php echo $html->script('core/superfish.js'); ?>
        <?php echo $html->script('core/custom.js'); ?>
        <?php echo $html->script('app/chosen.jquery.js'); ?>
        <?php echo $html->script('app/core.js'); ?> 
        <?php echo $html->script('humane-js/humane.min.js'); ?>
        <!-- End of Libraries -->	
    </head>
    <body>
        <!-- Container -->
        <div id="container">

            <!-- Header -->
            <div id="header">

                <!-- Top -->
                <div id="top">
                    <!-- Logo -->
                    <div class="logo"> 
                        <a href="" title="Administration Home" class="tooltip"><?php echo $html->image('core/mpouch.png', array('alt' => "Company Logo")); ?></a> 
                    </div>
                    <!-- End of Logo -->

                    <!-- Meta information -->
                    <div class="meta">
                        <p>Welcome, <?php echo $user['User']['first_name'] . " " . $user['User']['last_name']; ?>! <!--<a href="" title="1 new private message from Elaine!" class="tooltip">1 new message!</a>--></p>
                        <ul>
                            <li><a href="<?php echo $html->url('/admin/users/logout'); ?>" title="End administrator session" class="tooltip"><span class="ui-icon ui-icon-power"></span>Logout</a></li>
                            <!--<li><a href="" title="Change current settings" class="tooltip"><span class="ui-icon ui-icon-wrench"></span>Settings</a></li>-->
                            <li><a href="<?php echo $html->url('/admin/users/view/' . $user['User']['id']); ?>" title="Go to your account" class="tooltip dialog_opener"><span class="ui-icon ui-icon-person"></span>My account</a></li>
                            <li><a href="<?php echo $html->url('/admin/email/issue_report'); ?>" title="Report Issue" class="tooltip dialog_opener" data-width="800" data-height="550"><span class="ui-icon ui-icon-pencil"></span>Report Issue</a></li>
                        </ul>	
                    </div>
                    <!-- End of Meta information -->
                </div>
                <!-- End of Top-->

                <!-- The navigation bar -->
                <div id="navbar">
                    <ul class="nav">
                        <?php
                        $menu = ClassRegistry::init("Menu");
                        $menus = $menu->getNavigationMenus($user);

                        foreach ($menus as $menu) {
                            ?>
                            <li>
                                <?php echo $html->link($menu['Menu']['title'], array('controller' => $menu['Menu']['controller'], 'action' => $menu['Menu']['action'])); ?>

                                <?php
                                if (isset($menu['ChildMenu']) && count($menu['ChildMenu']) > 0) {
                                    echo "<ul>";
                                    foreach ($menu['ChildMenu'] as $childMenu) {
                                        echo "<li>" . $html->link($childMenu['title'], array('controller' => $childMenu['controller'], 'action' => $childMenu['action'])) . "</li>";
                                    }
                                    echo "</ul>";
                                }
                                ?>
                            </li>
                        <?php } ?>
                        <!--<li>
                            <a href="">Multi Level Menu</a>
                            <ul>
                                <li><a href="">Menu Link 1</a></li>
                                <li><a href="">Menu Link 2</a></li>
                                <li><a href="">Menu Link 3</a>
                                    <ul>
                                        <li><a href="">Menu Link 1</a></li>
                                        <li><a href="">Menu Link 2</a>
                                            <ul>
                                                <li><a href="">Menu Link 1</a></li>
                                                <li><a href="">Menu Link 2</a></li>
                                                <li><a href="">Menu Link 3</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">Menu Link 3</a></li>
                                        <li><a href="">Menu Link 4</a></li>
                                        <li><a href="">Menu Link 5</a></li>
                                        <li><a href="">Menu Link 6</a></li>
                                    </ul>
                                </li>
                                <li><a href="">Menu Link 4</a></li>
                                <li><a href="">Menu Link 5</a></li>
                                <li><a href="">Menu Link 6</a></li>
                            </ul>
                        </li>-->
                    </ul>
                </div>
                <!-- End of navigation bar" -->

                <!-- Search bar -->
                <!--<div id="search">
                    <form action="http://unithemes.net/search/" method="POST">
                        <input type="submit" value="" class="but" />
                        <input type="text" name="q" value="" placeholder="Search the admin panel" />
                    </form>
                </div>-->
                <!-- End of Search bar -->

            </div>
            <!-- End of Header -->

            <!-- Background wrapper -->
            <div id="bgwrap">

                <!-- Main Content -->
                <div id="content">
                    <div id="main">
                        <?php echo $content_for_layout; ?>
                    </div>
                </div>
                <!-- End of Main Content -->

                <!-- Sidebar -->
                <div id="sidebar">
                    
                    
                    
                    <h2>Reports</h2>
                    <ul>
                        <?php
                        
                        
                        
                        $menurep = ClassRegistry::init("Report");
                        $menureport=$menurep->getreports();
                        
                       
                        
                        
                        foreach($menureport as $key=>$rptval){
                            ?>
                        <li>
                            <a href="#"><?php echo strtoupper($key); ?></a>

                            <ul>
                                <?php
                                foreach ($rptval as $childrept) {
                                   
                                    ?>
                                    <li>
                                        <a href="<?php echo $html->url(array("controller"=>"Reports","action"=>"admin_viewreport",$childrept['id']))  ?>"><?php echo strtoupper($childrept['title']); ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php  } ?>
                        
                        
                        
                        
                    </ul>

                    <h2>Today</h2>
                    <!-- Datepicker -->
                    <div id="datepicker"></div>
                    <!-- End of Datepicker -->

                    <?php //echo $this->element('layout/stats'); ?>

                    
                    
                    
                    
                    
                    
                </div>
                <!-- End of Sidebar -->

            </div>
            <!-- End of bgwrap -->
        </div>
        <!-- End of Container -->

        <!-- Footer -->
        <div id="footer">
            <?php echo $this->element('layout/footer'); ?>
        </div>
        <!-- End of Footer -->

        <div id="general_dialog"></div>
        <div id="flyout" class="flyout">
            <div class="content"></div>
        </div>

        <iframe id="submit_frame" src="about:blank" height="1" width="1" style="position: absolute; opacity: 0;"></iframe>
        <?php echo $this->element('data/urls'); ?>
        
        <?php echo $html->script('tiny_mce/tiny_mce.js'); ?>
    </body>
</html>
