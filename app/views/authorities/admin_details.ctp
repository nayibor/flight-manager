<style>
    .details_wrapper {
        padding: 10px;
    }
    
    .details_wrapper h3 {
        color: #333;
        margin-bottom: 15px;
        font-size: 18px;
    }
    
    .details_wrapper td {
        padding: 4px;
        font-size: 11px;
        line-height: 18px;
    }
    
    .details_wrapper td:first-child {
        white-space: nowrap;
        font-weight: bold
    }
</style>

<div class="details_wrapper">
    <h3>Authority Details</h3>

    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>Country:</td>
            <td><?php echo $authority['AviationAuthority']['country']; ?></td>
        </tr>
        <tr>
            <td width="20%">Authority Name:</td>
            <td width="80%"><?php echo $authority['AviationAuthority']['name']; ?></td>
        </tr>
        <tr>
            <td valign="top">Address:</td>
            <td><?php echo str_replace("\n", "<br />", $authority['AviationAuthority']['address']); ?></td>
        </tr>
        <tr>
            <td>Telephone:</td>
            <td><?php echo $authority['AviationAuthority']['telephone']; ?></td>
        </tr>
        <tr>
            <td>Fax:</td>
            <td><?php echo $authority['AviationAuthority']['fax']; ?></td>
        </tr>
        <tr>
            <td>Telex:</td>
            <td><?php echo $authority['AviationAuthority']['telex']; ?></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><?php echo $authority['AviationAuthority']['email']; ?></td>
        </tr>
        <tr>
            <td>AFTN:</td>
            <td><?php echo $authority['AviationAuthority']['aftn']; ?></td>
        </tr>
        <tr>
            <td valign="top">Notes:</td>
            <td><?php echo str_replace("\n", "<br />", $authority['AviationAuthority']['notes']); ?></td>
        </tr>
    </table>
</div>