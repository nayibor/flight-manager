<style>
    #AviationAuthority_form td {
        padding: 5px;
    }
</style>
<form id="AviationAuthority_form" name="AviationAuthority_form" method="post" action="<?php echo $html->url( array('controller' => 'authorities', 'action' => 'add')); ?>">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>Country:</td>
            <td>
                <input placeholder="Country" type="text" name="data[AviationAuthority][country]" id="data[AviationAuthority][country]" required="" value="<?php echo isset($authority) ? $authority['AviationAuthority']['country'] : ""; ?>"/>
            </td>
        </tr>
        <tr>
            <td width="16%">Name:</td>
            <td width="84%">
                <input placeholder="Name" type="text" name="data[AviationAuthority][name]" id="data[AviationAuthority][name]" size="40" value="<?php echo isset($authority) ? $authority['AviationAuthority']['name'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td valign="top">Address:</td>
            <td>
                <textarea placeholder="Address" name="data[AviationAuthority][address]" style="width: 300px; height: 50px;"><?php echo isset($authority) ? $authority['AviationAuthority']['address'] : ""; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Telephone:</td>
            <td>
                <input placeholder="+233-000-0000" type="text" size="30" name="data[AviationAuthority][telephone]" value="<?php echo isset($authority) ? $authority['AviationAuthority']['telephone'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>Telex:</td>
            <td>
                <input placeholder="+233-000-0000" type="text" size="30" name="data[AviationAuthority][telex]" value="<?php echo isset($authority) ? $authority['AviationAuthority']['telex'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>Fax:</td>
            <td>
                <input placeholder="+233-000-0000" type="text" size="30" name="data[AviationAuthority][fax]" value="<?php echo isset($authority) ? $authority['AviationAuthority']['fax'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>
                <input name="data[AviationAuthority][email]" type="email" id="data[AviationAuthority][email]" placeholder="caa@country-airport.info" size="40" value="<?php echo isset($authority) ? $authority['AviationAuthority']['email'] : ""; ?>"  />
            </td>
        </tr>
        
        <tr>
            <td>AFTN:</td>
            <td>
                <input placeholder="AFTN" type="text" size="30" name="data[AviationAuthority][aftn]" id="data[AviationAuthority][aftn]" value="<?php echo isset($authority) ? $authority['AviationAuthority']['aftn'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>SITA:</td>
            <td>
                <input placeholder="SITA" type="text" size="30" name="data[AviationAuthority][sita]" id="data[AviationAuthority][sita]" value="<?php echo isset($authority) ? $authority['AviationAuthority']['sita'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td valign="top">Notes:</td>
            <td>
                <textarea placeholder="Notes" name="data[AviationAuthority][notes]" style="width: 470px; height: 100px;"><?php echo isset($authority) ? $authority['AviationAuthority']['notes'] : ""; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="hidden" name="data[AviationAuthority][id]" value="<?php echo isset($authority) ? $authority['AviationAuthority']['id'] : ''; ?>" />
                <input type="submit" class="button" name="submit_btn" id="submit_btn" value="Submit" />
                <input type="button" class="button" name="cancel_btn" id="cancel-form" value="Cancel" />
            </td>
        </tr>
    </table>
</form>