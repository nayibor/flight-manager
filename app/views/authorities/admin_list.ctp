<table id="authorities-table" class="fullwidth" cellspacing="0" cellpadding="5" border="0">
    <thead>
        <tr>
            <th width="10"><input type="checkbox" class="check_all" /></th>
            <th width="230" colspan="3">Country / Authority Name</th>
            <th style="text-align: center;">Options</th>
        </tr>
    </thead>

    <tbody>
        <?php $count = $this->Paginator->counter('%start%'); ?>
        <?php foreach ($authorities as $authority) { ?>
            <tr id="row-<?php echo $authority['AviationAuthority']['id']; ?>" data-id="<?php echo $authority['AviationAuthority']['id']; ?>">
                <td><input type="checkbox" value="<?php echo $authority['AviationAuthority']['id']; ?>" /></td>
                <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                <td class="country" nowrap><?php echo $authority['AviationAuthority']['country']; ?></td>
                <td class="name" width="230"><?php echo $authority['AviationAuthority']['name']; ?></td>
                <td class="options" align="center" nowrap>
                    <a class="dialog_opener edit_link" title="Edit Aviation Authority Information" data-height="600" href="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'add', $authority['AviationAuthority']['id'])); ?>">Edit</a>
                    <a class="delete_link" href="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'delete', $authority['AviationAuthority']['id'])); ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>