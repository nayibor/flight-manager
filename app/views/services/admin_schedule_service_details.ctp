
<style>

    #service_details h3 {
        font-weight: bold;
        font-size: 13px;
        color: #333;
    }

    #service_details table td {
        padding: 5px;
    }
</style>

<div id="service_details">
    <?php
    switch ($service['Service']['name']) {

        case 'Hotel':
            echo $this->element('service_forms/hotel_details', array('service' => $service['Service']));
            break;

        case 'Catering':
            echo $this->element('service_forms/catering', array('service' => $service['Service']));
            break;

        default:
            if (stristr($service['Service']['name'], 'Transport') != false && stristr($service['Service']['name'], "Ramp") === false  ) {
                echo $this->element('service_forms/transport', array('service' => $service['Service']));
            }
            
            else if( stristr($service['Service']['name'], 'Permit') != false ) {
                echo $this->element('service_forms/permits', array('service' => $service['Service']));
            }
            
            else {
                echo $this->element("service_forms/default", array('service' => $service['Service']));
            }
            
            break;

        /*case 'Fuel':
            echo $this->element('service_forms/fuelling');
            break;

        case 'Airport':
            echo $this->element('service_forms/airport');
            break;
        case 'Fuelling':
            echo $this->element('service_forms/fuelling');
            break;
        case 'Fuelling':
            echo $this->element('service_forms/fuelling');
            break;
        case 'Permits':
            echo $this->element('service_forms/permits');
            break;

        case 'Transport':
            echo $this->element('service_forms/transport');
            break;*/
    }
    ?>
</div>
