<style>
    #service_form_wrapper header {
        margin-bottom: 15px;
        border-bottom: dotted #ccc 1px;
        padding: 5px;
    }

    #service_form_wrapper section {
        padding: 5px;
        margin-bottom: 15px;
    }

    #service_details h3 {
        font-weight: bold;
        font-size: 13px;
        color: #333;
    }

    #service_details table td {
        padding: 5px;
    }
</style>

<div id="service_form_wrapper">
    <?php echo $this->Form->create(); ?>

    <header>
        <h2>Editing Service For Schedule</h2>

        <label for="services" style="width: 100px !important;">Service Type:</label> 
        <label style="font-weight: bold; width: 200px !important;"><?php echo $schedule_service['Service']['name']; ?></label>
    </header>


    <section id="service_form_details">
        <?php if (isset($schedule_service)) { ?>
            <div id="service_details"> 
                <?php
                switch ($schedule_service['Service']['name']) {

                    case 'Hotel':
                        echo $this->element('service_forms/hotel_details', array('schedule_service' => $schedule_service, 'service' => $schedule_service['Service']));
                        break;

                    case 'Catering':
                        echo $this->element('service_forms/catering', array('schedule_service' => $schedule_service, 'service' => $schedule_service['Service']));
                        break;

                    case 'Transport':
                        echo $this->element('service_forms/transport', array('schedule_service' => $schedule_service, 'service' => $schedule_service['Service']));
                        break;

                    case 'Airport':
                        echo $this->element('service_forms/airport');
                        break;
                    case 'Fueling':
                        echo $this->element('service_forms/fuelling');
                        break;

                    default:
                        if (stristr($schedule_service['Service']['name'], 'Transport') != false && stristr($schedule_service['Service']['name'], "Ramp") === false  ) {
                            echo $this->element('service_forms/transport', array('schedule_service' => $schedule_service, 'service' => $schedule_service['Service']));
                        } else if (stristr($schedule_service['Service']['name'], 'Permit') != false ) {
                            echo $this->element('service_forms/permits', array('schedule_service' => $schedule_service, 'service' => $schedule_service['Service']));
                        } else {
                            echo $this->element("service_forms/default", array('schedule_service' => $schedule_service, 'service' => $schedule_service['Service']));
                        }

                        break;
                }
                ?>
            </div>
            <?php } ?>
    </section>

    <footer>
        <input type="submit" class="button" value="Save Changes" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
        <input type="hidden" name="data[GroundRequestScheduleService][ground_request_schedule_id]" value="<?php echo $ground_request_schedule_id; ?>" />
        <input type="hidden" name="data[GroundRequestScheduleService][id]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['id'] : ""; ?>" />
    </footer>

<?php echo $this->Form->end(); ?>
</div>