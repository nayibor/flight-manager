<style>
    #service_form_wrapper header {
        margin-bottom: 15px;
        border-bottom: dotted #ccc 1px;
        padding: 5px;
    }

    #service_form_wrapper section {
        padding: 5px;
        margin-bottom: 15px;
    }

    #service_details h3 {
        font-weight: bold;
        font-size: 13px;
        color: #333;
    }

    #service_details table td {
        padding: 5px;
    }
</style>

<div id="service_form_wrapper">
    <?php echo $this->Form->create(); ?>

    <header>
        <h2>Add Service To Schedule</h2>

        <label for="services">Service Type:</label> 
        <select id="services" name="data[GroundRequestScheduleService][service_id]" style="width: 280px;" data-href="<?php echo $html->url(array('controller' => 'services', 'action' => 'scheduleServiceDetails')); ?>">
            <option val="">Select Service</option>
            <?php
            foreach( $services as $key => $value ) {
                $selected = isset($schedule_service) && $key == $schedule_service['Service']['id'] ? "selected=selected" : "";
                ?>
                <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
            <?php } ?>
        </select>
    </header>


    <section id="service_form_details">
        <?php if ( isset($schedule_service) ) { ?>
            <div id="service_details"> 
                <?php
                switch ($schedule_service['Service']['name']) {

                    case 'Hotel':
                        echo $this->element('service_forms/hotel_details', array('schedule_service' => $schedule_service));
                        break;

                    case 'Catering':
                        echo $this->element('service_forms/catering', array('schedule_service' => $schedule_service));
                        break;
                }
                ?>
            </div>
        <?php } else { ?>
            Select Service Type Above To Load Details/Definition Form Here
        <?php } ?>
    </section>

    <footer>
        <input type="submit" class="button" value="<?php echo isset($schedule_service) ? "Save Changes" : "Add Service"; ?>" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
        <input type="hidden" name="data[GroundRequestScheduleService][ground_request_schedule_id]" value="<?php echo $ground_request_schedule_id; ?>" />
        <input type="hidden" name="data[GroundRequestScheduleService][id]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['id'] : ""; ?>" />
    </footer>

    <?php echo $this->Form->end(); ?>
</div>