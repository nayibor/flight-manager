<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php echo $html->css('app/jobsheet.css');
echo $html->script('app/service.js');

?>

<style>
    #service_process_form textarea, #service_process_form input[type=text] {
        font-size: 13px;
        font-family: Tahoma;
    }
</style>

<!--<h2><?php __('Edit Service'); ?></h2>-->

<form  method="POST" id="service_process_form" name="service_process_form" action="<?php echo $this->here; ?>">
    <fieldset>
        <div class="input">
            <label for="role_name">Name</label>
            <input type="text" class="required" required name="data[Service][name]" id="data[Service][name]" value="<?php echo $services['Service']['name'] ?>"/>
        </div>
        <div class="input">
            <label for="role_name">Description</label>
            <textarea required name="data[Service][description]" id="data[Service][description]"><?php echo $services['Service']['description'] ?></textarea>
        </div>
        <input type="hidden" name="data[Service][id]" id="data[Service][name]" value="<?php echo $services['Service']['id']; ?>" />
    </fieldset>
    <input type="submit" class="button" id="process_button_btn"/>
    <input type="button" class="button" id="cancel-form" value="Cancel" />
</form>
<input type="hidden" name="service_process" id="service_process" value="<?php echo $html->url( array('controller' => 'Services', 'action' => 'admin_editservice'));    ?>"/>
