<?php
/**
 * Description of DateFormatHelper
 *
 * @author ice
 */
class DateFormatHelper extends Helper {
    
    function formatZulu($dateString) {
        if( !$dateString || $dateString == "0000-00-00 00:00:00" ) {
            echo "";
            return;
        }
        
        $date = date('dMy', strtotime($dateString));
        $time = date('Hi', strtotime($dateString)) . "Z";
        
        echo "<div style='text-align: center;'>$date <br />$time</div>"; // 
    }
}

?>
