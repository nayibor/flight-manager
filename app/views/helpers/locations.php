<?php

class LocationsHelper extends Helper {
    
    public function getAirportName($icao_iata) {
        $airport = ClassRegistry::init("Airport");
        
        // names maybe in ICAO/IATA format, so split and take the first option if a possible
        $icao = explode("/", $icao_iata);
        
        $airport_details = $airport->find('first', array(
            'conditions' => array(
                'OR' => array(
                    'ident' => $icao[0],
                    'iata_code' => $icao[0]
                )
            ),
            
            'recursive' => -1
        ));
        
        if( $airport_details ) {
            return " - " . $airport_details['Airport']['name'];
        }
        
        return "";
    }
}
?>
