<?php

class TreeHelper extends Helper {

    var $tab = "  ";
    var $index = 1;
    
    function show($name, $data) {
        list($modelName, $fieldName) = explode('/', $name);
        $output = $this->list_element($data, $modelName, $fieldName, 0);

        return $this->output($output);
    }

    function list_element($data, $modelName, $fieldName, $level) {
        $tabs = "\n" . str_repeat($this->tab, $level * 2);
        $li_tabs = $tabs . $this->tab;

        $output = $tabs . "<ul>";
        
        foreach ($data as $key => $val) {
            $input = '<input type="checkbox" name="check" id="check_' . ($this->index) . '" class="checker" value="' . $val[$modelName]['id'] . '" />';
            $output .= $li_tabs . "<li>" . $input . " <label for='check_$this->index' style='width: auto;'>" . $val[$modelName][$fieldName] . "</label>";
            $this->index++;
            
            if (isset($val['children'][0])) {
                $output .= $this->list_element($val['children'], $modelName, $fieldName, $level + 1);
                $output .= $li_tabs . "</li>";
            } else {
                $output .= "</li>";
            }

        }
        $output .= $tabs . "</ul>";

        return $output;
    }

}

?>