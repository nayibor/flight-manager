<style>
    #email_form .form_table > tbody > tr > td {
        padding: 6px;
    }
    
    .form_table .mail_field {
        font-size: 11px;
        padding: 6px;
        width: 550px;
    }

    .form_table textarea {
        width: 550px;
    }
</style>

<form name="email_form" id="email_form" action="<?php echo $html->url(array('controller' => 'email', 'action' => 'issue_report')); ?>" method="post">
    
    <h2>Report Issue Or Problem With Application</h2>
    
    <fieldset>
        <?php 
        $user = $this->Session->read('user');
        
        echo $this->element('email_composition_form', array(
            'to' => array(
                'Francis Adu-Gyamfi' => 'icewalker2g@yahoo.co.uk',
                'Nuku Ameyibor' => 'nayibor@gmail.com'
            ),
            'subject' => '',
            'message' => "
                <p>Please describe the nature of the issue/problem with the application</p>
                <ul>
                    <li>&nbsp;</li>
                </ul>
                
                <p>What steps when taken will help reproduce the issue being reported?</p>
                <ol>
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>
                </ol>
                
                <p>&nbsp;</p>
                <p>Regards, </p>
                <p>
                    {$user['User']['first_name']} {$user['User']['last_name']} <br />
                    {$user['UserRole']['name']}
                </p>
            "
        )); ?>
    </fieldset>

    <div style="padding: 10px;">
        <input type="submit" class="button" value="Submit Issue" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
    </div>
</form>


<script>
    try {
        tinyMCE.init({
            mode : "textareas",
            theme : "advanced",
            plugins : "advimage",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
			
            theme_advanced_resizing : true,
			
            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink", //,image,code
            theme_advanced_buttons2 : ""
        });
    } catch(e) {
        console.log(e);
    } 
</script>