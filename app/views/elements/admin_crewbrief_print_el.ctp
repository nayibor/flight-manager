<?php


$gr = ClassRegistry::init('GroundRequest');
$grq= ClassRegistry::init('GroundRequestQuality');
$req_data=$gr->findRequests($req_id);
$data = $grq->find('first',array('contain' => 'GroundRequest','recursive' => 2, 'conditions' => array('GroundRequestQuality.ground_request_id' => $req_id)));


?>
<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php echo $html->css('app/jobsheet.css');
?>

<style>
    .forward_details {
        border: double #ccc 3px;
        padding: 10px 20px;
    }

    ul li.input {
        display:list-item;
        margin-right: 10px;
        font-size: 13px;
        min-width: 160px;
        padding: 5px 0px;list-style-type: disc;

    }
    #footer ul li.input {
        display:inline-block;
        margin-right: 10px;
        font-size: 13px;
        min-width: 160px;
        padding: 5px 0px;list-style-type:disc;

    }


    .flyout_content td {
        padding: 5px;
    }

    .inline-bold {
        display: inline-block;
        font-weight: bold;
        border-bottom: dotted #999 1px;
    }

    .ordered_list {
        list-style-type: decimal;
        padding-left: 10px;
        margin: 5px;
        margin-left: 20px;
    }

    .flyout_content section, .flyout_content section td {
        margin-bottom: 10px;
        font-family:  Tahoma, Geneva, sans-serif;
        font-size: 13px;
    }

    .flyout_content section header {
        padding: 5px;
        margin-bottom: 10px;
        text-align: center;
    }

    .flyout_content  header h3 {
        text-transform: uppercase;
        font-weight: bold;
        font-size: 14px;
        font-family:  Tahoma, Geneva, sans-serif;
        text-align: left;
    }

    .flyout_content footer {
        text-align: center;
    }
    .error_jobsheet{
        font-weight:bold;
        color: red;
    }
    table thead th{
        text-align:left;
    }
    .status{
        font-weight: bolder;
        font-family: "Roman", Arial, Helvetica, sans-serif !important;
        display: inline;
        background-color: #222;
        color: #fff;

    }
    #handler_section{
        font-weight: bolder;
        font-family: "Roman", Arial, Helvetica, sans-serif !important;
        text-transform: uppercase;
        border-top:  dotted #999 1px;

    }
    #label_req{
        margin-bottom: 50px !important;
    }
    #qt label{
        /**  display: inline !important;
           **/
        width: 200px !important;
        padding-right: 5px !important;

    }

</style>





<div class="flyout_content">

    <header>
        <h3>GROUND HANDLING QUALITY  SERVICE REPORT</h3>
    </header>

    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td width="25%">Reference No:</td>
            <td width="25%"><?php echo $req_data[0]['GroundRequest']['ref_no']; ?></td>
            <td for="role_name">Operator:</td>
            <td for="role_name"><?php echo $req_data[0]['GroundRequest']['operation']; ?></td>
        </tr>
        <tr>
            <td width="25%">Location:</td>
            <td width="25%"><?php echo $req_data[0]['GroundRequest']['location']; ?></td>
            <td for="role_name">A/C Reg:</td>
            <td for="role_name"><?php echo $req_data[0]['GroundRequest']['region']; ?></td>
        </tr>
        <tr>
            <td> Date:</td>
            <td><?php echo date("Y-m-d",$req_data[0]['GroundRequest']['date_created']); ?></td>
            <td for="role_name">Type Of Ops:</td>
            <td for="role_name"><?php echo $req_data[0]['GroundRequest']['type']; ?></td>
        </tr>
        <tr>
            <td> Date Request Made:</td>
            <td><?php echo date("Y-m-d",$req_data[0]['GroundRequest']['date_created']); ?></td>
            <td for="role_name">A/C Type:</td>
            <td for="role_name"><?php  ?></td>
        </tr>
        <tr>
            <td for="role_name">Flight No:</td>
            <td for="role_name"><?php echo $req_data[0]['GroundRequest']['flight_no']; ?></td>
            <td for="role_name">Flight Date:</td>
            <td for="role_name"><?php  ?></td>
        </tr>

        <tr>    <td for="role_name">Origin:</td>
            <td for="role_name"><?php  ?></td>
            <td for="role_name">Destination:</td>
            <td for="role_name"><?php  ?></td>

        </tr>


    </table>

</div>


<header>
    <h3>Questionnaire</h3>
</header>

<div class="input">
    <p>
        <label>How responsive was Supervisor ?</label>
        <input value="prompt" <?php echo $data['GroundRequestQuality']['response_sup']=="prompt" ? "checked=checked" : ""; ?>  type="radio" name="data[GroundRequestQuality][response_sup]" id="data[GroundRequestQuality][response_sup]">Prompt
        <input value="fair" <?php echo $data['GroundRequestQuality']['response_sup']=="fair" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][response_sup]" id="data[GroundRequestQuality][response_sup]">Fair
        <input value="slow" <?php echo $data['GroundRequestQuality']['response_sup']=="slow" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][response_sup]" id="data[GroundRequestQuality][response_sup]">Slow
    </p>

</div>
<div class="input">
    <p>
        <label for="role_name">Was equipment positioning quick enough ?</label>
        <input value="good" <?php echo $data['GroundRequestQuality']['equip_pos']=="good" ? "checked=checked" : ""; ?> type="radio" name="data[GroundRequestQuality][equip_pos]" id="data[GroundRequestQuality][equip_pos]">Good
        <input value="not_bad" <?php echo $data['GroundRequestQuality']['equip_pos']=="not_bad" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][equip_pos]" id="data[GroundRequestQuality][equip_pos]">Not Bad
        <input value="slow" <?php echo $data['GroundRequestQuality']['equip_pos']=="slow" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][equip_pos]" id="data[GroundRequestQuality][equip_pos]">Slow

    </p>
</div>
<div class="input">
    <p>
        <label for="role_name">Was handling to your satisfaction ?</label>
        <input value="yes" <?php echo $data['GroundRequestQuality']['handling_sat']=="yes" ? "checked=checked" : ""; ?> type="radio" name="data[GroundRequestQuality][handling_sat]" id="data[GroundRequestQuality][handling_sat]">Yes
        <input value="not_bad" <?php echo $data['GroundRequestQuality']['handling_sat']=="not_bad" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][handling_sat]" id="data[GroundRequestQuality][handling_sat]">Not Bad
        <input value="slow" type="radio" <?php echo $data['GroundRequestQuality']['handling_sat']=="slow" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][handling_sat]" id="data[GroundRequestQuality][handling_sat]">Bad
    </p>

</div>
<div class="input">
    <p>
        <label for="role_name">Were You provided with all your needs ?</label>
        <input value="yes" type="radio" <?php echo $data['GroundRequestQuality']['needs']=="yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][needs]" id="data[GroundRequestQuality][needs]">Yes
        <input value="no" type="radio" <?php echo $data['GroundRequestQuality']['needs']=="no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][needs]" id="data[GroundRequestQuality][needs]">No
    </p>
</div>
<div class="input">
    <p>
        <label for="role_name">Was Fuel Truck On Time ?</label>
        <input value="yes" type="radio" <?php echo $data['GroundRequestQuality']['fuel']=="yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][fuel]" id="data[GroundRequestQuality][fuel]">Yes
        <input value="no" type="radio" <?php echo $data['GroundRequestQuality']['fuel']=="no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][fuel]" id="data[GroundRequestQuality][fuel]">No
    </p>

</div>
<div class="input">
    <p>
        <label for="role_name">Was Catering Delivered ON Time ?</label>
        <input value="yes" type="radio" <?php echo $data['GroundRequestQuality']['catering']=="yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][catering]" id="data[GroundRequestQuality][catering]">Yes
        <input value="no" type="radio" <?php echo $data['GroundRequestQuality']['catering']=="no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering]" id="data[GroundRequestQuality][catering]">No
    </p>
</div>
<div class="input">
    <p>
        <label for="role_name">Was Catering Quality/Quantity To Your Satisfaction ?</label>
        <input value="yes" type="radio" <?php echo $data['GroundRequestQuality']['catering_quality']=="yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]">Yes
        <input value="no" type="radio" <?php echo $data['GroundRequestQuality']['catering_quality']=="no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]">No
    </p>

</div>
<div class="input">
    <p>
        <label for="role_name">General Assessment</label>
        <input value="excellent" type="radio" <?php echo $data['GroundRequestQuality']['general_assessment']=="excellent" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]">Excellent
        <input value="good" type="radio" <?php echo $data['GroundRequestQuality']['general_assessment']=="good" ? "checked=checked" : ""; ?>   name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]">Good
        <input value="fair" type="radio" <?php echo $data['GroundRequestQuality']['general_assessment']=="fair" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]">Fair
        <input value="bad" type="radio" <?php echo $data['GroundRequestQuality']['general_assessment']=="bad" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]">Bad

    </p>
</div>
<div class="input">
    <label for="role_name">Comments:</label>
    <div id="comments_div">
        <?php echo $data['GroundRequestQuality']['comments'] ?>
    </div>
</div>



