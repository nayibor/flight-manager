<?php 
$hotel = ClassRegistry::init('Hotel');
$hotels = $hotel->find('list', array('order' => array('name')));
?>


<div id="hotel_details">
    <h3>Hotel Reservation</h3>

    <table border="0" cellspacing="0" cellpadding="8" width="100%">
        <tr>
            <td width="200">Name</td>
            <td>
                <input type="text" size="40" name="data[GroundRequestScheduleService][name]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['name'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                City :
            </td>
            <td>
                <input type="text" name="data[GroundRequestScheduleService][hotel_reservation_city]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['hotel_reservation_city'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Contact Information:
            </td>
            <td>
                <input type="text" name="data[GroundRequestScheduleService][contact_number]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['contact_number'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Reservation Number
            </td>
            <td>
                <input type="text" name="data[GroundRequestScheduleService][hotel_reservation_no]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['hotel_reservation_no'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Type of Room
            </td>
            <td>
                <input type="text" name="data[GroundRequestScheduleService][hotel_reservation_rm_type]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['hotel_reservation_rm_type'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Number of Rooms To Reserve
            </td>
            <td>
                <input type="number" value="1" min="1" max="50" size="4" name="data[GroundRequestScheduleService][hotel_reservation_rooms]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['hotel_reservation_rooms'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                PIC Room No.:
            </td>
            <td>
                <input type="text" name="data[GroundRequestScheduleService][hotel_pic_room_no]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['hotel_pic_room_no'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                PIC Cell No.:
            </td>
            <td>
                <input type="text" name="data[GroundRequestScheduleService][hotel_pic_cell_no]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['hotel_pic_cell_no'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Payment
            </td>
            <td>
                <input type="text"  name="data[GroundRequestScheduleService][payment_type]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['payment_type'] : ""; ?>" />
            </td>
        </tr>

        <tr>
            <td>
                Remarks
            </td>
            <td>
                <input type="text" size="40" name="data[GroundRequestScheduleService][remarks]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['remarks'] : ""; ?>" />
            </td>
        </tr>

    </table>
</div>