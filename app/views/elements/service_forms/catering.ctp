
<div id="hotel_details">
    <h3>Catering Request</h3>

    <table border="0" cellspacing="0" cellpadding="8" width="100%">
        <tr>
            <td width="200">Caterer</td>
            <td>
                <input type="text" size="40" name="data[GroundRequestScheduleService][name]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['name'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                Contact Information:
            </td>
            <td>
                <input type="text" name="data[GroundRequestScheduleService][contact_number]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['contact_number'] : ""; ?>" />
            </td>
        </tr>
        
        <tr>
            <td>
                Payment
            </td>
            <td>
                <input type="text"  name="data[GroundRequestScheduleService][payment_type]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['payment_type'] : ""; ?>" />
            </td>
        </tr>
        
        <tr>
            <td>
                Remarks
            </td>
            <td>
                <input type="text" size="40" name="data[GroundRequestScheduleService][remarks]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['remarks'] : ""; ?>" />
            </td>
        </tr>
    </table>
</div>