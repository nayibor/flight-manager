
<div id="hotel_details">
    <h3>Transport</h3>

    <table border="0" cellspacing="0" cellpadding="8" width="100%">
        <tr>
            <td width="120">Service Provider</td>
            <td colspan="3">
                <input type="text" value="" size="40" name="data[GroundRequestScheduleService][name]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['name'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td colspan="4"  style="font-weight: bold; height: 35px; vertical-align: center;">The following information will appear on the Crew Brief Form</td>
        </tr>
        <tr>
            <td>Contact Number</td>
            <td><input type="text" value="" name="data[GroundRequestScheduleService][contact_number]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['contact_number'] : ""; ?>" /></td>
            <td>Vehicle Type:</td>
            <td><input type="text"  name="data[GroundRequestScheduleService][transport_vehicle_type]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['transport_vehicle_type'] : ""; ?>" /></td>
        </tr>
        
        <tr>
            <td>Driver's Name:</td>
            <td><input type="text"  name="data[GroundRequestScheduleService][transport_driver_name]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['transport_driver_name'] : ""; ?>" /></td>
            <td>Payment Type:</td>
            <td><input type="text" size="15" name="data[GroundRequestScheduleService][payment_type]" placeholder="CASH or CREDIT" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['payment_type'] : ""; ?>" /></td>
        </tr>
        
        <tr>
            <td colspan="4" style="font-weight: bold; height: 35px; vertical-align: center;">The following information will appear on the Job Sheet for this flight schedule</td>
        </tr>
        <tr>
            <td>Type of Cars:</td>
            <td><input type="text"  name="data[GroundRequestScheduleService][transport_vehicle_types]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['transport_vehicle_types'] : ""; ?>" /></td>
            <td>Pick Up Date & Time:</td>
            <td><input type="text"  name="data[GroundRequestScheduleService][transport_pickup_dt]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['transport_pickup_dt'] : ""; ?>" /></td>
        </tr>
        <tr>
            <td width="130">No. of Cars IN:</td>
            <td><input type="text"  name="data[GroundRequestScheduleService][transport_cars_in]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['transport_cars_in'] : ""; ?>" /></td>
            <td>No. of Cars OUT:</td>
            <td><input type="text"  name="data[GroundRequestScheduleService][transport_cars_out]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['transport_cars_out'] : ""; ?>" /></td>
        </tr>
        <tr>
            <td>PAX Co-ordinator Name & Number</td>
            <td colspan="3">
                <input type="text" size="40" name="data[GroundRequestScheduleService][transport_pax_coordinator]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['transport_pax_coordinator'] : ""; ?>" />
            </td>
        </tr>
    </table>
</div>