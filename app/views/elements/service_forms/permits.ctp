<?php
$permits = ClassRegistry::init('Permit')->find('all', array(
    'fields' => array('id', 'ref_no', 'permit_number', 'permit_validity', 'type'),
    'order' => array('Permit.created_dt' => 'desc'),
    'limit' => 50,
    'recursive' => -1)
);

?>
<div id="hotel_details">
    <p>Select the permit obtained to pre-fill the data in the forms or enter the information manually</p>

    <table border="0" cellspacing="0" cellpadding="8" width="100%">
        <tr>
            <td>SELECT PERMIT OBTAINED</td>
            <td>
                <select name="data[GroundRequestScheduleService][permit_id]" id="permits" style="width: 200px;">
                    <option value="">SELECT PERMIT</option>
                    <?php foreach ($permits as $permit) { 
                        $selected = $permit['Permit']['id'] == $schedule_service['GroundRequestScheduleService']['permit_id'] ? "selected=selected" : ""; ?>
                        <option <?php echo $selected; ?> value="<?php echo $permit['Permit']['id']; ?>" 
                                data-number="<?php echo $permit['Permit']['permit_number']; ?>"
                                data-validity="<?php echo $permit['Permit']['permit_validity']; ?>"
                                data-type="<?php echo $permit['Permit']['type']; ?>">
                                    <?php echo $permit['Permit']['ref_no']; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top: solid #ccc 1px;">&nbsp;</td>
        </tr>
        <tr>
            <td width="200">PERMIT NUMBER</td>
            <td>
                <input id="permit_number" type="text" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['permit_no'] : ""; ?>" size="40" name="data[GroundRequestScheduleService][permit_no]" />
            </td>
        </tr>

        <tr>
            <td>PERMIT VALIDITY</td>
            <td>
                <input id="permit_validity" type="text"  name="data[GroundRequestScheduleService][permit_validity]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['permit_validity'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                PERMIT OVERFLIGHT
            </td>
            <td>
                <input id="permit_overflight" type="text"  name="data[GroundRequestScheduleService][permit_overflight]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['permit_overflight'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                PERMIT LANDING
            </td>
            <td>
                <input id="permit_landing" type="text" name="data[GroundRequestScheduleService][permit_landing]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['permit_landing'] : ""; ?>" />
            </td>
        </tr>

        <tr>
            <td>
                SERVICE PROVIDER
            </td>
            <td>
                <input id="remarks" type="text" size="40" name="data[GroundRequestScheduleService][name]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['name'] : "S. B. MAN & CO LTD."; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                CONTACT NUMBER
            </td>
            <td>
                <input id="remarks" type="text" size="40" name="data[GroundRequestScheduleService][contact_number]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['contact_number'] : ""; ?>" />
            </td>
        </tr>
        <tr>
            <td>
                REMARKS
            </td>
            <td>
                <input id="remarks" type="text" size="40" name="data[GroundRequestScheduleService][remarks]" value="<?php echo isset($schedule_service) ? $schedule_service['GroundRequestScheduleService']['remarks'] : ""; ?>" />
            </td>
        </tr>
    </table>
</div>

<script>
    $("#permits").change(function() {
        var option = this.options[ this.selectedIndex ];
        
        $("#permit_number").val( $(option).data('number') );
        $("#permit_validity").val( $(option).data('validity') );
        $("#permit_overflight").val( $(option).data('type').indexOf("overflight") > -1 ? "YES" : "N/A" );
        $("#permit_landing").val( $(option).data('type').indexOf("overflight") == -1 ? "YES" : "N/A" );
    });
</script>