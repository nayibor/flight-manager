<?php
$ser = ClassRegistry::init('Service');
$gr = ClassRegistry::init('GroundRequest');
$grs= ClassRegistry::init('GroundRequestService');
$grd= ClassRegistry::init('GroundRequestDate');
$hnd=ClassRegistry::init('Handler');


$services=$ser->findServices();
$req_data=$gr->find('all', array(
        'conditions' => array('GroundRequest.id' => $req_id)));
$grservices=$grs->find("all",array('conditions' => array(
                'GroundRequestService.ground_request_id' =>$req_id)));
$grarray=array();
foreach($grservices as $val) {
    $grarray[]=$val['GroundRequestService']['service_id'];
}
$grservices = $grs->find("all", array(
        'conditions' => array('GroundRequestService.ground_request_id' => $req_id)
));


$grdates = $grd->find("first", array(
        'conditions' => array('GroundRequestDate.ground_request_id' => $req_id)
));

//for  finding all data associated with the request jobsheet
$handlers =$hnd->findHandlers("GroundOps");
$status='true';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>JOBSHEET</title>
        <style type="text/css">
            body, td {font-family: Tahoma, Verdana; font-size: 13px; line-height: 20px;}
            #wrapper {width: 980px; margin-left: auto; margin-right: auto; padding: 20px 40px; box-sizing: border-box; }
            #wrapper header {
                margin-bottom: 20px; padding: 5px; border-bottom: dotted #ccc 2px;
            }

            .forward_details {
                border: double #ccc 3px;
                padding: 10px 20px;
            }

            ul li.input {
                display:list-item;
                margin-right: 10px;
                font-size: 13px;
                min-width: 160px;
                padding: 5px 0px;list-style-type: disc;

            }
            #footer ul li.input {
                display:inline-block;
                margin-right: 10px;
                font-size: 13px;
                min-width: 160px;
                padding: 5px 0px;list-style-type:disc;

            }


            .flyout_content td {
                padding: 5px;
            }

            .inline-bold {
                display: inline-block;
                font-weight: bold;
                border-bottom: dotted #999 1px;
            }

            .ordered_list {
                list-style-type: decimal;
                padding-left: 10px;
                margin: 5px;
                margin-left: 20px;
            }

            .flyout_content section, .flyout_content section td {
                margin-bottom: 10px;
                font-family:  Tahoma, Geneva, sans-serif;
                font-size: 13px;
            }

            .flyout_content section header {
                padding: 5px;
                margin-bottom: 10px;
                text-align: center;
            }

            .flyout_content  header h3 {
                text-transform: uppercase;
                font-weight: bold;
                font-size: 14px;
                font-family:  Tahoma, Geneva, sans-serif;
                text-align: left;
            }

            .flyout_content footer {
                text-align: center;
            }
            .error_jobsheet{
                font-weight:bold;
                color: red;
            }
            table thead th{
                text-align:left;
            }
            .status{
                font-weight: bolder;
                font-family: "Roman", Arial, Helvetica, sans-serif !important;
                display: inline;
                background-color: #222;
                color: #fff;

            }
            #handler_section{
                font-weight: bolder;
                font-family: "Roman", Arial, Helvetica, sans-serif !important;
                text-transform: uppercase;
                border-top:  dotted #999 1px;

            }
            #label_req{
                margin-bottom: 50px !important;
            }
        </style>

    </head>

    <body>
        <div id="wrapper">
            <div class="flyout_content">
                <br><br>
                <header>
                    <h3>GROUND HANDLING REQUEST</h3>
                </header>


                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <tr>
                        <td width="25%">Reference No:</td>
                        <td width="25%"><?php echo $req_data[0]['GroundRequest']['ref_no']; ?></td>
                        <td width="25%"></td>       <td width="25%"></td>

                    </tr>
                    <tr>
                        <td width="25%">Location:</td>
                        <td width="25%"><?php echo $req_data[0]['GroundRequest']['location']; ?></td>
                    </tr>
                    <tr>
                        <td> Date:</td>
                        <td><?php echo date("Y-m-d",$req_data[0]['GroundRequest']['date_created']); ?></td>
                    </tr>

                </table>

                <!--this part is for the request details   --->
                <section>
                    <table width="100%" border="0" cellspacing="0" cellpadding="2" id="handler_section">
                        <tr>
                            <td width="25%">Attn:</td>
                            <td width="25%"> <?php
                                foreach( $handlers as $val ) {
                                    if ($req_data[0]['GroundRequest']['handler_id'] == $val['Handler']['id'] ) {
                                        echo $val['Handler']['name'];
                                    }
                                }
                                ?></td>
                            <td width="25%"></td>       <td width="25%"></td>

                        </tr>
                        <tr>
                            <td width="25%">From:</td>
                            <td width="25%" >
                                Ground Operations</td>
                            <td width="25%"></td>       <td width="25%"></td>

                        </tr>
                    </table></section>

                <label id="label_req">WE RQST YOUR ASSISTANCE WITH THE BELOW FLIGHT:</label>



                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                        <td width="30%">Operation:</td>
                        <td width="30%"><?php echo $req_data[0]['GroundRequest']['operation']; ?></td>
                        <td width="30%"></td>       <td width="25%"></td>
                    </tr>
                    <tr>
                        <td>AirCraft Type:</td>
                        <td><?php echo $req_data[0]['GroundRequest']['type']; ?></td>
                        <td width="25%"></td>       <td width="25%"></td>
                    </tr><tr>
                        <td>Region:</td>
                        <td><?php echo $req_data[0]['GroundRequest']['region']; ?></td>
                        <td width="25%"></td>       <td width="25%"></td>
                    </tr>
                    <tr>
                        <td>Mtow:</td>
                        <td><?php echo $req_data[0]['GroundRequest']['mtow']; ?></td>
                        <td width="25%"></td>       <td width="25%"></td>
                    </tr><tr>
                        <td>Flight No:</td>
                        <td>
                            <?php echo $req_data[0]['GroundRequest']['flight_no']; ?>
                        </td>
                        <td width="25%"></td>       <td width="25%"></td>
                    </tr>
                    <tr>
                        <td>Crew:</td>
                        <td><?php echo $req_data[0]['GroundRequest']['crew']; ?></td>
                        <td width="25%"></td>       <td width="25%"></td>
                    <tr><tr>
                        <td width="25%">Purpose:</td>
                        <td width="25%"><?php echo $req_data[0]['GroundRequest']['purpose']; ?></td>
                        <td width="25%"></td>       <td width="25%"></td>

                    </tr>
                </table>



                <section>
                    <h4>SCHEDULE</h4>

                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <thead>
                        <th> </th>

                        </thead>
                        <?php foreach( $grdates as $val ) { ?>
                        <tr>
                            <td width="100%"><?php echo date('dMy',$val['GroundRequestDate']['date_departure'])." ETD ".$val['GroundRequestDate']['country_code_etd']." ".date('hi',$val['GroundRequestDate']['date_departure'])."Z"." &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".
                                            date('dMy',$val['GroundRequestDate']['date_arrival']). " ETA ".$val['GroundRequestDate']['country_code_eta']." ".date('hi',$val['GroundRequestDate']['date_arrival'])."Z"." "."(".$val['GroundRequestDate']['comment'].")"; ?>
                            </td>
                            <td></td>    <td></td>
                        </tr>
                            <?php } ?>

                    </table>

                </section>

                <section>
                    <h4>WE REQ</h4>

                    <ul>

                        <?php
                        foreach( $services as $val ) {
                            if ( in_array($val['Service']['id'], $grarray) ) {
                                ?>
                        <li class="input">


                                    <?php  echo $val['Service']['description'];   ?>
                        </li>

                                <?php
                            }
                        }
                        ?>
                        <li>   PLS ADVISE ANY LIMITATIONS, YOU MAY HAVE AT THIS LOCATION.</li>
                        <li>PLS ADVISE ANY AIRPORT LIMITATIONS, IF ANY.</li>
                    </ul>
                    PLS KINDLY CONFIRM PROVISION OF ALL SERVICES.

                </section>


                <footer>
                    <div class="input">
                        <ul><li>RGDS</li><li>DONATA - CHARLIE TEAM | S.B-MAN (HQ), P. O. Box OS 1734, OSU – ACCRA. GHANA.</li><li>www.sbmangh.com </li</ul> </footer>
            </div>
        </div>
    </div>
</body>
</html>
