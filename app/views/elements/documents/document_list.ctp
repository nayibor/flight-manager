<style>
    #list-table.fullwidth tr:not(:last-child) td {
        border-bottom: solid #ddd 1px;
    }
    
    #list-table.fullwidth td p {
        font-size: 11px;
    }
    
    #list-table.fullwidth td a {
        font-size: 13px;
    }
</style>

<table border="0" cellpadding="5" cellspacing="0" id="list-table" class="fullwidth">
    <thead>
        <tr>
            <th width="25" style="text-align: center;"><input type="checkbox" class="check_all" /></th>
            <th colspan="2">Document Name</th>
            <th style="text-align: center;">Options</th>
        </tr>
    </thead>
    <tbody>
        <?php $count = 1; ?>
        <?php foreach ($documents as $document) { ?>
            <tr id="user-row-<?php echo $document['Document']['id']; ?>">
                <td valign="top">
                    <input type="checkbox" name="user_id[]" value="<?php echo $document['Document']['id']; ?>" />
                </td>
                <td width="20" valign="top"><?php echo $count++ . "."; ?></td>
                <td valign="top">
                    <h3 style="color: #0099cc">
                        <a href="<?php echo $html->url(array('controller' => 'documents', 'action' => 'view', $document['Document']['id'])); ?>"><?php echo $document['Document']['name']; ?></a>
                    </h3>
                    <p><?php echo  str_replace("\n", "<br />", substr($document['Document']['abstract'], 0, 400)); ?></p>
                </td>
                <td class="options" align="center" valign="top">
                    <!--<a title="Edit Selected Document" class="dialog_opener edit_link" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'edit', $document['Document']['id'])); ?>" data-height="450">Edit</a>-->
                    <a title="View Document" class="view_link" href="<?php echo $html->url(array('controller' => 'documents', 'action' => 'view', $document['Document']['id'])); ?>">View</a>
                    <a title="Delete Document From System" class="delete_link" data-id="<?php echo $document['Document']['id']; ?>" href="<?php echo $html->url(array('controller' => 'documents', 'action' => 'delete', $document['Document']['id'])); ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>