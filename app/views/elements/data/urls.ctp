<input type="hidden" id="country-auto-complete-source" value="<?php echo $html->url( array('controller' => 'airports', 'action' => 'country_autocomplete')); ?>" />
<input type="hidden" id="iata-auto-complete-source" value="<?php echo $html->url( array('controller' => 'airports', 'action' => 'iata_autocomplete')); ?>" />

<input type="hidden" id="logout-url" value="<?php echo $html->url( array('controller' => 'users', 'action' => 'logout')); ?>" />