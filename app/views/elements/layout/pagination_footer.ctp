
<style>
    .pane-footer {
        font-size: 11px;
        background-color: #f1f1f1;
        padding: 5px;
        border-top: solid #bbb 1px;
    }

    .pane-footer ul {
        display: inline-block;
        overflow: hidden;
        vertical-align: middle;
    }

    .pane-footer li {
        display: inline-block;
        padding: 3px 6px;
        background-color: #ccc;
        margin-right: 2px;
    }

    .pane-footer li:hover {
        background-color: #999;
        color: #fff;
        cursor: pointer;
    }

    .pane-footer .blocks span.group {
        padding: 2px 4px;
        border-right: dotted 1px #ccc;
    }

    .pane-footer input[type=text], .pane-footer input[type=number] {
        text-align: center;
        width: 16px;
        margin: 0px;
        padding: 4px;
    }

    .pane-footer a {margin: 0px 5px; text-decoration: none; color: #069;}
    .pane-footer a:hover {color: #336;}
    .pane-footer .disabled { color: #ccc; }
</style>

<footer class="pane-footer">
    <div class="blocks">

        <?php
        //out of %count% total, starting on record %start%, ending on %end%'
        
        if( isset($url_params) ) {
            $url_params['page'] = 1;
        }
        else {
            $url_params = array('page' => 1);
        }
        
        $url = $this->Paginator->url($url_params);
        
        echo $this->Paginator->counter(array(
            'format' => '<span class="group">Page <input type="text" value="%page%" size="3" data-url="' . $url . '" /> of <b>%pages%</b></span>
                         <span class="group">Show <input type="text" value="%limit%" size="3" /> Records per Page</span> '
        ));
        ?>

        <span class="group">
            
            <?php //echo $this->Paginator->first('« First', array('class' => 'disabled')); ?>
            <?php echo " &laquo;" . $this->Paginator->prev('Prev', $url_params, null, array('class' => 'disabled')); ?>
            <?php echo $this->Paginator->next('Next', $url_params, null, array('class' => 'disabled')) . " &raquo;"; ?> 
            <?php //echo $this->Paginator->last('Last »', array('class' => 'disabled')); ?> 
        </span>
    </div>
</footer>

<script>
    $(document).ready(function() {
        var result_selector = '<?php echo isset($result_area) ? $result_area : ''; ?>';
        var use_ajax = <?php echo isset($ajax) && $ajax ? 'true' : 'false'; ?>;
        var params = <?php echo json_encode($url_params); ?>; 
        
        var doSearch = function(url) {
            
            $.ajax({
                url: url,
                type: 'get',
                data: params,
                success: function(data) {
                    if( result_selector ) {
                        $(result_selector).html(data);
                    }
                    
                    else {
                        $($(".ui-tabs-panel").get( $("#tabs").tabs('option', 'selected') )).html(data);
                    }
                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                }
            }); 
        };
        
        $(".pane-footer input[type=text]").click(function() {
            this.focus();
            this.select()
        }).keydown(function(e) {
            if( e.keyCode == 13 ) {
                var url = $(this).data('url');
                url = url.replace(":1", ":" + $(this).val());
                
                if( use_ajax ) {
                    doSearch(url);
                }
                
                else {
                    window.location.href = url;
                }
            }
        });
        
        $(".pane-footer .blocks a").click(function(e) {
            
            if( use_ajax ) {
                e.preventDefault();

                var url = $(this).attr('href');
                doSearch(url);
            }
        }); 
    });
</script>