<style>
    .dir_tools, .dir_tools select, .dir_tools input {
        font-size: 11px;
        font-family: "Tahoma";
    }

    .dir_tools label {
        display: inline-block;
        width: 65px !important;
        font-size: 11px !important;
        font-weight: bold;
    }

    .dir_tools #search_area {
        width: 150px !important;
    }

    .dir_tools div {
        padding: 3px;
    }

    #results_flyout {
        display: none;
        border: solid #bbb 1px;
        padding: 6px;
        border-radius: 5px;
        box-shadow: 1px 1px 2px #ccc;
        position: absolute;
        left: 250px;
        top: 20%;
        min-width: 400px;
        min-height: 100px;
        background-color: #fff;
        font-size: 11px;
    }

    #results_flyout h3 {
        font-weight: bold;
        border-bottom: solid #ddd 1px;
        padding: 8px 0px;
        background-image: url('<?php echo $html->url('/img/core/search_16x16.png'); ?>');
        background-position: left;
        background-repeat: no-repeat;
        padding-left: 24px;
    }

    #results_flyout #results_pane {
        padding: 5px;
    }

    #results_flyout #results_pane td {
        padding: 4px;
    }

    #results_flyout #results_pane a {
        color: #069;
        text-decoration: none;
    }

    #results_flyout #results_pane a:hover {
        color: #333;
    }

</style>

<h2>Directory Tools</h2>

<div class="dir_tools">

    <div>
        <label for="search_area">Find: </label>
        <select id="search_area">
            <option value="">Select Area</option>
            <option value="Airport">Airport</option>
            <option value="AviationAuthority">Aviation Authority</option>
            <option value="Caterer">Caterer</option>
            <option value="Handler">Handler</option>
            <option value="Hotel">Hotel</option>
        </select>
    </div>

    <div id="filter_area">
        <label>By: </label>
        <select id="filter">
            <option value="">Select Field</option>
            <option value="name">Name</option>
            <option value="telephone">Telephone Number</option>
        </select>
    </div>

    <div id="field_area">
        <label>Search For: </label>
        <input type="text" id="search_field" />
    </div>
</div>

<h2>Weather Reference</h2>
<div class="dir_tools">
    <p>Enter 4-letter ICAO station identifier. If more than one, be sure to insert one or more spaces between stations.
        (Example: KDEN KBOS KORD @KS)</p>
    <div>
        <div><input type="text" placeholder="ICAO" id="station_ids" size="25" /></div>
        <div>
            <b>Format: </b>
            <label for="raw"><input type="radio" name="std_trans" id="raw" checked /> RAW </label>
            <label for="translated" style="width: 100px !important;"><input type="radio" name="std_trans" id="translated" /> Translated</label>
        </div>
        <div style="padding-top: 10px;">
            <input type="button" class="button" id="get_tafs" value="Get TAFs" />
            <input type="button" class="button" id="get_tafs_metars" value="Get TAFs and METARs" />
        </div>
    </div>

    <iframe id="weather_frame" height="1" width="1" style="opacity: 0;" src="about:blank;"></iframe>
</div>

<div class="notams">
    <h2>NOTAMs</h2>
    <p>
        To view NOTAMs, please visit 
        <a href="https://pilotweb.nas.faa.gov/PilotWeb/" target="_blank">Federal Aviation Administration</a>
    </p>
</div>

<input type="hidden" id="dir_tools_fields_url" value="<?php echo $html->url(array('controller' => 'managers', 'action' => 'directory_search_fields')); ?>" />
<input type="hidden" id="dir_tools_search_url" value="<?php echo $html->url(array('controller' => 'managers', 'action' => 'directory_search')); ?>" />

<div id="results_flyout">
    <h3>Search Results</h3>
    <a href="#" class="closer_btn"></a>

    <div id="results_pane"></div>
</div>

<?php echo $html->script('app/directory_tools.js'); ?>