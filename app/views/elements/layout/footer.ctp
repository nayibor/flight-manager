<?php $user = $this->Session->read('user'); ?>

<p class="mid">
    <a href="<?php echo $html->url(array('admin' => true, 'controller' => 'dashboard')); ?>" title="Main Page" class="tooltip">Home</a>&middot;
    <a class="dialog_opener" href="<?php echo $html->url('/admin/users/view/' . $user['User']['id']); ?>" title="Manager Your Profile" class="tooltip">My Account</a>&middot;
    <a href="<?php echo $html->url('/admin/users/logout'); ?>" title="End Session" class="tooltip">Logout</a>
</p>
<p class="mid">
    <!-- Change this to your own once purchased -->
    &copy; <?php echo Configure::read('app_name'); ?> <?php echo date('Y'); ?>, <?php echo Configure::read("company_name"); ?>. All rights reserved.
    <!-- -->
</p>