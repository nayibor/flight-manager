<?php

function address_string($input) {
    if (is_array($input)) {
        $parts = array();

        foreach ($input as $name => $address) {
            if (is_numeric($name)) {
                $parts[] = "$address <$address>";
            } else {
                $parts[] = "$name <$address>";
            }
        }

        return implode(", ", $parts);
    } else {
        return $input;
    }
}
?>

<style>
    .form_table .mail_field {
        font-size: 11px;
        padding: 6px;
        width: 550px;
    }

    .form_table textarea {
        width: 550px;
    }
</style>

<table border="0" cellpadding="8" cellspacing="0" width="100%" class="form_table">

    <tr>
        <td width="80">To:</td>
        <td><input type="text" class="mail_field" size="65" name="to" id="handler_email" required="" value="<?php echo isset($to) ? address_string($to) : ""; ?>" /></td>
    </tr>

    <tr>
        <td>CC:</td>
        <td>
            <input type="text" class="mail_field" size="65" name="cc" id="client_email" value="<?php echo isset($cc) ? address_string($cc) : ""; ?>" placeholder="Name <test@server.com>" />
        </td>
    </tr>
    <tr>
        <td>Subject:</td>
        <td>
            <input type="text" class="mail_field" size="65" name="subject" id="mail_subject" required="" value="<?php echo isset($subject) ? $subject : ""; ?>" />
        </td>
    </tr>

    <tr>
        <td valign="top">
            Message
        </td>
        <td>
            <textarea name="message" id="<?php echo isset($editor_id) ?  $editor_id : 'message'; ?>" style="height: 230px; width: 560px;"><?php echo isset($message) ? $message : ""; ?></textarea>
        </td>
    </tr>
</table>

