<?php
/**
 * This element contains the body needed to complete a request form
 * 
 * @params $permit The Permit data array needed to fill the content of this element 
 */
?>

<style>
    #print_wrapper, #print_wrapper td {
        font-size: 9pt !important;
        line-height: 11pt;
    }
    
    #pdf_wrapper, #pdf_wrapper td {
        font-size: 9pt !important;
        line-height: 11pt;
    }
</style>

 <?php

//print_r($permit['PermitSchedule']);

$date_sch_fwd=array();
$date_scd_string='';
$i=0;
//echo "size---".count($permit['PermitSchedule']); 
foreach($permit['PermitSchedule'] as $val) 
{
 $i++;

if(isset($val['arr_eta']) && $val['arr_eta']!='0000-00-00 00:00:00')
{
$fwform=date('jS M, Y', strtotime($val['arr_eta']));
}


if(count($permit['PermitSchedule'])==$i && ($permit['Permit']['type']== "landing" || $permit['Permit']['type']== "tech stop"))
{
$date_sch_fwd[]= $fwform;
break;
}


if(isset($val['dept_eta']) && $val['dept_eta']!='0000-00-00 00:00:00')
{

if($permit['Permit']['type']!= "landing" && date('jS M, Y', strtotime($val['dept_eta']))!= date('jS M, Y', strtotime($val['arr_eta'])) )
{$fwform.=' / '.date('jS M, Y', strtotime($val['dept_eta']));
}
else if($permit['Permit']['type']== "landing" || $permit['Permit']['type']== "tech stop")
{//$fwform.=' / '.date('jS M, Y', strtotime($val['dept_eta']));
}
}
$date_sch_fwd[]= $fwform;


}


//print_r($date_sch_fwd);
$date_scd_string = implode(" / ",$date_sch_fwd);
//echo ($date_scd_string);

?>  



<div id="authority_address" style="margin: 20px 0px;">
    <table border="0" cellspacing="0" cellpadding="4" width="400">
        <tr>
            <td valign="top"><b>ATTN:</b></td>
            <td colspan="3" id="address"><?php echo isset($permit['AviationAuthority']['address']) ? str_replace("\n", "<br />", $permit['AviationAuthority']['address']) : ""; ?></td>
        </tr>
        <tr>
            <td>TEL:</td>
            <td colspan="3" id="telephone"><?php echo isset($permit['AviationAuthority']['address']) ? $permit['AviationAuthority']['telephone'] : ""; ?></td>
        </tr>
        <tr>
            <td width="10%">FAX:</td>
            <td id="fax"><?php echo isset($permit['AviationAuthority']['address']) ? $permit['AviationAuthority']['fax'] : ""; ?></td>
            <td width="10%" align="right">EMAIL:</td>
            <td id="email"><?php echo isset($permit['AviationAuthority']['address']) ? $permit['AviationAuthority']['email'] : ""; ?></td>
        </tr>
        <tr>
            <td>Date:</td>
            <td id="date"><?php echo isset($permit['PermitStatus'][0]) ? date('jS F, Y', strtotime($permit['PermitStatus'][0]['change_dt'])) : date('jS F, Y'); ?></td>
        </tr>
    </table>
</div>

<section>

    <h3 style="font-weight: bold; color: #222; text-align: left; padding-left: 5px; margin: 15px 0px; text-transform: uppercase;">
        SUBJECT: URGENT <?php echo $permit['Permit']['type']; ?> PERMIT REQUEST FOR THE <?php echo $date_scd_string; ?> .
    </h3>
    
    <div style="text-transform: uppercase; padding-left: 5px; margin: 15px 0px; font-weight: bold;">
        S.B - MAN, RESPECTFULLY REQUESTS <?php echo $permit['Permit']['type']; ?> PERMIT, ON BEHALF OF <?php echo $permit['Permit']['operator_name']; ?> BASED ON THE FOLLOWING DETAILS:
    </div>

    <div class="data_row">
        <table border="0" cellspacing="0" cellpadding="5" width="94%">
            <tr>
                <td width="25%" ><strong>Name of Operator:</strong></td>
                <td colspan="5"><?php echo $permit['Permit']['operator_name']; ?></td>
            </tr>
            <tr>
                <td valign="top"><strong>Address of Operator:</strong></td>
                <td colspan="5"><?php echo $permit['Permit']['operator_addr']; ?></td>
            </tr>
            <tr>
                <td><strong>Aircraft Type:</strong></td>
                <td width="15%" nowrap><?php echo $permit['Permit']['aircraft_type']; ?></td>
                <td width="15%"><strong>Registration:</strong></td>
                <td width="15%"><?php echo $permit['Permit']['aircraft_regn']; ?></td>
                <td width="15%"><strong>Flight No:</strong>: </td>
                <td width="15%"><?php echo $permit['PermitSchedule'][0]['arr_flight_no']; ?></td>
            </tr>

            <tr>
                <td width="25%" valign="top"><strong>Estimated Date & Time of Arrival:</strong></td>
                <td colspan="5" valign="top"><?php echo $this->element('permits/schedule_format', array('permit' => $permit)); ?></td>
            </tr>
            <tr>
                <td valign="top"><strong>Point of Departure & Destination:</strong></td>
                <td colspan="5" valign="top">
                    <?php
                    foreach($permit['PermitSchedule'] as $schedule) { 
                        echo "<div>" . $schedule['arr_origin'] . " - " . $schedule['arr_dest'] . " - " . $schedule['dept_dest'] . "</div>"; 
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td nowrap valign="top"><strong>Pilot's Name / Nationality:</strong></td>
                <td colspan="2">
                    <?php echo $permit['Permit']['captain_name']; ?>, 
                    <?php echo $permit['Permit']['captain_nationality']; ?>. 
                </td>
                <td colspan="2"><strong>Crew On Board: </strong></td>
                <td><?php echo $permit['Permit']['crew_on_board']; ?></td>
            </tr>
            <?php if (stristr($permit['Permit']['type'], 'landing') != false) { ?>
                <tr>
                    <td><strong>Receiving Party:</strong></td>
                    <td colspan="5"><?php echo $permit['Permit']['receiving_party']; ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td valign="top"><strong>Flight Purpose:</strong></td>
                <td colspan="2"><?php echo str_replace("\n", "<br / >", $permit['Permit']['flight_purpose']); ?></td>
                <td colspan="2"><strong>Souls On Board:</strong></td>
                <td><?php echo $permit['Permit']['souls_on_board']; ?></td>
            </tr>
            <tr>
                <td valign="top"><strong>Type of Cargo On Board:</strong></td>
                <td colspan="2" valign="top"><?php echo $permit['Permit']['cargo_type']; ?></td>
                <td colspan="2" valign="top"><strong>Type of Ammunition:</strong></td>
                <td valign="top"><?php echo $permit['Permit']['arms_type']; ?></td>
            </tr>
            <tr>
                <td><strong>Routes:</strong></td>
                <td colspan="5">APPROVED ATS ROUTES<?php //echo $permit['Permit']['flight_routes']; ?></td>
            </tr>
            <tr>
                <td valign="top"><strong>Other Information:</strong></td>
                <td colspan="5">
                    <!--<div>72 HRS LEEWAY INCASE OF DELAYED FLIGHT</div>-->
                    <div><?php echo str_replace("\n", "<br />", $permit['Permit']['misc_info']); ?></div>
                </td>
            </tr>
            <tr>
                <td><strong>Base of Aircraft:</strong></td>
                <td colspan="5"><?php echo $permit['Permit']['aircraft_base']; ?></td>
            </tr>
        </table>
    </div>

</section>

<?php if (isset($supervisor)) { ?>
    <footer style="margin-top: 25px; text-align: left;">
        <table width="100%" border="0" cellspacing="0" cellpadding="6">
            <tr>
                <td width="50%" style="text-transform: uppercase;">
                    B/RGDS - <?php echo $permit['PermitStatus'][0]['User']['first_name'] . " " . $permit['PermitStatus'][0]['User']['last_name']; ?> / OPS CONTROL
                </td>

                <td style="text-transform: uppercase;">
                    <?php echo isset($supervisor) ? $supervisor['User']['first_name'] . ' ' . $supervisor['User']['last_name'] : "-----"; ?> / OPS SUPERVISOR
                </td>
            </tr>

            <tr>
                <td>
                    Ref.No:- <?php echo $permit['Permit']['ref_no']; ?>
                </td>
            </tr>
        </table>

        <div style="padding-top: 15px; font-size: 10px;">
            <span style="text-transform: uppercase;">S.B.MAN & CO LTD - <?php echo $permit['Permit']['type']; ?> CLEARANCE APPLICATION FORM </span>
        </div>
    </footer>
<?php } ?>