<style type="text/css">
    .forward_details {
        border: double #333 3px;
        padding: 5px 15px;
        margin: 10px 0px;
    }

    .flyout_content td {
        padding: 5px;
    }

    .inline-bold {
        display: inline;
        font-weight: bold;
        border-bottom: dotted #999 1px;
    }

    .ordered_list {
        list-style-type: decimal;
        padding-left: 10px;
        margin: 5px;
        margin-left: 20px;
    }
    
    .flyout_content p {
        color: #111;
    }

    .flyout_content section, .flyout_content section td {
        margin-bottom: 10px;
        font-family:  Tahoma, Geneva, sans-serif;
        font-size: 13px;
    }

    .flyout_content section header {
        padding: 5px;
        margin-bottom: 10px;
        text-align: center;
    }

    .flyout_content section header h3 {
        text-transform: uppercase;
        font-weight: bold;
        font-size: 14px;
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif !important;
    }

    #pdf_wrapper, #pdf_wrapper td, #pdf_wrapper p {
        font-size: 10pt !important;
    }

    #pdf_wrapper #caution {
        font-size: 9pt !important;
    }

    #pdf_wrapper #caution ol {
	 margin: 4px;
	 padding: 4px;
	 padding-left: 28px;
    }
    
    #print_wrapper, #print_wrapper td, #print_wrapper p {
        font-size: 10pt !important;
        line-height: 14px;
    }

    #pdf_wrapper #caution {
        font-size: 10pt !important;
	  line-height: 14px;	
    }

    #print_wrapper #caution ol {
	 margin: 4px;
	 padding: 4px;
	 padding-left: 28px;
    }

</style>

<?php
/**
 * This element contains the body needed to complete a request form
 * 
 * @params $permit The Permit data array needed to fill the content of this element 
 */
?>

<style media="print">
    #print_wrapper {
        font-size: 10pt !important;
        line-height: 14pt;
    }
    
    #pdf_wrapper {
        font-size: 10pt !important;
        line-height: 14pt;
    }
</style>

 <?php

//print_r($permit['PermitSchedule']);

$date_sch_fwd=array();
$date_scd_string='';
$i=0;
//echo "size---".count($permit['PermitSchedule']); 
foreach($permit['PermitSchedule'] as $val) 
{
 $i++;

if(isset($val['arr_eta']) && $val['arr_eta']!='0000-00-00 00:00:00')
{
$fwform=date('jS M, Y', strtotime($val['arr_eta']));
}


if(count($permit['PermitSchedule'])==$i && $permit['Permit']['type']== "landing")
{
$date_sch_fwd[]= $fwform;
break;
}


if(isset($val['dept_eta']) && $val['dept_eta']!='0000-00-00 00:00:00')
{

if($permit['Permit']['type']!= "landing" && date('jS M, Y', strtotime($val['dept_eta']))!= date('jS M, Y', strtotime($val['arr_eta'])) )
{$fwform.=' / '.date('jS M, Y', strtotime($val['dept_eta']));
}
else if($permit['Permit']['type']== "landing")
{//$fwform.=' / '.date('jS M, Y', strtotime($val['dept_eta']));
}
}
$date_sch_fwd[]= $fwform;


}


//print_r($date_sch_fwd);
$date_scd_string = implode(" / ",$date_sch_fwd);
//echo ($date_scd_string);

?>  


   



<div class="forward_details">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td width="20%">Tel No:</td>
            <td width="25%"><?php echo $permit['Permit']['company_tel']; ?></td>
            <td width="20%">Our File Ref:</td>
            <td width="25%"><?php echo $permit['Permit']['ref_no']; ?></td>
        </tr>
        <tr>
            <td valign="top">Company Sent To:</td>
            <td valign="top"><?php echo $permit['Permit']['company_name']; ?></td>
            <td valign="top">Purchase Order No.:</td>
            <td valign="top"><?php echo $permit['Permit']['company_po_no']; ?></td>
        </tr>
        <tr>
            <td>For The Attn:</td>
            <td><?php echo $permit['Permit']['company_contact']; ?></td>
            <td>Date:</td>
            <td><?php echo date('jS F, Y'); ?></td>
        </tr>
        <tr>
            <td>Originator:</td>
            <td colspan="2">S. B. MAN OPS CONTROL</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>

<header>
    <h3><?php echo ucfirst($permit['Permit']['type']); ?> Permission</h3>
</header>  

<section>
    <p>As per C.A.A File Ref: <span class="inline-bold"><?php echo $permit['Permit']['permit_ref_no']; ?></span></p>

    <p>Ref. Your request for 
        <span class="inline-bold" style="text-transform: capitalize;">
            <?php echo $permit['Permit']['type']; // == "landing" ? "Tech. Landing" : "Overflight"; ?>
        </span> 

        <?php echo stristr($permit['Permit']['type'], "overflight") != false ? "Permission Over " : "Permission In "; ?>

        <span class="inline-bold">
            <?php echo $permit['Permit']['permit_location']; ?>
        </span> 
        dated 
        <span class="inline-bold">
            <?php echo date('jS M, Y', strtotime($permit['Permit']['application_dt']));  ; ;  ?>
        </span>. 
    </p>

    <div style="padding: 10px;">
        <table width="90%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
                <td width="27%">Aircraft Type:</td>
                <td width="73%"><?php echo $permit['Permit']['aircraft_type']; ?></td>
            </tr>
            <tr>
                <td>Aircraft Registration:</td>
                <td><?php echo $permit['Permit']['aircraft_regn']; ?></td>
            </tr>
            <tr>
                <td>Flight Date:</td>
                <td><?php echo $date_scd_string; ?></td>
            </tr>
            <tr>
                <td>Route:</td>
                <td><?php echo $permit['Permit']['flight_routes']; ?></td>
            </tr>
            <tr>
                <td colspan="2" style="height: 40px;" valign="bottom">
                    <b>Approval Has Been Granted To The Above Aircraft</b>
                </td>
            </tr>
            <tr>
                <td><?php echo strpos($permit['Permit']['type'], "overflight") === false ? "L/C" : "O/C"; ?> Permit No.: </td>
                <td><?php echo $permit['Permit']['permit_number']; ?></td>
            </tr>
            <tr>
                <td>Validity:</td>
                <td><?php echo $permit['Permit']['permit_validity']; ?></td>
            </tr>
        </table>
    </div>

</section>

<?php if (isset($staff)) { ?>
    <footer style="margin-top: 20px; margin-bottom: 15px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="10">
            <tr>
                <td colspan="2">
                    MOST KIND RGDS - 
                </td>
            </tr>
            <tr>
                <td width="50%" style="text-transform: uppercase;"><?php echo $staff['User']['first_name'] . " " . $staff['User']['last_name']; ?></td>
            </tr>
            <tr>
                <td colspan="2" style="text-transform: uppercase;">
                    <?php echo $supervisor ? $supervisor['User']['first_name'] . ' ' . $supervisor['User']['last_name'] : "-----"; ?> / SUPERVISOR - OPS CONTROL
                </td>
            </tr>

            <tr>
                <td>
                    Ref.No:- <?php echo $permit['Permit']['ref_no']; ?>
                </td>
            </tr>
        </table>

        <div id="caution" style="padding-top: 15px; font-size: 11px; ">
            Caution Statement:- 
		<ol type="a">
            <li>Please notify us immediately there is the need to change aircraft mentioned in confirmation or change of route.</li>
            <li>Please notify us if permit expires before its usage. Do not attempt to use an expired permit.</li>
		</ol>
            
            (HQ) P. O. Box OS 1734, Osu - Accra Ghana. 
            Tel: +233 24 4321 771(H24) Email:fltops@sbmangh.com 	 SITA/ARINC: ACCSM7X       AFTN: KACCGCKX   www.sbmangh.com <br /><br />

            <span style="text-transform: uppercase;">S.B.MAN & CO LTD - <?php echo $permit['Permit']['type']; ?> CLEARANCE FORWARDING FORM </span>

        </div>
    </footer>
<?php } ?>