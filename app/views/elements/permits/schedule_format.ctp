
<?php foreach ($permit['PermitSchedule'] as $schedule) { ?>
    <div style="text-transform: uppercase; margin-bottom: 5px;">
        <?php echo $schedule['arr_flight_no'] ?>
        <?php echo strstr($schedule['arr_etd'], "0000") == false ? date("dMy", strtotime($schedule['arr_etd'])) : "TBA"; ?>
        <?php echo $schedule['arr_origin'] ?>
        <?php echo strstr($schedule['arr_etd'], "00:00:00") == false ? date("Hi", strtotime($schedule['arr_etd'])) . 'Z' : "TBA"; ?>
        -
        <?php echo strstr($schedule['arr_eta'], "0000") == false ? date("dMy", strtotime($schedule['arr_eta'])) : "TBA"; ?>
        <?php echo $schedule['arr_dest'] ?>
        <?php echo strstr($schedule['arr_eta'], "00:00:00") == false ? date("Hi", strtotime($schedule['arr_eta'])) . 'Z' : "TBA"; ?>

        <br />

        <?php if( strstr($schedule['dept_etd'], "0000") == false || strstr($schedule['dept_eta'], "0000") == false ) { ?>
            <?php echo $schedule['dept_flight_no'] ?>
            <?php echo strstr($schedule['dept_etd'], "0000") == false ? date("dMy", strtotime($schedule['dept_etd'])) : "TBA"; ?>
            <?php echo $schedule['dept_origin'] ?>
            <?php echo strstr($schedule['dept_etd'], "00:00:00") == false ? date("Hi", strtotime($schedule['dept_etd'])) . 'Z' : "TBA"; ?>

            <?php if( strstr($schedule['dept_eta'], "0000") == false ) { ?>
                -
                <?php echo strstr($schedule['dept_eta'], "0000") == false ? date("dMy", strtotime($schedule['dept_eta'])) : "TBA"; ?>
                <?php echo $schedule['dept_dest'] ?>
                <?php echo strstr($schedule['arr_etd'], "00:00:00") == false ? date("Hi", strtotime($schedule['dept_eta'])) . 'Z' : "TBA"; ?>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>