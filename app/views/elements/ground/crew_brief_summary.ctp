<?php
$user = $this->Session->read('user');

$grss = ClassRegistry::init('GroundRequestScheduleService');
$grs = ClassRegistry::init('GroundRequestSchedule');
$gr = ClassRegistry::init('GroundRequest');
$handler = ClassRegistry::init('Handler');
$arpf = ClassRegistry::init('AirportFrequency');


if ((isset($options)) && (isset($options['schedule_id']))) {
    $schedule_id = $options['schedule_id'];
}

$crewbrief_array = array();
$crew_data = $grss->find('all', array(
    'conditions' => array('GroundRequestScheduleService.ground_request_schedule_id' => $schedule_id)
        ));

$scdata = $grs->find('first', array(
    'conditions' => array('GroundRequestSchedule.id' => $schedule_id)
        ));
$crewbrief_array['schedule_data'] = $scdata['GroundRequestSchedule'];

$rqdata = $gr->find('first', array(
    'conditions' => array('GroundRequest.id' => $scdata['GroundRequestSchedule']['ground_request_id'])
        ));
$crewbrief_array['request_data'] = $rqdata['GroundRequest'];

$hddata = $handler->find('first', array(
    'conditions' => array('Handler.id' => $scdata['GroundRequestSchedule']['handler_id'])
        ));
$crewbrief_array['handler_data'] = $hddata['Handler'];


//for finding the freqency of airport
$dest_icao = explode("/", $scdata['GroundRequestSchedule']['arr_dest']);
$freq_ap = $arpf->find('first', array(
    'conditions' => array(
        "AirportFrequency.airport_ident" => $dest_icao[0],
        "AirportFrequency.type" => 'TWR'
    )
        ));

$crewbrief_array['freq_arp'] = sizeof($freq_ap != 0) ? $freq_ap['AirportFrequency']['frequency_mhz'] : "";

if (isset($crew_data[0])) {

    //for getting all the services and puttin them in an array
    foreach ($crew_data as $val) {
        if (stristr($val['Service']['name'], 'Permit') != false) {
            $crewbrief_array['Permits'][] = $val['GroundRequestScheduleService'];
        } else if (stristr($val['Service']['name'], 'Transport') != false && stristr($val['Service']['name'], 'Ramp') === false) {
            $crewbrief_array['Transport'][] = $val['GroundRequestScheduleService'];
        } else {
            $crewbrief_array[$val['Service']['name']] = $val['GroundRequestScheduleService'];
        }

        $crewbrief_array['schedule_data'] = $val['GroundRequestSchedule'];
    };
}
?>


<style>
    .brief_form {
        padding: 5px;
    }

    .brief_form table td {
        padding: 4px;
    }

    .brief_form table td.header {
        border-bottom: dotted #ccc 1px;
        font-weight: bold;
        padding-top: 10px;
    }

    #pdf_wrapper, #pdf_wrapper td { font-size: 8pt !important; }    
    #pdf_wrapper .brief_form table td { padding: 1.5px !important; line-height: 11.5pt; }
    #pdf_wrapper .brief_form table .header { padding-top: 3px !important; }

    #print_wrapper, #print_wrapper td { font-size: 8pt !important; font-family: 'Tahoma'}
    #print_wrapper .brief_form table td { padding: 1.5px !important; line-height: 11.5pt; }
    #print_wrapper .brief_form table .header { padding-top: 3px !important; }
</style>

<style media="print">
    #print_wrapper, #print_wrapper td { font-size: 7.5pt !important; font-family: 'Tahoma'}
    #print_wrapper .brief_form table td { padding: 1.5px !important; line-height: 11.5pt; }
</style>

<div class="brief_form">

    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr>
            <td colspan="7"><b>CREW FLIGHT BRIEFING</b></td>
        </tr>
        <tr>
            <td>Ref.No:-</td>
            <td colspan="5"><?php echo $crewbrief_array['schedule_data']['ref_no']; ?></td>
        </tr>
        <tr>
            <td nowrap>Date & Time:</td>
            <td colspan="6"><?php echo date('jS M Y, h:i A'); ?></td>
        </tr>
        <tr>
            <td colspan="10" style="padding: 0px; font-size: 8px;">&nbsp;</td>
        </tr>
        <tr>
            <td>ATTN:</td>
            <td colspan="3"><?php echo $crewbrief_array['request_data']['company_contact']; ?></td>
            <td></td>
            <td></td>
            <td>FROM:</td>
            <td colspan="3">SBMAN OPS - <?php echo $user['User']['last_name']; ?></td>
        </tr>
        <tr>
            <td>COMPANY:</td>
            <td colspan="3"><?php echo $crewbrief_array['request_data']['company_name']; ?></td>
            <td></td>
            <td></td>
            <td>PHONE:</td>
            <td colspan="3">+233 24 4321771</td>
        </tr>
        <tr>
            <td>PHONE:</td>
            <td colspan="3"><?php echo $crewbrief_array['request_data']['company_phone']; ?></td>
            <td></td>
            <td></td>
            <td>FAX:</td>
            <td colspan="3">+1 888 826 1781</td>
        </tr>
        <tr>
            <td>FAX:</td>
            <td colspan="3"><?php echo $crewbrief_array['request_data']['company_fax']; ?></td>
            <td></td>
            <td></td>
            <td>E-MAIL:</td>
            <td colspan="3"><a href="mailto:fltops@sbmangh.com">fltops@sbmangh.com</a></td>
        </tr>
        <tr>
            <td>E-MAIL:</td>
            <td colspan="3"><a href="mailto:<?php echo $crewbrief_array['request_data']['company_email']; ?>"></a><?php echo $crewbrief_array['request_data']['company_email']; ?></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="10" rowspan="2" dir="ltr" style="padding: 12px 3px;">
                CAUTION    STATEMENT: IF THIS BRIEFING IS NOT INTENDED FOR YOU, USING ITS CONTENTS IN    ANY WAY IS STRICTLY PROHIBITED.
            </td>
        </tr>
        <tr> </tr>
        <tr>
            <td colspan="2">AIRCRAFT OPERATOR :</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['operator']); ?></td>
            <td colspan="2">PURPOSE OF FLT.:</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['purpose']); ?></td>
        </tr>
        <tr>
            <td colspan="2">AIRCRAFT TYPE:</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['type']); ?></td>
            <td colspan="2">NATURE OF CARGO :</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['nature_cargo']); ?></td>
        </tr>
        <tr>
            <td colspan="2">NAMEOF CAPTAIN:</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['name_captain']); ?></td>
            <td colspan="2">AIRCRAFT M.T.O.W:</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['mtow']); ?></td>
        </tr>
        <tr>
            <td colspan="2">LEAD PASSENGER:</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['lead_passenger']); ?></td>
            <td colspan="2">AIRCRAFT REGN.:</td>
            <td colspan="3"><?php echo strtoupper($crewbrief_array['request_data']['region']); ?></td>
        </tr>

        <tr>
            <td colspan="10" class="header">SCHEDULE</td>
        </tr>
        <tr>
            <td>FLT NO.</td>
            <td>ORIGIN</td>
            <td>DEST.</td>
            <td>ETD</td>
            <td>ETA</td>
            <td nowrap>CGO WGT</td>
            <td>PAX NO</td>
        </tr>
        <tr>
            <td nowrap><?php echo strtoupper($crewbrief_array['schedule_data']['arr_flight_num']); ?></td>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_origin']); ?></td>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
            <td nowrap><?php echo strtoupper(date("dMy - Hi e", strtotime($crewbrief_array['schedule_data']['arr_etd']))); ?></td>
            <!--<td nowrap><?php echo strtoupper(date("dMy", strtotime($crewbrief_array['schedule_data']['arr_etd']))); ?></td>-->
            <td nowrap><?php echo strtoupper(date("dMy - Hi e", strtotime($crewbrief_array['schedule_data']['arr_eta']))); ?></td>
            <!--<td nowrap><?php echo strtoupper(date("dMy", strtotime($crewbrief_array['schedule_data']['arr_eta']))); ?></td>-->
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_cargo']); ?></td>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_pax_no']); ?></td>
        </tr><?php ?>
        <tr>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['dept_flight_num']); ?></td>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['dept_origin']); ?></td>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['dept_dest']); ?></td>
            <td nowrap><?php echo strtoupper(date("dMy - Hi e", strtotime($crewbrief_array['schedule_data']['dept_etd']))); ?></td>
            <!--<td nowrap><?php echo strtoupper(date("dMy", strtotime($crewbrief_array['schedule_data']['dept_etd']))); ?></td>-->
            <td nowrap><?php echo strtoupper(date("dMy - Hi e", strtotime($crewbrief_array['schedule_data']['dept_eta']))); ?></td>
            <!--<td nowrap><?php echo strtoupper(date("dMy", strtotime($crewbrief_array['schedule_data']['dept_eta']))); ?></td>-->
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['dept_cargo']); ?></td>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['dept_pax_no']); ?></td>

        </tr>

        <tr>
            <td colspan="10" class="header">HANDLER    AND AIRPORT INFORMATION</td>
        </tr>
        <tr>
            <td>AIRPORT</td>
            <td colspan="2">HANDLER</td>
            <td colspan="3">TEL/FAX/SITA/EMAIL</td>
            <td>F'QUENCY</td>
            <td>PAYMENT</td>
            <td colspan="2">REMARKS</td>
        </tr>
        <tr>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
            <td colspan="2"><?php echo "SBMAN&CO"; ?></td>
            <td colspan="3"><?php echo "00233 24 432 1771/ACCSM7X"; ?></td>
            <td><?php echo strtoupper(($crewbrief_array['freq_arp'] != "") ? $crewbrief_array['freq_arp'] . " mhz " : ""); ?></td>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['airport_fees_on']); ?></td>
            <td colspan="2"><?php echo $crewbrief_array['schedule_data']['handling_remarks']; ?></td>
        </tr>

        <tr>
            <td colspan="10" class="header">FLIGHT    CATERING</td>
        </tr>
        <tr>
            <td>AIRPORT</td>
            <td colspan="2">Catering</td>
            <td colspan="3">TEL/FAX/SITA/EMAIL</td>
            <td>PAYMENT</td>
            <td colspan="3">REMARKS</td>
        </tr>
        <tr>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
            <td colspan="2"><?php echo isset($crewbrief_array['Catering']) ? $crewbrief_array['Catering']['name'] : "N/A"; ?></td>
            <td colspan="3"><?php echo isset($crewbrief_array['Catering']) ? $crewbrief_array['Catering']['contact_number'] : "N/A"; ?></td>
            <td><?php echo isset($crewbrief_array['Catering']) ? $crewbrief_array['Catering']['payment_type'] : "N/A"; ?></td>
            <td colspan="3"><?php echo isset($crewbrief_array['Catering']) ? $crewbrief_array['Catering']['remarks'] : "N/A"; ?></td>
        </tr>

        <tr>
            <td colspan="10" class="header">FUEL</td>
        </tr>
        <tr>
            <td>AIRPORT</td>
            <td colspan="2">INTO-PLANE</td>
            <td>CONTACT</td>
            <td>PAYMENT</td>
            <td colspan="2">REMARKS</td>
        </tr>
        <tr>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
            <td colspan="2"><?php echo isset($crewbrief_array['Fuel']) ? $crewbrief_array['Fuel']['remarks'] : "N/A"; ?></td>
            <td ><?php echo isset($crewbrief_array['Fuel']) ? $crewbrief_array['Fuel']['contact_number'] : "N/A"; ?></td>
            <td><?php echo isset($crewbrief_array['Fuel']) ? $crewbrief_array['Fuel']['payment_type'] : "N/A"; ?></td>
            <td><?php echo isset($crewbrief_array['Fuel']) ? $crewbrief_array['Fuel']['remarks'] : "N/A"; ?></td>
        </tr>

        <tr>
            <td colspan="10" class="header">HOTEL</td>
        </tr>
        <tr>
            <td>AIRPORT</td>
            <td>CITY</td>
            <td colspan="2">NAME</td>
            <td colspan="2">TEL/FAX/EMAIL</td>
            <td nowrap>RSVN. NO.</td>
            <td>RM TYPE</td>
            <td>ROOMS</td>
            <td>PAYMENT</td>
        </tr>
        <tr>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
            <td><?php echo isset($crewbrief_array['Hotel']) ? $crewbrief_array['Hotel']['hotel_reservation_city'] : "N/A"; ?></td>
            <td colspan="2"><?php echo isset($crewbrief_array['Hotel']) ? $crewbrief_array['Hotel']['name'] : "N/A"; ?></td>
            <td colspan="2"><?php echo isset($crewbrief_array['Hotel']) ? $crewbrief_array['Hotel']['contact_number'] : "N/A"; ?></td>
            <td><?php echo isset($crewbrief_array['Hotel']) ? $crewbrief_array['Hotel']['hotel_reservation_no'] : "N/A"; ?></td>
            <td><?php echo isset($crewbrief_array['Hotel']) ? $crewbrief_array['Hotel']['hotel_reservation_rm_type'] : "N/A"; ?></td>
            <td><?php echo isset($crewbrief_array['Hotel']) ? $crewbrief_array['Hotel']['hotel_reservation_rooms'] : "N/A"; ?></td>
            <td><?php echo isset($crewbrief_array['Hotel']) ? $crewbrief_array['Hotel']['payment_type'] : "N/A"; ?></td>
        </tr>

        <tr>
            <td colspan="10" class="header">PERMITS</td>
        </tr>
        <tr>
            <td>TERRITORY</td>
            <td colspan="2">PERMIT NO.</td>
            <td>LANDING</td>
            <td>OVERFLIGHT</td>
            <td>VALIDITY</td>
            <td colspan="3">REMARKS</td>
        </tr>
        <?php
        if (isset($crewbrief_array['Permits'])) {

            foreach ($crewbrief_array['Permits'] as $permit) {
                ?>
                <tr>
                    <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
                    <td colspan="2"><?php echo isset($permit) ? $permit['permit_no'] : "N/A"; ?></td>
                    <td><?php echo isset($permit) ? $permit['permit_landing'] : "N/A"; ?></td>
                    <td><?php echo isset($permit) ? $permit['permit_overflight'] : "N/A"; ?></td>
                    <td><?php echo isset($permit) ? $permit['permit_validity'] : "N/A"; ?></td>
                    <td colspan="3"><?php echo isset($permit) ? $permit['remarks'] : "N/A"; ?></td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
                <td colspan="2">N/A</td>
                <td>N/A</td>
                <td>N/A</td>
                <td>N/A</td>
                <td colspan="3">N/A</td>
            </tr>
            <?php
        }
        ?>

        <tr>
            <td colspan="10" class="header">TRANSPORT</td>
        </tr>
        <tr>
            <td>TERRITORY</td>
            <td colspan="2" nowrap>COMPANY NAME</td>
            <td colspan="2" >TEL/FAX/EMAIL</td>
            <td colspan="2" >VEHICLE TYPE</td>
            <td colspan="2" >DRIVER NAME</td>
            <td colspan="1">PAYMENT</td>
        </tr>
        <?php
        if (isset($crewbrief_array['Transport'])) {
            foreach ($crewbrief_array['Transport'] as $transport) {
                ?>
                <tr>
                    <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
                    <td colspan="2" ><?php echo isset($transport['name']) ? $transport['name'] : "N/A"; ?></td>
                    <td colspan="2" ><?php echo isset($transport['contact_number']) ? $transport['contact_number'] : "N/A"; ?></td>
                    <td colspan="2" ><?php echo isset($transport['transport_vehicle_type']) ? $transport['transport_vehicle_type'] : "N/A"; ?></td>
                    <td colspan="2" ><?php echo isset($transport['transport_driver_name']) ? $transport['transport_driver_name'] : "N/A"; ?></td>
                    <td colspan="1"><?php echo isset($transport['payment_type']) ? $transport['payment_type'] : "N/A"; ?></td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>

                <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
                <td colspan="2" >N/A</td>
                <td colspan="2" >N/A</td>
                <td colspan="2" >N/A</td>
                <td colspan="2" >N/A</td>
                <td colspan="1">N/A</td>
            </tr> 
            <?php
        }
        ?>

        <tr>
            <td colspan="10" class="header">SECURITY</td>
        </tr>
        <tr>
            <td>TERRITORY</td>
            <td colspan="2" nowrap>COMPANY NAME</td>
            <td colspan="2" >TEL/FAX/EMAIL</td>
            <td colspan="2">PAYMENT</td> 
            <td colspan="3" >REMARKS</td>

        </tr>
        <tr>
            <td><?php echo strtoupper($crewbrief_array['schedule_data']['arr_dest']); ?></td>
            <td colspan="2" ><?php echo isset($crewbrief_array['Security']) ? $crewbrief_array['Security']['name'] : "N/A"; ?></td>
            <td colspan="2" ><?php echo isset($crewbrief_array['Security']) ? $crewbrief_array['Security']['contact_number'] : "N/A"; ?></td>
            <td colspan="2"><?php echo isset($crewbrief_array['Security']) ? $crewbrief_array['Security']['payment_type'] : "N/A"; ?></td>
            <td colspan="3" ><?php echo isset($crewbrief_array['Security']) ? $crewbrief_array['Security']['remarks'] : "N/A"; ?></td>
        </tr>
    </table>
</div>

<?php
$no_docs_locs = array('DIAP', 'FKYS', 'FKKD', 'FPST', 'GLRB', 'GFLL', 'GAGO', 'GABS', 'DAAT', 'DAAT/TMR', 'DXXX', 'DAOO');
$airport = strtoupper($crewbrief_array['schedule_data']['arr_dest']);

if (in_array($airport, $no_docs_locs)) {
    ?>
    <P style="font-size: 10px; text-align: center; margin-top: 10px;">
        <b>PLEASE NOTE THAT 3RD PARTY SUPPORTING DOCUMENTS AT THIS LOCATION CAN ONLY BE PROVIDED IN NOT LESS THAN 30 DAYS</b>
    </P>
<?php } ?>

<input type="hidden" name="crewbrief_print_url" id="crewbrief_print_url" value="<?php echo $html->url(array('controller' => 'ground', 'action' => 'crew_brief_print', $crewbrief_array['schedule_data']['id'])); ?>"  />