<div>
    <?php echo $schedule['GroundRequestSchedule']['arr_flight_num'] ?>
    <?php echo strstr($schedule['GroundRequestSchedule']['arr_etd'], "0000") == false ? date("dMy", strtotime($schedule['GroundRequestSchedule']['arr_etd'])) : "TBA"; ?>
    <?php echo $schedule['GroundRequestSchedule']['arr_origin'] ?>
    <?php echo strstr($schedule['GroundRequestSchedule']['arr_etd'], "00:00:00") == false ? date("Hi", strtotime($schedule['GroundRequestSchedule']['arr_etd'])) . 'Z' : "TBA"; ?>
    -
    <?php echo strstr($schedule['GroundRequestSchedule']['arr_eta'], "0000") == false ? date("dMy", strtotime($schedule['GroundRequestSchedule']['arr_eta'])) : "TBA"; ?>
    <?php echo $schedule['GroundRequestSchedule']['arr_dest'] ?>
    <?php echo strstr($schedule['GroundRequestSchedule']['arr_eta'], "00:00:00") == false ? date("Hi", strtotime($schedule['GroundRequestSchedule']['arr_eta'])) . 'Z' : "TBA"; ?>

    <br />

    <?php if (strstr($schedule['GroundRequestSchedule']['dept_etd'], "0000") == false || strstr($schedule['GroundRequestSchedule']['dept_eta'], "0000") == false) { ?>
        <?php echo $schedule['GroundRequestSchedule']['dept_flight_num'] ?>
        <?php echo strstr($schedule['GroundRequestSchedule']['dept_etd'], "0000") == false ? date("dMy", strtotime($schedule['GroundRequestSchedule']['dept_etd'])) : "TBA"; ?>
        <?php echo $schedule['GroundRequestSchedule']['dept_origin'] ?>
        <?php echo strstr($schedule['GroundRequestSchedule']['dept_etd'], "00:00:00") == false ? date("Hi", strtotime($schedule['GroundRequestSchedule']['dept_etd'])) . 'Z' : "TBA"; ?>

        <?php if (strstr($schedule['GroundRequestSchedule']['dept_eta'], "0000") == false) { ?>
            -
            <?php echo strstr($schedule['GroundRequestSchedule']['dept_eta'], "0000") == false ? date("dMy", strtotime($schedule['GroundRequestSchedule']['dept_eta'])) : "TBA"; ?>
            <?php echo $schedule['GroundRequestSchedule']['dept_dest'] ?>
            <?php echo strstr($schedule['GroundRequestSchedule']['dept_etd'], "00:00:00") == false ? date("Hi", strtotime($schedule['GroundRequestSchedule']['dept_eta'])) . 'Z' : "TBA"; ?>
        <?php } ?>
    <?php } ?>
</div>