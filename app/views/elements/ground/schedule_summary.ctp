<?php 
$status = "pending";

if( $schedule['GroundRequestSchedule']['arr_ata'] != "" && strpos($schedule['GroundRequestSchedule']['arr_ata'], "00-00") === false ) {
    $status = "arrived";
}

if( $schedule['GroundRequestSchedule']['dept_atd'] != "" && strpos($schedule['GroundRequestSchedule']['dept_atd'], "00-00") === false  ) {
    $status = "departed";
}

if( $schedule['GroundRequestSchedule']['status'] == "cancelled" ) {
    $status = $schedule['GroundRequestSchedule']['status'];
}

?>

<style>
    .pending .dates {color: black;}
    .arrived .dates {color: #cc6600;}
    .departed .dates {color: green; }
    .cancelled .dates {color: red; }
</style>

<div class="schedule <?php echo $status; ?>">
    <h4 class="divider collapsed">

        <a href="#" class="expander">&nbsp;</a>

        <span class="dates">
            <?php echo $this->element('ground/schedule_format', array('schedule' => $schedule)); ?>
        </span>

        <?php //if ( $schedule['GroundRequest']['jobsheet_status'] == "open" ) { ?>
            <span class="options">
                <span style="font-weight: bold; display: inline-block; width: 75px;"><?php echo $schedule['GroundRequestSchedule']['ref_no'];?></span>
                
                <span class="menu_button menu_button_schedule">
                    <a href="#" id="menu_butt">Actions</a>

                    <ul class="menu" tabindex="15">
                        <li data-status="open" class="open_only flyout_opener" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_schedule_edit?req_id=' . $schedule['GroundRequestSchedule']['id'])); ?>" data-load-callback="Services.init">Edit Schedule</li>
                        <li data-status="open" class="open_only cancel_link" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_schedule_cancel', $schedule['GroundRequestSchedule']['id'])); ?>">Cancel Schedule</li>
                        <li class="open_only divider" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                        <li id="send_handler" data-status="open" class="open_only flyout_opener" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'send_handler', $schedule['GroundRequestSchedule']['id'])); ?>">Send To Handler</li>
                        <li id="update_actuals" data-status="open" class="open_only closed_only flyout_opener" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'schedule_actuals_update', $schedule['GroundRequestSchedule']['id'])); ?>">Update ATA/ATD</li>
                        <li id="re-open" data-status="open" class="closed_only canceled_only" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'admin_schedule_open', $schedule['GroundRequestSchedule']['id'])); ?>">Re-Instate Request</li>
                        <li class="divider" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                        <li data-status="open" class="flyout_opener" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'request_template', $schedule['GroundRequestSchedule']['id'])); ?>">View Handling Request</li>
                        <li data-status="open" class="flyout_opener" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'crew_brief', $schedule['GroundRequestSchedule']['id'])); ?>">View Crew Brief</li>
                        <li data-status="open" class="flyout_opener" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'schedule_jobsheet', $schedule['GroundRequestSchedule']['id'])); ?>">View JobSheet</li>
                        <li data-status="open" class="flyout_opener" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'service_quality_report', $schedule['GroundRequestSchedule']['id'])); ?>">Quality Control Request</li>
                        <li class="open_only divider" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                        <li data-status="open" class="open_only delete_link" href="<?php echo $html->url(array('controller' => 'Ground', 'action' => 'admin_schedule_delete', $schedule['GroundRequestSchedule']['id'])); ?>">Delete Schedule</li>
                    </ul>
                </span>

            </span>
        <?php //} ?>
    </h4>

    <div class="services">
        <table border="0" cellspacing="0" cellpadding="6" width="100%">
            <tr>
                <td valign="top" style="border-right: dotted #ddd 1px;">
                    <b>Handler</b>
                    <ul>
                        <li><?php echo $schedule['Handler']['name']; ?></li>
                    </ul>

                    <b>Services Requested</b>

                    <ul>
                        <?php foreach( $schedule['GroundRequestScheduleService'] as $services ) { ?>
                            <li><?php echo $services['Service']['name']; ?></li>
                        <?php } ?>
                    </ul>
                </td>
                <td valign="top">
                    <b>Jobsheet Reference No.</b>
                    
                    <ul>
                        <li><?php echo $schedule['GroundRequestSchedule']['ref_no']; ?></li>
                    </ul>
                    
                    <b>Attached Documents</b>

                    <ul id="attachment_link_list">
                        <?php foreach( $schedule['GroundRequestScheduleAttachment'] as $attachments ) { ?>
                            <li>
                                <a target="_blank" href="<?php echo $html->url(array('controller' => 'ground', 'action' => 'attachments_view', $attachments['id'])); ?>"><?php echo $attachments['file_name']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Comments</b> <br />
                    
                    <?php echo str_replace("\n", "<br />", $schedule['GroundRequestSchedule']['comment']); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
