<?php
if ((isset($options)) && (isset($options['schedule_id']))) {
    $schedule_id = $options['schedule_id'];
}

$scd = ClassRegistry::init('GroundRequestSchedule');
$schedule_data = $scd->find('first', array(
    'contain' => array(
        'GroundRequest',
        'GroundRequestScheduleService' => array(
            'Service'
        ),
        'Handler'
    ),
    'conditions' => array(
        'GroundRequestSchedule.id' => $schedule_id
    )
        )
);

$services = ClassRegistry::init('Service')->find('all', array('recursive' => -1, 'order' => array('name')));
?>

<style type="text/css">
    .forward_details {
        border: none !important;
        padding: 5px 5px;
    }

    .forward_details table tr td {
        font-family: Tahoma;
        font-size: 11px;
    } 

    .forward_details table tr td:nth-child(odd) {
        font-weight: bold;
        width: 15%;
        white-space: nowrap;
    }

    .forward_details table tr td:nth-child(even) {
        width: 16%;
        text-transform: capitalize;
    }
    
    .services_list {
        border-spacing: 2px;
    }

    .services_list td {
        font-weight: normal !important;
        padding: 2px !important;
        font-size: 11px;
        vertical-align: top;
    }
    
    .services_list .service_name {
        white-space: normal !important;
        width: 130px !important;
    }
    
    .services_list .check_td {
        white-space: normal;
        width: 30px !important;
    }
    
    .services_list .checker {
        content: "&nbsp; &nbsp;";
        width: 25px !important;
        text-align: center;
        border: solid #333 1px !important;
        padding: 1px !important;
        font-size: 11px;
        line-height: 11px;
    }
    
    .services_list .checker img {
        height: 12px;
    }

    .services_list .checker.checked {
        background-image: url(<?php echo $html->url("/img/checkmark.jpeg"); ?>);
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .flyout_content td {
        padding: 5px;
    }

    .inline-bold {
        display: inline-block;
        font-weight: bold;
        border-bottom: dotted #999 1px;
    }

    .ordered_list {
        list-style-type: decimal;
        padding-left: 10px;
        margin: 5px;
        margin-left: 20px;
    }

    .flyout_content section {
        margin-bottom: 10px;
        font-family:  Tahoma, Geneva, sans-serif;
        font-size: 13px;
    }

    .flyout_content section header {
        padding: 5px;
        margin-bottom: 10px;
        text-align: center;
    }

    .flyout_content section h3 {
        text-transform: capitalize;
        font-weight: bold;
        font-size: 16px;
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif !important;
        border-bottom: dotted #ccc 1px;
        padding-bottom: 3px;
        margin: 3px 0px;
    }

    .flyout_content footer {
        text-align: center;
    }
    .error_jobsheet{
        font-weight:bold;
        color: red;
    }
    table thead th{
        text-align:left;
    }
    .status{
        font-weight: bolder;
        font-family: "Roman", Arial, Helvetica, sans-serif !important;
        display: inline;
        background-color: #222;
        color: #fff;

    }
</style>

<h3 style="text-decoration: underline; text-align: center; color: #333; border: none; margin: 5px 0px;">GROUND HANDLING SUPERVISION AND ADMINISTRATION</h3>

<section>
    <h3>Merchant Card Details</h3>

    <div class="forward_details">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td width="15%">Card Name/Number:</td>
                <td width="15%" nowrap><?php echo $schedule_data['GroundRequest']['card_number'] != "" ? $schedule_data['GroundRequest']['card_number'] : "___________________"; ?></td>
                <td width="15%">Holders Name:</td>
                <td width="15%" nowrap><?php echo $schedule_data['GroundRequest']['holder_name'] != "" ? $schedule_data['GroundRequest']['holder_name'] : "___________________"; ?></td>
                <td>Card Expiry Date:</td>
                <td nowrap style="border-bottom: solid #333 1px;"><?php echo $schedule_data['GroundRequest']['card_expiry_date']; ?></td>
            </tr>
            <tr>
                <td>Company To Invoice:</td>
                <td><?php echo $schedule_data['GroundRequest']['company_name']; ?></td>
                <td>P. O. No:</td>
                <td nowrap><?php echo $schedule_data['GroundRequest']['po_number'] != "" ? $schedule_data['GroundRequest']['po_number'] : "___________________"; ?></td>
            </tr>
        </table>
    </div>
</section>

<section>

    <h3>Request Details</h3>

    <div class="forward_details">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td >Job No.:</td>
                <td><?php echo $schedule_data['GroundRequestSchedule']['ref_no']; ?></td>
                <td>Location</td>
                <td><?php echo strtoupper($schedule_data['GroundRequestSchedule']['arr_dest']); ?></td>
                <td>Handler:</td>
                <td><?php echo $schedule_data['Handler']['name']; ?></td>
            </tr>
            <tr>
                <td>Operator:</td>
                <td><?php echo $schedule_data['GroundRequest']['operator']; ?></td>
                <td>Aircraft Type:</td>
                <td><?php echo $schedule_data['GroundRequest']['type']; ?></td>
                <td>Aircraft Regn:</td>
                <td><?php echo $schedule_data['GroundRequest']['region']; ?></td>
            </tr>
            <tr>
                <td>MTOW:</td>
                <td><?php echo $schedule_data['GroundRequest']['mtow']; ?></td>
                <td>Flight No:</td>
                <td>
                    <?php echo $schedule_data['GroundRequestSchedule']['arr_flight_num']; ?>
                </td>
                <td>Service Type:</td>
                <td>
                    <?php echo $schedule_data['GroundRequest']['purpose']; ?>
                </td>
            </tr>
            <tr>
                <td>Arrival Date:</td>
                <td><?php echo strtotime($schedule_data['GroundRequestSchedule']['arr_ata']) > -1 ? date('jS M, Y', strtotime($schedule_data['GroundRequestSchedule']['arr_ata'])) : "_____________"; ?></td>
                <td>Arrival Time:</td>
                <td><?php echo strtotime($schedule_data['GroundRequestSchedule']['arr_ata']) > -1 ? date('H:i', strtotime($schedule_data['GroundRequestSchedule']['arr_ata'])) . "Z" : "___________"; ?></td>
                <td>Origin (ICAO/IATA):</td>
                <td><?php echo strtoupper($schedule_data['GroundRequestSchedule']['arr_origin']); ?></td>
            </tr>
            <tr>
                <td>Dept Date:</td>
                <td><?php echo strtotime($schedule_data['GroundRequestSchedule']['dept_atd']) > -1 ? date('jS M, Y', strtotime($schedule_data['GroundRequestSchedule']['dept_atd'])) : "_____________"; ?></td>
                <td>Dept Time:</td>
                <td><?php echo strtotime($schedule_data['GroundRequestSchedule']['dept_atd']) > -1 ? date('H:i', strtotime($schedule_data['GroundRequestSchedule']['dept_atd'])) . "Z" : "___________"; ?></td>
                <td>Dest. (ICAO/IATA):</td>
                <td><?php echo strtoupper($schedule_data['GroundRequestSchedule']['dept_dest']); ?></td>
            </tr>
        </table>
        
        <table border="0" cellpadding="5" cellspacing="0" width="65%">
            <tr>
                <td style="width: 12% !important;" nowrap>Crew In:</td>
                <td style="width: 12% !important;"><?php echo $schedule_data['GroundRequestSchedule']['arr_crew_no'] . " "; ?></td>
                <td style="width: 12% !important;" nowrap>Crew Out:</td>
                <td style="width: 12% !important;"><?php echo $schedule_data['GroundRequestSchedule']['dept_crew_no'] . " "; ?></td>
                <td style="width: 12% !important;" nowrap>PAX In:</td>
                <td style="width: 12% !important;"><?php echo $schedule_data['GroundRequestSchedule']['arr_pax_no'] . " "; ?></td>
                <td style="width: 12% !important;" nowrap>PAX Out:</td>
                <td style="width: 12% !important;"><?php echo $schedule_data['GroundRequestSchedule']['dept_pax_no'] . " "; ?></td>
            </tr>
        </table>
    </div>
</section>


<section>

    <?php
    $hotel_details = null;
    $transport_details = null;

    foreach ($schedule_data['GroundRequestScheduleService'] as $service) {
        if (stristr($service['Service']['name'], "Hotel") != false) {
            $hotel_details = $service;
        }

        if (stristr($service['Service']['name'], "Transport") != false) {
            $transport_details = $service;
        }
    }
    ?>

    <h3>Ramp & Hotel Services</h3>

    <div class="forward_details">
        <table border="0" cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td width="15%"><b>Hotel Name:</b></td>
                <td width="20%"><?php echo isset($hotel_details) ? $hotel_details['name'] : ""; ?></td>
                <td>PIC Room #:</td>
                <td><?php echo isset($hotel_details) ? $hotel_details['hotel_pic_room_no'] : ""; ?></td>
                <td>Cell No:</td>
                <td><?php echo isset($hotel_details) ? $hotel_details['hotel_pic_cell_no'] : ""; ?></td>

            </tr>
            <tr>
                <td>Type of Cars:</td>
                <td><?php echo isset($transport_details) ? $transport_details['transport_vehicle_types'] : ""; ?></td>
                <td width="15%"><b>No of Cars In:</b></td>
                <td width="20%"><?php echo isset($transport_details) ? $transport_details['transport_cars_in'] : ""; ?></td>
                <td width="15%"><b>No of Cars Out:</b></td>
                <td width="20%"><?php echo isset($transport_details) ? $transport_details['transport_cars_out'] : ""; ?></td>
            </tr>
            <tr>
                <td>Pick up Date & Time:</td>
                <td><?php echo isset($transport_details) && strtotime($transport_details['transport_pickup_dt']) !== false ? date("d/m/y H:i", strtotime($transport_details['transport_pickup_dt'])) : ""; ?></td>
                <td colspan="2">PAX Co-ordinator (Name & Number):</td>
                <td colspan="2"><?php echo isset($transport_details) ? $transport_details['transport_pax_coordinator'] : ""; ?></td>
            </tr>
        </table>
    </div>
</section>

<section>
    <h3>Services Requested</h3>
        <table border="0" cellpadding="3" cellspacing="0" width="100%" class="services_list">
            <?php
            $i = 0;
            foreach ($services as $service) {
                if ($i == 0 || $i % 4 == 0) {
                    echo "<tr>";
                }
                ?>
                <td class="service_name"><?php echo $service['Service']['name']; ?></td>
                
                <?php
                $checked = false;
                foreach ($schedule_data['GroundRequestScheduleService'] as $val) {
                    if ($val['Service']['name'] == $service['Service']['name']) {
                        echo "<td class='check_td'><div class='checker'>" . $html->image( $html->url("/img/checkmark.png", true) ) . "</div></td>";
                        $checked = true;
                        break;
                    }
                }

                if (!$checked) {
                    echo "<td class='check_td'><div class='checker'>&nbsp;</div></td>";
                }
                ?>
                
                <?php
                if (++$i % 4 == 0) {
                    echo "</tr>";
                }
            }
            ?>
        </table>
</section>

<section>
    <h3>Comments</h3>

    <div style="padding: 5px; box-sizing: border-box; height: 30px;"><?php echo $schedule_data['GroundRequestSchedule']['comment']; ?></div>
</section>
