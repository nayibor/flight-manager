<?php
$user = $this->Session->read('user');

$grschedule = ClassRegistry::init('GroundRequestSchedule');
$schedules = array();

if ( isset($schedule_ids) ) {

    $schedules = $grschedule->find('all', array(
        'contain' => array(
            'GroundRequest',
            'GroundRequestScheduleService' => array(
                'Service' => array(
                    'fields' => array('name', 'description')
                )
            ),
            'Handler' => array(
                'fields' => array('name')
            )
        ),
        'conditions' => array(
            'GroundRequestSchedule.id' => $schedule_ids
        )
            ));

    //debug($schedules);
}

function concat($object, $className, $field, $glue) {
    $refs = array();
    foreach ($object as $schedule) {
        $refs[] = $schedule[$className][$field];
    }

    return implode($glue, $refs);
}
?>
<style>
    #form_area {
        font-size: 11px;
    }
    #form_area td {
        padding: 5px;
        font-size: 11px;
    }

    #form_area h2 {
        margin-bottom: 15px;
        font-weight: 400;
        text-align: center;
        text-decoration: underline;
    }

    #pdf_wrapper #form_area td, #pdf_wrapper #form_area {
        font-size: 10pt !important;
        line-height: 12pt;
    }

    #print_wrapper #form_area td, #print_wrapper #form_area {
        font-size: 12pt !important;
        line-height: 16pt;
    }
</style>

<div class="watermark" style="z-index: -1; opacity: 0.7; position: absolute; left: 50%; margin-left: -330px;">
    <?php echo $html->image('app/ground/handling_watermark.png'); ?>
</div>

<div class="form_content" style="z-index: 4;">
    <div id="form_area">
        <h2>GROUND HANDLING REQUEST</h2>

        <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
                <td width="15%"><strong>REF. NO.:</strong></td>
                <td width="85%"><strong><?php echo concat($schedules, 'GroundRequestSchedule', 'ref_no', ' & '); ?></strong></td>
            </tr>
            <tr>
                <td><strong>LOCATION: </strong></td>
                <td>
                    <strong>
                        <?php echo concat($schedules, 'GroundRequestSchedule', 'arr_dest', " / "); ?>
                        <?php echo $locations->getAirportName($schedules[0]['GroundRequestSchedule']['arr_dest']); ?>
                    </strong>
                </td>
            </tr>
            <tr>
                <td><strong>DATE:</strong></td>
                <td><strong><?php echo date('jS F, Y'); ?></strong></td>
            </tr>
        </table>

        <div style="border-bottom: solid #333 1px; margin: 5px 0px;"></div>

        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td width="15%">ATTN:</td>
                <td width="85%"><?php echo $schedules[0]['Handler']['name']; ?></td>
            </tr>
            <tr>
                <td>FROM:</td>
                <td>GROUND OPERATIONS</td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
        </table>

        <p><strong>WE REQUEST YOUR ASSISTANCE WITH THE BELOW  FLIGHT:-</strong>

        <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
                <td width="15%">OPTR:</td>
                <td width="85%"><?php echo $schedules[0]['GroundRequest']['operator']; ?></td>
            </tr>
            <tr>
                <td>TYPE:</td>
                <td><?php echo $schedules[0]['GroundRequest']['type']; ?></td>
            </tr>
            <tr>
                <td>REGN:</td>
                <td><?php echo $schedules[0]['GroundRequest']['region']; ?></td>
            </tr>
            <tr>
                <td>MTOW:</td>
                <td><?php echo $schedules[0]['GroundRequest']['mtow']; ?></td>
            </tr>
            <tr>
                <td>FLT#:</td>
                <td><?php echo concat($schedules, 'GroundRequestSchedule', 'arr_flight_num', ' & '); ?></td>
            </tr>
            <tr>
                <td>CREW:</td>
                <td><?php echo $schedules[0]['GroundRequest']['crew']; ?></td>
            </tr>
            <tr>
                <td>PURP:</td>
                <td><?php echo $schedules[0]['GroundRequest']['purpose']; ?></td>
            </tr>
            <tr>
                <td valign="top">SKED:</td>
                <td style="text-transform: uppercase;">
                    <?php
                    foreach ($schedules as $schedule) {
                        echo $this->element('ground/schedule_format', array('schedule' => $schedule));
                    }
                    ?>
                </td>
            </tr>
        </table>

        <p><b>WE REQUEST</b></p>

        <ul style="list-style: circle outside; padding-left: 24px; margin-bottom: 15px;">

            <?php
            if ( $schedule['GroundRequestSchedule']['handling_fees_on'] == "" ) {
                echo "<li><div class='ui-state-error ui-corner-all' style='padding: 5px; margin-bottom: 5px;'>Payment Method For <b>Ground Handling Services</b> Not Specified. <a class='flyout_opener' href='{$html->url(array('action' => 'admin_schedule_edit', $schedule['GroundRequestSchedule']['id']))}'>Edit Handling Request</a></div></li>";
            }
            else {
                echo "<li>GROUND HANDLING SERVICES ON " . ($schedule['GroundRequestSchedule']['handling_fees_on'] == "credit" ? "S.B-MAN ACCOUNT" : "CASH BASIS") . "</li>";
            }

            if ( $schedule['GroundRequestSchedule']['airport_fees_on'] == "" ) {
                echo "<li><div class='ui-state-error ui-corner-all' style='padding: 5px; margin-bottom: 5px;'>Payment Method For <b>Airport Fees</b> Not Specified. <a class='flyout_opener' href='{$html->url(array('action' => 'admin_schedule_edit', $schedule['GroundRequestSchedule']['id']))}'>Edit Handling Request</a></div></li>";
            }
            else {
                echo "<li>AIRPORT FEES ON " . ($schedule['GroundRequestSchedule']['airport_fees_on'] == "credit" ? "S.B-MAN ACCOUNT" : "CASH BASIS") . "</li>";
            }
            ?>

            <?php
            foreach ($schedules as $schedule) {
                foreach ($schedule['GroundRequestScheduleService'] as $schedule_service) {
                    ?>
                    <li style="text-transform: uppercase;"><?php echo $schedule_service['Service']['description']; ?></li>
                    <?php
                }
            }
            ?>
            <li>ON  TIME MOVEMENT MESSAGES TO <a href="mailto:fltops@sbmangh.com">fltops@sbmangh.com</a>, SITA: ACCSM7X, or +233 24 432-1771, 20 629-8260</li>
            <li>PLS  ADVISE ANY NOTAMS THAT MAY AFFECT THE OPERATIONS OF THIS FLIGHT.</li>
            <li>PLS  ADVISE ANY LIMITATIONS, YOU MAY HAVE AT THIS LOCATION.</li>
            <li>PLS  ADVISE ANY AIRPORT LIMITATIONS, IF ANY.</li>
            <li>
                <B style="text-transform: uppercase;">
                    TO GUARANTEE FAST PAYMENT IT IS IMPORTANT THAT ALL INVOICES WITH SUPPORTING DOCUMENTS AND OUR ATTACHED QUALITY SERVICE REPORT WHICH IS TO BE FILLED AND SIGNED BY CAPTAIN OR FLIGHT MANAGER IS SENT TO US NOT MORE THAN 07 DAYS AFTER FLIGHT OPERATION
                </B>
            </li>
        </ul>
        <p>PLS KINDLY CONFIRM PROVISION OF ALL SERVICES.</p>
        <p>RGDS | <?php echo strtoupper($user['User']['first_name'] . " " . $user['User']['last_name'] . " - " . $user['User']['team']); ?> | S.B - MAN (HQ), <br />P. O. Box OS 1734,  OSU - ACCRA. GHANA. www.sbmangh.com</p>

    </div>
</div>