
<?php
$grs = ClassRegistry::init('GroundRequestSchedule');
$req_data = $grs->find('first', array(
    'conditions' => array(
        'GroundRequestSchedule.id' => $schedule_id
    ),
    'contain' => array(
        'GroundRequest', 'GroundRequestQuality'
    )
        ));
?>

<style>

    form fieldset {
        padding: 10px;
        border: solid #aaa 1px;
        border-radius: 10px;
    }

    form label {
        width: 140px !important;
    }

    form td span {
        width: 50px;
        display: inline-block;
    }
    
    #print_wrapper form td span, #pdf_wrapper form td span {
        width: 80px;
        display: inline-block;
    }

    #comments_block {
        display: none;
    }

    #print_wrapper form input[type=radio],
    #pdf_wrapper form input[type=radio] {

    }

    #print_wrapper form input[type=radio]:after,
    #pdf_wrapper form input[type=radio]:after {
        content: "";
        display: inline-block;
        vertical-align: top;
        border: solid #333 1px;
        width: 12px;
        height: 12px;
        margin-right: 5px;
        box-sizing: border-box;
    }

    #print_wrapper, #print_wrapper td {
        font-size: 11pt !important;
    }

    #pdf_wrapper, #pdf_wrapper td {
        font-size: 9pt !important;
        line-height: 9pt;
        font-weight: normal;
    }

    #service_report_form textarea,
    #service_report_form textarea {
        border: solid #333 1px;
        display: none;
    }

    #service_report_form #comments_block,
    #service_report_form #comments_block {
        display: block;
    }

    #service_report_form #comments_block p,
    #service_report_form #comments_block p {
        line-height: 15px;
        border-bottom: dotted #333 1px;
    }
</style>

<h3 style="margin: 20px 0px;">GROUND HANDLING QUALITY  SERVICE REPORT</h3>

<form method="POST" id="service_report_form">

    <table border="0" width="100%" cellpadding="5" cellspacing="0" align="center">
        <tr>
            <td nowrap>Job Reference No:</td>
            <td><?php echo $req_data['GroundRequestSchedule']['ref_no']; ?></td>
        </tr>
        <tr >
            <td>Date:</td>
            <td>
                <?php
                /* if (isset($req_data['GroundRequestSchedule'])) {
                  echo date('jS F, Y', strtotime($req_data['GroundRequestSchedule']['created_dt']));
                  } */
                echo date('jS F, Y');
                ?>
            </td>
            <td>Location:</td>
            <td>
                <b><?php echo $req_data['GroundRequestSchedule']['arr_dest']; ?></b>
                <?php echo $locations->getAirportName($req_data['GroundRequestSchedule']['arr_dest']); ?>
            </td>
        </tr>
        <tr >
            <td width="20%">Operator:</td>
            <td width="30%"><?php echo $req_data['GroundRequest']['operator']; ?></td>
            <td width="20%">A/C Regn:</td>
            <td width="30%"><?php echo $req_data['GroundRequest']['region']; ?></td>
        </tr>

        <tr >
            <td >Type Of Ops:</td>
            <td ><?php echo $req_data['GroundRequest']['purpose']; ?></td>
            <td >A/C Type:</td>
            <td ><?php echo $req_data['GroundRequest']['type']; ?></td>
        </tr>

        <tr >
            <td>Flight No:</td>
            <td><?php echo $req_data['GroundRequestSchedule']['arr_flight_num']; ?></td>
            <td>Flight Date:</td>
            <td><?php echo date('jS F, Y', strtotime($req_data['GroundRequestSchedule']['arr_etd'])); ?></td>
        </tr>

        <tr >
            <td>Origin:</td>
            <td><?php echo $req_data['GroundRequestSchedule']['arr_origin']; ?></td>
            <td>Destination:</td>
            <td><?php echo $req_data['GroundRequestSchedule']['dept_dest']; ?></td>
        </tr>

    </table>
    <br />

    <p>
        Dear Captain or Flight Manager, <br />
        Please kindly complete this questionnaire so as to help us better serve you at all times (please tick where applicable)
    </p>

    <br />

    <table id="qt" border="0" width="100%" align="center" cellpadding="7" cellspacing="0">
        <tr>

            <td width="45%">How Responsive Was Supervisor?</td>
            <td>
                <input value="prompt" <?php echo $req_data['GroundRequestQuality']['response_sup'] == "prompt" ? "checked=checked" : ""; ?>  type="radio" name="data[GroundRequestQuality][response_sup]" id="data[GroundRequestQuality][response_sup]"><span>Prompt</span>
                <input value="fair" <?php echo $req_data['GroundRequestQuality']['response_sup'] == "fair" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][response_sup]" id="data[GroundRequestQuality][response_sup]"><span>Fair</span>
                <input value="slow" <?php echo $req_data['GroundRequestQuality']['response_sup'] == "slow" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][response_sup]" id="data[GroundRequestQuality][response_sup]"><span>Slow</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['response_sup'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][response_sup]" id="data[GroundRequestQuality][response_sup]"><span>N/A</span>
            </td>

        </tr>
        <tr>

            <td >Was Equipment Positioning Quick Enough?</td>
            <td>
                <input value="good" <?php echo $req_data['GroundRequestQuality']['equip_pos'] == "good" ? "checked=checked" : ""; ?> type="radio" name="data[GroundRequestQuality][equip_pos]" id="data[GroundRequestQuality][equip_pos]"><span>Good</span>
                <input value="not_bad" <?php echo $req_data['GroundRequestQuality']['equip_pos'] == "not_bad" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][equip_pos]" id="data[GroundRequestQuality][equip_pos]"><span>Not Bad</span>
                <input value="slow" <?php echo $req_data['GroundRequestQuality']['equip_pos'] == "slow" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][equip_pos]" id="data[GroundRequestQuality][equip_pos]"><span>Slow</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['equip_pos'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][equip_pos]" id="data[GroundRequestQuality][equip_pos]"><span>N/A</span>
            </td>

        </tr>
        
        <tr>

            <td >Were You Provided With All Your Needs?</td>
            <td>
                <input value="yes" type="radio" <?php echo $req_data['GroundRequestQuality']['needs'] == "yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][needs]" id="data[GroundRequestQuality][needs]"><span>Yes</span>
                <input value="no" type="radio" <?php echo $req_data['GroundRequestQuality']['needs'] == "no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][needs]" id="data[GroundRequestQuality][needs]"><span>No</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['needs'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][needs]" id="data[GroundRequestQuality][needs]"><span>N/A</span>
            </td>
        </tr>
        <tr>

            <td >Was Fuel Truck On Time?</td>
            <td>
                <input value="yes" type="radio" <?php echo $req_data['GroundRequestQuality']['fuel'] == "yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][fuel]" id="data[GroundRequestQuality][fuel]"><span>Yes</span>
                <input value="no" type="radio" <?php echo $req_data['GroundRequestQuality']['fuel'] == "no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][fuel]" id="data[GroundRequestQuality][fuel]"><span>No</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['fuel'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][fuel]" id="data[GroundRequestQuality][fuel]"><span>N/A</span>
            </td>

        </tr>
        <tr>

            <td >Was Catering Delivered On Time?</td>
            <td>
                <input value="yes" type="radio" <?php echo $req_data['GroundRequestQuality']['catering'] == "yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][catering]" id="data[GroundRequestQuality][catering]"><span>Yes</span>
                <input value="no" type="radio" <?php echo $req_data['GroundRequestQuality']['catering'] == "no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering]" id="data[GroundRequestQuality][catering]"><span>No</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['catering'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering]" id="data[GroundRequestQuality][catering_quality]"><span>N/A</span>
            </td>
        </tr>
        <tr>

            <td >Was Catering Quality/Quantity To Your Satisfaction?</td>
            <td>
                <input value="yes" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>Yes</span>
                <input value="no" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>No</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>N/A</span>
            </td>

        </tr>
        
        <tr>

            <td >Was Transportation To Your Satisfaction?</td>
            <td>
                <input value="yes" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>Yes</span>
                <input value="no" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>No</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>N/A</span>
            </td>

        </tr>
        
        <tr>

            <td >Was Hotel Arrangement To Your Satisfaction ?</td>
            <td>
                <input value="yes" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "yes" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>Yes</span>
                <input value="no" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "no" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>No</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['catering_quality'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][catering_quality]" id="data[GroundRequestQuality][catering_quality]"><span>N/A</span>
            </td>

        </tr>
        
        
        <tr>

            <td >Was Handling To Your Satisfaction?</td>
            <td>
                <input value="yes" <?php echo $req_data['GroundRequestQuality']['handling_sat'] == "yes" ? "checked=checked" : ""; ?> type="radio" name="data[GroundRequestQuality][handling_sat]" id="data[GroundRequestQuality][handling_sat]"><span>Yes</span>
                <input value="not_bad" <?php echo $req_data['GroundRequestQuality']['handling_sat'] == "not_bad" ? "checked=checked" : ""; ?> type="radio"  name="data[GroundRequestQuality][handling_sat]" id="data[GroundRequestQuality][handling_sat]"><span>Not Bad</span>
                <input value="slow" type="radio" <?php echo $req_data['GroundRequestQuality']['handling_sat'] == "slow" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][handling_sat]" id="data[GroundRequestQuality][handling_sat]"> <span>Bad</span>
                <input value="na" type="radio" <?php echo $req_data['GroundRequestQuality']['handling_sat'] == "na" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][handling_sat]" id="data[GroundRequestQuality][handling_sat]"><span>N/A</span>
            </td>

        </tr>
        <tr>

            <td >General Assessment</td>
            <td>
                <input value="excellent" type="radio" <?php echo $req_data['GroundRequestQuality']['general_assessment'] == "excellent" ? "checked=checked" : ""; ?> name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]"><span>Excellent</span>
                <input value="good" type="radio" <?php echo $req_data['GroundRequestQuality']['general_assessment'] == "good" ? "checked=checked" : ""; ?>   name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]"><span>Good</span>
                <input value="fair" type="radio" <?php echo $req_data['GroundRequestQuality']['general_assessment'] == "fair" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]"><span>Fair</span>
                <input value="bad" type="radio" <?php echo $req_data['GroundRequestQuality']['general_assessment'] == "bad" ? "checked=checked" : ""; ?>  name="data[GroundRequestQuality][general_assessment]" id="data[GroundRequestQuality][general_assessment]"><span>Bad</span>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br /><b>Comments</b>
                <textarea style="width: 100%; height: 200px; border: solid #444 3px;"  name="data[GroundRequestQuality][comments]" id="data[GroundRequestQuality][comments]"><?php echo $req_data['GroundRequestQuality']['comments'] ?></textarea>
                <div id="comments_block">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </div>
            </td>
        </tr>
    </table>
</form>

<footer style="margin-top: 20px; text-align: left;">

    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <!--<td>
                Supervisor -  Signature:________________
            </td>-->
            <td>
                Capt./FM Sign:_____________________
            </td>
        </tr>
        <tr>
            <!--<td>
                Name:___________________
            </td>-->
            <td>
                Name: ___________________
            </td>
        </tr>
        <tr>
            <!--<td>
                Date:___________________
            </td>-->
            <td>
                Date: ___________________
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <div class="foot-note" style="font-size: 8pt !important;">
                    S.B.MAN & CO - Ground Ops Quality Control Report <!--08/06 – Issue 1 – rev.12’09	-->
                    <a href="http://www.sbmangh.com/" target="_blank">www.sbmangh.com</a>
                </div>
            </td>
        </tr>
    </table>	
</footer>
