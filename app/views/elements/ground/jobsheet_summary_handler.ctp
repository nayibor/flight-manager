<style>
    .flyout_content footer td {
        font-size: 12px;
    }
</style>
<div class="flyout_content">

    <section class="summary">
        <?php echo $this->element('ground/jobsheet_summary'); ?>
    </section>

    <footer style="margin-top: 20px; text-align: left;">

        <table width="100%" cellpadding="3" cellspacing="0" border="0">
            <tr>
                <td colspan="2">
                    <em>NB: Please ensure -  Capt. Signs, Carnet Card not on Invalid List, Tail# is depicted on card.</em>
                </td>
            </tr>
            <tr>
                <td>
                    Supervisor -  Signature:________________
                </td>
                <td>
                    Capt./FM Sign:_____________________
                </td>
            </tr>
            <tr>
                <td>
                    Name:___________________
                </td>
                <td>
                    Name: ___________________
                </td>
            </tr>
            <tr>
                <td>
                    Date:___________________
                </td>
                <td>
                    Date: ___________________
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="foot-note" style="font-size: 8pt;">
                        S.B.MAN & CO - Ground Ops Job Sheet Doc <!--08/06 – Issue 1 – rev.12’09	-->
                        <a href="http://www.sbmangh.com/" target="_blank">www.sbmangh.com</a>
                    </div>
                </td>
            </tr>
        </table>	
    </footer>
</div>
