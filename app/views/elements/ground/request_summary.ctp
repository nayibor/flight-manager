<h3>Flight Information</h3>

<div style="padding: 10px; margin-bottom: 15px;">
    <table id="summary_table" border="0" cellpadding="6" cellspacing="0" width="100%">
        <tr>
            <td >Company Requesting</td>
            <td><?php echo $groundRequest['GroundRequest']['company_name']; ?></td>
        </tr>

        <tr>
            <td >Operator</td>
            <td><?php echo $groundRequest['GroundRequest']['operator']; ?></td>
        </tr>

        <tr>
            <td >Aircraft Type</td>
            <td><?php echo $groundRequest['GroundRequest']['type']; ?></td>
        </tr>

        <tr>
            <td >Registration</td>
            <td><?php echo $groundRequest['GroundRequest']['region']; ?></td>
        </tr>

        <tr>
            <td >Max. Take Off Weight</td>
            <td><?php echo $groundRequest['GroundRequest']['mtow']; ?></td>
        </tr>

        <!--<tr>
            <td >Flight No.</td>
            <td><?php //echo $groundRequest['GroundRequest']['flight_no']; ?></td>
        </tr>-->

        <tr>
            <td >Purpose of Flight</td>
            <td><?php echo $groundRequest['GroundRequest']['purpose']; ?></td>
        </tr>
    </table>
</div>