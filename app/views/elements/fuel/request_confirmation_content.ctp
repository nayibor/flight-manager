<style>
    #confirmation_wrapper {

    }

    #confirmation_wrapper td {
        padding: 4px;
    }
    
    #pdf_wrapper, #pdf_wrapper #content_wrapper td {
        font-size: 10pt;
    }
</style>

<div id="confirmation_wrapper">

    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td width="23%">To:</td>
            <td width="27%"><?php echo $request['FuelRequest']['company_name']; ?></td>
            <td width="15%">ATTN:</td>
            <td width="35%"><?php echo $request['FuelRequest']['company_attn']; ?></td>
        </tr>
        <tr>
            <td>From:</td>
            <td><?php $user = $this->Session->read('user'); echo $user['User']['first_name']; ?></td>
            <td>FAX#:</td>
            <td><?php echo $request['FuelRequest']['company_fax']; ?></td>
        </tr>
        <tr>
            <td>Date:</td>
            <td><?php echo strtoupper(date('dMy')); ?></td>
            <td>Phone#:</td>
            <td><?php echo $request['FuelRequest']['company_tel']; ?></td>
        </tr>
        <tr>
            <td>Sale Order No:</td>
            <td><?php echo $request['FuelRequest']['ref_no'] . " (" . $request['FuelRequest']['confirmation_code'] . ")"; ?></td>
            <td>Email:</td>
            <td><?php echo $request['FuelRequest']['company_email']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"><strong>FUEL CONFIRMATION</strong></td>
        </tr>
        <tr>
            <td colspan="4">S. B. MAN &amp; Co. Ltd. has arranged <?php echo $request['FuelRequest']['product']; ?> on your behalf as follows:</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>Airline / Operator:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['aircraft_optr']; ?></td>
        </tr>
        <tr>
            <td>Aircraft Type:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['aircraft_type']; ?></td>
        </tr>
        <tr>
            <td>Registration No:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['aircraft_regn']; ?></td>
        </tr>
        <tr>
            <td>Flight No:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['aircraft_flight_no']; ?></td>
        </tr>
        <tr>
            <td>Location:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['arr_dest']; ?></td>
        </tr>
        <tr>
            <td>FBO / Agent / Fueler:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['fueler']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">**** ALL TIMES ARE LOCAL OR GMT / UTC ****</td>
        </tr>
        <tr>
            <td>ETA:</td>
            <td colspan="3"><?php echo strtoupper(date('dMy - Hi e', strtotime($request['FuelRequest']['arr_eta']))); ?></td>
        </tr>
        <tr>
            <td>ETD:</td>
            <td colspan="3"><?php echo strtoupper(date('dMy - Hi e', strtotime($request['FuelRequest']['dept_etd']))); ?></td>
        </tr>
        <tr>
            <td>DEST:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['dept_dest']; ?></td>
        </tr>
        <tr>
            <td>EST. UPLIFT (USG):</td>
            <td colspan="3"><?php echo $request['FuelRequest']['est_uplift']; ?></td>
        </tr>
        <tr>
            <td>PRODUCT:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['product']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top">Remarks:</td>
            <td colspan="3">
                <?php //echo str_replace("\n", "<br />", $request['FuelRequest']['remarks']); ?>
                <ol>
                    <li>72 HOURS LEEWAY ALLOWED, IN CASE OF ANY DELAY.</li>
                    <li>MOST FUEL PRICES CHANGE ON MONTHLY OR FORTNIGHTLY BASES. 
                        IF YOUR FLIGHT EXTENDS INTO THE NEXT MONTH, PLEASE CHECK PRICES AGAIN 
                        FOR CHANGES THAT MAY OCCUR AFTER THIS DATE. AFTER THE TRIP, YOU WILL BE 
                        INVOICED AT THE PRICE IN EFFECT ON THE DATE OF THE  FUEL UPLIFT.
                    </li>
                </ol>
            </td>
        </tr>
    </table>
</div>

<?php 
if( isset($show_footer) ) {
    echo $this->element("fuel/request_footer");
}
?>