<style>
    #content_wrapper {

    }

    #content_wrapper td {
        padding: 4px;
    }
    
    #pdf_wrapper, #pdf_wrapper #content_wrapper td {
        font-size: 10pt;
    }
</style>

<div id="content_wrapper">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td width="20%">To:</td>
            <td width="30%"><span style="text-transform: uppercase;"><?php echo $request['FuelRequest']['fuel_service_provider'] != "" ? $request['FuelRequest']['fuel_service_provider'] : "<b>N/A</b>"; ?></span></td>
            
            <td width="15%">ATTN:</td>
            <td width="35%">OPERATIONS</td>
        </tr>
        <tr>
            <td>From:</td>
            <td><!-- STANLEY --> <?php $user = $this->Session->read('user'); echo strtoupper($user['User']['first_name']); ?></td>
            
            <td nowrap>Purchase Order No:</td>
            <td><?php echo $request['FuelRequest']['ref_no']; ?></td>
        </tr>
        <tr>
            <td>Date:</td>
            <td><?php echo strtoupper(date('dMy')); ?></td>
            
            <td>Copy:</td>
            <td>N/A</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"><strong>FUEL REQUEST</strong></td>
        </tr>
        <tr>
            <td colspan="4">Please fuel the following S.B-MAN flight with <?php echo $request['FuelRequest']['fuel_service_provider'] != "" ? $request['FuelRequest']['fuel_service_provider'] : "<b>N/A</b>"; ?> Contract Fuel as follows:</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Aircraft Type:</td>
            <td><?php echo $request['FuelRequest']['aircraft_type']; ?></td>
        </tr>
        <tr>
            <td>Registration No:</td>
            <td><?php echo $request['FuelRequest']['aircraft_regn']; ?></td>
        </tr>
        <tr>
            <td>Flight No:</td>
            <td><?php echo $request['FuelRequest']['aircraft_flight_no']; ?></td>
        </tr>
        <tr>
            <td>Location:</td>
            <td><?php echo $request['FuelRequest']['arr_dest']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">**** ALL TIMES ARE LOCAL OR GMT / UTC</td>
        </tr>
        <tr>
            <td>ETA:</td>
            <td colspan="3"><?php echo strtoupper(date('dMy - Hi e', strtotime($request['FuelRequest']['arr_eta']))); ?></td>
        </tr>
        <tr>
            <td>ETD:</td>
            <td colspan="3"><?php echo strtoupper(date('dMy - Hi e', strtotime($request['FuelRequest']['dept_etd']))); ?></td>
        </tr>
        <tr>
            <td>DEST:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['dept_dest']; ?></td>
        </tr>
        <tr>
            <td nowrap>EST. UPLIFT (USG):</td>
            <td colspan="3"><?php echo $request['FuelRequest']['est_uplift']; ?></td>
        </tr>
        <tr>
            <td>PRODUCT:</td>
            <td colspan="3"><?php echo $request['FuelRequest']['product']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td valign="top">Remarks:</td>
            <td colspan="3">
                <ol>
                    <li>PLEASE ALLOW 72 HOURS LEEWAY, IN CASE OF ANY DELAY.</li>
                    <li>PLEASE ADVICE THE DETAILS OF INTO-PLANE SERVICE PROVIDER</li>
                    <li>PLEASE INVOICE S.B. MAN & COMPANY LTD. AS AGREED</li>
                </ol>
            </td>
        </tr>
    </table>
</div>

<?php 
if( isset($show_footer) ) {
    echo $this->element("fuel/request_footer");
}
?>