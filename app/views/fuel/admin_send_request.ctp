<style>
    #send_wrapper {

    }

    #send_wrapper td {
        padding: 4px;
    }
</style>

<form action="<?php echo $html->url(array('controller' => 'email', 'action' => 'sendFuelRequestForm')); ?>" method="post" >
    <div id="send_wrapper">
        <h2>Sending Fuel Request</h2>
        <fieldset> 
            <?php
            echo $this->element('email_composition_form', array(
                'to' => array("TRAMP OIL" => "trampdispatch@wfscorp.com"),
                'cc' => array(
                    "S. B. MAN Fuel" => "sbman.fuel@sbmangh.com",
                    "Daniel Valentine" => 'dvalent@wfscorp.com',
                    "London Dispatch" => "londisp@wfscorp.com"
                ),
                'subject' => '',
                'editor_id' => 'request_editor',
            ));
            ?>

            <table border="0" cellspacing="0" id="">
                <tr>
                    <td>Attachments:</td>
                    <td>
                        <input type="checkbox" value="attach_request_form" name="attach_request_form" checked="" readonly />
                        <label for="attach_request_form">Request Form</label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>

    <div class="footer" style="padding: 0px 0px;">
        <input type="submit" class="button" value="Send Mail" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
        <input type="hidden" name="request_id" value="<?php echo $request['FuelRequest']['id']; ?>" />
    </div>
</form>

<script>
    setTimeout(function() {
        try {
            tinyMCE.init({
                mode : "exact",
                theme : "advanced",
                elements: "request_editor",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
			
                theme_advanced_resizing : true,
			
                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,code", //,image,code
                theme_advanced_buttons2 : ""
            });
        } catch(e) {
            console.log(e);
        } 
    }, 500);
    
</script>