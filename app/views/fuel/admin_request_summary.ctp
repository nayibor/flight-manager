<style>
    .data_row {
        padding: 5px 0px;
    }
</style>

<div class="details" style="font-family: tahoma; font-size: 11px; line-height: 20px;">

    <?php
    $statuses = array('on hold', 'canceled', 'closed', 'uplifted');
    if (in_array($request['FuelRequest']['status'], $statuses)) {
        ?>
        <div class="data_row">
            <b>Request Status</b>
            <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
                <tr>
                    <td width="110">Status: </td>
                    <td><?php echo strtoupper($request['FuelRequest']['status']); ?></td>
                </tr>
                <tr>
                    <td>Comments:</td>
                    <td><?php echo isset($request['FuelRequestStatus'][0]) ? $request['FuelRequestStatus'][0]['comments'] : ""; ?></td>
                </tr>
            </table>
        </div>
    <?php } ?>
    
    
    <div class="data_row">
        <b>Request Information</b> 
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td width="120" valign="top">Purchase Order No: </td>
                <td><?php echo $request['FuelRequest']['ref_no']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Date Requested: </td>
                <td><?php echo date("d/m/y - H:i e", strtotime($request['FuelRequest']['created_dt'])); ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Confirmation SO#: </td>
                <td><?php echo $request['FuelRequest']['confirmation_code']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Ticket No.: </td>
                <td><?php echo $request['FuelRequest']['confirmation_ticket_no']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Confirmation Date: </td>
                <td><?php echo $request['FuelRequest']['confirmation_dt'] ? date("d/m/y - H:i e", strtotime($request['FuelRequest']['confirmation_dt'])) : ""; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="data_row">
        <b>Company Information</b> 
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td width="120" valign="top">Name: </td>
                <td><?php echo $request['FuelRequest']['company_name']; ?></td>
            </tr>
            <tr>
                <td width="120" valign="top">ATTN: </td>
                <td><?php echo $request['FuelRequest']['company_attn']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Email: </td>
                <td><?php echo $request['FuelRequest']['company_email']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Telephone: </td>
                <td><?php echo $request['FuelRequest']['company_tel']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Fax: </td>
                <td><?php echo $request['FuelRequest']['company_fax']; ?></td>
            </tr>
            <tr>
                <td width="110" valign="top">Card Holder Name: </td>
                <td>
                    <?php echo $request['FuelRequest']['company_card_name']; ?>
                </td>
            </tr>
            <tr>
                <td width="110" valign="top">Card Number: </td>
                <td>
                    <?php echo $request['FuelRequest']['company_card_info']; ?>
                </td>
            </tr>
            <tr>
                <td width="110" valign="top">Card Expires: </td>
                <td>
                    <?php echo $request['FuelRequest']['company_card_expiry_dt']; ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="data_row">
        <b>Flight Information</b> 
        <table border="0" cellspacing="0" cellpadding="5" width="100%" style="margin: 0px 15px;">
            <tr>
                <td width="120">Operator Name:</td>
                <td><?php echo $request['FuelRequest']['aircraft_optr']; ?></td>
            </tr>
            <tr>
                <td>Aircraft Type:</td>
                <td><?php echo $request['FuelRequest']['aircraft_type']; ?></td>
            </tr>
            <tr>
                <td width="110">Aircraft Regn.:</td>
                <td><?php echo $request['FuelRequest']['aircraft_regn']; ?></td>
            </tr>
            <tr>
                <td>Flight No.:</td>
                <td><?php echo $request['FuelRequest']['aircraft_flight_no']; ?></td>
            </tr>
            <tr>
                <td>Location:</td>
                <td><?php echo $request['FuelRequest']['arr_dest']; ?></td>
            </tr>
            <tr>
                <td>FBO / Agent / Fueler:</td>
                <td><?php echo $request['FuelRequest']['fueler']; ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>ETA:</td>
                <td><?php echo strtoupper(date('dMy - Hi e', strtotime($request['FuelRequest']['arr_eta']))); ?></td>
            </tr>
            <tr>
                <td>ETD:</td>
                <td><?php echo strtoupper(date('dMy - Hi e', strtotime($request['FuelRequest']['dept_etd']))); ?></td>
            </tr>
            <tr>
                <td>Destination:</td>
                <td><?php echo $request['FuelRequest']['dept_dest']; ?></td>
            </tr>
            <tr>
                <td>Est. Uplift (UGS):</td>
                <td><?php echo $request['FuelRequest']['est_uplift']; ?></td>
            </tr>
            <tr>
                <td>Product:</td>
                <td><?php echo $request['FuelRequest']['product']; ?></td>
            </tr>
            <tr>
                <td valign="top">Remarks:</td>
                <td><?php echo str_replace("\n", "<br />", $request['FuelRequest']['remarks']); ?></td>
            </tr>
        </table>
    </div>
</div>
