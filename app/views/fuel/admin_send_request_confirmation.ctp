<style>
    #send_wrapper {

    }

    #send_wrapper td {
        padding: 4px;
    }
</style>

<form action="<?php echo $html->url(array('controller' => 'email', 'action' => 'sendFuelRequestConfirmation')); ?>" method="post" >
    <div id="send_wrapper">
        <h2>Sending Fuel Confirmation</h2>

        <fieldset>
            <?php
            echo $this->element('email_composition_form', array(
                'to' => array($request['FuelRequest']['company_name'] => $request['FuelRequest']['company_email']),
                'cc' => array("S. B. MAN Fuel" => "sbman.fuel@sbmangh.com"),
                'subject' => 'Fuel Request Confirmation',
                'editor_id' => 'rc_editor',
            ));
            ?>
            <table border="0" cellspacing="0" id="">
                <tr>
                    <td>Attachments:</td>
                    <td>
                        <input type="checkbox" value="attach_request_form" name="attach_request_form" checked="" readonly />
                        <label for="attach_request_form" style="width: 300px;">Request Confirmation Form</label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>

    <div class="footer" style="padding: 0px 0px;">
        <input type="submit" class="button" value="Send Mail" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
        <input type="hidden" name="request_id" value="<?php echo $request['FuelRequest']['id']; ?>" />
    </div>
</form>

<script>
    setTimeout(function() {
        try {
            tinyMCE.init({
                mode : "exact",
                theme : "advanced",
                elements: "rc_editor",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
			
                theme_advanced_resizing : true,
			
                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,code", //,image,code
                theme_advanced_buttons2 : ""
            });
        } catch(e) {
            console.log(e);
        } 
    }, 500);
    
</script>