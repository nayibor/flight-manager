<div class="form">
    <?php echo $this->Form->create(); ?>
        <fieldset>
            <legend>Enter Request Confirmation Reference Information from WFS</legend>

            <div class="input">
                <label>SBMAN Ref No: </label>
                <strong><?php echo $request['FuelRequest']['ref_no']; ?></strong>
            </div>

            <div class="input">
                <label>Location: </label>
                <strong><?php echo $request['FuelRequest']['arr_dest']; ?></strong>
            </div>

            
            <div class="input">
                <label>Ref. SO#</label>
                <input type="text" name="data[FuelRequest][confirmation_code]" required class="required" value="<?php echo $request['FuelRequest']['confirmation_code']; ?>" />  
            </div>
            
            <div class="input">
                <label>Ticket No:</label>
                <input type="text" name="data[FuelRequest][confirmation_ticket_no]" value="<?php echo $request['FuelRequest']['confirmation_ticket_no']; ?>" />  
            </div>
            
            <div class="input">
                <label>FBO / Agent / Fueler</label>
                <input type="text" name="data[FuelRequest][fueler]" required class="required" value="<?php echo $request['FuelRequest']['fueler']; ?>" />  
            </div>
            
            <div class="input">
                <label>Product Price</label>
                <input type="text" name="data[FuelRequest][product_selling_price]" value="<?php echo $request['FuelRequest']['product_selling_price']; ?>" />  
            </div>
        </fieldset>

        <div>
            <input type="submit" class="button" id="submit_btn" value="Update Request" />
            <input type="button" class="button" id="cancel-form" value="Cancel" />
            <input type="hidden" name="data[FuelRequest][id]" value="<?php echo $request['FuelRequest']['id']; ?>" />
        </div>
    <?php echo $this->Form->end(); ?>
</div>