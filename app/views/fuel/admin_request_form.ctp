<style>
    .fuel_form_wrapper {

    }

    .fuel_form_wrapper td {
        padding: 3px;
    }

    .fuel_form_wrapper tr.header {
        font-size: 15px;
        font-weight: bold;
    }

    .fuel_form_wrapper tr.header td {
        border-bottom: dotted #999 1px;
    }
</style>

<h2>Fuel Request Details</h2>

<form action="<?php echo $this->here; ?>" method="post" name="fuel_form">
    <div class="fuel_form_wrapper">
        <table border="0" cellpadding="10" width="100%">
            <!--<tr class="header">
                <td colspan="4">Reference Information</td>
            </tr>
            <tr>
                <td>Reference Number:</td>
                <td colspan="3">
                    <input type="text" name="data[FuelRequest][ref_no]" required value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['ref_no'] : $next_ref_no; ?>" />
                    <span class="ui-corner-all ui-state-error" style="padding: 5px; display: inline-block;">Shown Temporarily. Will Be Removed/Hidden Later</span>
                </td>
            </tr>-->
            <tr>
                <td nowrap>Fuel Service Provider:</td>
                <td colspan="3">
                    <input type="text" name="data[FuelRequest][fuel_service_provider]" required value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['fuel_service_provider'] : "World Fuel Services"; ?>" />
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr class="header">
                <td colspan="4">Company & Payment Details</td>
            </tr>
            <tr>
                <td width="15%">Company Name:</td>
                <td><input type="text" name="data[FuelRequest][company_name]" required value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_name'] : ""; ?>" /></td>
                <td width="17%">For ATTN:</td>
                <td><input type="text" name="data[FuelRequest][company_attn]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_attn'] : ""; ?>" /></td>
            </tr>
            <tr>
                <td>Telephone:</td>
                <td><input type="text" name="data[FuelRequest][company_tel]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_tel'] : ""; ?>" /></td>
                <td>Fax:</td>
                <td><input type="text" name="data[FuelRequest][company_fax]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_fax'] : ""; ?>" /></td>
            </tr>
            <tr>
                <td>Email: </td>
                <td><input type="text" name="data[FuelRequest][company_email]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_email'] : ""; ?>" /></td>
                <td>Card Holder Name:</td>
                <td><input type="text" name="data[FuelRequest][company_card_name]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_card_name'] : ""; ?>" /></td>
            </tr>
            <tr>
                <td>Card No & Type: </td>
                <td><input type="text" name="data[FuelRequest][company_card_info]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_card_info'] : ""; ?>" /></td>
                <td>Card Expiry Date:</td>
                <td><input type="text" name="data[FuelRequest][company_card_expiry_dt]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['company_card_expiry_dt'] : ""; ?>" /></td>
            </tr>

            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr class="header">
                <td colspan="4">Flight Details</td>
            </tr>
            <tr>
                <td>Airline / Operator</td>
                <td><input type="text" name="data[FuelRequest][aircraft_optr]" required value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['aircraft_optr'] : ""; ?>" ></td>
                <td>Aircraft Type:</td>
                <td><input type="text" name="data[FuelRequest][aircraft_type]" required value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['aircraft_type'] : ""; ?>" /></td>
            </tr>
            <tr>
                <td>Aircraft Regn. No.:</td>
                <td><input type="text" name="data[FuelRequest][aircraft_regn]" required value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['aircraft_regn'] : ""; ?>" /></td>
                <td>Flight No:</td>
                <td><input type="text" name="data[FuelRequest][aircraft_flight_no]" required value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['aircraft_flight_no'] : ""; ?>" /></td>
            </tr>
            <tr>
                <td>Location:</td>
                <td><input type="text" class="location" name="data[FuelRequest][arr_dest]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['arr_dest'] : ""; ?>"  /></td>
                <td>Destination:</td>
                <td><input type="text" class="location" name="data[FuelRequest][dept_dest]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['dept_dest'] : ""; ?>" /></td>
                
            </tr>
            <tr>
                <td>ETA: </td>
                <td><input type="text" class="datetime" name="data[FuelRequest][arr_eta]" value="<?php echo isset($fuelRequest) ? date('Y-m-d H:i', strtotime($fuelRequest['FuelRequest']['arr_eta'])) : ""; ?>" style="vertical-align: top;" /></td>
                <td>ETD: </td>
                <td><input type="text" class="datetime" name="data[FuelRequest][dept_etd]" value="<?php echo isset($fuelRequest) ? date('Y-m-d H:i', strtotime($fuelRequest['FuelRequest']['dept_etd'])) : ""; ?>" style="vertical-align: top;" /></td>
            </tr>
            <tr>
                <td>Product: </td>
                <td><input type="text" name="data[FuelRequest][product]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['product'] : ""; ?>" /></td>
                <td>EST. UPLIFT (USG):</td>
                <td><input type="text" name="data[FuelRequest][est_uplift]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['est_uplift'] : ""; ?>" /></td>
            </tr>
            <!--<tr>
                <td valign="top">Remarks: </td>
                <td colspan="3">
                    <textarea style="width: 90%; height: 60px;" name="data[FuelRequest][remarks]"><?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['remarks'] : ""; ?></textarea>
                </td>
            </tr>-->
        </table>
    </div>

    <div class="footer" style="padding: 10px; text-align: center;">
        <input type="hidden" name="data[FuelRequest][id]" value="<?php echo isset($fuelRequest) ? $fuelRequest['FuelRequest']['id'] : ""; ?>" />
        <input type="submit" class="button" value="Add Request" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
    </div>
</form>