<?php echo $html->css('app/columns.css'); ?>

<div class="columns">

    <div class="column" id="users_column">
        <div class="header">Fuel Request Management</div>
        <div class="subheader">
            <ul style="display: inline-block;">
                <li class="button">
                    <a id="btn_new_request" title="New Fuel Request" href="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'requestForm')); ?>">New Request</a>
                </li>
                <!--<li id="selected" class="menu_button" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'changeStatus')); ?>">
                    <a href="#">With Selected</a>

                    <ul class="menu">
                        <li data-status="open">Open Requests</li>
                        <li data-status="canceled">Cancel Requests</li>
                        <li data-status="closed">Close Requests</li>
                    </ul>
                </li>-->
            </ul>


            <div style="display: inline-block; margin-left: 40px;">
                Find Request: <input class="search" type="text" placeholder="SBF001-11" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'list', '?status=open')); ?>" onclick="this.focus();"/> 
                By: <select id="search_term" style="font-size: 11px;">
                    <option value=""></option>
                    <option value="FuelRequest.ref_no" selected data-placeholder="SBFXXX-XX">SBF Ref. No.</option>
                    <option value="FuelRequest.company_name" data-placeholder="Company Name">Company Name</option>
                    <option value="FuelRequest.confirmation_code" data-placeholder="e.g. 444444">Sale Order #</option>
                    <option value="FuelRequest.arr_dest" data-placeholder="e.g. DNNA">Location</option> 
                    <option value="FuelRequest.aircraft_optr" data-placeholder="Operator">Operator</option>
                    <option value="FuelRequest.aircraft_type" data-placeholder="Aircraft Type">Aircraft Type</option>
                    <option value="FuelRequest.aircraft_regn" data-placeholder="Aircraft Regn">Aircraft Regn</option>
                    <option value="FuelRequest.status" data-placeholder="e.g. open">Status</option>
                </select>

            </div>

        </div>
        <div class="content">

            <div class="master_block" style="width: 55%;">

                <div id="tabs">
                    <ul>
                        <li><a title="open_perms" href="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'list', '?status=open')); ?>">Open Requests</a></li>
                        <li><a title="closed_perms" href="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'list', '?status=uplifted')); ?>">Closed Requests</a></li>
                        <li><a title="cancelled_perms" href="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'list', '?status=canceled')); ?>">Canceled Requests</a></li>
                    </ul>
                </div>
            </div>

            <div class="details_block" style="width: 45%;">
                <!--<h3>Permit Details</h3>-->
                <div class="details_options">
                    <ul class="buttons">
                        <li id="edit-request" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'requestForm')); ?>">Edit</li>
                        <li id="delete-request" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'delete_request')); ?>">Delete</li>

                        <li id="actions" class="menu_button activatable" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'setStatus')); ?>">
                            <span>Actions &nbsp; </span>
                            <ul class="menu" style="width: 170px;">
                                <!--<li id="send-to-caa" data-type="html" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'requestForm')); ?>">Forward To WFS</li>-->
                                <li id="send-request" data-type="html" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'viewRequest')); ?>" >View/Send Request</li>
                                <li id="enter-confirmation" data-type="html" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'confirmationForm')); ?>" >Enter Confirmation Info</li>
                                <li id="confirmation-form" data-type="html" data-status="approved" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'requestConfirmation')); ?>">Generate Confirmation</li>
                                <li class="divider" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                                <li id="attach_original_request" data-type="html" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'addAttachment')); ?>" >Add/View Request Documents</li>
                                <li class="divider" style="height: 1px; padding: 0px; background-color: #777;">&nbsp;</li>
                                <li id="hold-request" data-type="html" data-status="on hold" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'changeStatus')); ?>" >Hold Request</li>
                                <li id="cancel-request" data-type="html" data-status="canceled" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'changeStatus')); ?>">Cancel Request</li>
                                <li id="close-request" data-type="html" data-status="closed" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'changeStatus')); ?>">Close Request</li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="details_pane">

                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="authority_details_url" value="<?php echo $html->url(array('controller' => 'authorities', 'action' => 'contact_details')); ?>" />

<?php echo $html->script('app/attachments.js'); ?>
<?php echo $html->script('app/fuel_requests.js'); ?>