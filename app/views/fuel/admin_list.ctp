

<div>
    <table width="100%" border="0" cellspacing="0" cellpadding="5" id="requests_table" class="fullwidth <?php echo $status; ?>">
        <thead>
            <tr>
                <th width="5%"><input type="checkbox" class="check_all" /></th>
                <th colspan="2">REF No.</th>
                <th nowrap>COMPANY NAME</th>
                <th>AIRCRAFT OPTR</th>
                <th>LOCATION</th>
                <th>STATUS</th>
            </tr>
        </thead>

        <tbody>
            <?php 
            $count = $this->Paginator->counter('%start%');  // passed from controller
            foreach ($requests as $request) { ?>
                <tr data-id="<?php echo $request['FuelRequest']['id']; ?>" data-url="<?php echo $html->url(array('controller' => 'fuel', 'action' => 'request_summary', $request['FuelRequest']['id'])); ?>">
                    <td><input type="checkbox" value="<?php echo $request['FuelRequest']['id']; ?>" /></td>
                    <td align="right"><span class="numbers"><?php echo $count++ . "."; ?></span> </td>
                    <td nowrap><?php echo $request['FuelRequest']['ref_no']; ?></td>
                    <td class="company_name" nowrap><?php echo $request['FuelRequest']['company_name']; ?></td>
                    <td><?php echo $request['FuelRequest']['aircraft_optr']; ?></td>
                    <td><?php echo $request['FuelRequest']['arr_dest']; ?></td>
                    <td class="status"><?php echo $request['FuelRequest']['status']; ?></td>

                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<?php echo $this->element('layout/pagination_footer', array('ajax' => true, 'url_params' => array('status' => $status))); ?>

<!--
<div class="pane-footer">
    Page 
    <ul>
        <?php 
        /*for($i = 1; $i <= $pages; $i++) { 
            $offset = ($i - 1) * $pg_size; // passed from controller
            $class = $offset == $pg_offset ? "selected" : "";
            echo $offset . " - " . $pg_offset; ?>
            <li class="<?php echo $class; ?>" data-url="<?php echo $html->url( array('controller' => 'fuel', 'action' => 'list', '?status=' . $status . "&offset=" . $offset . (isset($term) ? "&term=" . $term : "") )); ?>"><?php echo $i; ?></li>
        <?php }*/ ?>
    </ul>
</div>
-->