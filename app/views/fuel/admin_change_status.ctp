<style>
    .change_form td {
        padding: 5px;
    }
</style>

<form name="change_form" class="change_form" action="<?php echo $html->url(array('controller' => 'Fuel', 'action' => 'changeStatus')); ?>">

    <fieldset>
        <table>
            <tr>
                <td><b>Request Ref. No.:</b></td>
                <td><?php echo $request['FuelRequest']['ref_no']; ?></td>
            </tr>
            <tr>
                <td><b>New Status</b></td>
                <td>
                    <?php
                    $status = $status == "closed" ? 'uplifted' : $status;
                    $statuses = array('open' => 'Open', 'on hold' => 'On Hold', 'canceled' => 'Canceled', 'uplifted' => 'Uplifted', 'deleted' => "Deleted")
                    ?>
                    <select name="data[FuelRequest][status]" required>
                        <option value="">Select Status</option>
                        <?php
                        foreach ($statuses as $key => $value) {
                            $selected = $key == $status ? "selected" : "";
                            echo "<option value='$key' $selected>$value</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><b>Billed</b></td>
                <td><input type="checkbox" name="data[FuelRequest][billed]" value="1" <?php echo $request['FuelRequest']['billed'] == 1 ? "checked=checked" : ""; ?> /></td>
            </tr>
            <tr>
                <td valign="top"><b>Comments:</b></td>
                <td><textarea name="comments" style="width: 350px; height: 120px;" placeholder="Reason For Status Change" required></textarea></td>
            </tr>
        </table>
    </fieldset>
    
    <div style="padding: 10px;">
        <input type="submit" class="button" value="Change Status" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
        <input type="hidden" name="data[FuelRequest][id]" value="<?php echo $request['FuelRequest']['id']; ?>" />
    </div>
</form>
