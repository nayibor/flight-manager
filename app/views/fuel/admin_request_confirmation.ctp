<h2>Fuel Request Confirmation</h2>

<?php echo $this->element('fuel/request_confirmation_content', array('request' => $request)); ?>

<div class="footer" style="padding: 10px; margin-top: 10px;">
    <input type="button" class="button dialog_opener" value="Send Confirmation" data-height="550" data-width="800" data-url="<?php echo $html->url(array('action' => 'sendRequestConfirmation', $request['FuelRequest']['id'])); ?>" />
    <input type="button" class="button" value="Print" id="rc_print_btn" data-url="<?php echo $html->url(array('action' => 'printRequestConfirmation', $request['FuelRequest']['id'])); ?>"/>
    <input type="button" class="button" value="Download" id="rc_download" data-url="<?php echo $html->url(array('action' => 'printRequestConfirmation', $request['FuelRequest']['id'], 'download')); ?>"/>
    <input type="button" class="button" value="Cancel" id="cancel-form" />
</div>

<script>
    $("#rc_print_btn").click(function() {
        window.open( $(this).data('url') );
    });
    
    $("#rc_download").click(function() {
        window.location.href = $(this).data('url');
    });
</script>