<style>
    .supervision table td {
        padding: 10px 5px;
    }
</style>

<div class="supervision">
    <h2><?php echo $department['Department']['name']; ?></h2>

    <?php echo $this->Form->create(); ?>
        <table cellpadding="5" cellspacing="0" border="0 width="100%">
            <tr>
                <td>Staff</td>
                <td colspan="3">
                    <select name="data[DepartmentSupervisor][user_id]">
                        <?php foreach( $users as $user ) {
                            $selected = isset($schedule) && $schedule['DepartmentSupervisor']['user_id'] == $user['User']['id'] ? "selected" : ""; ?>
                            <option <?php echo $selected; ?> value="<?php echo $user['User']['id']; ?>"><?php echo $user['User']['first_name'] . ' ' . $user['User']['last_name']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Start Time</td>
                <td><input type="text" name="data[DepartmentSupervisor][start_dt]" class="datetime" value="<?php echo isset($schedule) ? $schedule['DepartmentSupervisor']['start_dt'] : ""; ?>" /></td>
                <td>End Time</td>
                <td><input type="text" name="data[DepartmentSupervisor][end_dt]" class="datetime" value="<?php echo isset($schedule) ? $schedule['DepartmentSupervisor']['end_dt'] : ""; ?>" /></td>
            </tr>
            <tr>
                <td>Shift</td>
                <td colspan="3">
                    <input type="radio" name="data[DepartmentSupervisor][shift_type]" id="morning" value="morning" <?php echo isset($schedule) && $schedule['DepartmentSupervisor']['shift_type'] == "Morning" ? "checked" : "" ?> />
                    <label for="morning">Morning</label>

                    <input type="radio" name="data[DepartmentSupervisor][shift_type]" id="evening" value="evening" <?php echo isset($schedule) && $schedule['DepartmentSupervisor']['shift_type'] == "Evening" ? "checked" : "" ?>/>
                    <label for="evening">Evening</label>
                    
                    <input type="radio" name="data[DepartmentSupervisor][shift_type]" id="allday" value="all day" <?php echo isset($schedule) && $schedule['DepartmentSupervisor']['shift_type'] == "All Day" ? "checked" : "" ?> />
                    <label for="allday">All Day</label>
                </td>
            </tr>

            <tr>
                <td colspan="4" style="padding-top: 40px;">
                    <input type="submit" value="<?php echo isset($schedule) ? "Save Changes" : "Add Schedule"; ?>" />
                    <input type="button" id="cancel-form" value="Cancel" />
                    <input type="hidden" name="data[DepartmentSupervisor][department_id]" value="<?php echo $department['Department']['id']; ?>" />
                    
                    <?php if( isset($schedule) ) { ?>
                        <input type="hidden" name="data[DepartmentSupervisor][id]" value="<?php echo $schedule['DepartmentSupervisor']['id']; ?>" />
                    <?php } ?>
                </td>
            </tr>
        </table>
    <?php echo $this->Form->end(); ?>

</div>