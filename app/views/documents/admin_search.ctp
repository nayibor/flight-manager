<style>
    .term_highlight {
        background-color: yellow;
    }
    
    blockquote {
        padding: 12px 10px;
        border-left: solid #ccc 4px;
        font-size: 12px;
        font-style: italic;
        margin-bottom: 10px;
        background-color: #f6f6f6;
        
    }
</style>

<blockquote>Search Results For <b><?php echo $term; ?></b></blockquote>

<?php 
$search_terms = array( strtolower($term), strtoupper($term), ucwords($term), ucfirst($term) );
$replace_terms = array();

foreach($search_terms as $term) {
    $replace_terms[] = "<span class='term_highlight'>$term</span>";
}

foreach($documents as &$document) {
    $document['Document']['name'] = str_replace($search_terms, $replace_terms, $document['Document']['name']);
    $document['Document']['abstract'] = str_replace($search_terms, $replace_terms, $document['Document']['abstract']);
}

echo $this->element('documents/document_list', array('documents' => $documents)); 

?>

