<style>
    .dash_header {
        padding: 8px;
        border: solid #ddd 1px;
        border-radius: 3px;
        background-color: #f9f9f9;
        margin-bottom: 10px;
        position: relative;
    }

    .dash_header h2 {
        margin: 0px;
    }

    .dash_header span {
        position: absolute;
        right: 12px;
        top: 6px;
        font-size: 12px;
    }

    .dash_header span input[type=text] {
        font-size: 12px;
    }
</style>
<?php echo $html->css('app/columns.css'); ?>

<div class="dash_header">
    <h2>Miscellaneous Documents</h2>

    <span>
        Find Document: <input type="text" placeholder="Search Text Here" id="search_field" data-url="<?php echo $this->Html->url(array('controller' => 'documents', 'action' => 'search')); ?>" style="width: 240px;" />
    </span>
</div>

<div class="columns">
    <div class="column span1">
        <div class="header">Document Categories</div>
        <div class="content">
            <ul id="category-list" class="list">
               <?php 
                foreach($categories as $category) {
                    $url = $html->url( array('controller' => 'documents', 'action' => 'list', $category['DocumentCategory']['id']));
                    echo "<li data-id='{$category['DocumentCategory']['id']}'><a href='" . $url . "'>" . $category['DocumentCategory']['name'] . "</a></li>";
                }
                ?>
            </ul>

        </div>
    </div>

    <div class="column span3" id="documents_main">
        <div class="header">Recent Documents</div>
        <div class="subheader">
            <ul>
                <li class="button"><a class="dialog_opener" id="add_document" title="Add Document" href="<?php echo $html->url(array('controller' => 'documents', 'action' => 'add')); ?>" data-width="800" data-height="550">Add Document</a></li>
                <li class="button"><a id="delete_selected" href="<?php echo $html->url(array('controller' => 'documents', 'action' => 'delete_selected')); ?>">Delete Selected</a></li>
            </ul>
        </div>
        <div class="content">
            <?php echo $this->element('documents/document_list'); ?>
        </div>

        <div class="footer">
            Page: <?php echo $this->Paginator->numbers(); ?>
        </div>
    </div>

</div>

<input type="hidden" id="users_list_url" value="<?php echo $html->url(array('controller' => 'users', 'action' => 'getUsersList')); ?>" />

<?php echo $this->Html->script('app/attachments.js'); ?>
<?php echo $this->Html->script('app/documents/docmanager.js'); ?>