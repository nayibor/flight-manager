<style>
    #add_table td {
        font-size: 12px;
        padding: 5px;
    }

    #add_table :invalid {
        border: solid #900 2px;
    }
</style>

<form id="AttachmentForm" action="<?php echo $this->here; ?>" target="upload-frame" method="post" enctype="multipart/form-data">
    <table id="add_table" width="100%" cellpadding="5">
        <tr>
            <td>Document Category</td>
            <td>
                <select name="data[Document][document_category_id]" required="" id="document_category_id">
                    <option value="">- Select Category -</option>
                    <?php
                    foreach ($categories as $category) {
                        $name = $category['DocumentCategory']['name'];
                        $id = $category['DocumentCategory']['id'];
                        echo "<option value='$id'>$name</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td width="20%" valign="top">Choose File:</td>
            <td>
                <input type="file" style="width: 100%" required="" name="attachments[]" id="attachments_fs" />
                <br /><br />

                (One File May Be Selected At A Time. Max Total File Upload Size <b id="max_size_value"><?php echo ini_get("upload_max_filesize"); ?></b>)
            </td>
        </tr>
        <tr>
            <td valign="top">&nbsp;</td>
            <td style="padding-bottom: 25px;" id="file_list">&nbsp;</td>
        </tr>
        
        <tr>
            <td valign="top">Name of Document:</td>
            <td>
                <input type="text" name="data[Document][name]" width="100%" class="input" style="width: 400px;" />
            </td>
        </tr>

        <tr>
            <td valign="top">Document Abstract:</td>
            <td>
                <textarea name="data[Document][abstract]" width="100%"></textarea>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="submit" class="button" id="up_save_btn" value="Upload & Save"/>
                <input type="button" class="button" id="cancel-form" value="Cancel" />

            </td>
        </tr>
    </table>
</form>

<iframe width="10" height="10" src="about:home" style="opacity: 0;" name="upload-frame" id="upload-frame" />