<?php 
echo $html->css('app/columns.css'); 
echo $html->css('app/roles.css'); 
echo $html->css('app/jobsheet.css');
echo $html->script('app/hotel.js');
?>

<form  method="POST" id="hotel_process_form" action="<?php echo $this->here; ?>">
    <fieldset>
        <div class="input">
            <label for="role_name">Name</label>
            <input type="text" class="required" required name="data[Hotel][name]" id="data[Hotel][name]" value="<?php echo $hotel['Hotel']['name'] ?>"/>
        </div>
        <div class="input">
            <label for="role_name" style="vertical-align: top;">Address</label>
            <textarea name="data[Hotel][address]" cols="40" rows="4" style="display: inline-block; width: 400px; height: 50px;"><?php echo $hotel['Hotel']['address']; ?></textarea>
        </div>
        
        <div class="input">
            <label for="role_name">Country</label>
            <input type="text" name="data[Hotel][country]" size="30" value="<?php echo $hotel['Hotel']['country'] ?>" required />
        </div>
        
        <div class="input">
            <label for="role_name">Email </label>
            <input type="text" name="data[Hotel][email]" value="<?php echo $hotel['Hotel']['email']; ?>" />
        </div>
        <div class="input">
            <label for="role_name">Distance From Airport </label>
            <input type="text" name="data[Hotel][distance]" value="<?php echo $hotel['Hotel']['distance'] ?>"  />
        </div>
        <div class="input">
            <label for="role_name">Stars</label>
            <div style="display: inline-block;">
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    $checked = $hotel['Hotel']['stars'] == $i ? "checked=checked" : "";
                    echo "<input type='checkbox' name='data[Hotel][stars]' value='{$i}' class='star' {$checked} />";
                }
                ?>
            </div>
        </div>
        <div class="input">
            <label for="role_name">Contact Number </label>
            <input type="text" name="data[Hotel][contact_number]" value="<?php echo $hotel['Hotel']['contact_number'] ?>" />
        </div>
        <div class="input">
            <label for="role_name">Fax Number </label>
            <input type="text" name="data[Hotel][fax]" value="<?php echo $hotel['Hotel']['fax'] ?>" />
        </div>
        <div class="input">
            <label for="role_name">Number Of Rooms </label>
            <input type="number" name="data[Hotel][rooms]" size="6" value="<?php echo $hotel['Hotel']['rooms'] ?>" />
        </div>
        
        <div class="input">
            <label for="role_name">Website</label>
            <input type="text" name="data[Hotel][website]" size="60" value="<?php echo $hotel['Hotel']['website'] ?>" />
        </div>
        
        <div class="input">
            <label for="role_name" style="vertical-align: top;">Policy</label>
            <textarea name="data[Hotel][cancellation_policy]" cols="25" rows="10" style="display: inline-block; width: 370px;"><?php echo $hotel['Hotel']['cancellation_policy']; ?></textarea>
        </div>
        
        <input type="hidden" name="data[Hotel][id]" value="<?php echo $hotel['Hotel']['id'] ?>"/>

    </fieldset>

    <input type="submit" class="button" id="process_button_btn" value="Save Changes"/>
    <input type="button" class="button" id="cancel-form" value="Cancel"/>

</form>

