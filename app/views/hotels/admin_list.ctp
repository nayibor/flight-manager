<table border="0" class="fullwidth" cellpadding="8" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th width="20" align="right"><input type="checkbox" value="" id="checkall" /></th>
            <th colspan="2">Hotel Name</th>
            <th style="text-align: center;" width="100">Options</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $count = $this->Paginator->counter('%start%');
        foreach ($hotels as $val) {
            ?>
            <tr data-url="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'details', $val['Hotel']['id'])); ?>">
                <td align="right"><input type="checkbox" value="<?php echo $val['Hotel']['id']; ?>" /></td>
                <td align="right" width="20"><?php echo $count++ . "."; ?></td>
                <td class="hotel_name"><?php echo $val['Hotel']['name'] ?></td>
                <td align="center">
                    <a class="dialog_opener edit_link" title="Edit Hotel Information" href="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'edit', $val['Hotel']['id'])); ?>">Edit</a>
                    <a class="del" data-id="<?php echo $val['Hotel']['id'] ?>" del_url="<?php echo $html->url(array('controller' => 'hotels', 'action' => 'delete', $val['Hotel']['id'])); ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>