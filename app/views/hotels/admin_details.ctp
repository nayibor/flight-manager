<style>
    .details_table td {
        padding: 8px;
        font-size: 11px;
    }
</style>

<h2>Hotel Details</h2>

<table border="0" cellspacing="0" cellpadding="10" width="96%" class="details_table" align="center" style="margin: 0px auto;">
    <tr>
        <td width="120">Name</td>
        <td><?php echo $hotel['Hotel']['name']; ?></td>
    </tr>

    <tr>
        <td>Address</td>
        <td><?php echo str_replace("\n", "<br />", $hotel['Hotel']['address']); ?></td>
    </tr>
    
    <tr>
        <td>Country</td>
        <td><?php echo $hotel['Hotel']['country']; ?></td>
    </tr>

    <tr>
        <td>Contact Number</td>
        <td><?php echo $hotel['Hotel']['contact_number']; ?></td>
    </tr>
    
    <tr>
        <td>Fax Number</td>
        <td><?php echo $hotel['Hotel']['fax']; ?></td>
    </tr>
    
    <tr>
        <td>Email Address</td>
        <td><a href="mailto:<?php echo $hotel['Hotel']['email']; ?>"><?php echo $hotel['Hotel']['email']; ?></a></td>
    </tr>

    <tr>
        <td>Rating</td>
        <td>
            <?php
            for ($i = 1; $i <= 5; $i++) {
                $checked = $hotel['Hotel']['stars'] == $i ? "checked=checked" : "";
                echo "<input name='star{$hotel['Hotel']['id']}' type='radio' value='{$i}' class='star' {$checked} />";
            }
            ?>
        </td>
    </tr>

    <tr>
        <td>Distance from Airport</td>
        <td><?php echo $hotel['Hotel']['distance'] . " Km"; ?></td>
    </tr>
    
    <tr>
        <td>Number of Rooms</td>
        <td><?php echo $hotel['Hotel']['rooms']; ?></td>
    </tr>
    
    <tr>
        <td>Website</td>
        <td><a href="<?php echo $hotel['Hotel']['website']; ?>" target="_blank"><?php echo $hotel['Hotel']['website']; ?></a></td>
    </tr>
    
    <tr>
        <td>Hotel Policy:</td>
        <td><?php echo str_replace("\n", "<br />", $hotel['Hotel']['cancellation_policy']); ?></td>
    </tr>
</table>