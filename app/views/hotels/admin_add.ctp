<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php
echo $html->css('app/jobsheet.css');
echo $html->script('app/hotel.js');
?>


<!--<h2><?php __('Add Hotel'); ?></h2>-->

<form  method="POST" id="hotel_process_form" action="<?php echo $this->here; ?>">
    <fieldset>
        <div class="input">
            <label for="role_name">Name</label>
            <input type="text" class="required" required name="data[Hotel][name]" size="50" />
        </div>
        <div class="input">
            <label for="role_name" style="vertical-align: top;">Address</label>
            <textarea name="data[Hotel][address]" cols="40" rows="4" style="display: inline-block; width: 400px; height: 50px;"></textarea>
        </div>
        
        <div class="input">
            <label for="role_name">Country</label>
            <input type="text" name="data[Hotel][country]" size="30" />
        </div>
        
        <div class="input">
            <label for="role_name">Contact Number </label>
            <input type="text" name="data[Hotel][contact_number]" />
        </div>
        <div class="input">
            <label for="role_name">Fax Number </label>
            <input type="text" name="data[Hotel][fax]" />
        </div>
        <div class="input">
            <label for="role_name">Email </label>
            <input type="text" name="data[Hotel][email]" />
        </div>
        <div class="input">
            <label for="role_name">Distance From Airport</label>
            <input type="text" name="data[Hotel][distance]" id="data[Hotel][distance]" />
        </div>
        <div class="input">
            <label for="role_name">Stars</label>
            <div style="display: inline-block;">
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    echo "<input type='checkbox' name='data[Hotel][stars]' value='{$i}' class='star' />";
                }
                ?>
            </div>
        </div>

        <div class="input">
            <label for="role_name">Number Of Rooms </label>
            <input type="number" name="data[Hotel][rooms]" size="6" value="10" />
        </div>
        
        <div class="input">
            <label for="role_name">Website</label>
            <input type="text" name="data[Hotel][website]" size="60" />
        </div>
        <div class="input">
            <label for="role_name" style="vertical-align: top;">Policy</label>
            <textarea name="data[Hotel][cancellation_policy]" cols="40" rows="10" style="display: inline-block; width: 370px;"></textarea>
        </div>

    </fieldset>

    <input type="submit" class="button" id="process_button_btn" value="Add Hotel"/>
    <input type="button" class="button" id="cancel-form" value="Cancel" />

</form>
<input type="hidden" name="hotel_process" id="hotel_process" value="<?php echo $html->url(array('controller' => 'Hotels', 'action' => 'admin_addhotel')); ?>"/>
<input type="hidden" name="update_redir" id="update_redir" value="<?php echo $html->url(array('controller' => 'Hotels', 'action' => 'admin_index')); ?>"/>
