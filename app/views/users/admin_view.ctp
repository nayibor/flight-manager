<style type="text/css">
    .view dl {
        width: 450px;
        overflow: hidden;
    }

    .view dt {
        width: 120px;
        display: inline-block;
        padding: 4px;
        float: left;
    }

    .view dd {
        width: 300px;
        display: inline-block;
        padding: 4px;
    }
    #status{
        font-style: italic;
        font-family: sans-serif;
        width: 100px;
    }
</style>

<div class="users view">
    <h2><?php __('User'); ?></h2>
    <dl><?php
$i = 0;
$class = ' class="altrow"';
?>
<!--        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
        <?php echo $user['User']['id']; ?>
            &nbsp;
        </dd>-->
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('First Name'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
            <?php echo $user['User']['first_name']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Last Name'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
            <?php echo $user['User']['last_name']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Username'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
            <?php echo $user['User']['username']; ?>
            &nbsp;
        </dd>
        <!--        
        
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Password'); ?></dt>
                <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
        <?php echo $user['User']['password']; ?>
                    &nbsp;
                </dd>
                <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Pass Salt'); ?></dt>
                <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
        <?php echo $user['User']['pass_salt']; ?>
                    &nbsp;
                </dd>-->
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Email'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
            <?php echo $user['User']['email']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('User Role'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
            <?php echo $user['UserRole']['name']; // $this->Html->link(, array('controller' => 'user_roles', 'action' => 'view', $user['UserRole']['id'])); ?>
            &nbsp;
        </dd><dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Last Access'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
            <?php echo $user['User']['last_access_dt']; ?>
            &nbsp;
        </dd>
        <!--<dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Security Question'); ?></dt>
         <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
        <?php echo $user['User']['security_question']; ?>
            &nbsp;
        </dd>
       <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Security Answer'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
        <?php echo $user['User']['security_answer']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php __('Active'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
        <?php echo $user['User']['active']; ?>
            &nbsp;
        </dd>-->
    </dl>
</div>
<br></br>
<div class="users view">

    <h2>Change Password</h2>
    <dl>
        <dt class="altrow">Old Password</dt>
        <dd  class="altrow"><input type="password" id="old_password" name="old_password"  /><label id="status"></label></dd>
        <dt  class="altrow">New Password</dt>
        <dd  class="altrow"><input type="password" id="new_password" name="new_password" /></dd>
        <dt  class="altrow">Repeat New Password</dt>
        <dd  class="altrow"><input type="password" id="new_password_repeat" name="new_password_repeat" /><label id="status_new"></label></dd>
        <dt class="altrow"></dt>
        <dd  class="altrow">  
            <input type="submit" id="reset_password" name="reset_password" value="Reset Password" class="button" />
            <input type="button" class="button" id="cancel-form" value="Cancel" />
        </dd>
    </dl>
    <input type="hidden" name="userid" id="userid" value="<?php echo $user['User']['id'] ?>" />

    <input type="hidden" name="username" id="username" value="<?php echo $user['User']['username'] ?>" />
    <input type="hidden" name="check_oldpass" id="reset_url" value="<?php echo $html->url(array('controller' => 'Users', 'action' => 'admin_check_old_oldpasss')); ?>" />
    <input type="hidden" name="reset_pass" id="reset_pass" value="<?php echo $html->url(array('controller' => 'Users', 'action' => 'reset_pass')); ?>" />
    <input type="hidden" name="logout_url" id="logout_url" value="<?php echo $html->url(array('controller' => 'Users', 'action' => 'logout')); ?>" />

</div>
<?php echo $html->script('app/reset_password.js'); ?>
