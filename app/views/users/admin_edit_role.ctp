<form action="<?php echo $this->here; ?>" method="post" >
    <fieldset>
        <legend>Edit User Role Information</legend>

        <div class="input">
            <label for="role_name">Role Name</label>
            <input type="text" class="required" required name="data[UserRole][name]" value="<?php echo $role['UserRole']['name']; ?>" />
        </div>
        
        <div class="input">
            <label for="role_name">Role Tag</label>
            <input type="text" class="required" required name="data[UserRole][role_tag]" value="<?php echo $role['UserRole']['role_tag']; ?>" />
        </div>
        
        <div class="input">
            <label for="role_name">Description</label>
            <textarea type="text" name="data[UserRole][role_description]" cols="20" rows="3"><?php echo $role['UserRole']['role_description']; ?></textarea>
        </div>
        
        <div>
            <input type="hidden" name="data[UserRole][id]" value="<?php echo $role['UserRole']['id']; ?>" id="UserRoleId" />
            <input type="submit" value="Submit" />
            <input type="button" value="Cancel" id="cancel-form" />
        </div>
    </fieldset>
</form>