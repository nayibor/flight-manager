
<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->script('app/users.js'); ?>

<h2><?php __('Manage Users'); ?></h2>

<div class="columns">
    <div class="column span1">
        <div class="header">User Roles</div>
        <div class="content">
            <ul id="category-list" class="list">
                <?php 
                foreach($roles as $role) {
                    $url = $html->url( array('controller' => 'users', 'action' => 'loadRoleUsers', $role['UserRole']['id']));
                    echo "<li><a href='" . $url . "'>" . $role['UserRole']['name'] . "</a></li>";
                }
                ?>
            </ul>
            
        </div>
    </div>
    
    <div class="column span3" id="users_column">
        <div class="header">Users</div>
        <div class="subheader">
            <ul>
                <li class="button"><a class="dialog_opener" id="add_user" title="Add New User" href="<?php echo $html->url( array('controller' => 'users', 'action' => 'add')); ?>" data-width="800" data-height="500">Add User</a></li>
                <li class="button"><a id="delete_selected_users" href="<?php echo $html->url( array('controller' => 'users', 'action' => 'delete_selected')); ?>">Delete Selected Users</a></li>
            </ul>
        </div>
        <div class="content">
            Select Category On Left To Load Users Here
        </div>
        
        <div class="footer">
            Page
        </div>
    </div>
    
</div>

<input type="hidden" id="users_list_url" value="<?php echo $html->url( array('controller' => 'users', 'action' => 'getUsersList')); ?>" />
