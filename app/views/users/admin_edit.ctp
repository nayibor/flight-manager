<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php __('Admin Edit User'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('username');
        echo $this->Form->input('email');
        //echo $this->Form->input('user_role_id');
        ?>
        <div class="input select">
            <label>User Role</label>
            <select name="data[User][user_role_id]" class="required" required="required">
                <option value="">Select Role</option>
                <?php
                foreach ($roles as $role) {
                    $selected = $role['UserRole']['id'] == $this->data['User']['user_role_id'] ? "selected=selected" : "";
                    ?>
                    <option value="<?php echo $role['UserRole']['id']; ?>" <?php echo $selected; ?>><?php echo $role['UserRole']['name']; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        
        <div class="input select">
            <label>Department</label>
            <select name="data[User][department_id]" class="required" required="required">
                <option value="">Select Department</option>
                <?php
                foreach ($departments as $department) { 
                    $selected = $department['Department']['id'] == $this->data['User']['department_id'] ? "selected=selected" : ""; ?>
                    <option value="<?php echo $department['Department']['id']; ?>" <?php echo $selected; ?>><?php echo $department['Department']['name']; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        
        <div class="input select">
            <label>Team</label>
            <select name="data[User][team]" class="required">
                <option value="">Select Team</option>
                <?php
                $teams = array('Alpha Team', 'Bravo Team', 'Charlie Team', 'Delta Team');
                foreach ($teams as $team) { 
                    $selected = $team == $this->data['User']['team'] ? "selected=selected" : ""; 
                    ?>
                    <option value="<?php echo $team; ?>" <?php echo $selected; ?>><?php echo $team; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </fieldset>
    <div>
        <input type="submit" class="button" value="Save Changes" />
        <input type="button" class="button" value="Cancel" id="cancel-form" />
    </div>
    <?php $this->Form->end(); ?>
</div>
