
<?php echo $this->Form->create(); ?>
<fieldset>
    <legend>Add User Role</legend>

    <div class="input">
        <label for="role_name">Role Name</label>
        <input type="text" class="required" required name="data[UserRole][name]" />
    </div>

    <div class="input">
        <label for="role_name">Role Tag</label>
        <input type="text" class="required" required name="data[UserRole][role_tag]" />
    </div>

    <div class="input">
        <label for="role_name">Description</label>
        <textarea type="text" name="data[UserRole][role_description]" cols="20" rows="3"></textarea>
    </div>

    <div>
        <input type="submit" value="Save Role" />
        <input type="button" value="Cancel" id="cancel-form" />
    </div>
</fieldset>
<?php echo $this->Form->end(); ?>