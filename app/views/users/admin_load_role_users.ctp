<table border="0" cellpadding="5" cellspacing="0" class="fullwidth">
    <thead>
        <tr>
            <th width="25" style="text-align: center;"><input type="checkbox" class="check_all" /></th>
            <th colspan="2">Name</th>
            <th style="text-align: center;">Options</th>
        </tr>
    </thead>
    <tbody>
        <?php $count = 1; ?>
        <?php foreach ($users as $user) { ?>
            <tr id="user-row-<?php echo $user['User']['id']; ?>">
                <td>
                    <input type="checkbox" name="user_id[]" value="<?php echo $user['User']['id']; ?>" />
                </td>
                <td width="20"><?php echo $count++ . "."; ?></td>
                <td>
                    <?php echo $user['User']['first_name'] . " " . $user['User']['last_name']; ?>
                </td>
                <td class="options" align="center">
                    <a title="Edit Selected User Information" class="dialog_opener edit_link" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>" data-height="450">Edit</a>
                    <a title="Delete User From System" class="delete_link" data-id="<?php echo $user['User']['id']; ?>" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'delete', $user['User']['id'])); ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
