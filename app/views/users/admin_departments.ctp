<style type="text/css">
    .department {
        padding: 10px;
        margin-bottom: 10px;
        background-color: #fdfdff;
        border-radius: 5px;
        border: solid #f1f1f1 1px;
    }
    
    .department > h3 {
        border-bottom: dotted #ccc 1px;
        font-size: 14px;
        font-weight: 400;
        padding-bottom: 3px;
        color: #555;
    }
    
    .department .description {
        padding: 4px;
        line-height: 20px;
    }
    
    .department .schedules {
        margin-left: 20px;
        padding: 5px;
    }
    
    .department .schedules > header {
        margin-bottom: 10px;
        
    }
    
    .department a {
        text-decoration: none;
        color: #6c1818;
        font-size: 11px;
        margin: 0px 5px;
    }
    
    .department a:hover {
        color: #df9175;
    }
    
    .department .schedules td {
        font-size: 11px;
        padding: 6px;
    }
    
    .department .schedules thead td {
        color: #555;
        border-bottom: solid #eee 1px;
    }
    
</style>

<h2>Manage Department Supervision</h2>

<?php foreach( $departments as $department ) { ?>
    <div class="department">
        <h3>
            <?php echo $department['Department']['name']; ?>
            
            (<a class="dialog_opener schedule_add" title="Add Supervision Schedule" href="<?php echo $html->url( array('controller' => 'departments', 'action' => 'supervision_schedule', $department['Department']['id'])); ?>">Add Supervision Schedule</a>)
        </h3>

        <div class="description" style="font-size: 12px;"><?php echo $department['Department']['description']; ?></div>

        <div class="schedules">

            <table border="0" cellspacing="5" cellpadding="5" width="650" class="">
                <thead>
                <tr>
                    <td width="25%" colspan="2">Supervisor Name</td>
                    <td width="15%">Shift Type</td>
                    <td width="40%">Schedule</td>
                    <td align="center">Options</td>
                </tr>
                </thead>

                <?php $count = 1; foreach( $department['DepartmentSupervisor'] as $supervisors ) { ?>
                    <tr>
                        <td width="20" align="right"><?php echo $count++ . "."; ?></td>
                        <td><?php echo $supervisors['User']['first_name'] . " " . $supervisors['User']['last_name']; ?></td>
                        <td><?php echo $supervisors['shift_type']; ?></td>
                        <td><?php echo date('d M, Y - H:i', strtotime($supervisors['start_dt'])) . ' - ' . date('d M, Y - H:i', strtotime($supervisors['end_dt'])); ?></td>
                        <td align="center">
                            <a class="dialog_opener edit_link" title="Edit Schedule Information" href="<?php echo $html->url( array('controller' => 'departments', 'action' => 'supervision_schedule', $department['Department']['id'], $supervisors['id'])); ?>">Edit</a>
                            <a class="delete_link" href="<?php echo $html->url( array('controller' => 'departments', 'action' => 'delete_schedule', $supervisors['id'])); ?>">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            </table>

        </div>
    </div>
<?php } ?>

<?php echo $html->script('app/departments.js'); ?>