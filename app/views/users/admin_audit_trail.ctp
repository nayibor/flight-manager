
<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php echo $html->script('app/audit_trail.js'); ?>

<h2><?php __('User Action Audit Trail'); ?></h2>

<div id="tabs">
    <ul>
        <li><a href="#user_actions">Per User Actions</a></li>
        <li><a href="#search_actions">Search</a></li>
    </ul>

    <div id="user_actions">
        <div class="columns">
            <div id="roles" class="column">
                <div class="header">
                    User List
                    
                    <div class="actions">
                        <button id="purge_btn">Purge Actions</button>
                    </div>
                </div>
                <!--<div class="subheader">
                    <ul>
                        <li><a class="dialog_opener" id="add_role" title="Add New User Role" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'add_user_role')); ?>">Add New Role</a></li>
                        <li><a id="delete_selected_roles" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'delete_selected')); ?>">Delete Selected</a></li>
                    </ul>
                </div>-->
                <div class="content">
                    <table border="0" cellpadding="5" cellspacing="0" id="users_table" class="fullwidth">
                        <thead>
                            <tr>
                                <th colspan="2">Staff Name</th>
                                <th style="text-align: center;">Last Login</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 1; ?>
                            <?php
                            foreach( $users as $user ) {
                                $id = $user['User']['id'];
                                $url = $html->url(array('controller' => 'users', 'action' => 'role_menus', $user['User']['id']));
                                ?>
                                <tr data-id="<?php echo $user['User']['id']; ?>" data-url="<?php echo $url; ?>">
                                    <td width="10" align="right"><?php echo $count++ . "."; ?></td>
                                    <td>
                                        <?php
                                        $id = $user['User']['id'];
                                        $url = $html->url(array('controller' => 'users', 'action' => 'edit_role', $user['User']['id']));
                                        echo "<a class='role_link' href='#'>" . $user['User']['last_name'] . " " . $user['User']['first_name'] . "</a>";
                                        ?>
                                    </td>
                                    <td align="center">
                                        <?php echo $user['User']['last_access_dt'] != "0000-00-00 00:00:00" ? date('jS M y - H:i', strtotime($user['User']['last_access_dt'])) : ""; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="column span2" id="actions_display">
                <div class="header">Recent Actions</div>

                <div class="content">

                </div>
            </div>

        </div>
    </div>

    <div id="search_actions">
        <div class="columns">
            <div class="column" id="users_column">
                <div class="subheader">

                    <div style="margin-left: 10px;">
                        
                        <ul>
                            <li style="display: inline-block;">
                                Find Actions By:  <input id="search_field" class="search" type="text" placeholder="Description" data-url="<?php echo $html->url(array('controller' => 'users', 'action' => 'searchActions')); ?>" onclick="this.focus();"/> 
                            </li>
                            
                            <li style="display: inline-block; border-left: dotted #999 1px; padding-left: 20px;">
                                OR Date Range: 
                                <input class="date" type="text" placeholder="Start Date" id="start_dt" onclick="this.focus();" />
                                <input class="date" type="text" placeholder="End Date" id="end_dt" onclick="this.focus();" />

                                <input type="button" value="Search" id="search_trigger" />
                            </li>
                        </ul>
                        
                    </div>

                </div>
                <div class="content">
                </div>
            </div>
        </div>

    </div>
</div>


<input type="hidden" id="user_actions_list_url" value="<?php echo $html->url(array('controller' => 'users', 'action' => 'audit_list')); ?>" />
<input type="hidden" id="user_actions_search_url" value="<?php echo $html->url(array('controller' => 'users', 'action' => 'audit_search')); ?>" />
<input type="hidden" id="purge_actions_url" value="<?php echo $html->url(array('controller' => 'users', 'action' => 'purge_actions')); ?>" />