
<?php echo $html->css('app/columns.css'); ?>
<?php echo $html->css('app/roles.css'); ?>
<?php echo $html->script('app/users.js'); ?>
<?php echo $html->script('app/roles.js'); ?>

<h2><?php __('Manage Roles and Permissions'); ?></h2>

<div class="columns">
    <div id="roles" class="column">
        <div class="header">User Roles</div>
        <div class="subheader">
            <ul>
                <li class="button"><a class="dialog_opener" id="add_role" title="Add New User Role" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'add_user_role')); ?>">Add New Role</a></li>
                <!--<li><a id="delete_selected_roles" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'delete_selected')); ?>">Delete Selected</a></li>-->
            </ul>
        </div>
        <div class="content">
            <table border="0" cellpadding="5" cellspacing="0" id="roles_table" class="fullwidth">
                <thead>
                    <tr>
                        <th colspan="2">Role Name</th>
                        <th style="text-align: center;">Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count = 1; ?>
                    <?php
                    foreach ($roles as $role) {
                        $id = $role['UserRole']['id'];
                        $url = $html->url(array('controller' => 'users', 'action' => 'role_menus', $role['UserRole']['id']));
                        ?>
                        <tr data-id="<?php echo $role['UserRole']['id']; ?>" data-url="<?php echo $url; ?>">
                            <td width="10" align="right"><?php echo $count++ . "."; ?></td>
                            <td>
                                <?php
                                $id = $role['UserRole']['id'];
                                $url = $html->url(array('controller' => 'users', 'action' => 'edit_role', $role['UserRole']['id']));
                                echo "<a class='role_link dialog_opener' data-role-id='$id' href='" . $url . "'>" . $role['UserRole']['name'] . "</a>";
                                ?>
                            </td>
                            <td align="center">
                                <a class="edit_link dialog_opener" title="Edit User Role Information" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'edit_role', $role['UserRole']['id'])); ?>">Edit</a>
                                <a class="delete_link" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'delete_role', $role['UserRole']['id'])); ?>">Delete</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>

    <div class="column span2" id="menus_column">
        <div class="header">Permission</div>

        <div class="content">
            <div style="padding: 3px; border-bottom: dotted #ccc 1px;">Select User Role On Left To Load Role Permissions Here</div>

            <div style="text-align: right; padding: 3px;">
                <button id="save_permissions" data-url="<?php echo $html->url(array('controller' => 'users', 'action' => 'role_menus')); ?>" disabled="disabled">Save Permissions</button>
            </div>

            <div id="menus">
                <?php echo $tree->show('Menu/title', $menus); ?>
            </div>
        </div>
    </div>

</div>



<input type="hidden" id="users_list_url" value="<?php echo $html->url(array('controller' => 'users', 'action' => 'getUsersList')); ?>" />