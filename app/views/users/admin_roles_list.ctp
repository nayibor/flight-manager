<table border="0" cellpadding="5" cellspacing="0" id="roles_table" class="fullwidth">
    <thead>
        <tr>
            <th colspan="2">Role Name</th>
            <th style="text-align: center;">Options</th>
        </tr>
    </thead>
    <tbody>
        <?php $count = 1; ?>
        <?php
        foreach( $roles as $role ) {
            $id = $role['UserRole']['id'];
            $url = $html->url(array('controller' => 'users', 'action' => 'role_menus', $role['UserRole']['id']));
            ?>
            <tr data-id="<?php echo $role['UserRole']['id']; ?>" data-url="<?php echo $url; ?>">
                <td width="10" align="right"><?php echo $count++ . "."; ?></td>
                <td>
                    <?php
                    $id = $role['UserRole']['id'];
                    $url = $html->url(array('controller' => 'users', 'action' => 'role_menus', $role['UserRole']['id']));
                    echo "<a class='role_link' data-role-id='$id' href='" . $url . "'>" . $role['UserRole']['name'] . "</a>";
                    ?>
                </td>
                <td align="center">
                    <a class="edit_link dialog_opener" title="Edit User Role Information" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'edit_role', $role['UserRole']['id'])); ?>">Edit</a>
                    <a class="delete_link" href="<?php echo $html->url(array('controller' => 'users', 'action' => 'delete_role', $role['UserRole']['id'])); ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>