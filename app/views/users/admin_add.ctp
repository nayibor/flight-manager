<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php __('Admin Add User'); ?></legend>
        <?php
        echo $this->Form->input('first_name', array('required' => 'required', 'class' => 'required'));
        echo $this->Form->input('last_name', array('required' => 'required', 'class' => 'required'));
        echo $this->Form->input('email', array('type' => 'email', 'required' => 'required', 'class' => 'required'));
        //echo $this->Form->input('user_role_id', array('required' => 'required', 'class' => 'required')); ?>
        <div class="input select">
            <label>User Role</label>
            <select name="data[User][user_role_id]" class="required" required="required">
                <option value="">Select Role</option>
                <?php
                foreach ($roles as $role) { ?>
                    <option value="<?php echo $role['UserRole']['id']; ?>"><?php echo $role['UserRole']['name']; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        
        <div class="input select">
            <label>Department</label>
            <select name="data[User][department_id]" class="required" required="required">
                <option value="">Select Department</option>
                <?php
                foreach ($departments as $department) { ?>
                    <option value="<?php echo $department['Department']['id']; ?>"><?php echo $department['Department']['name']; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        
        <div class="input select">
            <label>Team</label>
            <select name="data[User][team]" class="required">
                <option value="">Select Team</option>
                <?php
                $teams = array('Alpha Team', 'Bravo Team', 'Charlie Team', 'Delta Team');
                foreach ($teams as $team) { ?>
                    <option value="<?php echo $team; ?>"><?php echo $team; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <?php
        echo $this->Form->input('username', array('required' => 'required', 'class' => 'required'));
        echo $this->Form->input('password', array('required' => 'required', 'class' => 'required'));
        echo $this->Form->input('confirm_password', array('type' => 'password', 'required' => 'required'));
        ?>
    </fieldset>
    <div>
        <input type="submit" value="Add User" class="button" />
        <input type="button" value="Cancel" id="cancel-form" class="button" />
    </div>
    <?php echo $this->Form->end(); ?>
</div>