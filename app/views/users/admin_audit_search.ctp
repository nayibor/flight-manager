<div>
    <table width="100%" border="0" cellspacing="0" cellpadding="5" id="audit_list_table" class="fullwidth">
        <thead>
            <tr>
<!--                <th width="5%"><input type="checkbox" class="check_all" /></th>-->
                <th colspan="2">User</th>
                <th nowrap>Action</th>
                <th nowrap>Date</th>
<!--                <th>OPTR</th>
                <th nowrap>C/S / FLGT/No.</th>
                <th>REGN</th>
                <th>LOCATION</th>
                <th>STATUS</th>-->
            </tr>
        </thead>

        <tbody>
            <?php 
            $count = 1; //$pg_offset;  // passed from controller
            foreach ($userActions as $userAction) { ?>
                <tr data-id="<?php echo $userAction['UserAction']['id']; ?>" data-url="<?php echo $html->url(array('controller' => 'permits', 'action' => 'details', $userAction['UserAction']['id'])); ?>">
                    <!--<td><input type="checkbox" value="<?php echo $userAction['UserAction']['id']; ?>" /></td>-->
                    <td width="30" align="right"><span class="numbers"><?php echo $count++ . "."; ?></span> </td>
                    <td><?php echo $userAction['User']['last_name'] . " " . $userAction['User']['first_name']; ?></td>
                    <td nowrap width="50%">
                        <?php echo $userAction['UserAction']['action_desc']; ?>
                    </td>
                    <td nowrap><?php echo date('jS M y - H:i', strtotime($userAction['UserAction']['action_dt'])); ?></td>


                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>