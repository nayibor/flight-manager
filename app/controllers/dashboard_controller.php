<?php

class DashboardController extends AppController {

    var $name = "Dashboard";
    var $uses = array('UserRoleMenu','User','UserRole','Menu','Permit','GroundRequestSchedule');
    var $layout = "dashboard_layout";

    function beforeFilter() {
        if ($this->Session->check('user') == false) {
            $this->redirect('/admin/users/login/');
        }
    }

    //this checks the permits and sees the status of each permit
    //to be used for notifications
    public function check_Permits() {

        
    }

    public function admin_index() {

        $user = $this->Session->read('user');

        $user_menus = $this->UserRoleMenu->find('list', array(
                'fields' => array("menu_id"),
                'conditions' => array('user_role_id' => $user['User']['user_role_id']),
                'recursive' => -1
        ));

        $menus = $this->Menu->find('all',array(
                'conditions' => array('id' => $user_menus, 'level' => 2),
                'recursive' => -1,
                'order' => array('position')
        ));
        
        $permit_count = $this->Permit->getTotalOpen();
        $permit_due_today = $this->Permit->getTotalOpenDueToday();
        $flights_today = $this->GroundRequestSchedule->getTotalDueToday();
        
        $this->set(compact('permit_count', 'permit_due_today', 'flights_today', 'menus'));
    }
}
?>