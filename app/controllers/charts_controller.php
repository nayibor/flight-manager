<?php

class ChartsController extends AppController {

    public $name = "Charts";
    public $uses = array('Chart', 'ChartParameter');

    function beforeFilter() {
        if ($this->Session->check('user') == false) {
            $this->redirect('/admin/users/login/');
        }
    }

    public function admin_getParameters($chart_id) {
        $this->autoRender = false;

        $parameters = $this->ChartParameter->find('all', array(
            'conditions' => array('chart_id' => $chart_id),
            'recursive' => -1
                ));

        echo json_encode($parameters);
    }

    public function admin_getChartData() {
        $this->autoRender = false;

        if (isset($_POST['chart_id'])) {
            $chart_id = $_POST['chart_id'];
            $params = get_object_vars(json_decode($_POST['params']));

            $data = $this->Chart->generateData($chart_id, $params);

            echo json_encode($data);
        }
    }

    public function admin_handling_requests() {
        
    }

}

?>