<?php

class UploadComponent extends Object {

    /**
     * Uploads a file and saves the binary data to the database. A new record
     * will be created for the image data, or a record will be updated if a record
     * ID is specified as the parameter.
     * 
     * The method will return an instance of the GameGraphic object if the $save_game_graphic
     * parameter is set to true. If false, the image data would simply returned so it can be
     * used in another process if required
     * 
     * @param int $game_graphic_id
     * @param bool $save_game_graphic set to true to save game graphic, false to return graphic data
     * 
     * @return GameGraphic or Graphic Binary Data
     */
    function uploadFiles($uploadedFile = NULL, $game_graphic_id = false, $save_to_db = false) {

        if ( $uploadedFile == NULL ) {
            $uploadedFile = $_FILES['uploadfile'];
        }
        
        $files = array();

        if ( is_array($uploadedFile['name']) ) {
            $uploaded = count($uploadedFile['name']);

            for( $i = 0; $i < $uploaded; $i++ ) {

                $fileType = $uploadedFile['type'][$i];

                $typeOk = true;

                # if typeOk upload the file
                if ( $typeOk ) {

                    // get the file data
                    $file_contents = file_get_contents($uploadedFile['tmp_name'][$i]);

                    $files[] = array(
                        'file_name' => $uploadedFile['name'][$i],
                        'data' => $file_contents,
                        'mime_type' => $fileType
                    );
                }
            }

            return $files;
            
        }
        
        // single file upload
        else {
            $file_contents = file_get_contents($uploadedFile['tmp_name']);
            $fileType = $uploadedFile['type'];
            
            $files[] = array(
                'data' => $file_contents,
                'mime_type' => $fileType,
                'file_name' => $uploadedFile['name']
            );
            
            return $files;
        }
    }

}

?>