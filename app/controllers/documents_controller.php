<?php

/**
 * @property Document $Document Description
 * @property DocumentCategory $DocumentCategory Description
 */
class DocumentsController extends AppController {

    public $uses = array('Document', 'DocumentCategory');
    public $components = array('Upload');
    public $paginate = array(
        'Document' => array(
            'fields' => array('Document.name', 'Document.id', 'Document.mime_type', 'Document.abstract'),
            'order' => array('Document.id' => 'desc'),
            'limit' => 10
        )
    );

    function beforeFilter() {
        if ($this->action != "admin_login" && $this->action != "admin_logout") {
            if ($this->Session->check('user') == false) {
                $this->redirect('/admin/users/login/');
            }
        }
    }

    public function admin_index() {

        $categories = $this->DocumentCategory->find('all', array('recursive' => -1));
        $documents = $this->paginate('Document');

        $this->set('documents', $documents);
        $this->set('categories', $categories);
    }

    public function admin_add() {

        if (isset($_POST['data'])) {
            $this->autoRender = false;

            $user = $this->Session->read('user');

            if (isset($_FILES['attachments'])) {
                $attachments = $this->Upload->uploadFiles($_FILES['attachments']);
                debug($attachments);
                $data = array();

                foreach ($attachments as $attachment) {
                    $att = array(
                        'document_category_id' => $_POST['data']['Document']['document_category_id'],
                        'abstract' => $_POST['data']['Document']['abstract'],
                        'name' => $_POST['data']['Document']['name'] != '' ? $_POST['data']['Document']['name'] : $attachment['file_name'],
                        'mime_type' => $attachment['mime_type'],
                        'file_name' => $attachment['file_name'],
                        'content' => $attachment['data'],
                        'user_id' => $user['User']['id']
                    );

                    $data[] = $att;
                }

                $this->Document->saveAll($data);
            }

            return;
        }

        $categories = $this->DocumentCategory->find('all', array(
            'recursive' => -1
                ));

        $this->set('categories', $categories);
    }

    public function admin_list($document_category_id) {
        $documents = $this->paginate('Document', array(
            'Document.document_category_id' => $document_category_id
                ));

        $this->set('documents', $documents);
    }

    public function admin_delete($document_id) {
        $this->autoRender = false;

        $this->Document->id = $document_id;
        $this->Document->delete();
    }

    public function admin_view($document_id) {
        $this->autoRender = false;

        $attachment = $this->Document->find('first', array(
            'conditions' => array('Document.id' => $document_id),
            'recursive' => -1)
        );

        header("Content-type: " . $attachment['Document']['mime_type']);
        header("Content-Disposition: inline; filename=\"" . $attachment['Document']['file_name'] . "\"");

        echo $attachment['Document']['content'];
    }

    public function admin_search() {
        if (isset($_POST['term'])) {
            $documents = $this->paginate('Document', array(
                'or' => array(
                    'Document.name LIKE' => "%{$_POST['term']}%",
                    'Document.abstract LIKE' => "%{$_POST['term']}%"
                ))
            );

            $this->set('documents', $documents);
            $this->set('term', $_POST['term']);
        }
    }

}

?>
