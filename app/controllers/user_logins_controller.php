<?php
class UserLoginsController extends AppController {

	var $name = 'UserLogins';

	function index() {
		$this->UserLogin->recursive = 0;
		$this->set('userLogins', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user login', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('userLogin', $this->UserLogin->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->UserLogin->create();
			if ($this->UserLogin->save($this->data)) {
				$this->Session->setFlash(__('The user login has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user login could not be saved. Please, try again.', true));
			}
		}
		$users = $this->UserLogin->User->find('list');
		$bankBranches = $this->UserLogin->BankBranch->find('list');
		$this->set(compact('users', 'bankBranches'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user login', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->UserLogin->save($this->data)) {
				$this->Session->setFlash(__('The user login has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user login could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->UserLogin->read(null, $id);
		}
		$users = $this->UserLogin->User->find('list');
		$bankBranches = $this->UserLogin->BankBranch->find('list');
		$this->set(compact('users', 'bankBranches'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user login', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->UserLogin->delete($id)) {
			$this->Session->setFlash(__('User login deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User login was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->UserLogin->recursive = 0;
		$this->set('userLogins', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user login', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('userLogin', $this->UserLogin->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->UserLogin->create();
			if ($this->UserLogin->save($this->data)) {
				$this->Session->setFlash(__('The user login has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user login could not be saved. Please, try again.', true));
			}
		}
		$users = $this->UserLogin->User->find('list');
		$bankBranches = $this->UserLogin->BankBranch->find('list');
		$this->set(compact('users', 'bankBranches'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user login', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->UserLogin->save($this->data)) {
				$this->Session->setFlash(__('The user login has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user login could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->UserLogin->read(null, $id);
		}
		$users = $this->UserLogin->User->find('list');
		$bankBranches = $this->UserLogin->BankBranch->find('list');
		$this->set(compact('users', 'bankBranches'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user login', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->UserLogin->delete($id)) {
			$this->Session->setFlash(__('User login deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User login was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>