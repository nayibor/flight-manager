<?php

class CaterersController extends AppController {

    var $name = 'Caterers';
    var $uses = array('Menu', "Caterer");
    var $paginate = array(
        'Caterer' => array(
            'order' => array('name'),
            'limit' => 25
        )
    );

    function beforeFilter() {
        if ($this->action != "admin_login" && $this->action != "admin_logout") {
            if ($this->Session->check('user') == false) {
                $this->redirect('/admin/users/login/');
            }
        }
    }

    public function admin_delete($id) {
        $this->Caterer->delete($id, false);
        $this->redirect('/admin/Caterers');
    }

    public function admin_add() {

        if (isset($_POST['data']['Caterer'])) {
            $this->autoRender = false;
            $this->Caterer->save($_POST['data']["Caterer"]);
        }
    }

    public function admin_edit($id = null) {

        if (isset($_POST['data']['Caterer'])) {
            $this->autoRender = false;
            $this->Caterer->save($_POST['data']["Caterer"]);
            
            echo json_encode($_POST['data']);
            return;
        }
        
        $caterer = $this->Caterer->read(null, $id);
        $this->set(compact('caterer'));
    }

    public function admin_details($caterer_id) {
        $caterer = $this->Caterer->find('first', array(
            'conditions' => array('Caterer.id' => $caterer_id),
            'recursive' => -1
                ));

        $this->set('caterer', $caterer);
    }
    
    public function admin_list() {
        $caterers = $this->paginate('Caterer', array(
            $_POST['term'] . " LIKE" => '%' . $_POST['field'] . "%"
        ));
        
        $this->set('caterers', $caterers);
    }

}

?>
