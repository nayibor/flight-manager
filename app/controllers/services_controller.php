<?php
class ServicesController extends AppController {

    var $name = 'Services';
    var $uses = array('Service','GroundRequestScheduleService');

    function beforeFilter() {
        if( $this->action != "admin_login" && $this->action != "admin_logout") {
            if ($this->Session->check('user') == false) {
                $this->redirect('/admin/users/login/');
            }
        }
    }

    public function admin_index() {
        $services=$this->Service->findServices();
        $this->set(compact('services'));

    }

    public function admin_delservice($id) {
        $this->autoRender = false;
        $this->Service->delete($id, false);
        $this->redirect('/admin/Services');
    }


    public function admin_addservice($id = null) {

        if( isset($_POST['data']['Service'])) {
            $this->Service->create();
            $this->Service->save($_POST['data']['Service']);
            return;
        }

    }

    public function admin_editservice($id=null) {
        
        $this->autoLayout = false;

        if( isset($_POST['data']['Service']) ) {
            $this->autoRender = false;
            $this->Service->save($_POST['data']["Service"]);
        }

        else {
            $services = $this->Service->findServices($id);
            $this->set(compact('services'));
        }
    }
    
    public function admin_scheduleService($ground_request_schedule_id = NULL) {
        
        if( isset( $_POST['data']) ) {
            $this->autoRender = false;
            
            $requestService = new GroundRequestScheduleService();
            $requestService->set($_POST['data']['GroundRequestScheduleService']);
            $requestService->save();
            
            $requestService = $this->GroundRequestScheduleService->find('first', array(
                'fields' => array('id','name','contact_number'),
                'contain' => array('Service' => array(
                    'fields' => array('name')
                )),
                'conditions' => array('GroundRequestScheduleService.id' => $requestService->id )
            ));
            
            echo json_encode( $requestService );
            return;
        }
        
        $services = $this->Service->find('list', array('order' => array('name')));
        
        $this->set('services', $services);
        $this->set('ground_request_schedule_id', $ground_request_schedule_id);
    }
    
    public function admin_scheduleServiceEdit($ground_request_schedule_id = NULL) {
        if( isset( $_POST['data']) ) {
            $this->autoRender = false;
            
            $requestService = new GroundRequestScheduleService();
            $requestService->set($_POST['data']['GroundRequestScheduleService']);
            $requestService->save();
            
            $requestService = $this->GroundRequestScheduleService->find('first', array(
                'fields' => array('id','name','contact_number'),
                'contain' => array('Service'),
                'conditions' => array('GroundRequestScheduleService.id' => $requestService->id )
            ));
            
            echo json_encode( $requestService );
            return;
        }
        
        if( isset($_POST['schedule_service_id']) ) {
            $schedule_service = $this->GroundRequestScheduleService->find('first', array(
                'contain' => array('Service'),
                'conditions' => array(
                    'GroundRequestScheduleService.id' => $_POST['schedule_service_id']
                ))
            );
            
            $this->set('schedule_service', $schedule_service);
        }
        
        $this->set('ground_request_schedule_id', $ground_request_schedule_id);
    }
    
    public function admin_scheduleServiceDetails() {
        
        $service = $this->Service->findById($_POST['service_id']);
        
        $this->set('service', $service);
    }
    
    public function admin_deleteScheduleService() {
        $this->autoRender = false;
        
        if( isset( $_POST['schedule_service_id']) ) {
            $this->GroundRequestScheduleService->id = $_POST['schedule_service_id'];
            $this->GroundRequestScheduleService->delete();
        }
    }
}
?>