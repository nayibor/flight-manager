<?php

class ReportsController extends AppController {

    var $name = "Reports";
    var $uses = array('UserRoleMenu', 'Menu', "Report", 'Chart', 'ChartCategory', 'ReportParameter');
    var $layout = "report_layout";
    public $components = array('Pdf', 'Upload');
    public $helpers = array('DateFormat');
    var $paginate = array(
        'order' => array(
            'Report.id' => 'asc'
        )
    );

    function beforeFilter() {
        if ($this->Session->check('user') == false) {
            $this->redirect('/admin/users/login/');
        }
    }

    public function admin_index() {
        $this->layout = "default";

        $user = $this->Session->read('user');

        $user_menus = $this->UserRoleMenu->find('list', array(
            'fields' => array("menu_id"),
            'conditions' => array('user_role_id' => $user['User']['user_role_id']),
            'recursive' => -1)
        );

        $menus = $this->Menu->find('all', array(
            'contain' => array('ParentMenu'),
            'conditions' => array(
                'Menu.id' => $user_menus, 'Menu.level' => 3, 'ParentMenu.title' => 'Reports'
            ),
            'recursive' => 1,
            'order' => array('Menu.position'))
        );

        $this->set('menus', $menus);
    }

    public function admin_reports() {
        $this->layout = "default";
    }

    public function admin_charts() {
        $this->layout = "default";

        $chart_categories = $this->ChartCategory->find('all', array(
            'contain' => array(
                'Chart' => array('order' => array('title'))
            )
                ));
        $this->set('chart_categories', $chart_categories);
    }

    public function admin_reportlist() {

        $report_menus = $this->Report->find("all");


        $this->set(compact('report_menus'));
    }

    public function admin_del($report_id) {

        $conditions = array("Report.id" => $report_id);
        $this->Report->deleteAll($conditions, false);
        echo json_encode(array("status" => true));
        exit;
    }

    //this is used for adding new reports
    public function admin_add($report_id = null) {

        if ($report_id != null) {

            $report_data = $this->Report->find("first", array("conditions" => array("Report.id" => $report_id)));
            $this->set(compact('report_data'));
        }

        if (isset($_POST['data'])) {
            $data = $_POST['data']['Report'];
            $this->Report->save($data);
            echo json_encode(array("status" => "true"));
            exit();
        }
    }

    public function admin_test_page() {

        //$this->autoLayout=false;
        $this->autoRender = false;
        $data = $this->paginate('Permit');
        print_r($data);
    }

    public function admin_create_report($report_id = null, $type = null, $params_data = null) {

        $conditions = "";
        $cont_array = array();
        $params = array();
        if (isset($_GET['params'])) {
            $params = get_object_vars(json_decode($_GET['params']));
        } else if ($params_data != null) {
            $dt = urldecode($params_data);
            // print_r(json_decode($dt));
            $params = get_object_vars(json_decode($dt));
        }

        if (sizeof($params) > 0) {
            foreach ($params as $key => $val) {
                $cont_array[] = $key . " like '%" . $val . "%'";
            }
          ///   $conditions = " where " . $conditions . implode(" and ", $cont_array);
        }
      
        $query = $this->Report->find("first", array("conditions" => array("Report.id" => $report_id)));
        $columns = array();
        $keys = array();
        $title = $query['Report']['title'];
        $results = $this->Report->query($query['Report']['params'] . $conditions);
        if (count($results) > 0) {

            foreach ($results[0] as $val) {
                foreach ($val as $key => $keyval) {

                    $columns[] = $key;
                }
            }
        }

        return array('title' => $title, "res" => $results, "col" => $columns);
    }

    //this is for loading the real report
    public function admin_vreport($report_id = null, $type = null, $params_data = null) {

        $this->autoLayout = false;

        $data = $this->admin_create_report($report_id, $type, $params_data);
        $results = $data['res'];
        $columns = $data['col'];
        $title = $data['title'];

        if ($type == "download" and $type != null) {
            $this->Pdf->download($data['title'], '/reports/admin_dreport', array("title" => $data['title'], "report_id" => $report_id, 'results' => $data['res'], 'columns' => $data['col']));
        } else {
            $this->set(compact('results', 'columns', 'title'));
        }
    }

    public function admin_preport($report_id = null, $type = null, $params_data = null) {

        $this->layout = "print_layout";
        $data = $this->admin_create_report($report_id, $type, $params_data);
        $results = $data['res'];
        $columns = $data['col'];
        $title = $data['title'];
        $this->set(compact('results', 'columns', 'title'));
        $this->set('title_for_layout', $title . " Report");
    }

    public function admin_getParameters($report_id) {
        $this->autoRender = false;

        $parameters = $this->ReportParameter->find('all', array(
            'conditions' => array('report_id' => $report_id),
            'recursive' => -1
                ));



        echo json_encode($parameters);
    }

    //will have to write a function tommorow which will merge all the existing functionlity and put 
    // 
}

?>
