<?php

class ManagersController extends AppController {

    var $name = "Managers";
    var $uses = array('Handler', 'Service', 'Hotel', 'AviationAuthority', 'Users', 'UserRoleMenu', 'Menu', 'Airport','Caterer');
    var $helpers = array('Paginator');
    var $paginate = array(
        'Handler' => array(
            'limit' => 15,
            'order' => array('Handler.country' => 'asc', 'Handler.name' => 'asc')
        ),
        'Service' => array(
            'limit' => 25,
            'order' => array('Service.name' => 'asc')
        ),
        'Hotel' => array(
            'limit' => 25,
            'order' => array('Hotel.name' => 'asc')
        ),
        'Caterer' => array(
            'limit' => 25,
            'order' => array('Caterer.name' => 'asc')
        ),
        'AviationAuthority' => array(
            'limit' => 20,
            'order' => array('AviationAuthority.country' => 'asc', 'AviationAuthority.name' => 'asc')
        )
    );

    function beforeFilter() {
        if ($this->action != "admin_login" && $this->action != "admin_logout") {
            if ($this->Session->check('user') == false) {
                $this->redirect('/admin/users/login/');
            }
        }
    }

    public function admin_index() {

        $user = $this->Session->read('user');

        $user_menus = $this->UserRoleMenu->find('list', array(
            'fields' => array("menu_id"),
            'conditions' => array('user_role_id' => $user['User']['user_role_id']),
            'recursive' => -1)
        );

        $menus = $this->Menu->find('all', array(
            'contain' => array('ParentMenu'),
            'conditions' => array(
                'Menu.id' => $user_menus, 'Menu.level' => 3, 'ParentMenu.title' => 'Data Management'
            ),
            'recursive' => 1,
            'order' => array('Menu.position'))
        );

        $this->set('menus', $menus);
    }

    public function admin_handlers() {
        $this->Handler->recursive = -1;
        $this->set('handlers', $this->paginate('Handler'));
    }

    public function admin_services() {
        $this->Service->recursive = -1;
        $this->set('services', $this->paginate('Service'));
    }

    public function admin_hotels() {
        $this->Hotel->recursive = -1;
        $this->set('hotels', $this->paginate('Hotel'));
    }
    
    public function admin_caterers() {
        $this->Caterer->recursive = -1;
        $this->set('caterers', $this->paginate('Caterer'));
    }

    public function admin_airports() {
        
    }

    public function admin_civil_aviations() {
        $this->AviationAuthority->recursive = -1;
        $this->set('authorities', $this->paginate('AviationAuthority'));
    }

    public function admin_directory_search_fields() {
        $this->autoRender = false;

        if (isset($_POST['area'])) {
            $area = $_POST['area'];
            echo json_encode($this->{$_POST['area']}->getColumnTypes());
        }
    }

    public function admin_directory_search() {
        if (isset($_POST['area'])) {

            $area = $_POST['area'];
            
            $field = isset($_POST['field']) ? $_POST['field'] : 'name';

            $conditions = array($field . " LIKE" => "%" . $_POST['term'] . "%");

            if ($field == "id") {
                $conditions = array($field => $_POST['term']);
            }

            $results = $this->{$area}->find('all', array(
                'conditions' => $conditions,
                'recursive' => -1,
                'limit' => 15,
                'order' => array('name' => 'asc'))
            );

            $this->set('results', $results);
            $this->set('area', $area);
        }
    }

}

?>
