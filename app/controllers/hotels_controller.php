<?php

class HotelsController extends AppController {

    var $name = 'Hotels';
    var $uses = array('Menu', "Hotel");
    var $paginate = array(
        'Hotel' => array(
            'order' => array('name'),
            'limit' => 25
        )
    );

    function beforeFilter() {
        if ($this->action != "admin_login" && $this->action != "admin_logout") {
            if ($this->Session->check('user') == false) {
                $this->redirect('/admin/users/login/');
            }
        }
    }

    public function admin_index() {
        //   $menus = $this->findMenu();
        $hotels = $this->Hotel->findHotels();
        $this->set(compact('hotels'));
    }

    public function admin_delete($id) {
        $this->Hotel->delete($id, false);
        $this->redirect('/admin/Hotels');
    }

    public function admin_add() {

        if (isset($_POST['data']['Hotel'])) {
            $this->autoRender = false;
            $this->Hotel->save($_POST['data']["Hotel"]);
        }
    }

    public function admin_edit($id = null) {

        if (isset($_POST['data']['Hotel'])) {
            $this->autoRender = false;
            $this->Hotel->save($_POST['data']["Hotel"]);
            
            echo json_encode($_POST['data']);
            return;
        }
        
        $vhotel = $this->Hotel->findHotels($id);
        $hotel = $vhotel[0];
        $this->set(compact('hotel'));
    }

    public function admin_details($hotel_id) {
        $hotel = $this->Hotel->find('first', array(
            'conditions' => array('Hotel.id' => $hotel_id),
            'recursive' => -1
                ));

        $this->set('hotel', $hotel);
    }
    
    public function admin_list() {
        $hotels = $this->paginate('Hotel', array(
            $_POST['term'] . " LIKE" => '%' . $_POST['field'] . "%"
        ));
        
        $this->set('hotels', $hotels);
    }

}

?>
