<?php

/**
 * Description of data_controller
 *
 * @author Francis Adu-Gyamfi <francis.adu-gyamfi@matrixdesigns.org>
 */
class DataController extends AppController {
    //put your code here
    
    public $name = "Data";
    public $uses = array('GroundRequestScheduleAttachment', 'PermitAttachment');
    
    public function admin_export($type = 'ground_requests') {
        $this->autoRender = false;
        
        if( $type == "ground_requests") {
            $attach_ids = $this->GroundRequestScheduleAttachment->find('list', array(
                'fields' => array('id'),
                'offset' => 2200
            ));
            
            $count = 1;
            
            foreach($attach_ids as $id) {
                $request = $this->GroundRequestScheduleAttachment->find('first', array(
                    'conditions' => array(
                        'GroundRequestScheduleAttachment.id' => $id
                    ),
                    'recursive' => -1
                ));
                
                $dir = WWW_ROOT . 'files/gra/';
                
                @mkdir($dir, 0777, true);
                
                $filename = $dir . $request['GroundRequestScheduleAttachment']['file_name'];
                
                if( !file_exists($filename) ) {
                    $saved = file_put_contents($filename, $request['GroundRequestScheduleAttachment']['attachment']);

                    if( $saved ) {
                        echo "{$count}. Saved: " . $filename . "<br />";
                    }
                }
                
                $count++;
            }
        }
        
        else if( $type == 'permits') {
            $attach_ids = $this->PermitAttachment->find('list', array(
                'fields' => array('id'),
                'offset' => 1000
            ));
            
            $count = 1;
            
            foreach($attach_ids as $id) {
                $request = $this->PermitAttachment->find('first', array(
                    'conditions' => array(
                        'PermitAttachment.id' => $id
                    ),
                    'recursive' => -1
                ));
                
                $dir = WWW_ROOT . 'files/pa/';
                
                @mkdir($dir, 0777, true);
                
                $filename = $dir . $request['PermitAttachment']['file_name'];
                
                if( !file_exists($filename) ) {
                    $saved = file_put_contents($filename, $request['PermitAttachment']['attachment']);

                    if( $saved ) {
                        echo "{$count}. Saved: " . $filename . "<br />";
                    }
                }
                
                $count++;
            }
        }
    }
    
    public function admin_update($type = 'ground_requests') {
        $this->autoRender = false;
        
        if( $type == "ground_requests") {
            $attachments = $this->GroundRequestScheduleAttachment->find('list', array(
                'fields' => array('id','file_name')
            ));
            
            $count = 1;
            $records = array();
            
            foreach($attachments as $id => $file_name) {
                
                $filename = 'files/gra/' . $file_name;
                
                $records[] = array(
                    'id' => $id,
                    'attachment' => $filename
                );
                
                $count++;
            }
            
            $this->GroundRequestScheduleAttachment->saveAll($records);
        }
        
        else if( $type == 'permits') {
            
            $attachments = $this->PermitAttachment->find('list', array(
                'fields' => array('id','file_name')
            ));
            
            $count = 1;
            $records = array();
            
            foreach($attachments as $id => $file_name) {
                
                $filename = 'files/pa/' . $file_name;
                
                $records[] = array(
                    'id' => $id,
                    'attachment' => $filename
                );
                
                $count++;
            }
            
            $this->PermitAttachment->saveAll($records);
        }
    }
}
