<?php

class OperationsController extends AppController {
    
    var $name = "Operations";
    var $uses = array('Permit');
    
    function beforeFilter() {
        
        if ($this->Session->check('user') == false) {
            $this->redirect('/admin/users/login/');
        }
    }
    
    public function admin_index() {
        $this->redirect("/admin/operations/dashboard");
    }
    
    public function admin_dashboard() {
        //$this->layout = "dashboard_layout";
        
        $permits = $this->Permit->getOpenPermits();
        
        $this->set('permits', $permits);
    }
    
    public function admin_permits() {
        
        $permits = $this->Permit->getOpenPermits();
        
        $this->set('permits', $permits);
    }
    
    /*public function admin_permits() {
        $this->layout = "default";
        
        $permits = $this->Permit->find('all', array(
            'offset' => 0,
            'limit' => 20,
            'order' => array('Permit.id' => 'desc'),
            'recursive' => -1
        ));
        
        $this->set('permits', $permits);
    }*/
    
    public function admin_fuel() {
        
    }
    
    public function admin_caaData() {
        $this->autoRender = false;
        
        echo json_encode( array( 
            array('id' => 1, 'label' => 'more', 'value' => 'less'),
            array('id' => 2, 'label' => 'less', 'value' => 'even less')
        ));
    }
}
?>
