<?php

class ImportController extends AppController {
    public $name = "Import";
    public $uses = array('Groundops2009', 'GroundRequest', 'GroundRequestSchedule', 'Handler', 'Service');
    
    public function test() {
        $this->autoRender = false;
        
        $groundops = $this->Groundops2009->find('all');
        
        $data = array();
        
        foreach($groundops as $record) {
            
            if( !stristr($record['Groundops2009']['REF NO'], "SBG") ) {
                continue;
            }
            
            $row = array(
                'GroundRequest' => array(
                    'operator' => $record['Groundops2009']['OPERATOR'],
                    'type' => $record['Groundops2009']['A/C TYPE'],
                    'region' => $record['Groundops2009']['A/C REGN'],
                    'mtow' => '',
                    'crew' => '',
                    'purpose' => $record['Groundops2009']['P-O-F'],
                    'company_name' => $record['Groundops2009']['PRINCIPAL'],
                    'payment_type_requested' => strtolower($record['Groundops2009']['SRVS RQSTD']),
                    'payment_type_used' => strtolower($record['Groundops2009']['SRVS OFFD']),
                    'jobsheet_status' => in_array(strtolower($record['Groundops2009']['STATUS']), array('completed', 'cancelled')) ? "closed" : "open"
                ),
                'GroundRequestSchedule' => array(
                    array(
                        'ref_no' => $record['Groundops2009']['REF NO'],
                        'handler_id' => $this->Handler->getHandlerId($record['Groundops2009']['HANDLER']),
                        'arr_flight_num' => $this->getFlightNo($record['Groundops2009']['FLT NO']),
                        'arr_origin' => $record['Groundops2009']['ORIGIN ICAO/IATA'],
                        'arr_etd' => date('Y-m-d H:i:s', strtotime($record['Groundops2009']['ORIGIN ETD'])),
                        'arr_atd' => date('Y-m-d H:i:s', strtotime($record['Groundops2009']['ORIGIN ATD'])),
                        'arr_dest' => $record['Groundops2009']['DEST ICAO/IATA'],
                        'arr_eta' => date('Y-m-d H:i:s', strtotime($record['Groundops2009']['DEST ETA'])),
                        'arr_ata' => date('Y-m-d H:i:s', strtotime($record['Groundops2009']['DEST ATA'])),
                        'dept_flight_num' => $this->getFlightNo($record['Groundops2009']['FLT NO'], 'dept'),
                        'dept_origin' => $record['Groundops2009']['DEST ICAO/IATA'],
                        'dept_etd' => date('Y-m-d H:i:s', strtotime($record['Groundops2009']['DEST ETD'])),
                        'dept_atd' => date('Y-m-d H:i:s', strtotime($record['Groundops2009']['DEST ATD'])),
                        'dept_dest' => $record['Groundops2009']['DDEST ICAO/IATA'],
                        'status' => strtolower($record['Groundops2009']['STATUS']),
                        'comment' => $record['Groundops2009']['REMARKS']
                    )
                )
            );
            
            if( $row['GroundRequestSchedule'][0]['arr_atd'] == '1970-01-01 00:00:00' ) {
                $row['GroundRequestSchedule'][0]['arr_atd'] = '0000-00-00 00:00:00';
            }
            
            if( $row['GroundRequestSchedule'][0]['arr_ata'] == '1970-01-01 00:00:00' ) {
                $row['GroundRequestSchedule'][0]['arr_ata'] = '0000-00-00 00:00:00';
            }
            
            if( $row['GroundRequestSchedule'][0]['dept_atd'] == '1970-01-01 00:00:00' ) {
                $row['GroundRequestSchedule'][0]['dept_atd'] = '0000-00-00 00:00:00';
            }
            
            $this->GroundRequest->saveAll($row);
            
            $data[] = $row;
        }
        
        pr($data);
        //$this->GroundRequest->saveAll($data);
    }
    
    public function getFlightNo($flight_no, $route = 'arr') {
        
        if( stristr($flight_no, "/") ) {
            $parts = explode("/", $flight_no);
            
            if( $route == 'arr' ) {
                return $parts[0];
            }
            
            else if($route == 'dept') {
                return str_replace(substr($parts[0], -strlen($parts[1])), $parts[1], $parts[0]);
            }
        }
        
        return $flight_no;
    }
    
    public function transferFuelData() {
        $this->autoRender = false;
        
        $fuelDelivery = ClassRegistry::init('FuelDelivery')->find('all');
        $fuelRequest = ClassRegistry::init('FuelRequest');
        
        $data = array('FuelRequest' => array());
        foreach($fuelDelivery as $request) {
            $optr_pic = explode("/",$request['FuelDelivery']['OPRT & P I C']);
            $status = "open"; $billed = 0;
                
            if( $request['FuelDelivery']['REMARKS'] != "" ) {
                switch($request['FuelDelivery']['REMARKS']) {
                    case 'CNXLLD': 
                        $status = 'canceled'; 
                        break;
                    case 'UPLIFTED': 
                        $status = 'uplifted'; 
                        $billed = 1; 
                        break;
                }
            }
            
            $data['FuelRequest'][] = array(
                'ref_no' => $request['FuelDelivery']['PO NUMBER'],
                'created_dt' => date('Y-m-d', strtotime($request['FuelDelivery']['DATE OF RQST'])),
                'aircraft_optr' => $optr_pic[0],
                'aircraft_pic' => isset($optr_pic[1]) ? $optr_pic[1] : null,
                'aircraft_type' => $request['FuelDelivery']['A/C TYPE'],
                'aircraft_regn' => $request['FuelDelivery']['A/C REG'],
                'aircraft_flight_no' => $request['FuelDelivery']['FLT NO'],
                'company_card_info' => $request['FuelDelivery']['CARD NO & TYPE'],
                'company_card_expiry_dt' => $request['FuelDelivery']['EXPIRY DATE'],
                'fueler' => $request['FuelDelivery']['INTO PLANE'],
                'est_uplift' => $request['FuelDelivery']['QTY RQST - USG'],
                'est_uplift_ltrs' => $request['FuelDelivery']['QTY RQST - LTRS'],
                'est_uplift_dt' => date('Y-m-d', strtotime($request['FuelDelivery']['ETA/DATE UPLIFT'])),
                'actual_uplift' => $request['FuelDelivery']['ACTUAL QTY - USG'],
                'actual_uplift_ltrs' => $request['FuelDelivery']['ACTUAL QTY - LTRS'],
                'arr_dest' => $request['FuelDelivery']['ORGN - ICAO/IATA'],
                'dept_dest' => $request['FuelDelivery']['DEST - ICAO/IATA'],
                'product' => $request['FuelDelivery']['INTO PLANE'],
                'product_selling_price' => $request['FuelDelivery']['SELLING PRICE'],
                'confirmation_code' => $request['FuelDelivery']['CNTRL NO'],
                'confirmation_ticket_no' => $request['FuelDelivery']['TICKET NO'],
                'status' =>  $status,
                'billed' => $billed
            );
        }
        pr($data);
        $fuelRequest->saveAll($data['FuelRequest']);
    }
    
    public function transferPermitData() {
        $this->autoRender = false;
        
        
    }
}
?>
