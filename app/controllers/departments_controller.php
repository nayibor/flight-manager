<?php

class DepartmentsController extends AppController {
    
    public $name = "Departments";
    public $uses = array('Department','DepartmentSupervisor');
    
    public function admin_supervision_schedule($department_id, $schedule_id = NULL) {
        
        if( isset($_POST['data']) ) {
            $this->DepartmentSupervisor->create();
            $this->DepartmentSupervisor->save($_POST['data']);
            
            return;
        }
        
        if( $schedule_id != NULL ) {
            $this->set('schedule', $this->DepartmentSupervisor->find('first', array(
                'conditions' => array('id' => $schedule_id),
                'recursive' => -1
            )));
        }
        
        $this->set('department', $this->Department->findById($department_id));
        
        $this->set('users', $this->User->find('all', array(
            'fields' => array('id', 'first_name','last_name'),
            'conditions' => array('department_id' => $department_id),
            'recursive' => -1))
        );
    }
    
    public function admin_delete_schedule($schedule_id) {
        $this->autoRender = false;
        
        $this->DepartmentSupervisor->id = $schedule_id;
        $this->DepartmentSupervisor->save( array('deleted' => 1) );
        
        $this->redirect( $_SERVER['HTTP_REFERER'] );
        
    }
}
?>
