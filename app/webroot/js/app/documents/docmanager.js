
var DocumentManager = {
    init: function() {
        this.bindBtns();
        this.bindNav();
    },
    bindNav: function() {
        $("#category-list li a").click(function(e) {
            e.preventDefault();

            $(this).parent().addClass("current").siblings().removeClass("current");
            $("#documents_main .header").html("Documents In <b>" + $(this).html() + "</b>");

            var url = $(this).attr('href');

            $.get(url, function(data) {
                $("#documents_main .content").html(data);
            });
        });

        $("#search_field").change(function() {
            var url = $(this).data('url');
            var query = "term=" + $(this).val();
            
            $.post(url, query, function(data) {
                $("#documents_main .content").html(data);
            });
        });
    },
    bindBtns: function() {
        $("#add_document").data('load-callback', DocumentManager.dialogOpened);

        $(".view_link").live('click', function(e) {
            e.preventDefault();

            window.open( $(this).attr('href') );
        });
    },
    dialogOpened: function() {
        Attachments.init({
            submit: function() {
                var category_id = $("#document_category_id").val();

                $("#category-list li").each(function() {
                    if ($(this).data('id') == category_id) {
                        $(this).find("a").click();
                        return false;
                    }
                });
            }
        });
    }
};

$(document).ready(function() {
    DocumentManager.init();
});
