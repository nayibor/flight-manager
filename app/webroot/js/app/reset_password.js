
var reset_password = {
    
    oldpassStatus:"false",
    
    init:function(){
        var _this = this;
        
        $("#reset_password").click( function(event){
            
       
            if(reset_password.oldpassStatus=="false")
            {
                $("#status").html("Old Password InCorrect."); 
                $("#status").css("color","red");
                $("#old_password").focus();
                reset_password.oldpassStatus="false";
                return;    
            }
             
            
            if($("#new_password").val()!=$("#new_password_repeat").val() || $("#new_password").val()==""){
                $("#status_new").html("New Password Field(s) Empty Or Dont Match."); 
                $("#status_new").css("color","red");
                $("#new_password_repeat").focus();
              
                return;
            }
        
            if($("#new_password").val()==$("#new_password_repeat").val() && $("#new_password").val()!=""){
                $("#status_new").html("New Password Fields  Match."); 
                $("#status_new").css("color","green");
            }
        
            reset_password.reset_password();
            
        });
               
               
               
        $("#old_password").change(function(event){
            reset_password.oldpassStatus="false";
            reset_password.check_old_password();
            event.preventDefault();
        });    
    },
    
    check_old_password :function(old_password){
     
        var $query="username="+$("#username").val()+"&password="+$("#old_password").val();
        
        $.ajax({
            url:$("#reset_url").val(),
            data: $query,
            dataType: 'json',
            type: 'post',
            beforeSend:function(){
                   
                $("#status").html("Checking Old Password...") ;
                $("#status").css("color","black");

            },
            success:function(data){
                if(data.status=="true"){
                    $("#status").html("Old Password Correct.") ;
                    $("#status").css("color","green");
                    reset_password.oldpassStatus="true";
                }
                
                if(data.status=="false"){
                    $("#status").html("Old Password InCorrect."); 
                    $("#status").css("color","red");
                    $("#old_password").focus();
                    reset_password.oldpassStatus="false";
                }
            }
        });
    },
  
    checkNewPass:function(){
      
      
    },
  
  
    reset_password:function(){
      
        var $query="userid="+$("#userid").val()+"&password="+$("#new_password").val();
        $.ajax({
            url:$("#reset_pass").val(),
            data: $query,
            dataType:"json",
            type: 'post',
            beforeSend:function(){
                   
                $("#reset_password").val("Resetting Password...") ;
                $("#status").css("color","black");

            },
            success:function(data){
                $("#reset_password").val("Reset Password") ;    
                alert("Password Has Been Reset.You Will Be Logged Out");
                window.location.href=$("#logout_url").val();
            }
          
        });
    }
  
}

$(document).ready(function(){
    reset_password.init(); 
});

