

var authorities = {
    
    init: function() {
        $(".add_link").data('callback','authorities.addListing');
        $(".edit_link").data('callback', 'authorities.refreshPage');
        
        
        this.setupGridBindings();
        this.setupSearchFields();
    },
    
    setupGridBindings: function() {
        $(".fullwidth tbody tr").live('click', function() {
             var authority_id = $(this).data('id');
             authorities.loadAuthorityDetails(authority_id);
        });
        
        $(".fullwidth tbody tr:first").click();
    },
    
    setupSearchFields: function() {
        $("#search_field").keyup(function(e) {
            
            if( e.keyCode == 13 ) {
                var url = $(this).data('url');
                var params = "field=" + $(this).val() + "&term=" + $("#search_by").val();
            
                $.post(url, params, function(data) {
                    $(".master_block").html(data);
                    authorities.setupGridBindings();
                });
            }
        });
    },
    
    loadAuthorityDetails: function(authority_id) {
        var url = $("#authority_details_url").val();
        var query = {authority_id: authority_id};
        
        $.ajax({
            url: url,
            data: query,
            type: 'post',
            //dataType: 'html',
            success: function(data) {
                $(".details_block .display").html(data);
            },
            error: function(xhr) {
                console.log(xhr.responseText);
            }
        });
    },
    
    addListing: function(data) {
        
        var id = data.AviationAuthority.id;
        var $tr = $("<tr />").attr('id', "row-" + id).data('id', id);
        
        $tr.append(
            $("<td />").append( $("<input />").attr({'type': 'checkbox'}).val(id) )
        ).append(
            $("<td />").attr({align: 'right', width: '20'}).html($(".fullwidth tr").length + "." )
        );
        
        var columns = ['country', 'name'/*, 'telephone','telex','email'*/];
        for(var key in data.AviationAuthority) {
            if( columns.indexOf(key) > -1 ) {
                $("<td />").addClass(key).html( data.AviationAuthority[key] ).appendTo($tr);
            }
        }
        
        $tr.append(
            $("<td />").addClass('options').attr('align','center')
                .append(
                    $("<a />").attr({
                        'href': $("#edit_link_base").val() + "/" + id,
                        'class': 'edit_link dialog_opener',
                        'title': 'Edit Aviation Authority Information'
                    }).data({
                        'height': '550',
                        'callback': 'authorities.refreshPage'
                    }).html('Edit')
                )
                .append(
                    $("<a />").attr({
                        'href': $("#delete_link_base").val() + "/" + id,
                        'class': 'delete_link'
                    }).html('Delete')
                )
        );
            
        $(".fullwidth tbody").append( $tr );
    },
    
    refreshPage: function(data) {
        var id = data.AviationAuthority.id;
        $("#row-" + id + " td").each(function() {
            var $cell = $(this);
            for(var key in data.AviationAuthority) {
                if( $cell.hasClass(key) ) {
                    $cell.html( data.AviationAuthority[key] );
                }
            }
        });
    }
};

$(document).ready(function() {
    authorities.init() ;
});