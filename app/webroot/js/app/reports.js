/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var reports ={
    
    loadreport:function(){
       
            
    
          
          var url=$("#report_url").val();
              
              $.ajax({
              url:url,
              dataType:"html",
              success:function(data){
                  
                  $("#report_list").html(data);
              }
                  
              })
      
           
       
        
    },
    
    
    //this is for initing and for performing various stuff after initing
    init:function(){
       var _this=this;
       _this.loadreport();
      
      
      
      $("#download").live('click',function(event){
        
        var wind=  window.open($(this).attr("data-url"));
          window.setTimeout(function(){
              wind.close();
              
              
          },2000)
//        /
//        $.ajax({
//                url:,
//               type:"post",
//               success:function(data){
//                  
//                 
//               }
//                
//                
//                
//           

          });
     
      
      
        //for editing reports
        $(".edit_link").live('click',function(event){
            
                event.preventDefault();
            
             var url=$(this).attr("href");
            $.ajax({
                url:url,
                dataType:"html",
               type:"post",
               success:function(data){
                  
                  core.showDialog({
                                title: 'New Report',
                                content: data,
                                close: function(data) {
                                 
                                }
                            });
                   
               }
                
                
                
            })
            
        });
        
        
           //for deleting reports
        $(".del_link").live('click',function(event){
            event.preventDefault();
            if(confirm("Are You Sure You Want To Delete This Record ?")){
            var url=$(this).attr("href");
            $.ajax({
                url:url,
                dataType:"json",
               type:"post",
               success:function(data){
                       reports.loadreport();

               }
            
           
        });
            }
            else{
                return false;
            }
        
        });
        
        
        //for saving reports
        $("#submit_report").live('click',function(event){
   
      var status=true;  
      $(".required").each(function(){   
            if($(this).val()=="")
                 {
                   status=false;
                 }
                 
          
          });

     if(!status){return false;}

           var query=$("#report_form").serialize();
            var url=$("#add_new_url").val();
          
            $.ajax({
                url:url,
                dataType:"json",
               data:query,
               type:"post",
               success:function(data){
                  
                  core.closeDialog();
                  reports.loadreport();
                   
               }
                
                
                
            })
            
            
        });
        
        
       
        
         $("#cancel_submit_report").live('click',function(event){
             event.preventDefault()
            core.closeDialog();
            
        });
        
        
        //this is for showing the new report dialog
        $("#add_new_report").live('click',function(event)
        {
           var url=$("#add_new_url").val();    
       
            $.ajax({
                url: url,
                dataType: "html",
                type: 'get',
                error:function(){
                    
                    
                },
                success:function(data){
                    
                     core.showDialog({
                                title: 'New Report',
                                content: data,
                                close: function(data) {
                                 
                                }
                            });
                }
            }
    );
            
        });
        
        
        
    }
    
    
}



$(document).ready(function(){
    
    reports.init();
    
    
    
})


