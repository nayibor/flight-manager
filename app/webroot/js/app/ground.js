
var Ground = {

    init: function() {
        var self=this;
        $("#tabs").tabs();
        $(".data_expire_date").datepicker({
            showButtonPanel: true,
            showOn: "both",
            buttonImage: "../img/core/ico-12.png",
            buttonImageOnly: true,
            minDate: -7,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm:00'
      
        });

        $(".print_form").click(function(e){
            //alert($("#print_url").val());
            window.open($("#print_url").val());
        });

        $('#process_form').submit(function(event){
            event.preventDefault();
            self.processForm();
        });

    },
    processForm:function(){
        var url_req = $("#member_game_status_update_url").val();
        var query = $("#process_form").serialize();
        $.ajax({
            url:url_req,
            data: query,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                window.location.href = $("#add_redir").val();
            },
            error: function(xhr) {
                alert(xhr.responseText);
            }
        });

        
    }
};

$(document).ready(function() {
    Ground.init(); 
});