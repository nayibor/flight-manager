
var ground = {

    init: function() {
        setInterval(function(){
            $("#tabs").tabs('load',   $("#tabs").tabs('option', 'selected'));
        }, 1000*60*2);
        
        // make flyout show above dialogs
        $("#flyout").css('z-index', 2500);
        
        $("#tabs").bind("tabsload", function(event, ui) {
            var panel_id = ui.panel.id;
            
            $(".dialog_opener").data('load-callback', function() {
                /**
                 * Fix z-indexes to prevent Action Menu Overlap
                 */
                var zIndexFix = 10;
                $(".schedule").each(function() {
                    $(this).css('z-index', zIndexFix);
                    $(this).find('.menu_button').css('z-index', zIndexFix--);
                });
                
                $(".schedule .menu_button").click(function(e) {
                    e.preventDefault();
                    var $menu = $(this).find(".menu");

                    if($menu.is(":visible")){
                        $menu.hide();

                    } else {
                        $menu.show().focus();
                    }
                });
                
                // hide all action buttons
                $(".schedule .menu .open_only, .schedule .menu .canceled_only").hide();
                
                $(".schedule .menu_button .menu").blur(function() {
                    $(this).hide();
                });

                $(".schedule a.expander").click(function(e) {
                    e.preventDefault();
                        
                    $(this).toggleClass('expanded');
                    $(this).parents('.schedule').find(".services").slideToggle();
                    $(this).parents(".schedule").toggleClass("expanded");
                        
                    return false;
                });
            });
        });
        
        $("#gr_table tbody tr").live('click', function() {
            $("#gr_table tbody tr").removeClass("selected");
            $(this).addClass("selected");
        });
        
        $("#filter_field").keyup(function() {
            var val = $(this).val();
            
            if( !val || val.length == 0 ) {
                $("#gr_table tbody tr").show();
                return;
            }
            
            $("#gr_table td.ref_no").each(function() {
                if( $(this).text().indexOf(val) > -1 ) {
                    $(this).parent().show();
                }
                
                else {
                    $(this).parent().hide();
                }
            });
        });
        
        $(".pane-footer span a").live('click', function(e) {
            e.preventDefault();
            
            var self = this;
            var url = $(this).attr("href");
            
            $.get(url, function(data) {
                $(self).parents("#dashboard_list").html(data);
            });
        });
    },

    getDashContent: function() {
        var url = $("#dash_type").val();
        $.get(url, function(content) {
            $("#dashboard_list").html(content);
        });
    }
};

$(document).ready(function() {
    ground.init() ;
});

