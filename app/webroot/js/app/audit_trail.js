

var audit_trail = {

    init: function() {

        $("#users_table tbody tr").click(function() {
            var url = $("#user_actions_list_url").val();
            var query = "user_id=" + $(this).data('id');

            $.ajax({
                url: url,
                data: query,
                //dataType: 'json',
                type: 'post',
                success: function(data) {
                    $("#actions_display .content").html(data);
                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                }
            });
        });

        $("#search_field").keyup(function(e) {
            if (e.keyCode == 13) {
                audit_trail.search("action_desc=" + $(this).val());
            }
        });

        $("#search_actions input.date").datetimepicker({
            'dateFormat': 'yy-mm-dd',
            'timeFormat': 'hh:mm:ss'
        });

        $("#search_trigger").click(function() {
            audit_trail.search("start_dt=" + $("#start_dt").val() + "&end_dt=" + $("#end_dt").val());
        });

        $("#purge_btn").click(function() {
            audit_trail.purgeActions();
        });
    },
    search: function(query) {
        var url = $("#user_actions_search_url").val();

        $.ajax({
            url: url,
            data: query,
            //dataType: 'json',
            type: 'post',
            success: function(data) {
                $("#search_actions .content").html(data);
            },
            error: function(xhr) {
                console.log(xhr.responseText);
            }
        });
    },
    
    purgeActions: function() {
        if (!confirm("This action will delete any user actions that are more than 30 days old. Records deleted cannot be recovered, unless via a data backup restore")) {
            return;
        }

        var url = $("#purge_actions_url").val();
        $.ajax({
            url: url,
            type: 'post',
            success: function(data) {
                console.log(data);
            },
            error: function(xhr) {
                console.log(xhr.responseText);
            }
        });
    }
};

$(document).ready(function() {
    audit_trail.init();
});