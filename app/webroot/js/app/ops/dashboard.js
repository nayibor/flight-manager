

var opsDashboard = {
    
    init: function() {
        
        //this.getDashContent();
        
        //setInterval(this.getDashContent, 1000 * 60 * 1);
        
        $("#permits_table tr").live('click', function() {
            $("#permits_table tr").removeClass("selected");
            $(this).addClass("selected");
            
            var url = $(this).data('url');
            $.get(url, function(data) {
                core.showDialog({
                    title: 'Permit Request Details',
                    content: data,
                    width: 700,
                    height: 600,
                    load: function() {
                        $("#general_dialog .details").css('width', 570);
                    }
                }); 
            });
        });
        
        $("#filter_field").keyup(function() {
            var val = $(this).val();
            
            if( !val || val.length == 0 ) {
                $("#gr_table tbody tr").show();
                return;
            }
            
            $("#permits_table td.ref_no").each(function() {
                if( $(this).text().indexOf(val) > -1 ) {
                    $(this).parent().show();
                }
                
                else {
                    $(this).parent().hide();
                }
            });
        });
        
        $(".pane-footer li").live('click', function() {
            var self = this;
            var url = $(this).data('url');
            
            $.get(url, function(data) {
                $(self).parents("#dashboard_list").html(data);
            });
        });
    },
    
    getDashContent: function() {
        var url = $("#dashboard_list_url").val();
        $.get(url, function(content) {
            $("#dashboard_list").html(content);
        });
    }
};

$(document).ready(function() {
    opsDashboard.init() ;
});

