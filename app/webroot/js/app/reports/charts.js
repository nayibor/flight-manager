
var Charts = {
    
    init: function() {
        this.setupReportList();
        this.setupChartList();
    },
    
   
    /**
     *
     *Setup the functionality for the reports
     *
     */
    setuprequest:function(type_id){
        var url='';
        switch(type_id){
      
            case 'report' :
                url =  $("#reports-list ul li.active a").attr("href");

                break ;
          
          
          
          
        }
        
        var data = {};
        $("#param_area input").each(function() {
               
            data[ $(this).attr('name') ] = $(this).val();
        });

        data.params=JSON.stringify(data);
            

        $.get(url,data, function(data) {
                
            $("#report_area").html(data);

            
        })
    },
    
    setupReportList: function() {
        
        $("#reports-list ul li").click(function(e) {
            e.preventDefault();
            $("#reports-list ul li").each(function(){
                 
                $(this).removeClass("active");
            });
             
            $(this).addClass("active");
            
            var $link = $(this);
            var url = $link.find('a').attr('data-params-id');
            
            $.get(url, function(data) {
                //console.log(data);
                Charts.showChartParameters(data,"report");
                $("#charts-list ul li").removeClass("current");
                $link.addClass('current');
                
                $("#chart_column .header").html("Report Details - " + $link.text());
                
                if(!$("#generate-button").hasClass("report"))
                {
                    $("#generate-button").addClass("report");
                
                }

                
            //$("#report_area").html(data);
            
            
            
            
            } ,"json");
        });
             
        $(".dwbutton").live('click',function(){
                   
            var url =  $("#reports-list ul li.active a").attr("dlink");
            Charts.setupReport(url,"Downloading Report",false) ;
    
        });
        
        
        $(".prbutton").live('click',function(){
            var url =  $("#reports-list ul li.active a").attr("plink");
            Charts.setupReport(url,"Printing Report",true) ;


       
        })
                  
        //this will be used for getting the  actual reports
        $("#generate-button.report").live('click',function(){
             
            Charts.setuprequest('report');
            
     
             
        })  ;
        
    },
    
    setupReport:function(url,title,open){
        
        var data = {};
        $("#param_area input").each(function() {
               
            data[ $(this).attr('name') ] = $(this).val();
        });

        data.params=JSON.stringify(data);
          
        //url=url+"/"+data;
        // consol.log(url);
          
        if( 'humane' in window ) {
            humane.info(title);
        } 
            
        var $this = $(this);
        setTimeout(function() {
            if(open==true){
                window.open(url);
   
            }else{
                window.location.href = url;

            }
        }, 1000);

    },
    /**
     * Setup the functionality for all the chart list and parameter items
     */
    setupChartList: function() {
        
        $("#charts-list ul li").click(function(e) {
            e.preventDefault();
            
            var $link = $(this);
            var url = $link.find('a').attr('href');
            
            $.get(url, function(data) {
                Charts.showChartParameters(data,"chart");
                
                $("#charts-list ul li").removeClass("current");
                $link.addClass('current');
                
                $("#chart_column .header").html("Chart Details - " + $link.text());
                if(!$("#generate-button").hasClass("chart"))
                {
                    $("#generate-button").addClass("chart");
                 
                }
                

            }, 'json');
        });
        
  
  
  
        $("#generate-button.chart").live('click',function() {
            var chart_id = $("#charts-list li.current").data('id');
            var url = $(this).data('url');
            var data = {};
            
            $("#param_area input").each(function() {
                data[ $(this).attr('name') ] = $(this).val();
            });
            
            $.ajax({
                url: url,
                data: {
                    'chart_id': chart_id, 
                    'params': JSON.stringify(data)
                },
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    Charts.showChart(data);
                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                }
            });
        });
    },
    
    
    
    showChartParameters: function(parameters,type) {
        var ul = document.createElement('ul');
        $(ul).attr('id', 'report-parameters');
        
        
      
        $.each(parameters, function(k) {
            var li = document.createElement('li');
            var input = document.createElement('input');
            
            if(typeof type !="undefined" && type=="report")
            {
                console.log("Parameters ");
                console.log(parameters[k].ReportParameter);
                $(li).append($('<label>' + parameters[k].ReportParameter.label + '</label> '));
                $(li).append( $(input).attr({
                    'type': parameters[k].ReportParameter.type, 
                    'name': parameters[k].ReportParameter.name, 
                    'placeholder': parameters[k].ReportParameter.label, 
                    'class': parameters[k].ReportParameter.type,
                    'value': parameters[k].ReportParameter.initial_value
                }) ).appendTo(ul);
            
                if( parameters[k].ReportParameter.required == 1 ) {
                    $(input).attr("required", "required");
                }
                
                
            }
            else if(typeof type !="undefined" && type=="chart")
            {
                $(li).append($('<label>' + parameters[k].ChartParameter.label + '</label> '));
                $(li).append( $(input).attr({
                    'type': parameters[k].ChartParameter.type, 
                    'name': parameters[k].ChartParameter.name, 
                    'placeholder': parameters[k].ChartParameter.label, 
                    'class': parameters[k].ChartParameter.type,
                    'value': parameters[k].ChartParameter.initial_value
                }) ).appendTo(ul);
            
                if( parameters[k].ChartParameter.required == 1 ) {
                    $(input).attr("required", "required");
                }
            }
            
            
            
            
        });		
      
        
        
        $('#param_area').html(ul);
        
        $("#param_area input.date").each(function() {
            if( this.type == "text" ) {
                $(this).datepicker({
                    dateFormat: 'yy-mm-dd'
                });
            }
        });
        
        $("#param_area input.month").each(function() {
            if( this.type == "text" ) {
                $(this).datepicker({
                    dateFormat: 'yy-mm'
                });
            }
        });
        
        $("#param_area input.datetime[type=text]").datetimepicker({
            dateFormat: 'yy-mm-dd'
        });
        
        $("#generate-button").show();
        $("#chart_area,#report_area").html("");
    },
    
    showChart: function(chart_info) {
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'chart_area',
                type: chart_info.chart.type,
                marginRight: 50,
                marginBottom: 50
            },
            title: {
                text: chart_info.chart.title,
                x: -20 //center
            },
            subtitle: {
                text: chart_info.chart.subtitle,
                x: -20
            },
            xAxis: {
                categories: chart_info.categories
            },
            yAxis: {
                title: {
                    text: chart_info.chart.y_axis_label
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                    this.x +': '+ this.y;
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: chart_info.series
        });
    }
};

$(document).ready(function() {
    Charts.init();
});