
var roles = {
    
    init: function() {
        
        $("#roles_table tr").click(function(e) {
            e.preventDefault();
            
            var url = $(this).attr("data-url");
            var query = "user_role_id=" + $(this).attr("data-id");
            
            $.post(url, query, function(data) {
                $("#menus li input[type=checkbox]").removeAttr("checked");
                $("#save_permissions").attr("disabled", "disabled");
                
                $.each(data, function(k,v) {
                    $("#menus li input[type=checkbox]").each(function() {
                        if( $(this).val() == v.UserRoleMenu.menu_id ) {
                            $(this).attr("checked", "checked");
                        }
                    });
                });
                                
            },'json');
        });
        
        $("#menus input[type=checkbox]").change(function(e) {
            //e.preventDefault();
            
            if( $(this).is(":checked") ) {
                $(this).parent("li").find("li input[type=checkbox]").attr("checked","checked");
            }
            
            else {
                $(this).parent("li").find("li input[type=checkbox]").removeAttr("checked");
            }
            
            $("#save_permissions").removeAttr("disabled");
        });
        
        // add functionality to save permissions
        $("#save_permissions").click(function() {
            
            var menu_ids = [];
            $("#menus li input[type=checkbox]:checked").each(function() {
                menu_ids.push($(this).val());
            });
            
            var role_id = $("#roles_table tr.selected").attr("data-id");
            
            var query = "user_role_id=" + role_id + "&menu_ids=" + menu_ids;
            var url = $(this).attr("data-url");
            
            $.post(url, query, function(data) {
                $("#save_permissions").attr("disabled","disabled");
            });
        });
    }
};

$(document).ready(function() {
    roles.init(); 
});
