
var directory_tools =  {
    
    init: function() {
        this.setupDirectorySearch();
        this.setupWeatherSearch();
    },
    
    setupWeatherSearch: function() {
        
        $("#get_tafs").click(function() {
            var url = "http://www.aviationweather.gov/adds/tafs/"; //?station_ids=DGAA&std_trans=translated&submit_taf=Get+TAFs"
            var query = "?station_ids=" + $("#station_ids").val() + "&std_trans=" + ($("#raw").is(":checked") ? "raw" : "translated")
                      + "&submit_tafs=" + $(this).val();
            
            var path = url + "" + encodeURI(query);
            
            core.showResultsFlyout({
                title: "Weather Reference Information For " + $("#station_ids").val() ,
                content: "<iframe src='" + path + "' width='100%' height='440' frameborder=0></iframe>",
                width: 700,
                height: 500
            });
        });
        
         $("#get_tafs_metars").click(function() {
            var url = "http://www.aviationweather.gov/adds/tafs/"; //?station_ids=DGAA&std_trans=translated&submit_taf=Get+TAFs"
            var query = "?station_ids=" + $("#station_ids").val() + "&std_trans=" + ($("#raw").is(":checked") ? "raw" : "translated")
                      + "&submit_both=" + $(this).val();
            
            var path = url + "" + encodeURI(query);
            
            core.showResultsFlyout({
                title: "Weather Reference Information For " + $("#station_ids").val(),
                content: "<iframe src='" + path + "' width='100%' height='440' frameborder=0></iframe>",
                width: 700,
                height: 500
            });
        });
    },
    
    setupDirectorySearch: function() {
        $(".dir_tools #search_area").change(function() {
            var area = $(this).val();
            var url = $("#dir_tools_fields_url").val();
            var query = "area=" + area;
            
            $.post(url, query, function(data) {
                $(".dir_tools #filter option").remove();
                
                if( data ) {
                    $.each(data, function(key, value) {
                        var option = $("<option />").val(key).html(key.toUpperCase());
                        
                        if( key == "country" && area == "AviationAuthority") {
                            option.attr("selected", "selected");
                        }
                        
                        else if( key == "name" && ['Handler','Hotel','Caterer'].indexOf(area) > -1 ) {
                            option.attr("selected", "selected");
                        }
                        
                        else if( key == "iata_code" && area == "Airport" ) {
                            option.attr("selected", "selected");
                        }
                        
                        option.appendTo($(".dir_tools #filter"));
                    });
                }
            }, 'json');
        });
        
        $(".dir_tools #search_field").keyup(function(e) {
            if( e.keyCode == 13 ) {
                var url = $("#dir_tools_search_url").val();
                var query = "area=" + $("#search_area").val() + "&field=" + $(".dir_tools #filter").val() + "&term=" + $(this).val();
                
                $.ajax({
                    url: url,
                    data: query,
                    type: 'post',
                    success: function(data) {
                        core.showResultsFlyout({
                            content: data
                        });
                        
                        $("#results_pane a.result_link").click(function(e) {
                            e.preventDefault();
                            
                            query = "area=" + $("#search_area").val() + "&field=id&term=" + $(this).data('id');
                            
                            $.post(url, query, function(data) {
                                core.showResultsFlyout({
                                    content: data
                                });
                            });
                        });
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                    }
                });
            }
        });
        
        $("#results_flyout .closer_btn").click(function() {
            core.closeResultsFlyout();
        });
    }
};

