
var users = {
  
    dialog: null,
    dialogCanClose: false,
    
    init: function() {
        this.setupNavbar();
        this.setupCategoryList();
    },
    
    setupNavbar: function() {
        
        // add the needed callbacks to the .dialog_openers
        $("#add_user").data('callback', "users.refreshUsers");
        $("#add_role").data('callback', 'users.refreshRoles');
        $("#roles_table .edit_link").data('callback', 'users.refreshRoles');
        
        $("#delete_selected_users").click(function(e) {
            e.preventDefault();
            
            users.deleteSelectedUsers();
        });
    },
    
    setupCategoryList: function() {
        
        /*$(".content .delete_link").live('click', function(e) {
            e.preventDefault();
            
            if( !confirm("Delete Selected Record From System?") ) {
                return;
            }
            
            var $link = $(this);
            var url = $(this).attr("href");
            
            $.get(url, function() {
                $link.parents("tr").remove();
            });
        });*/
        
        $("#category-list li a").click(function(e) {
            e.preventDefault();
            
            $("#category-list li").removeClass("current");
            $(this).parent().addClass("current");
             
            var url = $(this).attr("href");
            var category_name = $(this).html();
            
            $.get(url, function(data) {
                $("#users_column .content").html(data);
                
                $("#users_column .header").html("Users In " + category_name);
                $("#users_column .edit_link").data('callback', 'users.refreshUsers');
            });
        });
    },
    
    /**
     * Callback for the Add User Button
     */
    refreshUsers: function() {
        if( $("#category-list li.current").length > 0 ) {
            $("#category-list li.current a").click();
        }
        
        else {
            $("#category-list li:first a").click();
        }
    },
    
    refreshRoles: function(data) {
        $("#roles .content").html(data);
        $("#roles_table .edit_link").data('callback', 'users.refreshRoles');
    },
    
    deleteSelectedUsers: function() {
        var $inputs = $("#users_column table.fullwidth tbody tr input:checked");
            
        if( $inputs.length == 0 ) {
            alert("Please Select Users To Remove From System");
            return;
        }
            
        else {
            if( !confirm("Delete Selected Users and Related Data From System?") ) {
                return;
            }
        }
            
        var ids = [];
        $inputs.each(function() {
            ids.push( $(this).val() );
        });
            
        var query = "ids=" + ids;
        var url = $(this).attr("href");
            
        $.post(url, query, function() {
                
            $inputs.each(function() {
                $(this).parents("tr").remove();
            });
        });
    }
};


$(document).ready(function() {
    users.init(); 
});