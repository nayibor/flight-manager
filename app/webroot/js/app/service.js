
var services = {

    init: function(){

        $('.del').bind('click',function(e){
            e.preventDefault();
            
            var $this = $(this);
            var id = $this.attr("data-id");
            
            if( id != null && confirm("Are You Sure You Want to Delete This Record ?") ){
                //window.location.href = $(this).attr("del_url");
                
                var url = $this.attr("del_url");
                $.get(url, function() {
                    $this.parents("tr").remove();
                });
            }
        });

        $("#add_service").data('callback', function() {
            window.history.go(0);
        });
        
        $(".edit_link").data("callback", function() {
            window.history.go(0);
        });
    }
}

$(document).ready(function(){
    services.init();
});