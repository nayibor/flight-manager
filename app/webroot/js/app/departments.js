
var departments = {
    
    init: function() {
        
        $(".schedule_add, .edit_link").data({
            'load-callback': function() {
                $(".supervision .datetime").datetimepicker({
                    dateFormat: 'yy-mm-dd',
                    timeFormat: 'hh:mm:ss'
                });
            },
            'callback': function() {
                window.history.go(0);
            },
            width: 700,
            height: 280
        });
        
        $(".delete_link").click(function() {
            return confirm('Are you sure you want to to delete this schedule?');
        });
    }
};

$(document).ready(function() {
    departments.init();
});
