
var Attachments = { 
    
    callbacks: {
        submit: []
    },
    
    init: function(params) {
        
        if( typeof params['submit'] != "undefined" ) {
            this.addCallback('submit', params['submit']);
        }
        
        var self = this;
        self.filesSelected = false;
        
        $("#attachments_fs").change(function(evt) {
            if( evt.target.files != null ) {
                    
                var total_size = 0;
                    
                var $ul = $("<ol />").css({
                    'list-style-type': 'decimal',
                    'margin-left': '15px'
                });
                
                $.each(evt.target.files, function(k,v) {
                    var size = (v.size / 1024).toFixed(2) + " KB";
                    total_size += v.size;
                            
                    if( parseFloat(size) > 1024 ) {
                        size = (parseFloat(size) / 1024).toFixed(2) + " MB";
                    }
                            
                    $ul.append( $("<li />").html(v.name + " (" + size + ")") );
                });
                    
                var max_size_raw = $("#max_size_value").html();
                var max_size_value = parseInt(max_size_raw.replace("M", ""));
                    
                if( total_size > (max_size_value * 1024 * 1024) ) {
                    $("#file_list").html("").append("<span style='color: #900; font-weight: bold;'>Error! Max Upload Size of " + max_size_raw + " Exceeded</span>");
                    $("form input[type=submit]").attr('disabled','');
                }
                    
                else {
                    $("#file_list").html("<b>Files To Upload</b> <br /><br />").append( $ul ); 
                    $("form input[type=submit]").removeAttr('disabled');
                }
            }
                
            // IE and other boys
            else {
                $("form input[type=submit]").removeAttr('disabled');
            }
                
            self.filesSelected = true;
        });
            
        $("#upload-frame").load(function(e) {
            if( self.filesSelected ) {
                core.closeDialog();
                self.fireCallbacks('submit');
            }
        });
            
        $("#up_save_btn").click(function(e) {
            e.preventDefault();
            $("#upload_status").html("Attaching Documents To Flight Schedule...");
            $("#AttachmentForm").unbind("submit").submit();
        });
            
        $("#attachment_add_btn").click(function() {
            $("#attachments-block").hide();
            $("#upload-block").show('fast');
        });
        
        $("#attachments-block .delete-link").click(function(e) {
            e.preventDefault();
            
            if( !confirm("Are you sure you want to delete the selected attachment?") ) {
                return;
            }
            
            var link = $(this);
            $.get( link.attr('href'), function() {
                link.parents("tr").remove();
            });
        });
    },
    
    addCallback: function(type, func) {
        if( typeof this.callbacks[type] == "undefined" ){
            this.callbacks[type] = [];
        }
        
        this.callbacks[type].push(func);
    },
    
    fireCallbacks: function() {
        var type = arguments[0];
        var args = [];
        
        for(var i = 1; i < arguments.length; i++) {
            args.push( arguments[i] );
        }
        
        for(i = 0; i < this.callbacks[type].length; i++) {
            this.callbacks[type][i].apply(this, args);
        }
    }
};
